<?php
require_once '../app/Mage.php'; 
require_once '../redback/RedbackServices.php';
umask(0);
Mage::app('default');
Mage::getSingleton('core/session', array('name'=>'frontend'));

$code = $_GET['code'];
$customer = $_GET['customer'];
$cust_zip = $_GET['cust_zip'];

$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$Offer_Code = $write->fetchOne("SELECT Offer_Code FROM Keycodes where Keycode='".$code."' ");
$Custnbr_Required = $write->fetchOne("SELECT Custnbr_Required FROM catalog_offers where Offer_Code='".$Offer_Code."' ");
if($Custnbr_Required=='True' ){
	// Redback call
	$objRedBackApi = new RedbackServices();
	$data =array(	
						"Keycode"=>$code,
						"Title"=>'4',
						"Catalog"=>'',
						"CustNumber" => $customer,
						"CustName" => '',
						"CustFirstName"=>'',
						"CustLastName"=>'',
						"CustCompany"=>'',
						"CustAdd1" => '',
						"CustAdd2" => '',
						"CustCity"=>'',
						"CustState"=>'',
						"CustZip"=>$cust_zip,
						"CustPhone" => '',
						"CustEmail" => '',
						"CustClubFlag"=>'',
						"CustClubDisc"=>'',
						"CustClubDate"=>'',
						"CustError" => '',
						"CustMessage" => '',
						"debug_id"=>''
						);

	$responseCustomer = $objRedBackApi->keyCodeLoginService($data);

	if($responseCustomer['Message']=='Success' && empty($responseCustomer['CustError']) && !empty($responseCustomer['CustName']) ){
		echo '1';
	}else{
		echo '0';
	}
}
if($Custnbr_Required=='False'){
	echo '1';
}
?>
