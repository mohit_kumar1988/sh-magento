<?php
require_once '../app/Mage.php'; 
require_once '../redback/RedbackServices.php';
umask(0);
Mage::app('default');
Mage::getSingleton ( 'core/session', array ('name' => 'frontend' ) );

$email = $_REQUEST['email'];

if($email!=null){
    //REDBACK API CALL TO SEND THE EMAIL ID
    $objRedBackApi = new RedbackServices();
    $params = array('CustNumber' => '',
        'Title' => '4',
        'CustName' => '',
        'CustAdd1' => '',
        'CustAdd2' => '',
        'CustState' => '',
        'CustCity' => '',
        'CustZip' => '',
        'CustCountry' => '',
        'CustPhone' => '',
        'CustFax' => '',
        'CustEmail' => $email,
        'CustPin' => '',
        'CustClubFlag' => '',
        'CustClubDisc' => '',
        'CustClubDate' => '',
        'CustPinHint' => '',
        'CustOptInFlag' => '',
        'CustError' => '',
        'CustMessage' => '',
        'CustOrder' => '',
        'CustFirstName' => '',
        'CustLastName' => '',
        'ClubNumber' => '');

    $responseCustomer = $objRedBackApi->CustMasterAccessEmailZip($params);
    //print_r($responseCustomer)
    if ($responseCustomer['CustError'] == '0') {
        $session = Mage::getSingleton("core/session");
        Mage::getModel('core/cookie')->set('email_id', $email);

        $zipcode = $responseCustomer['CustZip'];
        //if zipcode is more than 5 length then take first 5
        if (strlen($zipcode) > 5) {
            $zipcode = substr($zipcode, 0, 5);
        }
        $stateCode = $responseCustomer['CustState'];
        if (empty($stateCode)) {
            $checkResource = Mage::getResourceSingleton('checkout/cart');
            $stateCode = $checkResource->get_zip_info($zipcode);
        }
        $customerPhone = str_replace(".", "", $responseCustomer['CustPhone']);
        $customerEmail = $responseCustomer['CustEmail'];
        $session->setCustNumber($responseCustomer['CustNumber']);
        $session->setCustAdd1($responseCustomer['CustAdd1']);
        $session->setCustAdd2($responseCustomer['CustAdd2']);
        $session->setCustState($stateCode);
        $session->setCustCity($responseCustomer['CustCity']);
        $session->setCustCountry($responseCustomer['CustCountry']);
        $session->setCustPhone($customerPhone);
        $session->setCustFax($responseCustomer['CustFax']);
        $session->setCustEmail($customerEmail);
        $session->setCustPin($zipcode);
        $session->setCustFirstName($responseCustomer['CustFirstName']);
        $session->setCustLastName($responseCustomer['CustLastName']);
        $session->setCustZip($zipcode);
        Mage::getSingleton('core/session')->setData("couponAccount",$responseCustomer['CustNumber']);
        Mage::getSingleton('core/session')->setData("couponZip",$zipcode);
        Mage::getModel('core/cookie')->set('couponZip', $zipcode);

        echo '
        <div id="billing_redback" style="margin-left: 170px;margin-top: 15px;" >
            <span><b>Billing Address</b></span>
            (<a href="javascript:void(0);"  onclick="removeCustomerDetail();" >This is not me</a>)
            <div class="addtext">'.
			$responseCustomer['CustName'].'<br>'.
            $responseCustomer['CustAdd1'].'<br>';

			if(trim($responseCustomer['CustAdd2']) != ''){
                echo $responseCustomer['CustAdd2'].'<br>';
            }

			echo $responseCustomer['CustCity']." ".$responseCustomer['CustState']." ".$responseCustomer['CustZip'].'<br>';
			echo $responseCustomer['CustPhone'].'<br>
			</div><br>
        </div>';
    }else{
        echo 'none';
    }
}else{
    $session = Mage::getSingleton("core/session");
    $session->setCustNumber('');
    $session->setCustAdd1('');
    $session->setCustAdd2('');
    $session->setCustState('');
    $session->setCustCity('');
    $session->setCustCountry('');
    $session->setCustPhone('');
    $session->setCustFax('');
    $session->setCustEmail('');
    $session->setCustPin('');
    $session->setCustFirstName('');
    $session->setCustLastName('');
    $session->setCustZip('');
    Mage::getSingleton('core/session')->setData("couponAccount",'');
    Mage::getSingleton('core/session')->setData("couponZip",'');
    echo 'none';
}


?>
