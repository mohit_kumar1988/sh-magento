<?php
require_once '../app/Mage.php'; 
umask(0);
Mage::app('default');
Mage::getSingleton('core/session', array('name'=>'frontend'));

$productQty = $_GET['qty'];
$action = $_GET['action'];
$productId = $_GET['productId'];
$itemId = $_GET['itemId'];

//$resource = Mage::getResourceSingleton('checkout/cart');
//$result = $resource->updateCart($itemId,$productId,$productQty,$action);
$cartHelper = Mage::helper('checkout/cart');
$_checkitems = $cartHelper->getCart()->getItems();
$checkCount = count($_checkitems);

$write =Mage::getSingleton('core/resource')->getConnection('core_write');
$keycode = Mage::getSingleton('core/session')->getData("offerCode");
$offerCode = 4; //For SH, the default offer code is always 4

if (!empty($keycode)) {
	$rule_id = $write->fetchOne("SELECT rule_id FROM  salesrule_coupon where code='".$keycode."' ");
	if(!empty($rule_id)){
			$current_date = date('Y-m-d');
			$offerCode = $write->fetchOne("SELECT Offer_Code  FROM  salesrule where rule_id='".$rule_id."' and '".$current_date."' BETWEEN  from_date AND to_date ");
			if(empty($offerCode)){
				$offerCode = 4;
			}
		
	}else{
		$offerCode = 4;
	}
}
//echo $offerCode;

$errorMessage='';
if($checkCount==1 && $productQty==0){
	$errorMessage='<ul class="messages"><li class="error-msg"><ul><li><span>Atleast one item should be in cart for checkout.</span></li></ul></li></ul>';
}elseif($checkCount==1 && $action == 'remove' ){
	$errorMessage='<ul class="messages"><li class="error-msg"><ul><li><span>Atleast one item should be in cart for checkout.</span></li></ul></li></ul>';
}else{

	if($action == 'remove'){
        $quoteItem = Mage::getModel('sales/quote_item')->load($itemId);
        $quote_id = $quoteItem->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quote_id);
        $offerProductId = $quoteItem->getOfferProductId();
        $checkForOffer = $write->fetchOne("select item_id from sales_flat_quote_item where quote_id='".$quote_id."' and product_id='".$offerProductId."' ");
        $flag=1;
        if($checkForOffer ) {
            $offerProductDetails = $write->fetchAll("SELECT name,product_id from sales_flat_quote_item where item_id=".$checkForOffer);
            if($checkCount > 2){
                $flag=1;
                $cartHelper->getCart()->removeItem($checkForOffer);
                $errorMessage='<ul class="messages"><li class="success-msg"><ul><li><span>Related offer product ( '.$offerProductDetails[0]['name'].' ) is deleted.</span></li></ul></li></ul>';
            }else{
                $flag=0;
                $errorMessage='<ul class="messages"><li class="error-msg"><ul><li><span>You can not delete this product, you need to delete related offer product ( '.$offerProductDetails[0]['name'].' ) first.</span></li></ul></li></ul>';
            }
        }
        if($flag){
            $cartHelper->getCart()->removeItem($itemId);
            $cartHelper->getCart()->save();
        }

	}
	else if($action == 'update' && $productQty==0){
		$cartHelper->getCart()->removeItem($itemId);
		$cartHelper->getCart()->save();
	}
	else if($action == 'update'){
		$_items = $cartHelper->getCart()->getItems();
		foreach ($_items as $_item) {
			if($_item->getId()==$itemId){
				$productId = $_item->getProductId();
				$product = Mage::getModel('catalog/product')->load($productId);
				if($product->getName()!='') 
				$prodName =  stripslashes($product->getName());
				else if($product->getCommonName()!='') 
					$prodName =   stripslashes($product->getCommonName()); 
				else if( $_product->getBotanicalName()!='' ) 
					$prodName =   stripslashes($product->getBotanicalName()); 
				else 
					$prodName =   stripslashes($product->getBreckName());
				$prodName = strip_tags(htmlspecialchars_decode(stripslashes($prodName)));
				$uom =  $product->getUnitOfMeasure();
				$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
				$stock_count = (int) $stock->getQty(); 
				$stock_min_qty = (int) $stock->getMinSaleQty(); 
				$stock_max_qty = (int) $stock->getMaxSaleQty(); 
				if( $productQty > $stock_count) {
					$errorMessage='<ul class="messages"><li class="error-msg"><ul><li><span>Quantity not available.</span></li></ul></li></ul>';
				}else if( $productQty < $stock_min_qty ){
					$errorMessage='<ul class="messages"><li class="error-msg"><ul><li><span>Minimum quantity allowed for '.$prodName.' is '.$stock_min_qty.'.</span></li></ul></li></ul>';
				}elseif($productQty > $stock_max_qty){
					$errorMessage='<ul class="messages"><li class="error-msg"><ul><li><span>Maximum quantity allowed for '.$prodName.' is '.$stock_max_qty.'.</span></li></ul></li></ul>';
				}else{

					if($uom > 1)
						$cross_qty = $uom;
					else
						$cross_qty = '1';

					$newqty = $uom * (ceil($productQty/$uom));

					if($newqty!=$productQty){
						$errorMessage='<ul class="messages"><li class="notice-msg"><ul><li><span>'.$prodName.' quantity adjusted from '.$productQty.' to '.$newqty.'.</span></li></ul></li></ul>';
						$qty = $newqty;
					}else{
						$qty = $productQty;
					}

					
					$_item->setQty($qty);
					$cartHelper->getCart()->save();
				}
			}
		}
	}


	$country = 'US';

	Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()
		->setCountryId($country)
		->setCollectShippingRates(true);

	Mage::getSingleton('checkout/session')->getQuote()
		->getShippingAddress()->collectShippingRates();

	$rates = Mage::getSingleton('checkout/session')->getQuote()
		->getShippingAddress()->getGroupedAllShippingRates();

	if (count($rates)) {
		$topRate = reset($rates);
		$code = $topRate[0]->code;

		try {
			Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()
				->setShippingMethod($code);
		
			Mage::getSingleton('checkout/session')->getQuote()->save();
	
			Mage::getSingleton('checkout/session')->resetCheckout();
	
		}
		catch (Mage_Core_Exception $e) {
			Mage::getSingleton('checkout/session')->addError($e->getMessage());
		}
		catch (Exception $e) {
			Mage::getSingleton('checkout/session')->addException(
				$e, Mage::helper('checkout')->__('Load customer quote error')
			);
		}

	}

			

}


$quote = Mage::getSingleton('checkout/session')->getQuote();
$quote_id = $quote->getId();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$results = $write->fetchAll("SELECT * FROM sales_flat_quote_address where address_type='shipping' and quote_id='".$quote_id."' ");
foreach($results  as $result){
	$address_id = $result['address_id'];
	$region_id = $result['region_id'];
}

$shippingRate = $write->fetchOne("SELECT price FROM  sales_flat_quote_shipping_rate where method='flatmodifiedshipping' and address_id='".$address_id."' ");


//$cartNewHelper = Mage::Singleton('checkout/session');
//$items = $cartNewHelper->getCart()->getItems();
$checkoutSession = Mage::getSingleton('checkout/session');
$items = $checkoutSession->getQuote()->getAllItems();
$taxAmount = 0;
echo $errorMessage;
echo '
<table align="center" width="100%" cellspacing="0" cellpadding="0" border="0" id="checkout-review-table">	 
	<tbody>
		<tr class="rstab first odd">
			<th width="15%" class="rstab-item" rowspan="1"><b>Item</b></th> 
			<th width="45%" class="rstab-item" colspan="1">&nbsp;</th> 
			<th width="10%" class="rstab-itemsec" rowspan="1"><b>Price</b> </th>
			<th width="10%" class="rstab-itemsec" colspan="1"><b>Quantity</b></th>
			<th width="10%" class="rstab-itemlast" rowspan="1"><b>Subtotal</b></th> 
		</tr>';

        $cartPrice=0;
		foreach ($items as $item) {
			$itemId = $item->getId(); 
			$product_id = $item->getProductId();
			$sales = Mage::getModel('sales/quote_item')->load($itemId);
			$taxAmount = $taxAmount + $sales->getTaxAmount();
			$_product = Mage::getModel('catalog/product')->load($product_id);
			$productSku = explode("_", $_product->getSku());
            $imgurl='';
            $prodUrl ='';

            if(count($productSku) > 1){
                $productId = Mage::getModel('catalog/product')->getIdBySku("$productSku[0]");
                $_product = Mage::getModel('catalog/product')->load($productId);
                $imgurl = $_product->getImageUrl();
                $prodUrl = Mage::getBaseUrl().$_product->getUrlKey().'.html';

            }else{
                $_product = Mage::getModel('catalog/product')->load($product_id);
                $imgurl = $_product->getImageUrl();
                $prodUrl = Mage::getBaseUrl().$_product->getUrlKey().'.html';
            }

			$zone='';
			$zoneArray = explode(',',$_product->getWebSearchZone());
			$zoneCount = count($zoneArray);
			if($zoneCount>1)
				$zone = '<div class="czone">Zone : '.$zoneArray[0].' - '.$zoneArray[$zoneCount-1].'</div>';
			else 
				$zone = '';

		 	$message='';
			$prodName = strip_tags(htmlspecialchars_decode(stripslashes($_product->getName())));
			$inventoryQty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty(); 
			$futureSeasonMessage = $_product->getFutureSeasonMessage();
			$restrictedStates = $_product->getRestrictedStates();
			$statesCode[] = explode(" ",$restrictedStates);

			if ($futureSeasonMessage){
				if($futureSeasonMessage == 1)
					$message =  "We're sorry, ".$prodName." is currently unavailable.";
				elseif($futureSeasonMessage == 2)
					$message =  "We are out of inventory for Spring shipping, but we are taking orders for Fall shipping.";
				elseif($futureSeasonMessage == 3 || $futureSeasonMessage==4)
					$message =  "Hurry! Limited Quantity <br />Only ".(int)$inventoryQty." left. BUY IT NOW!";
				elseif($futureSeasonMessage == 5)
					$message =  "This items ships immediately upon receipt of order.";
				elseif($futureSeasonMessage == 6)
					$message =  "<i style='font-size: 12px; color: #666; font-weight: normal;'>This item will ship at the recommended planting time for your area.</i>";
				else
					$message =  "In Stock Ships Today!";
			}

			$offerProductPrice = $_product->getFinalPrice();
			$uom  = $_product->getUnitOfMeasure();
			$qty = $item->getQty()/$uom ;
            $offerProductPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$_product->getSku()."' and  Offer_Code='".$offerCode."' and  '".$qty."' between Qty_Low AND Qty_High ");

			if(empty($offerProductPrice))
				$offerProductPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$_product->getSku()."' and  Offer_Code='4' and  '".$qty."' between Qty_Low AND Qty_High ");
			

			
			$prodPrice='';
			$itemQty ='';
			$subTotal = '';
			//$prodPrice1 = Mage::helper('core')->currency($_product->getPrice());
			$prodPrice = $uom . " for " . Mage::helper('core')->currency($offerProductPrice);
			$itemQty = $item->getQty();
			$subTotal = ($itemQty/$uom)*$offerProductPrice;

			if($subTotal < 1000 ) : 
				$prodsubTotal = Mage::helper('core')->currency($subTotal) ;
			else:
				$prodsubTotal = Mage::helper('checkout')->formatPrice($subTotal);
			endif;

		
			//$cartPrice = $offerProductPrice + $cartPrice;


			echo '<tr class="even">     
				<td class="rstab-product">
					<div class="rstab-productimg">
						<img src="'.$imgurl.'" style="width:90px;" >
					</div>
					<div style="margin-top:5px" class="rstab-productname">
						<span style="font-weight:bold;text-align:center">Item # '.$productSku[0].'</span><br>
					</div>
				</td>    
				<td class="rstab-product">		 
					<h2 style="text-align:left;padding:0px;" class="product-name cart-prod-name">
						<a href="'.$prodUrl.'">'.$prodName.'</a>
					</h2> 
					'.$zone.'	
					<div style="padding-right:30px;" class="cartYouSave">'.$message.'</div>
					<div class="czone"><br> Cannot ship to ('.$restrictedStates.')</div>
				</td>    		
			 	<td class="rstab-productprice">
					<span class="price">'.$prodPrice.'</span>		
				</td>
				<td class="rstab-productqty item_'.$itemId.'">
					<input type="hidden" class="input-text qty" id="productQty" name="qty" value="'.$itemQty.'">
					'.$itemQty.' <br/>
					<a onclick="updatediv(\''.$productId.'\',\''.$itemId.'\',\'remove\')" href="javascript:void(0)">Remove</a>		 
				</td>
				<td class="rstab-producttprice last">
					<span class="price">'.$prodsubTotal.'</span><div style="height:4px" ></div>
						
				</td>
		 	</tr> ';
		} ?>

<!-- Adding Free Item product in the Cart -->
     <?php

     /***** Load the Free Item Banner to check the status *****/
     $_freeBanner = Mage::getModel("banner/banner")->load('63');
     //$bannerStatus = $_freeBanner->getStatus();
     $bannerStatus = '1';
     /***** Load the Free Item Banner to check the status *****/

     /***** Get the Free Item Product *****/
     $productSku = '88738';
     $freeItemId = Mage::getModel('catalog/product')->getIdBySku($productSku);
     //$freeItemId = 10364; // SKU 21493
     $_freeitem = Mage::getModel('catalog/product')->load($freeItemId);
     $freeImgUrl = $_freeitem->getImageUrl();
     $freePrice = $_freeitem->getPrice();
     $freeSku = $_freeitem->getSku();
     $freeName = $_freeitem->getName();
     $freeUrl = $_freeitem->getUrlKey();
     $qty = '1';
     /***** Get the Free Item Product *****/

     if($bannerStatus == '1'): ?>
     <tr class="even">
         <td class="rstab-product"><div class="rstab-productimg"><img style="width:90px;" src="<?php echo $freeImgUrl; ?>"></div>
             <div style="margin-top:5px" class="rstab-productname"><span style="font-weight:bold;text-align:center">Item # <?php echo $freeSku; ?>
		</span><br><div id="example" class="section">
             </div></div></td>
         <td class="rstab-product">

             <h2 style="text-align:left;padding:0px;" class="fdetails">
                 <a href="<?php echo Mage::getBaseUrl().$_freeitem->getUrlKey().".html"; ?>"><?php echo $freeName.'!'; ?></a>
             </h2>

         </td>

         <td class="rstab-productprice">
             1 for <span class="price">$0.00</span>
         </td>

         <td class="rstab-productqty item_150083">
             <input type="hidden" class="input-text qty" id="productQty" name="qty" value="1">
             1
         </td>

         <td class="rstab-producttprice last">
             <span class="price">$0.00</span>
         </td>

     </tr>
     <?php endif; ?>

		<?php $regionCode = $write->fetchOne("SELECT code FROM  directory_country_region where region_id='".$region_id."' ");
		$regionFlag = 0;
		foreach($statesCode as $codes){
			foreach($codes as $code){
				$state[] = $code;
			}
		}
		if(in_array($regionCode,$state )){
			$regionFlag = 1;
		}
		$grandTotal = $quote->getGrandTotal();
        //echo $cartPrice."######";
		$subTotal = $quote->getBaseSubtotal();
		$couponCode = $quote->getCouponCode();
		$discount = $quote->getSubtotal() - $quote->getSubtotalWithDiscount();

		if($subTotal < 1000 ) : 
			$subTotalStr =  Mage::helper('core')->currency($subTotal) ;
		else:
			$subTotalStr = Mage::helper('checkout')->formatPrice($subTotal);
		endif;

		
		if($grandTotal < 1000 ) : 
			$grandTotalStr =  Mage::helper('core')->currency($grandTotal) ;
		else:
			$grandTotalStr = Mage::helper('checkout')->formatPrice($grandTotal);
		endif;

		
		if($taxAmount < 1000 ) : 
			$taxAmountStr =  Mage::helper('core')->currency($taxAmount) ;
		else:
			$taxAmountStr = Mage::helper('checkout')->formatPrice($taxAmount);
		endif;


		echo '<tr style="border-bottom:0px;" class="odd">
			<td colspan="5">
				<table width="98%" style="line-height: 25px;margin-top: 5px;">
					<tfoot>
		    			<tr class="even first">
							<td colspan="3" class="a-right" style=""> Product Total :</td>
							<td class="a-right last" style=""><span class="price">'.$subTotalStr.'</span>    </td>
						</tr>';
						if($discount){
							if($couponCode){
								$couponStr = '('.$couponCode.') ';
							}else{
								$couponStr = '';
							}
						echo '<tr class="even">
							<th class="a-right" style="" colspan="3">Discount '.$couponStr.' :</th>
							<td class="a-right last" style=""><span class="price">-'.Mage::helper('core')->currency($discount).'</span>            </td>
						</tr>';
						}
						if($shippingRate){
						echo '<tr class="odd">
							<td colspan="3" class="a-right" style=""> Shipping :</td>
							<td class="a-right last" style=""> <span class="price">'.Mage::helper('core')->currency($shippingRate).'</span>    </td>
						</tr>';
						}
						if($taxAmount){
						echo '<tr class="even">
							<td colspan="3" class="a-right" style="">Tax :</td>
							<td class="a-right last" style=""><span class="price">'.$taxAmountStr.'</span></td>
						</tr>';
						}
						echo '<tr class="last even">
							<td colspan="3" class="a-right" style=""> <strong>Grand Total :</strong></td>
							<td class="a-right last" style=""><strong><span class="price">'.$grandTotalStr.'</span></strong></td>
						</tr>
						<input type="hidden" name="restrictedState" id="restrictedState" value="'.$regionFlag.'" />
					</tfoot>
	 			</table>
			</td>
		</tr>
	</tbody>																				
</table>
';
?>
