<?php
require_once('Address_validation.php');
$address1=$_GET['address1'];
$address2='';
if($_GET['address2']){
    $address2=$_GET['address2'];
}
$city = $_GET['city'];
$state= $_GET['state'];
$zipcode= $_GET['zip'];
try{
    $av = new Address_validation();
    $av->setReturnType('json'); // Array or json (Default)
    $address = array("address1" => $address1,
        "address2" => 	$address2,
        "city"	=> $city,
        "state" => $state,
        "zipcode" => $zipcode,
        "country"	=> "US"
    );
    $av->setAddress($address);
    $data = $av->getResponse();
    $jsondata = json_decode($data);
    $defaulthtml ='';
    $defaulthtml .= '<li>';
    if($_GET['type']=='bill'){
        $defaulthtml .= '<input type="radio" value="'.$address1."".$address2."".$city." " .$state." ".$zipcode.'" name="region-new" onclick="updateaddressinfo(this)">';
    }else if($_GET['type']=='ship'){
        $defaulthtml .= '<input type="radio" value="'.$address1."".$address2."".$city." " .$state." ".$zipcode.'" name="region-new" onclick="updateshipaddressinfo(this)">';
    }
//	$defaulthtml .= $address1." ".$address2." ".$city." " .$state." ".$zipcode."<br/>";
    $defaulthtml .= "Proceed with my current address(Please be aware that delay may occur if your address is not accurate)<br/>";
    $defaulthtml .='<input type="hidden" name="region-city" class="region-city" value="'.$city.'" >';
    $defaulthtml .='<input type="hidden" name="region-state" class="region-state" value="'.$state.'" >';
    $defaulthtml .='<input type="hidden" name="region-address1" class="region-address1" value="'.$address1.'" >';
    $defaulthtml .='<input type="hidden" name="region-address2" class="region-address2" value="'.$address2.'" >';
    $defaulthtml .='<input type="hidden" name="region-zip" class="region-zip" value="'.$zipcode.'" >';
    if(isset($jsondata->error)){
        $error = objectToArray($jsondata->error);
        echo "none";
        exit;
    }else if(count($jsondata)>0){
        if(count($jsondata)==1){
            $code =objectToArray($jsondata[0]->code);
            $r_address1 =$jsondata[0]->address1;
            if(!empty($partial->address2))
                $r_address2 =$jsondata[0]->address2;
            else
                $r_address2 ='';
            $r_city =objectToArray($jsondata[0]->city);
            $r_state =objectToArray($jsondata[0]->state);
            $r_zipcode =objectToArray($jsondata[0]->zipcode);
            //IF Same as input then no need to show results
            if(strtolower($address1) == trim(strtolower($r_address1)) && strtolower($city) == strtolower($r_city['0']) && strtolower($state) == strtolower($r_state[0]) && strtolower($zipcode) == strtolower($r_zipcode[0])){
                echo "exact";exit;
            }else{
                $regionhtml='<div class="pop-head">Please verify your address</div>';
            }
        }else{
            $regionhtml='<div class="pop-head">Please change address to</div>';
        }
        $regionhtml .= '<ul>';
        foreach($jsondata as $partial){
            $code =objectToArray($partial->code);
            $r_address1 =$partial->address1;
            if(!empty($partial->address2))
                $r_address2 =$partial->address2;
            else
                $r_address2 ='';
            $r_city =objectToArray($partial->city);
            $r_state =objectToArray($partial->state);
            $r_zipcode =objectToArray($partial->zipcode);
            $regionhtml .= '<li>';
            if($_GET['type']=='bill'){
                $regionhtml .= '<input type="radio" value="'.$r_address1."".$r_address2."".$r_city['0']." " .$r_state[0]." ".$r_zipcode[0].'" name="region-new" onclick="updateaddressinfo(this)">';
            }else if($_GET['type']=='ship'){
                $regionhtml .= '<input type="radio" value="'.$r_address1."".$r_address2."".$r_city['0']." " .$r_state[0]." ".$r_zipcode[0].'" name="region-new" onclick="updateshipaddressinfo(this)">';
            }
            $regionhtml .= $r_address1." ".$r_address2." ".$r_city['0']." " .$r_state[0]." ".$r_zipcode[0]."<br/>";
            $regionhtml .='<input type="hidden" name="region-city" class="region-city" value="'.$r_city[0].'" >';
            $regionhtml .='<input type="hidden" name="region-state" class="region-state" value="'.$r_state[0].'" >';
            $regionhtml .='<input type="hidden" name="region-address1" class="region-address1" value="'.$r_address1.'" >';
            $regionhtml .='<input type="hidden" name="region-address2" class="region-address2" value="'.$r_address2.'" >';
            $regionhtml .='<input type="hidden" name="region-zip" class="region-zip" value="'.$r_zipcode[0].'" >';
            $regionhtml .= '</li>';
            //echo "City  ".$partial['address']['city']."  state ".$partial['address']['state']." zip".$partial['zip_code_low']."<br/>";
        }
        $regionhtml .= $defaulthtml;
        $regionhtml .= '</ul>';
        $regionhtml .='<a  class="close" onclick="closeaddressvalid()" href="javascript:void(0)"></a>';
        echo $regionhtml;
    }
}catch(Exception $e){
    echo $e->getMessage();
}

function objectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    }
    else {
        // Return array
        return $d;
    }
}

?>