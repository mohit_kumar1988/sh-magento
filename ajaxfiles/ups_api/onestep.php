<?php 
//error_reporting(E_ALL);
require_once 'inc/config.php';
/**
 * Include the configuration file
 */

$city = $_GET['city'];
$state= $_GET['state'];
$zipcode= $_GET['zip'];
$scity = $_GET['shipcity'];
$sstate= $_GET['shipstate'];
$szipcode= $_GET['shipzip'];
$billaddress = array(
    'city' => $city ,
    'state' => $state,
    'zip_code' => $zipcode
);  // end address

$shipaddress = array(
    'city' => $scity ,
    'state' => $sstate,
    'zip_code' => $szipcode
);

$customer_data = array(
); // end $customer_data

$validation = new UpsAPI_USAddressValidation($billaddress);
$xml = $validation->buildRequest($customer_data);

$response =$validation->sendRequest($xml, true);

// can be called with no arguments
$match_array = $validation->getMatches();

// can also be called with the results of getMatchType()
$match_type = $validation->getMatchType();
$match_array = $validation->getMatches($match_type);

$bfinal =false;
if($match_type=="Exact"){
	$bfinal =true;
}
	

$svalidation = new UpsAPI_USAddressValidation($shipaddress);
$sxml = $svalidation->buildRequest($customer_data);

$sresponse =$svalidation->sendRequest($sxml, true);

// can be called with no arguments
$match_array = $svalidation->getMatches();

// can also be called with the results of getMatchType()
$smatch_type = $svalidation->getMatchType();
$smatch_array = $svalidation->getMatches($smatch_type);
$sfinal=false;
if($smatch_type=="Exact"){
	$sfinal=true;
}
if($sfinal==true && $bfinal==true){
	echo "success";
}else{
	echo "failed";
}

?>
