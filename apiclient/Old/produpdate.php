<?php

require_once '../app/Mage.php';
umask(0);
Mage::app('default');
include 'web_conf.php';



$handle_order = fopen("ProductDescription.csv", "r");
$count = 0;
$ii=0;
$catArray=array();
// $products = Mage::getModel('catalog/product');
echo "<pre>";
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
while (($data = fgetcsv($handle_order, 100000, "|")) !== FALSE) {
	//print_r($data);
	//die();
	if($ii==20){
		break;
	}
	if($ii==0 || $data['1']==''){
		$ii++;
		continue;
	}
	$image ='';
	$product_skus = explode("," , $data['1']);
	foreach($product_skus as $sku){

		echo $sku;
		// get attribute set
		
		

		/// General Mandatory Attribute

		/// To import the SPRG_Product_Categories.csv to get category name on the basis of product_number and cat_nbr		

		$cathandle = fopen("SPRG_Product_Categories.csv", "r");
		$aa=0;
		$cat_nbr='';
		while (($catdata = fgetcsv($cathandle, 100000, "|")) !== FALSE) {
			
			if($aa==0 || $catdata['0']==''){
				$aa++;
				continue;
			}
			if($catdata[0]==$sku){
				//print_r($catdata);
				//echo $catdata[1];
				$cat_nbr[]= $catdata[1];		
			}
			$aa++;

		}

		if(count($cat_nbr) > 0){
			$catArray='';
			foreach($cat_nbr as $cat_value){
				/// To import the SPRG_Categories.csv to get category name on the basis of product_number		
				
				$catnamehandle = fopen("SPRG_Categories.csv", "r");
				$p=0;
				while (($catnamedata = fgetcsv($catnamehandle, 100000, "|")) !== FALSE) {
					
					if($p==0 || $catnamedata['0']==''){
						$p++;
						continue;
					}
					
					if($catnamedata[0]==$cat_value){
						//print_r($catnamedata);
						//echo $cat_value;
						$cat_name = addslashes($catnamedata[1]);	
						echo $cat_name."#####";	
						$cat_ids=$write->fetchOne("SELECT entity_id FROM catalog_category_entity_varchar where attribute_id = '41' and value='".$cat_name."'");	
						$catArray[]  = $cat_ids;
					}
					$p++;

				}
			}

		}
		$CategoryIDs[] =implode(",",$catArray);
		$prodsku[] = $sku;
		$title[] = $cat_name.' Plant'; //If there is no title, please provide the format to be used for default.  E.g. <Product Name> - <category name> plant
		$image ="http://springhillnursery.com/images/250/$sku.jpg";
		
		
		if($sku)
		{
			if(!file_exists("/var/www/springhill/media/import/".$sku.".jpg")){
				@copy( $image  , "/var/www/springhill/media/import/".$sku.".jpg" );
				if(!file_exists($image)){
					$img_val[] ='/noimage.gif';
				}else{
					$img_val[] ='/'.$sku.'.jpg';
				}
			}else{
				$img_val[] ='/'.$sku.'.jpg';
			}
			
		
		}
	
		$bestsellerindicator[] = 0;
		$lifetimeguarantee[] = 0;
		$webonlyplant[] = 0;
		$isnewplant[] = 0;
		$isonclearance[] = 0;
		
		$store[]="admin";
		$websites[]="base";
		$attribute_set[]="Springhills";
		$type[]="simple";
		$has_options[] = 0;
		$url_key[] = '';
		$url_path[] = '';
		$custom_design[] = '';
		$page_layout[]  = 'No Layout updates';
		$options_container[] = 'Block after info colomn';
		$country_of_manufacture[] = '';
		$msrp_enabled[]   = 'Use config';
		$msrp_display_actual_price_type[] = 'Use config';
		$gift_message_available[]  = 'No';
		$gift_wrapping_available[] = 'No';
		$is_returnable[] = 'Use config';

		$custom_layout_update[]  = '';
		$is_recurring[] = 'No';
		$visibility[]  = 'Catalog, Search';
		$enable_googlecheckout[] = 'Yes';
		$tax_class_id[]  = 'Taxable Goods';     
		$special_from_date[] = '';
		$special_to_date[] = '';
		$news_from_date[]  = '';
		$news_to_date[]  = '';
		$custom_design_from[]  = '';
		$custom_design_to[]    = '';

		$use_config_min_qty[]  ='1';
		$is_qty_decimal[]  = '0';
		
		$store_id[]   = '0';
		$product_type_id[]   = 'simple';
		$product_status_changed[]  = '';
		$product_changed_websites[]  = '';
		$is_in_stock[]  = '1';
		$low_stock_date[]  = '';
		$notify_stock_qty[]  = '';
		$use_config_notify_stock_qty[]  = '1';
		$manage_stock[]  = '0';
		$use_config_manage_stock[] = '0';
		$stock_status_changed_auto[]  = '0';
		$use_config_qty_increments[]  = '1';
		$qty_increments[]  = '0';
		$use_config_enable_qty_inc[]  = '1';
		$enable_qty_increments[]  = '0';
		$stock_status_changed_automatically[]   = '0';
		$use_config_enable_qty_increments[]  = '1';

		$AdditionalSearchWords[] = '' ;
		$LongDescription[] = addslashes($data[59]);
		$MetaKeywords[] = '';
		$MetaDescription[] = '';
		$ShortDescription[] = addslashes($data[59]);
		$weight[] = '1.0';
		$RecommendationURL[] = '';


		/// To import the data from Inv.csv haing same item_num in productdescription.csv Attribute		

		$invhandle = fopen("Inv.csv", "r");
		$jj=0;
		while (($invdata = fgetcsv($invhandle, 100000, "|")) !== FALSE) {

			if($jj==0 || $invdata['0']==''){
				$jj++;
				continue;
			}
			if($invdata[0]==$sku){
				$botanicalname[] = $invdata[1];		
				$RestrictedStates[] = $invdata[3];
			}
			$jj++;

		}
		

		/// To import the data from inventory.csv haing same product_number in productdescription.csv Attribute		

		$inventoryhandle = fopen("Inventory.csv", "r");
		$kk=0;
		while (($inventorydata = fgetcsv($inventoryhandle, 100000, "|")) !== FALSE) {

			if($kk==0 || $inventorydata['0']==''){
				$kk++;
				continue;
			}
			if($inventorydata[0]==$sku){
				$qty[] = $inventorydata[4];		
				$BackOrderDate[] = $inventorydata[11];
				
				$Season[] = $inventorydata[7];
				
				if($inventorydata[11]!= ""){
					$backorders[]  = '1' ;
					$use_config_backorders[] = '0';
				}else{
					$backorders[]  = '0' ;
					$use_config_backorders[] = '1';
				}

				if($inventorydata[3]=='Y'){
					$DiscontinuedObsolete[] ='1';
				}else{
					$DiscontinuedObsolete[] = '0';
				}

				if($inventorydata[9]=='Y'){
					$FutureSeasonIndicator ='1';
				}else{
					$FutureSeasonIndicator = '2';
				}

				if($inventorydata[10]=='y'){
					$FutureSeasonMessage[] = '1';
				}else{
					$FutureSeasonMessage[] = '0';
				}

				if($inventorydata[2]=='Y'){
					$status[] = 'Enabled';
				}else{
					$status[] = 'Disabled';
				}
			}
			$kk++;
		}
		
		/// To import the data from price.csv haing same product_number and offer_code=4 in productdescription.csv Attribute		

		$pricehandle = fopen("Price.csv", "r");
		$ll=0;
		$ProductOffersCode='';
		while (($pricedata = fgetcsv($pricehandle, 100000, "|")) !== FALSE) {

			if($ll==0 || $pricedata['0']==''){
				$ll++;
				continue;
			}
			if($pricedata[0]==$sku && $pricedata[1]!=4 && $pricedata[1]!=''){				
				$ProductOffersCode .= $pricedata[1].",";
			}
			if($pricedata[0]==$sku && $pricedata[1]==4){
				
				$CatalogCode[] = '';		
				$price[] = $pricedata[4];
				$MaxQuantity[] = $pricedata[3];		
				$salePrice[] = $pricedata[6];
				$wholesalePrice[] = '';

				if($pricedata[2]!=''){
					$min_sale_qty[] = $pricedata[2];
					$use_config_min_sale_qty[]  = '0';
				}else{
					$min_sale_qty[] = '1';
					$use_config_min_sale_qty[]  = '1';
				}
				if($pricedata[3]!=''){
					$max_sale_qty[] = $pricedata[3];
					$use_config_max_sale_qty[]  = '0';
				}else{
					$max_sale_qty[] = '1';
					$use_config_max_sale_qty[]  = '1';
				}

			}else{
				$min_sale_qty[] = '1';
				$use_config_min_sale_qty[]  = '1';
				$max_sale_qty[]   = '0';
				$use_config_max_sale_qty[]  = '1';
			}
			$ll++;

		}
		if($ProductOffers!=''){
			$ProductOffers[]=substr($ProductOffersCode,0,-1);
		}
		// Product Description Attribute
		$Usage[] = '';
		$Light[] = '' ;		
		$AdditionalInformation[] = addslashes($data[68]);		
		$BloomTime[] ='';
		$UnitofMeasure[] = 'Each';
		$UniqueCharacteristics[] = addslashes($data[59]);
		$SunExposure[] = '' ;
		$Spread[] = addslashes($data[35]);
		$Spacing[] = addslashes($data[36]);
		$ShippingSchedule[] ='';
		$SEOCopyBlock[] = '';
		$GrowthRate[] =  addslashes($data[58]);
		$Form[] = addslashes($data[64]);
		$FloweringDate[] = addslashes($data[46]);
		$FlowerColor[] =  addslashes($data[41]);
		$FoliageType[] = addslashes($data[37]);
		$DisplayHardinessZone[] = addslashes($data[51]);	
		$RelatedVideoURL[] ='';
		$HardinessZones[]	= 	addslashes($data[51]);
		$foliage[] = addslashes($data[24]);
		$HeightHabit[] = addslashes($data[34]); 
		$breck_name[] = addslashes($data[8]);
		$common_name[] = addslashes($data[9]);
		$family[] = addslashes($data[11]);
		if($data[6]=='TRUE'){
			$PlantingRequirements[] = '1';
		}else{
			$PlantingRequirements[] = '0';
		}		
		if($data[12]=='TRUE'){
			$shade[] = '1';
		}else{
			$shade[] = '0';
		}
		if($data[13]=='TRUE'){
			$partial_shade[] = '1';
		}else{
			$partial_shade[] = '0';
		}
		if($data[14]=='TRUE'){
			$full_sun[] = '1';
		}else{
			$full_sun[] = '0';
		}
		if($data[15]=='TRUE'){
			$borders[] = '1';
		}else{
			$borders[] = '0';
		}
		if($data[17]=='TRUE'){
			$rock_gardens[] = '1';
		}else{
			$rock_gardens[] = '0';
		}
		if($data[18]=='TRUE'){
			$ground_covers[] = '1';
		}else{
			$ground_covers[] = '0';
		}
		if($data[21]=='TRUE'){
			$longlastingingarden[] = '1';
		}else{
			$longlastingingarden[] = '0';
		}
		if($data[23]=='TRUE'){
			$cut_flower[] = '1';
		}else{
			$cut_flower[] = '0';
		}
		if($data[25]=='TRUE'){
			$long_lasting_cut[] = '1';
		}else{
			$long_lasting_cut[] = '0';
		}
		if($data[26]=='TRUE'){
			$goodforforcing[] = '1';
		}else{
			$goodforforcing[] = '0';
		}	
		if($data[27]=='TRUE'){
			$goodfornaturalizing[] = '1';
		}else{
			$goodfornaturalizing[] = '0';
		}	
		if($data[28]=='TRUE'){
			$multipliesannually[] = '1';
		}else{
			$multipliesannually[] = '0';
		}		
		if($data[30]=='TRUE'){
			$DRIEDFLOWERS[] = '1';
		}else{
			$DRIEDFLOWERS[] = '0';
		}
		if($data[31]=='TRUE'){
			$CONTAINER[] = '1';
		}else{
			$CONTAINER[] = '0';
		}
		
		$GoodForOther[] = addslashes($data[32]);
		$WebHeightSearch[] = addslashes($data[33]);
		$FlowerForm[] = addslashes($data[39]);	
		$AverageNumberofBlooms[] = addslashes($data[40]);
		$WebSearchFlowerColor[] = addslashes($data[42]);
		$Specialcareorplantinginstructions[] = addslashes($data[54]);
		$FRAGRANCE[] = addslashes($data[43]);
		$PlantingTime[] = addslashes($data[44]);
		$WebFloweringTimeSearch[] = addslashes($data[45]);
		$DURATION[] = addslashes($data[47]);
		$BLOOMSWITH[] =addslashes( $data[49]);
		$WebSearchZone[] = addslashes($data[50]);
		$TYPEOFFRUIT[] = addslashes($data[52]);
		$Shipped[] = addslashes($data[53]);
		$SOILREQUIREMENTS[] =addslashes( $data[56]);
		$ROOTSYSTEM[] = addslashes($data[57]);
		$twouniqueCharacteristics[] = addslashes($data[60]);
		$Keydifferencefromsimilarbulbs[] = addslashes($data[62]);
		$RESISTANCE[] = addslashes($data[65]);
		$PRUNING[] = addslashes($data[66]);
		$TIMEOFPRUNING[] = addslashes($data[67]);
		$Awards[] = addslashes($data[70]);

		if($data[76]=='TRUE'){
			$Indoorsonly[] = '1';
		}else{
			$Indoorsonly[] = '0';
		}

		if($data[77]=='TRUE'){
			$IndoorsOutdoorsinwarmtemperatures[] = '1';
		}else{
			$IndoorsOutdoorsinwarmtemperatures[] = '0';
		}
	
		if($data[78]=='TRUE'){
			$Canbepermantlyplantedingarden[] = '1';
		}else{
			$Canbepermantlyplantedingarden[] = '0';
		}

		$WINTERREQUIREMENTS[] = addslashes($data[79]);
		$TemperatureRequirements[] = addslashes($data[80]);
		$LightNeededIndoors[] = addslashes($data[81]);
		$WateringRequirements[] = addslashes($data[82]);
		$FertilizationRequirements[] = addslashes($data[83]);
		$SpecialCare[] = addslashes($data[84]);

		if($data[87]=='TRUE'){
			$DeerResistant[] = '1';
		}else{
			$DeerResistant[] = '0';
		}

		if($data[88]=='TRUE'){
			$SeasonShippedFall[] = '1';
		}else{
			$SeasonShippedFall[] = '0';
		}

		if($data[89]=='TRUE'){
			$SeasonShippedSpring[] = '1';
		}else{
			$SeasonShippedSpring[] = '0';
		}
		$count++;
	}

	$ii++;
	
}

for($i=0;$i<$count;$i++)  {
if($price[$i]==0 || $price[$i]=="" ){
	$price[$i]='0';
}

if($status[$i]=="" ){
	$status[$i]='Disabled';
}
$weight[$i]='1.0';

if($CategoryIDs[$i]==''){
	$CategoryIDs[$i]='0';
}

$productCreate = array(
			'categories' => $CategoryIDs[$i],
			'websites' => array(1),
			'name' => $title[$i],
			'sku' => $prodsku[$i],
			'description' => $ShortDescription[$i],
			'short_description' => $ShortDescription[$i],
			'weight' => '1',
			'status' => $status[$i],
			'visibility' => '4',
			'price' => '100',
			'tax_class_id' => 1,
			'meta_title' =>  $meta_title[$i],
			'meta_keyword' =>  $meta_keyword[$i],
			'meta_description' =>  $meta_description[$i],
		);
		
		print_r($productCreate);

		//$result = $client->call($session_id, 'catalog_product.create', array('simple', $attributeSet['set_id'], 'product_sku'));
		
		/*$result = $client->call($session_id, 'catalog_product.create', array('simple', $attributeSet['set_id'], 'product_sku', array(
			'categories' => $CategoryIDs[$i],
			'websites' => array(1),
			'name' => $name[$i],
			'sku' => $prodsku[$i],
			'description' => $ShortDescription[$i],
			'short_description' => $ShortDescription[$i],
			'weight' => '1',
			'status' => $status[$i],
			'visibility' => '4',
			'price' => '100',
			'tax_class_id' => 1,
			'meta_title' =>  $meta_title[$i],
			'meta_keyword' =>  $meta_keyword[$i],
			'meta_description' =>  $meta_description[$i],
		)));*/


}

?>
                                                                    
