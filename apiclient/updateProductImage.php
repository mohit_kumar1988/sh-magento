<?php 
require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
include 'web_conf.php';

$productArray = array('01076','00684','00685','00686','00689','00690','00691','00692','00693','00694','00696','64863','64864','65355','68980','74615','75410','76230','82571','82572','82576','82579','82581','82582','82583','82587','82588','82593','82600','82604','82609','82610','82615','82617','82619','82622','82623','82624','82625','82627','82629','82635','82640','82642','82643','82646','82651','83328','83555','84453','85252','85254','85256','85257','85258','85260','85371','85373','85376','85377','85379','85381','85384','85385','85386','85387','85388','85389','85390','85391','85392','85394','85396','85397','85398','85399','85400','85401','85404','85406','85409','85410','85411','85412','85414','85415','85417','85419','85420','85421','85422','85423','85424','85429','85433','85434','85435','85450','86910','86911','86912','87489','87509','87511','87514');
//'01076',
$productCollection = Mage::getModel('catalog/product')
                   ->getCollection()
                   ->addAttributeToSelect('entity_id')
                   ->addAttributeToSort('entity_id', 'desc');

echo count($productCollection);
$i=0;
foreach ($productCollection as $prod) {
    $productid = $prod->getId();
	$product = Mage::getModel('catalog/product')->load($productid);
	$sku = $product->getsku();
	if(in_array($sku,$productArray)){

        /**** Get required attributes for updating image *****/
        $productName = $product->getName();
        $purlKey=$product->getUrlKey();
        $checkUrlkey = strpos($purlKey, $sku);
        if($checkUrlkey == false) {
            $urlKey = $purlKey.'-'.$sku;
        } else {
            $urlKey = $purlKey;
        }


        /***** get the path for the product image *****/
        $getActualImagePath = '../media/import/'.$sku.".jpg";

        /***** Check the image exists or not in the catalog folder if exists copy or else show the message *****/
        if(!file_exists($getActualImagePath)){
            echo "Image is not available for the product : ".$sku."\n";
        }

        /***** If product exists then proceed *****/
        if(($productid!= "" || $productid!= '0') && file_exists($getActualImagePath)) {
            $images_sp_sku = Mage::getBaseDir('media') . '/catalog/product/' .  strtolower($sku[0]) .'/'. strtolower($sku[1]).'/'. strtolower($sku).'_*'  ;
            $ifiles = glob($images_sp_sku);
            unlink($ifiles);
            $isImage = base64_encode(file_get_contents($getActualImagePath));
            if($isImage) {

                $newImage = array(
                    'file' => array(
                    'name' => "$urlKey",
                    'content' => $isImage,'mime' => 'image/jpeg'),
                    'label'    => "$productName",
                    'position' => 1,
                    'types'    => array('image', 'small_image', 'thumbnail'),
                    'exclude'  => 0
                );

                try{
                    //unlink($getActualImagePath);
                    $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
                    $items = $mediaApi->items($productid);
                    foreach($items as $item){
                        $mediaApi->remove($productid, $item['file']);
                    }
                    $imageFilename = $client->call($session_id, 'product_media.create', array($productid, $newImage));
                    //print_r($imageFilename);
                    if($imageFilename){
                        echo $ii++."---yes---".$sku."\n";
                    }else{
                        echo "no-".$productid."\n";
                    }
                }catch(Exception $e){
                    echo "Exception 1: ".$sku."---".$e."\n";

                }

            }
            //die();
        }
	}
}
echo $ii;
?>
