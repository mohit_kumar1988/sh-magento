<?php

/*
 * Update InventoryFeed values with Product info
 */

require_once '../app/Mage.php';
umask(0);
Mage::app('default');
include 'web_conf.php';

$notInArry = array();
$inArray = array();
$connection = Mage::getSingleton('core/resource')->getConnection('core_read');

//$inFeedSelect = $connection->select()
       // ->from("InventoryFeed as ifeed", array('ifeed.Product_Number', 'ifeed.Active', 'ifeed.Obsolete', 'ifeed.Available', 'ifeed.Sellable_Quantity', 'ifeed.Season', 'ifeed.Cross_Sell_Items', 'ifeed.Future_Season_Indicator','ifeed.Future_Season_Message'))
       // ->order("ifeed.Product_Number");
$inFeedSelect ="SELECT * FROM InventoryFeed as ifeed, product_description_missing as PD WHERE PD.Item_Number = ifeed.Product_Number order by ifeed.Product_Number";
$inFeedArray = $connection->fetchAll($inFeedSelect);
$a=0;
foreach ($inFeedArray as $infKey => $infValue) {
    $sku = $infValue['Product_Number'];
    $_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

    if (!empty($_Pdetails['sku'])) {
        $status = ($infValue['Obsolete'] == "N") ? 1 : 2;
        $product_id = $_Pdetails->getId();
            $prod_details = Mage::getModel('catalog/product');
            $prod_details->load($product_id);

            $cross_data = explode(" ", $infValue['Cross_Sell_Items']);
            $inc = 1;
            foreach ($cross_data as $k => $v) {
                $pDetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $v);
                if (!empty($pDetails['sku'])) {
                    $pid = $pDetails->getId();
                    $param[$pid] = array('position' => $inc);
                    $inc++;
                }
            }

            if (!empty($param)) {
                try {
                    $prod_details->setCrossSellLinkData($param);
                    $prod_details->save();
                } catch (Exception $e) {
                    echo "Exception 1: " . $product_id . "==>" . $e . "\n";
                }
            }

            try {
                $result = $client->call($session_id, 'catalog_product.update', array($product_id, array(
                                'status' => $status,
                                'season' => $infValue['Season'],
                                'recommended_items' => $infValue['Cross_Sell_Items'],
                                'future_season_indicator' => $infValue['Future_Season_Indicator'],
                                'sellable_quantity' => $infValue['Sellable_Quantity'],
                                 'future_season_message'=>$infValue['Future_Season_Message']
                                )));
            } catch (Exception $e) {
                echo "Exception 2: " . $product_id . "==>" . $e . "\n";
            }

            $stockItemData = array(
                'qty' => $infValue['Available'],
            );

            try {
                $stock_result = $client->call($session_id, 'product_stock.update', array(
                            $product_id,
                            $stockItemData
               	));
            } catch (Exception $e) {
                echo "Exception 3: " . $product_id . "==>" . $e . "\n";
            }

            print $a++ . "####" . $sku . "--->" . $product_id . "\n";
        
    }
}

/*
 * END Update InventoryFeed values with Product info
 */
?>
