<?php

/*
 * Update InventoryFeed values with Product info
 */

require_once '../app/Mage.php';
umask(0);
Mage::app('default');
include 'web_conf.php';

$notInArry = array();
$inArray = array();
$connection = Mage::getSingleton('core/resource')->getConnection('core_read');

//$inFeedSelect = $connection->select()
       // ->from("InventoryFeed as ifeed", array('ifeed.Product_Number', 'ifeed.Active', 'ifeed.Obsolete',
        //    'ifeed.Available', 'ifeed.Sellable_Quantity', 'ifeed.Season', 'ifeed.Cross_Sell_Items',
        //    'ifeed.Future_Season_Indicator', 'ifeed.Future_Season_Message', 'ifeed.Recommended_Items'))
        //->order("ifeed.Product_Number");

$inFeedSelect ="SELECT * FROM InventoryFeed as ifeed, product_description_missing as PD WHERE PD.Item_Number = ifeed.Product_Number order by ifeed.Product_Number";
$inFeedArray = $connection->fetchAll($inFeedSelect);
$a = 0;
foreach ($inFeedArray as $infKey => $infValue) {
    $sku = $infValue['Product_Number'];
    $_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);


    if (!empty($_Pdetails['sku'])) {
        $product_id = $_Pdetails->getId();
        $cross_data = explode(" ", $infValue['Cross_Sell_Items']);
        $cross_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        foreach ($cross_data as $k => $v) {
            $pDetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $v);
            if (!empty($pDetails['sku'])) {
                $pid = $pDetails->getId();
                try {
                    $crossCountSelect = $connection->select()
                            ->from("catalog_product_link", array('*'))
                            ->where("product_id=" . $product_id)
                            ->where("linked_product_id=" . $pid)
                            ->where("link_type_id=5");
                    $crossCountArray = $connection->fetchAll($crossCountSelect);
                    if (count($crossCountArray) == 0) {
                        $cross_connection->beginTransaction();
                        $fields = array();
                        $fields['product_id'] = $product_id;
                        $fields['linked_product_id'] = $pid;
                        $fields['link_type_id'] = '5';
                        $cross_connection->insert('catalog_product_link', $fields);
                        $cross_connection->commit();
                    }
                } catch (Exception $e) {
                    echo "Exception 1: " . $product_id . "==>" . $e . "\n";
                }
            }
        }
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        try {
            if ($product) {
                $status = ($infValue['Obsolete'] == "N") ? 1 : 2;
                $product->setStatus($status);
                $product->setSeason($infValue['Season']);
                $product->setRecommendedItems($infValue['Recommended_Items']);
                $product->setFutureSeasonIndicator($infValue['Future_Season_Indicator']);
                $product->setSellableQuantity($infValue['Sellable_Quantity']);
                $product->setFutureSeasonMessage($infValue['Future_Season_Message']);

                $product->save();
            }
        } catch (Exception $e) {
            echo "Exception 2: " . $product_id . "==>" . $e . "\n";
        }
        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_id);
        $is_stock = "";
        $is_stock = ($infValue['Available'] > 0) ? '1' : '0';
        try {
            $stockItem->setQty($infValue['Available']);
            $stockItem->setIsInStock($is_stock);
            $stockItem->save();
        } catch (Exception $e) {
            echo "Exception 3: " . $product_id . "==>" . $e . "\n";
        }
        print $sku . "===>" . $product_id . "\n";
    }
}

/*
 * END Update InventoryFeed values with Product info
 */
?>
