<?php

/*
 * Update Price values with Product info
 */

require_once '../app/Mage.php';
umask(0);
Mage::app('default');
include 'web_conf.php';

$notInArry = array();
$inArray = array();
$connection = Mage::getSingleton('core/resource')->getConnection('core_read');

$pselect ="SELECT count(*) as count, P.Product_Number,P.Price FROM `InventoryFeed` as IFD, Price as P WHERE IFD.Active='Y' and IFD.`Product_Number` = P.`Product_Number` and P.Offer_Code=4 group by P.Product_Number";
$pRowsArray = $connection->fetchAll($pselect);

foreach ($pRowsArray as $pKey => $pValue) {
    $sku = $pValue['Product_Number'];

    $_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
	if ($_Pdetails == null)
		continue;

    $product_id = $_Pdetails->getId();
    if ($pValue['count'] == 1) {
        try {
            $resultPrice = $client->call($session_id, 'catalog_product.update', array($product_id, array(
                            'price' => $pValue['Price'],
                            )));
        } catch (Exception $e) {
            echo "Exception 1: ".$product_id . "==>" . $e . "\n";
        }
        print "Success: ".$_Pdetails['sku'] . "\n";
    }
	else if ($pValue['count'] > 1) {
        try {
            $tierPrices = $client->call($session_id, 'product_tier_price.info', $product_id);
        } catch (Exception $e) {
            echo "Exception 2: ".$product_id . "==>" . $e . "\n";
        }

        $pSubSelect = $connection->select()
                ->from('Price', array('*'))
                ->where("Product_Number =" . $_Pdetails['sku'])
                ->where("Offer_Code=4");
        $psubRowsArray = $connection->fetchAll($pSubSelect);

        $j = 1;
        foreach ($psubRowsArray as $id => $info) {
            if ($j == 1) {
                try {
                    $resultPrice = $client->call($session_id, 'catalog_product.update', array($product_id, array(
                                    'price' => $info['Price'],
                                    )));
                } catch (Exception $e) {
                    echo "Exception 3: ".$product_id . "==>" . $e . "\n";
                }
            }
			else {
                $tierPrices[] = array(
                    'website' => 'all',
                    'customer_group_id' => 'all',
                    'qty' => $info['Qty_Low'],
                    'price' => $info['Price']
                );
            }
            $j++;
        }

        if (!empty($tierPrices)) {
            try {
                $client->call($session_id, 'product_tier_price.update', array($product_id, $tierPrices));
            } catch (Exception $e) {
                echo "Exception 4: ".$product_id . "==>" . $e . "\n";
            }
        }
        
		print "Success: ".$_Pdetails['sku'] . "\n";
    }
}

/*
 * END Update Price values with Product info
 */
?>
