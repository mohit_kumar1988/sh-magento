<?php
require_once '../app/Mage.php';
umask(0);
Mage::app('default');

error_reporting(E_ALL);
$handle_order = fopen("SpringHillAdData.csv", "r");
$count = 0;
$i=0;
$catArray=array();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');

echo "<pre>";
while (($data = fgetcsv($handle_order, 100000, ",")) !== FALSE) {
    //print_r($data);
    //die();

    if($i==0) {
        $i++;
        continue;
    }

    $siteUrl = 'http://www.springhillnursery.com/';
    $urlStr ='';
    $sidKeyword = '';
    $sidStart = strpos($data[6], 'sid=');
    $sidEnd = strpos($data[6], '&', $sidStart);
    if ($sidEnd !== false)
        $sidKeyword = substr($data[6], $sidStart, $sidEnd);
    else
        $sidKeyword = substr($data[6], $sidStart);

    $uniqueId = '';
    // For Product Url rewriting
    $keyword='';
    $productPos = strpos($data['6'], '/p/');
    if ($productPos !== false) {
        $kwStart = strpos($data[6], '/p/');
        $kwEnd = strpos($data[6], '&', $kwStart);
        if ($kwEnd !== false)
            $keyword = substr($data[6], $kwStart, $kwEnd);
        else
            $keyword = substr($data[6], $kwStart);
        //echo $keyword;
        $productArray = explode('&',$keyword );
        $productSku =  substr($productArray[0],3);
        $productId = $write->fetchOne("SELECT entity_id FROM catalog_product_entity where  sku='".$productSku."'");
        if($productId){
            $product = Mage::getModel('catalog/product')->load($productId);
            $urlStr = $siteUrl.$product->getUrlKey().'.html?'.$sidKeyword;
            $uniqueId .= " Product-".$i;
        }
    }

    // For Search Url rewriting
    $keyword ='';
    $pos = strpos($data['6'], 'search.asp');
    if ($pos !== false) {
        $kwStart = strpos($data[6], 'ss=');
        if ($kwStart !== false){
            $kwEnd = strpos($data[6], '&', $kwStart);
            if ($kwEnd !== false)
                $keyword = substr($data[6], $kwStart, $kwEnd);
            else
                $keyword = substr($data[6], $kwStart);
            $urlStr = $siteUrl.'catalogsearch/result/?'.$keyword;
            $uniqueId .= " Product Search-".$i;
        }

    }

    // For Home page Url rewriting
    $defaultPos = stripos($data['6'], 'default.asp');
    if ($defaultPos !== false) {
        $urlStr = $siteUrl.'?'.$sidKeyword;
        $uniqueId .= " Home -".$i;
    }

// For category Url rewriting
    $catArray='';
    $keyword='';
    $catPos = strpos($data['6'], 'category.asp');
    if ($catPos !== false) {
        $kwStart = strpos($data[6], 'c=');
        $kwEnd = strpos($data[6], '&', $kwStart);
        if ($kwEnd !== false)
            $keyword = substr($data[6], $kwStart, $kwEnd);
        else
            $keyword = substr($data[6], $kwStart);
        $catArray = explode('&',$keyword );
        $catNumber =  substr($catArray[0],2);
        $catName = $write->fetchOne("SELECT name FROM sprg_categories where cat_nbr='".$catNumber."'");
        $cat_id = $write->fetchOne("SELECT entity_id FROM catalog_category_entity_varchar where attribute_id = '41' and value='".addslashes($catName)."'");
        if($cat_id){
            $category = Mage::getModel('catalog/category')->load($cat_id);
            $urlStr = $siteUrl.$category->getUrlPath().'?'.$sidKeyword;
            $uniqueId .= " Product Category-".$i;
        }
    }

    if(0 && $urlStr){

        $request_path = str_replace("http://www.springhillnursery.com/", "", $data['6']);
        $request_path = str_replace("http://springhillnursery.com/", "",$request_path);

        $id= $write->fetchOne("SELECT url_rewrite_id FROM core_url_rewrite where request_path='".addslashes($request_path)."'");

        if($id == null ){
            $i++;
            $write->query("insert into core_url_rewrite
				(`url_rewrite_id`,`store_id`,`id_path`,`request_path`,`target_path`,`is_system`,`options`,`description`,`category_id`,`product_id`)
				values ('',1,'SH-".$uniqueId."','".$request_path."','".$urlStr."',0,'RP',NULL,NULL,NULL)");
            echo  $i."Inserted ".$request_path."\n";
        }

    }
    $strFile .=$urlStr ."\n";



}
$myFile = "newUrl.txt";
$fh = fopen($myFile, 'w') or die("can't open file");
fwrite($fh, $strFile);
fclose($fh);

?>


