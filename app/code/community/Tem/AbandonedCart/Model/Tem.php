<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Tem
 * @package    Tem_AbandonedCart
 * @copyright  Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Tem_AbandonedCart_Model_Tem extends Varien_Object
{
    // path in configuration
    const TEM_CONFIG_GLOBAL_SETTING_PATH = 'tem/settings/';
	const TEM_CONFIG_ABANDONED_CART_PATH = 'tem/abandonedcart/';

    //path to cookie file
    //const COOKIE_FILE_LOCATION = '/var/cookie/cookie.txt';

    /**
     * Get information from configuration data
     */
   public function getConfigData($key, $path, $default=false)
    {
        if (!$this->hasData($key)) {
            $value = Mage::getStoreConfig($path . $key);

            $write = Mage::getSingleton("core/resource")->getConnection("core_write");
            $read = Mage::getSingleton("core/resource")->getConnection("core_read");
            $sql_read = "select value from core_config_data where path = 'advanced/modules_disable_output/Tem_AbandonedCart' ";            

            $statusValue=$write->fetchOne($sql_read);

            if (is_null($value) || false===$value || $statusValue == 1 ) { 

                $value = $default;
            }
            $this->setData($key, $value);
        }
        return $this->getData($key);
    }




    /**
     * Check if module is available for using, and customer email is defined
     * @return bool
     */
    public function isEnabled()
    {
        $isActive = $this->getConfigData('active', self::TEM_CONFIG_ABANDONED_CART_PATH) && $this->_getCustomerEmail();
        return $isActive;
    }

    /**
     * Get customer e-mail
     *
     * @return string
     */
    protected function _getCustomerEmail()
    {
        if ($this->hasCustomerEmail()){
            $customerEmail = $this->getCustomerEmail();
        } else if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerEmail = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
            $this->setCustomerEmail($customerEmail);
        } else if (!Mage::getSingleton('customer/session')->isLoggedIn() && $this->getQuote()->getBillingAddress()){
            $customerEmail = $this->getQuote()->getBillingAddress()->getEmail();
            $this->setCustomerEmail($customerEmail);
        } else {
            $customerEmail = '';
        }

        return $customerEmail;
    }


    /**
     * Get customer information for sending to the TEM
     * 
     * @return array with required customer information
     */
    protected function _getCustomerInfo()
    {
        $customerInfo = array();
        if (!Mage::getSingleton('customer/session')->isLoggedIn() && $this->getQuote()->getBillingAddress()){
            $customerInfo = array(
                'f_first_name' => $this->getQuote()->getBillingAddress()->getFirstname(),
                'f_last_name' => $this->getQuote()->getBillingAddress()->getLastname(),
            );
        } else if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customerInfo = array(
                'f_first_name' => Mage::getSingleton('customer/session')->getCustomer()->getFirstname(),
                'f_last_name' => Mage::getSingleton('customer/session')->getCustomer()->getLastname()
            );
        }

        return $customerInfo;
    }

    /**
     * Get base parameters that used in each request
     *
     * @return array
     */
    protected function _getBaseParams()
    {
        return array('c' => $this->getConfigData('client_id', self::TEM_CONFIG_GLOBAL_SETTING_PATH),
                     'ck'  => $this->getConfigData('client_key', self::TEM_CONFIG_GLOBAL_SETTING_PATH),
                     'e' => $this->_getCustomerEmail(),
                     'ch' => $this->getConfigData('channel_id', self::TEM_CONFIG_ABANDONED_CART_PATH));
    }


    /**
     * Change state
     *
     * @param string $currentState - state name
     * @param bool $sendCustomerInfo - send information about customer
     * @return Tem_AbandonedCart_Model_Tem
     */
    public function sendState($currentState, $sendCustomerInfo = false)
    {
        $params = array_merge(array('n_eboac'=> $currentState), $this->_getSpecialParams($currentState));

        if ($sendCustomerInfo && ($customerInfo = $this->_getCustomerInfo())){
            $params = array_merge($params, $customerInfo);
        }

        $requestUrl = $this->formingUrl($params);
		Mage::log($requestUrl);
        $this->sendRequest($requestUrl);
        return $this;
    }

    /**
     * Send all items in cart
     * 
     * @return Tem_AbandonedCart_Model_Tem
     */
    public function sendAllItems()
    {
        $items = $this->getQuote()->getAllItems();

        if (!empty($items)) {
            foreach($items as $item){
                $this->sendAddItem($item);
            }
        }
        return $this;
    }

    protected function _prepareItemInformation($item)
    {
        return array(
            'i_sku' => $item->getProduct()->getSku(),
            'i_qty' => ($item->hasQty()) ? $item->getQty() : 1,
            'i_name' => $item->getProduct()->getName(),
            'i_desc' => $item->getProduct()->getDescription(),
            'i_price' => $item->getCalculationPrice(),
            'i_img_url' => (string) Mage::helper('catalog/image')->init($item->getProduct(), 'thumbnail')->resize(75, 75),
            'i_page_url' => $item->getProduct()->getProductUrl()
        );
    }

    /**
     * Send add item request
     *
     * @param Mage_Sales_Model_Quote_Item $item
     */
    public function sendAddItem(Mage_Sales_Model_Quote_Item $item)
    {
        $itemParams = $this->_prepareItemInformation($item);
        $requestUrl = $this->formingUrl($itemParams);
        $this->sendRequest($requestUrl);
        return $this;
    }

    /**
     * Send remove item request
     * 
     * @param Mage_Sales_Model_Quote_Item $item
     */
    public function sendRemoveItem(Mage_Sales_Model_Quote_Item $item)
    {
        $itemParams = array(
            'i_sku' => $item->getProduct()->getSku(),
            'i_qty' => 0
        );
        
        $requestUrl = $this->formingUrl($itemParams);
        $this->sendRequest($requestUrl);
        return $this;
    }


    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->getCheckout()->getQuote();
    }


    protected function _getSpecialParams($currentState)
    {
        $specParams = array();
        switch ($currentState){
            case 'ViewCart' :
                $totals = $this->getQuote()->getTotals();
                $specParams['f_cart_amount'] = $totals['grand_total']->getValue();
                $specParams['f_view_cart_url'] = Mage::helper('tem_abandonedcart')->getQuoteLink($this->getQuote()->getId());
                break;

            case 'Billing' :
                $totals = $this->getQuote()->getTotals();
                $specParams['f_cart_amount'] = $totals['grand_total']->getValue();
                $specParams['f_view_cart_url'] = Mage::helper('tem_abandonedcart')->getQuoteLink($this->getQuote()->getId());
                break;

            case 'Welcome'  :
            case 'Shipping' :
            case 'ShippingMethod' :
            case 'Payment'  :
            case 'Review'   :
                break;

            case 'OrderComplete' :
                $totals = $this->getQuote()->getTotals();
                $specParams['c_web'] = $totals['subtotal']->getValue();

                $specParams['f_transaction_id'] = $this->getQuote()->getReservedOrderId();

                if ($couponCode = $this->getQuote()->getCouponCode()){
                    $specParams['c_codes'] = $couponCode;
                }

                break;
        }

        return $specParams;
    }

    /**
     * Send API request
     */
    protected function sendRequest($url)
    {
        if ($this->getConfigData('debug_mode', self::TEM_CONFIG_ABANDONED_CART_PATH)){
            Mage::log($url);
        }
        try{
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, TRUE);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		    //curl_setopt($ch, CURLOPT_COOKIEJAR, BP . self::COOKIE_FILE_LOCATION);
		    //curl_setopt($ch, CURLOPT_COOKIEFILE, BP . self::COOKIE_FILE_LOCATION);
		    curl_setopt($ch, CURLOPT_HEADER, 1);

		    $result = curl_exec($ch);
		    $errorCode = curl_errno($ch);

		    if ($errorCode != 0) {
		        Mage::throwException(Mage::helper('tem_abandonedcart')->__("Can't connect to the server."));
		    }
		    curl_close($ch);

		} catch (Exception $e){
			
		}
		return $result;
    }

    /**
     * Forming request URL
     * 
     * @param array $params
     * @return string
     */
    protected function formingUrl($params)
    {
        $params = array_merge($this->_getBaseParams(), $params);

        foreach ($params as $k=>$v){
            $urlParams[] = $k . '=' . urlencode($v);
        }

        return $this->getConfigData('api_url', self::TEM_CONFIG_ABANDONED_CART_PATH) . '?' . implode('&', $urlParams);
    }

    /**
     * send new information after customer login and merge quotes
     */
    public function quoteMergeAfter($quote)
    {
        $params = array('n_eboac'=> 'ViewCart', 'f_view_cart_url' => Mage::helper('tem_abandonedcart')->getQuoteLink($quote->getId()));

        if ($customerInfo = $this->_getCustomerInfo()){
            $params = array_merge($params, $customerInfo);
        }

        $requestUrl = $this->formingUrl($params);
        $this->sendRequest($requestUrl);
        return $this;
    }

}
