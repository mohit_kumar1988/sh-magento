<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Tem
 * @package    Tem_AbandonedCart
 * @copyright  Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Tem_AbandonedCart_CartController extends Mage_Core_Controller_Front_Action
{
    /**
     * Open quote by link
     */
    public function loadAction()
    {
        $quoteParam = $this->getRequest()->getParam('q');

        if (!empty($quoteParam)){
            $quoteId = Mage::helper('core')->decrypt(base64_decode(urldecode($quoteParam)));

            if (!empty($quoteId)) {
                $quote = Mage::getModel('sales/quote')
                        ->setStoreId(Mage::app()->getStore()->getId())
                        ->load($quoteId);

                if ($quote->getId() && $quote->getIsActive()){
                    Mage::getSingleton('checkout/session')->setQuoteId($quote->getId());
                    Mage::getSingleton('checkout/session')->resetCheckout();
                }
            }
        }

        $redirectUrl = Mage::getBaseUrl() . 'checkout/cart';
        
        if (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])){
            $redirectUrl .= '?' . $_SERVER['QUERY_STRING'];
        }

        $this->_redirectUrl($redirectUrl);
    }
}
