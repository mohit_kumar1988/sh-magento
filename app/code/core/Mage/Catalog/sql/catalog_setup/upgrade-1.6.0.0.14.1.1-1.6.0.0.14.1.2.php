<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/** @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$tableName = $installer->getTable('core/url_rewrite');
//drop old unique key UNQ_CORE_URL_REWRITE_ID_PATH_IS_SYSTEM_STORE_ID
$keyNameToDelete = $installer->getIdxName($tableName, array('id_path', 'is_system', 'store_id'),
    Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
$connection->dropIndex($tableName, $keyNameToDelete);

$newKeyFieldSet = array('store_id', 'is_system', 'id_path');
//create new unique key UNQ_CORE_URL_REWRITE_STORE_ID_IS_SYSTEM_ID_PATH
$keyNameToCreate = $installer->getIdxName($tableName, $newKeyFieldSet, Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
$connection->addIndex($tableName, $keyNameToCreate, $newKeyFieldSet, Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);

$installer->endSetup();
