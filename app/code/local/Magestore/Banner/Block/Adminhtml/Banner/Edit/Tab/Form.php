<?php

class Magestore_Banner_Block_Adminhtml_Banner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);

		$fieldset = $form->addFieldset('banner_form', array('legend'=>Mage::helper('banner')->__('Banner information')));
		
		$image_calendar = Mage::getBaseUrl('skin') .'adminhtml/default/default/images/grid-cal.gif';
		
		$fieldset->addField('name', 'text', array(
		  'label'     => Mage::helper('banner')->__('Name'),
		  'class'     => 'required-entry',
		  'required'  => true,
		  'name'      => 'name',
		));

		
		$fieldset->addField('ordering', 'text', array(
		  'label'     => Mage::helper('banner')->__('Order'),
		  'class'     => 'required-entry',
		  'required'  => true,
		  'name'      => 'ordering',
		));

		
		$fieldset->addField('status', 'select', array(
		  'label'     => Mage::helper('banner')->__('Status'),
		  'name'      => 'status',
		  'values'    => array(
			  array(
				  'value'     => 1,
				  'label'     => Mage::helper('banner')->__('Enabled'),
			  ),

			  array(
				  'value'     => 2,
				  'label'     => Mage::helper('banner')->__('Disabled'),
			  ),
		  ),
		));

		$fieldset->addField('client_id', 'select', array(
		  //'label'     => Mage::helper('banner')->__('Client'),
		  'name'      => 'client_id',
		  'values'    => Mage::registry('client_options'),
		  'required'  => false,	
		  'style'	  => 'display:none',  	  
		));

		$fieldset->addField('category_id', 'select', array(
		  'label'     => Mage::helper('banner')->__('Category'),
		  'name'      => 'category_id',
		  'values'    => Mage::registry('category_options'),
		  'class'     => 'required-entry',
		  'required'  => true,

		));


		$fieldset->addField('imptotal', 'text', array(
		  'label'     => Mage::helper('banner')->__('Impressions Purchased (left blank for unlimited)'),
		  'required'  => false,
		  'name'      => 'imptotal',
		));

		$fieldset->addField('clickurl', 'text', array(
		  'label'     => Mage::helper('banner')->__('Click URL'),
		  'class'     => 'required-entry',
		  'required'  => true,
		  'name'      => 'clickurl',
		 
		));

		
		$fieldset->addField('clicks', 'text', array(
		  'label'     => Mage::helper('banner')->__('Clicks'),
		  // 'class'     => 'required-entry',
		  'required'  => false,
		  'readonly'  => 'readonly',
		  'name'      => 'clicks',
		));

		$fieldset->addField('impmade', 'text', array(
		  'label'     => Mage::helper('banner')->__('Impmade'),
		  // 'class'     => 'required-entry',
		  'required'  => false,
		  'readonly'  => 'readonly',
		  'name'      => 'impmade',
		));
		  	

		$fieldset->addField('image', 'image', array(
		  'label'     => Mage::helper('banner')->__('Banner Image'),
		  'required'  => false,
		  'name'      => 'image',		
		));
		

		$fieldset->addField('width', 'text', array(
		  'label'     => Mage::helper('banner')->__('Width'),
		  'required'  => false,
		  'name'      => 'width',		
		));

		$fieldset->addField('height', 'text', array(
		  'label'     => Mage::helper('banner')->__('Height'),
		  'required'  => false,
		  'name'      => 'height',		
		));
		
		
		$fieldset->addField('startdate','date', array(
		  'label'     => Mage::helper('banner')->__('Start date'),
		  'required'  => true,
		  'format'	  => 'yyyy-MM-dd',
		  'image'     => $image_calendar,
		  'name'      => 'startdate',		
		));

		$fieldset->addField('enddate','date', array(
		  'label'     => Mage::helper('banner')->__('End date'),
		  'required'  => true,
		  'format'	  => 'yyyy-MM-dd',
		  'image'     => $image_calendar,
		  'name'      => 'enddate',		
		));
		
		$fieldset->addField('banner_cond','select', array(
		  'label'     => Mage::helper('banner')->__('Banner Condition'),
		  'required'  => true,
		  'name'      => 'banner_cond',
		  'values'    =>  array(
			  array(
				  'value'     => 1,
				  'label'     => Mage::helper('banner')->__('Yes'),
			  ),

			  array(
				  'value'     => 0,
				  'label'     => Mage::helper('banner')->__('No'),
			  ),
		  ),


		));
		$onchange = $fieldset->addField('show_cond','select', array(
		  'label'     => Mage::helper('banner')->__('Show Condition'),
		  'required'  => true,
		  'name'      => 'show_cond',
		  'values'    =>  array(
			  array(
				  'value'     => '',
				  'label'     => Mage::helper('banner')->__('Please Select'),
			  ),
			  array(
				  'value'     => 1,
				  'label'     => Mage::helper('banner')->__('Yes'),
			  ),

			  array(
				  'value'     => 0,
				  'label'     => Mage::helper('banner')->__('No'),
			  ),
		  ),


		));

		$onchange->setAfterElementHtml("<script>
        jQuery.noConflict();
		jQuery(document).ready(function($){
			var val =$('#show_cond').val();
			//alert(val);
	   		if(val==1){
				$('#condition_form .hor-scroll').show();
			}else{
				$('#link_cond').val('');
				$('#image_text').val('');
				$('#text_width').val('');
				$('#text_height').val('');
				$('#image_position').val('');
				$('#color').val('');
				$('#font_size').val('');
				$('#font_weight').val('');	
				$('#condition_form .hor-scroll').hide();					
				
			}
			$('#show_cond').change(function() {
		
				var val =$('#show_cond').val();
				//alert(val);
		   		if(val==1){
					$('#condition_form .hor-scroll').show();
				}else{
					$('#link_cond').val('');
					$('#image_text').val('');
					$('#text_width').val('');
					$('#text_height').val('');
					$('#image_position').val('');
					$('#color').val('');
					$('#font_size').val('');
					$('#font_weight').val('');	
					$('#condition_form .hor-scroll').hide();					
					
				}
	 	 	});
		});
		
        </script>");
		

		$fieldset = $form->addFieldset('condition_form', array('legend'=>Mage::helper('banner')->__('Banner Text Link Condition')));


		$fieldset->addField('link_cond', 'select', array(
		  'label'     => Mage::helper('banner')->__('Click URL Link To'),
		  'name'      => 'link_cond',
		  'required'  => true,
		  'values'    => array(
			  
			  array(
				  'value'     => '1',
				  'label'     => Mage::helper('banner')->__('Complete Image'),
			  ),
			   array(
				  'value'     => '2',
				  'label'     => Mage::helper('banner')->__('Only Text'),
			  ),                            
		  ),
		));

		$fieldset->addField('image_text', 'text', array(
		  'label'     => Mage::helper('banner')->__('Banner Image Text'),
		  'name'      => 'image_text',
		  'maxlength' => "255",
		 
		));

		if($this->getRequest()->getParam('id')){
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$attr_height =  $write -> fetchOne( "SELECT text_height  FROM banner  where id = '".$this->getRequest()->getParam('id')."'");
		}

		$fieldset->addField('text_width', 'text', array(
						'label'     => Mage::helper('banner')->__('Text Width & Heigth (in px)'),
						'name'      => 'text_width',
						'style'  => "width:129px",					
						'after_element_html' => '<span class="hint" id="replacevalue"> &nbsp; <input type="text" id="text_height" style ="width:129px" name="text_height" class=" input-text" value="'.$attr_height.'" /></span>',
				
		));

		$fieldset->addField('image_position', 'select', array(
		  'label'     => Mage::helper('banner')->__('Image Position'),
		  'name'      => 'image_position',
		  'values'    => array(
			 array(
				  'value'     => '',
				  'label'     => Mage::helper('banner')->__('--Please Select--'),
			  ),
			  array(
				  'value'     => 1,
				  'label'     => Mage::helper('banner')->__('Top Left'),
			  ),

			  array(
				  'value'     => 2,
				  'label'     => Mage::helper('banner')->__('Top Right'),
			  ),
			  array(
				  'value'     => 3,
				  'label'     => Mage::helper('banner')->__('Top Center'),
			  ),

			  array(
				  'value'     => 4,
				  'label'     => Mage::helper('banner')->__('Left'),
			  ),
			   array(
				  'value'     => 5,
				  'label'     => Mage::helper('banner')->__('Right'),
			  ),

			  array(
				  'value'     => 6,
				  'label'     => Mage::helper('banner')->__('Center'),
			  ),
			  array(
				  'value'     => 7,
				  'label'     => Mage::helper('banner')->__('Bottom Left'),
			  ),

			  array(
				  'value'     => 8,
				  'label'     => Mage::helper('banner')->__('Bottom Right'),
			  ),
			  array(
				  'value'     => 9,
				  'label'     => Mage::helper('banner')->__('Bottom Center'),
			  ),
		  ),
		));


		$fieldset->addField('color', 'text', array(
		  'label'     => Mage::helper('banner')->__('Font Color'),
		  'required'  => false,
		  'maxlength' => "6",
		  'class'     => 'color',
		  'name'      => 'color',		
		));

		$fieldset->addField('font_size', 'text', array(
		  'label'     => Mage::helper('banner')->__('Font Size (in px)'),
		  'required'  => false,
		  'name'      => 'font_size',		
		));

		$fieldset->addField('font_weight', 'select', array(
		  'label'     => Mage::helper('banner')->__('Font Weight'),
		  'required'  => false,
		  'name'      => 'font_weight',
		   'values'    => array(
			 array(
				  'value'     => '',
				  'label'     => Mage::helper('banner')->__('--Please Select--'),
			  ),
			  array(
				  'value'     => 'normal',
				  'label'     => Mage::helper('banner')->__('Normal'),
			  ),

			  array(
				  'value'     => 'bold',
				  'label'     => Mage::helper('banner')->__('Bold'),
			  ),

			),		
		));

		$fieldset = $form->addFieldset('extra_form', array('legend'=>Mage::helper('banner')->__('Banner Extra Information')));


		$fieldset->addField('custombannercode', 'editor', array(
		  'name'      => 'custombannercode',
		  'label'     => Mage::helper('banner')->__('Custom Banner Code'),
		  'title'     => Mage::helper('banner')->__('Custom Banner Code'),
		  'style'     => 'width:700px; height:150px;',
		  'wysiwyg'   => false,
		  'required'  => false,
		));


		$fieldset->addField('description', 'editor', array(
		  'name'      => 'description',
		  'label'     => Mage::helper('banner')->__('Description/notes'),
		  'title'     => Mage::helper('banner')->__('Description/notes'),
		  'style'     => 'width:700px; height:100px;',
		  'wysiwyg'   => false,
		  'required'  => false,
		));


		if ( Mage::getSingleton('adminhtml/session')->getBannerData() )
		{
			$banner_data = Mage::getSingleton('adminhtml/session')->getBannerData();

		  //$form->setValues(Mage::getSingleton('adminhtml/session')->getBannerData());
		  Mage::getSingleton('adminhtml/session')->setBannerData(null);
		} elseif ( Mage::registry('banner_data') ) {
		  //$form->setValues(Mage::registry('banner_data')->getData());
		  $banner_data = Mage::registry('banner_data')->getData(); 
		}

		if(isset($banner_data['image']) && $banner_data['image'] )
		{
			$banner_data['image'] = Mage::getBaseUrl('media') . 'banners/'.$banner_data['image'];
		}
		
		$form->setValues($banner_data);

		return parent::_prepareForm();
	}
}
