<?php

class Magestore_Banner_Block_Adminhtml_Block_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('block_form', array('legend'=>Mage::helper('banner')->__('Block information')));
     
	 $fieldset->addField('alias', 'text', array(
          'label'     => Mage::helper('banner')->__('Alias'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'alias',
      ));
	 
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('banner')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

	  $fieldset->addField('sort_order', 'text', array(
		  'label'     => Mage::helper('banner')->__('Sort Order'),
		  'class'     => 'required-entry',
		  'required'  => true,
		  'name'      => 'sort_order',
		));

	  $fieldset->addField('block_position', 'select', array(
			'name'      => 'block_position',
            'label'     => $this->__('Block Position'),
            'title'     => $this->__('Block Position'),
			'class'     => 'required-entry',
			'required'  => true,
            'values'    => Mage::helper('banner')->getBlockIdsToOptionsArray(),
		
        ));
	  
	  $fieldset->addField('show_title', 'select', array(
		  'label'     => Mage::helper('banner')->__('Show Title'),
		  'name'      => 'show_title',
		  'values'    => array(
			  array(
				  'value'     => 1,
				  'label'     => Mage::helper('banner')->__('Enabled'),
			  ),

			  array(
				  'value'     => 2,
				  'label'     => Mage::helper('banner')->__('Disabled'),
			  ),
		  ),
		));

	 $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('banner')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('banner')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('banner')->__('Disabled'),
              ),
          ),
      ));
	  
	  
	  
	  $fieldset->addField('client_id', 'select', array(
		  //'label'     => Mage::helper('banner')->__('Client'),
		  'name'      => 'client_id',
		  'values'    => Mage::registry('client_options'),
		  'required'  => false,	
		  'style'	  => 'display:none',		  
		));

		$fieldset->addField('category_id', 'select', array(
		  'label'     => Mage::helper('banner')->__('Category'),
		  'name'      => 'category_id',
		  'values'    => Mage::registry('category_options'),
		  'class'     => 'required-entry',
		  'required'  => true,		  
		));
      
	  
	  /*$fieldset->addField('target_method', 'select', array(
          'label'     => Mage::helper('banner')->__('Tartget'),
          'name'      => 'target_method',
          'values'    => array(
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('banner')->__('New Window with Browser Navigation'),
              ),

              array(
                  'value'     => 1,
                  'label'     => Mage::helper('banner')->__('Parent Window with Browser Navigation'),
              ),
			  
			   array(
                  'value'     => 2,
                  'label'     => Mage::helper('banner')->__('New Window without Browser Navigation'),
              ),
          ),
      ));*/
	  
	  $fieldset->addField('sort_method', 'select', array(
          'label'     => Mage::helper('banner')->__('Sort Type'),
          'name'      => 'sort_method',
          'values'    => array(
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('banner')->__('Randomise'),
              ),

              array(
                  'value'     => 1,
                  'label'     => Mage::helper('banner')->__('Ordering'),
              ),
			  			  
          ),
      ));

	  

		$fieldset->addField('width', 'text', array(
		  'label'     => Mage::helper('banner')->__('Width (px)'),
		  'name'      => 'width',
		  'class'     => 'required-entry',
		  'required'  => true,
		));
		
		$fieldset->addField('height', 'text', array(
		  'label'     => Mage::helper('banner')->__('Height (px)'),
		  'name'      => 'height',
		  'class'     => 'required-entry',
		  'required'  => true,
		));		
	  
	  $fieldset->addField('display_method', 'select', array(
          'label'     => Mage::helper('banner')->__('Display Type'),
          'name'      => 'display_method',
          'values'    => array(
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('banner')->__('Normal'),
              ),

              // array(
              //     'value'     => 1,
              //     'label'     => Mage::helper('banner')->__('Slide'),
              // ),
              
			  array(
                  'value'     => 2,
                  'label'     => Mage::helper('banner')->__('Pro Slide'),
              ),
			  
          ),
      ));
	  
	  $fieldset->addField('show_lable', 'select', array(
          'label'     => Mage::helper('banner')->__('Show Label'),
          'name'      => 'show_lable',
          'values'    => array(
              array(
                  'value'     => 0,
                  'label'     => Mage::helper('banner')->__('No'),
              ),

              array(
                  'value'     => 1,
                  'label'     => Mage::helper('banner')->__('Yes'),
              ),
			  			  
          ),
      ));
	  
	  $fieldset->addField('delay_time', 'text', array(
          'label'     => Mage::helper('banner')->__('Delay Time (ms)'),
          'name'      => 'delay_time',
      ));	

	  $fieldset->addField('speed', 'text', array(
          'label'     => Mage::helper('banner')->__("Speed of image's change (ms)"),
          'name'      => 'speed',
      ));		  
	
	  $fieldset->addField('num_banner', 'text', array(
          'label'     => Mage::helper('banner')->__('Number Banner Show'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'num_banner',
      ));
     
	 $fieldset->addField('header_text', 'editor', array(
          'name'      => 'header_text',
          'label'     => Mage::helper('banner')->__('Header Text'),
          'title'     => Mage::helper('banner')->__('Header Text'),
          'style'     => 'width:500px; height:80px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));
	 
	 $fieldset->addField('footer_text', 'editor', array(
          'name'      => 'footer_text',
          'label'     => Mage::helper('banner')->__('Footer Text'),
          'title'     => Mage::helper('banner')->__('Footer Text'),
          'style'     => 'width:500px; height:80px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));
	 
      $fieldset->addField('description', 'editor', array(
          'name'      => 'description',
          'label'     => Mage::helper('banner')->__('Description'),
          'title'     => Mage::helper('banner')->__('Description'),
          'style'     => 'width:700px; height:150px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));
	  
	  $onchange = $fieldset->addField('cookie_control','select', array(
	  'label'     => Mage::helper('banner')->__('Controlled By Cookie'),
	  'required'  => true,
	  'name'      => 'cookie_control',
	  'values'    =>  array(
		  array(
			  'value'     => 1,
			  'label'     => Mage::helper('banner')->__('Yes'),
		  ),

		  array(
			  'value'     => 0,
			  'label'     => Mage::helper('banner')->__('No'),
		  ),
	  ),


	));
	
	$onchange->setAfterElementHtml("<script>
        jQuery.noConflict();
		jQuery(document).ready(function($){
			$('#condition_form .hor-scroll').hide();
			var val =$('#cookie_control').val();
			//alert(val);
	   		if(val==1){
				$('#cookie-label').show();
				$('#cookie_name').addClass('required-entry');
				$('#condition_form .hor-scroll').show();
			}else{
				$('#cookie_name').val('');
				$('#cookie_value').val('');

				$('#cookie_name').removeClass('required-entry');
				$('#condition_form .hor-scroll').hide();					
				
			}
			$('#cookie_control').change(function() {
		
				var val =$('#cookie_control').val();
				//alert(val);
		   		if(val==1){
					$('#cookie_name').addClass('required-entry');
					$('#cookie-label').show();	
					$('#condition_form .hor-scroll').show();
				}else{
					$('#cookie_name').val('');
					$('#cookie_value').val('');
					$('#cookie-label').hide();
					$('#cookie_name').removeClass('required-entry');
					$('#condition_form .hor-scroll').hide();					
					
				}
	 	 	});
		});
		
        </script>");
	$fieldset = $form->addFieldset('condition_form', array('legend'=>Mage::helper('banner')->__('Cookie Control')));


	$fieldset->addField('cookie_name', 'text', array(
	  'label'     => Mage::helper('banner')->__('Cookie Name'),
	  'name'      => 'cookie_name',
	  'required'  => true,
	  'maxlength' => "255",
	));

	$fieldset->addField('cookie_value', 'text', array(
	  'label'     => Mage::helper('banner')->__('Cookie Value'),
	  'name'      => 'cookie_value',
	  'maxlength' => "255",
	 
	));
      if ( Mage::getSingleton('adminhtml/session')->getBlockData() )
      {
		$block_data =  	Mage::getSingleton('adminhtml/session')->getBlockData();
          //$form->setValues(Mage::getSingleton('adminhtml/session')->getBlockData());
          Mage::getSingleton('adminhtml/session')->getBlockData(null);

      } elseif ( Mage::registry('block_data') ) {
          $block_data = Mage::registry('block_data')->getData();
		  //$form->setValues(Mage::registry('block_data')->getData());
      }
	  
	  $form->setValues($block_data);
	  
      return parent::_prepareForm();
  }
}
