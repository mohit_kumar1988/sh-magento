<?php

class Magestore_Banner_Block_Adminhtml_Condition_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'banner';
        $this->_controller = 'adminhtml_condition';
        $banner_id = $this->getRequest()->getParam('banner_id');
		$id = $this->getRequest()->getParam('id');
        $this->_updateButton('save', 'label', Mage::helper('banner')->__('Save Condition Information'));
        //$this->_updateButton('delete', 'label', Mage::helper('banner')->__('Delete Condition'));
		$this->_removeButton('delete');
		$this->removeButton('back'); 
		$this->removeButton('reset');
		$this->_addButton('backnew', array(
            'label'   => Mage::helper('adminhtml')->__('Back'),
            'onclick' => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('*/*/index/banner_id/'.$banner_id) . '\')',
            'class'   => 'back',
            'level'   => 1
    	));


		$this->_addButton('deletenew', array(
            'label'   => Mage::helper('adminhtml')->__('Delete'),
            'onclick' => 'deleteConfirm(\'Are you sure you want to do this?\', \'' . Mage::helper('adminhtml')->getUrl('*/*/delete/id/'.$id.'/banner_id/'.$banner_id.'' ) . '\')',
            'class'   => 'delete',
    	));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => "saveAndContinueEdit('$banner_id')",
            'class'     => 'save',
        ), -100);
		
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('condition_description') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'condition_description');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'condition_description');
                }
            }

            function saveAndContinueEdit(bannerid){		
				editForm.submit($('edit_form').action+'back/edit/banner_id/'+bannerid+'/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('condition_data') && Mage::registry('condition_data')->getId() ) {
			
		   return Mage::helper('banner')->__("Edit Condition", $this->htmlEscape(Mage::registry('condition_data')->getAttrType()));
        } else {
            
			return Mage::helper('banner')->__('Add Condition');
        }
    }
}
