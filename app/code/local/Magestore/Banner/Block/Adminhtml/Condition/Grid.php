<?php

class Magestore_Banner_Block_Adminhtml_Condition_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();

		$this->setId('conditionGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);

	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('banner/condition')->getCollection();
		$collection->addFieldToSelect("*");
		$collection->addAttributeToFilter('banner_id' , array(' = '=> $this->getRequest()->getParam('banner_id') ));
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
	  $this->addColumn('id', array(
		  'header'    => Mage::helper('banner')->__('ID'),
		  'align'     =>'right',
		  'width'     => '50px',
		  'index'     => 'id',
	  ));

	  $this->addColumn('attr_type', array(
		  'header'    => Mage::helper('banner')->__('Attribute Type'),
		  'align'     =>'left',
		  'index'     => 'attr_type',
	  ));
	  
	  $this->addColumn('attr_code', array(
		  'header'    => Mage::helper('banner')->__('Attributes'),
		  'align'     =>'left',
		  'index'     => 'attr_code',
	  ));

	 $this->addColumn('relation', array(
		  'header'    => Mage::helper('banner')->__('Relation'),
		  'align'     =>'left',
		  'index'     => 'relation',
	  ));
	 $this->addColumn('name', array(
		  'header'    => Mage::helper('banner')->__('Attribute Value'),
		  'align'     =>'left',
		  'index'     => 'name',
		  'renderer'  => 'banner/adminhtml_renderer_attributevalue',

	  ));
	  
	 $this->addColumn('attr_cond', array(
		  'header'    => Mage::helper('banner')->__('Condition'),
		  'align'     =>'left',
		  'index'     => 'attr_cond',
	  ));
		$this->addColumn('sort_order', array(
		  'header'    => Mage::helper('banner')->__('Order'),
		  'align'     =>'center',
		  'width'     => '80px',
		  'index'     => 'sort_order',
	  ));
	 	  $this->addColumn('status', array(
		  'header'    => Mage::helper('banner')->__('Status'),
		  'align'     => 'left',
		  'width'     => '80px',
		  'index'     => 'status',
		  'type'      => 'options',
		  'options'   => array(
		      1 => 'Enabled',
		      2 => 'Disabled',
		  ),
	  ));
	  
	    $banner_id= $this->getRequest()->getParam('banner_id');
		$this->addColumn('action',
		    array(
		        'header'    =>  Mage::helper('banner')->__('Action'),
		        'width'     => '100',
		        'type'      => 'action',
		        'getter'    => 'getId',
		        'actions'   => array(
		            array(
		                'caption'   => Mage::helper('banner')->__('Edit'),
		                //'url'       => array('base'=> '*/*/edit' ,"banner_id" => $banner_id),
						'url'       => $this->getUrl('*/*/edit', array('id' => '$id', 'banner_id' => $banner_id)),


		            )
		        ),
		        'filter'    => false,
		        'sortable'  => false,
		        'index'     => 'stores',
		        'is_system' => true,
		));
	


	

		$this->addExportType('*/*/exportCsv', Mage::helper('banner')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('banner')->__('XML'));
	  
	  return parent::_prepareColumns();
	}

	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('condition');
		$banner_id = $this->getRequest()->getParam('banner_id');
		$this->getMassactionBlock()->addItem('delete', array(
			'label'    => Mage::helper('banner')->__('Delete'),
			'url'      => $this->getUrl('*/*/massDelete', array('banner_id'=>$banner_id )),
			'confirm'  => Mage::helper('banner')->__('Are you sure?')
		));

		$statuses = Mage::getSingleton('banner/status')->getOptionArray();
 
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('banner')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('banner')->__('Status'),
                         'values' => $statuses
        			)
        		)
        ));

	   
		return $this;
	}

	public function getRowUrl($row)
	{
		 $banner_id= $this->getRequest()->getParam('banner_id');
	  return $this->getUrl('*/*/edit', array('id' => $row->getId(),  'banner_id' => $banner_id));
	}
  

}
