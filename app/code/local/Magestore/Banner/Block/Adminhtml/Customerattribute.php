<?php
class Magestore_Banner_Block_Adminhtml_Customerattribute extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_customerattribute';
    $this->_blockGroup = 'banner';
    $this->_headerText = Mage::helper('banner')->__('Attribute Manager');
    $this->_addButtonLabel = Mage::helper('banner')->__('Add Attribute');
    parent::__construct();
	$this->_removeButton('add'); 
  }
}
