
<?php

class Magestore_Banner_Block_Adminhtml_Customerattribute_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('customerattribute_form', array('legend'=>Mage::helper('banner')->__('Attribute Information')));
		
		$image_calendar = Mage::getBaseUrl('skin') .'adminhtml/default/default/images/grid-cal.gif';

        
		  $fieldset->addField('attr_type', 'select', array(
			  'label'     => Mage::helper('banner')->__('Attribute Type'),
			  'name'      => 'attr_type',
			  'disabled'  => 'disabled',
			  'values'    => array(
				  array(
				      'value'     => 'Customer',
				      'label'     => Mage::helper('banner')->__('Customer Attributes'),
				  ),
                  array(
				       'value'     => 'CustomerAddress',
				       'label'     => Mage::helper('banner')->__('Customer Address Attributes'),
				                  ),
                  array(
				       'value'     => 'Category',
				       'label'     => Mage::helper('banner')->__('Product Category Attributes'),
				                  ),

				  array(
				      'value'     => 'Product',
				      'label'     => Mage::helper('banner')->__('Product Attributes'),
				  ),

				 /* array(
				      'value'     => 'Purchase',
				      'label'     => Mage::helper('banner')->__('Purchase Attributes'),
				  ),*/
			  ),
			));


            $id = $this->getRequest()->getParam('id');
            if($id==1) {
            $attributesInfo = Mage::getResourceModel('customer/attribute_collection')
            ->addSystemHiddenFilter()
            ->addExcludeHiddenFrontendFilter();

           /* $addresscollection = Mage::getResourceModel('customer/address_attribute_collection')
            ->addSystemHiddenFilter()
            ->addExcludeHiddenFrontendFilter();*/

            }else if($id==2) {
             $attributesInfo = Mage::getResourceModel('customer/address_attribute_collection')
            ->addSystemHiddenFilter()
            ->addExcludeHiddenFrontendFilter();

            }else if($id==3) {
              $attributesInfo = Mage::getResourceModel('eav/entity_attribute_collection')
		            ->setEntityTypeFilter(3)
		            ->addSetInfo()
		            ->getData();

            }else if($id==4) {
               $attributesInfo = Mage::getResourceModel('eav/entity_attribute_collection')
		            ->setEntityTypeFilter(4)
		            ->addSetInfo()
		            ->getData();
            }

			if(count($attributesInfo)>1){
		        $attr = array();
				foreach($attributesInfo as $attCode) {

		        if(!empty($attCode['frontend_label'])) {
		        $attr[] = array(
									'label' => $attCode['frontend_label'] ,
									'value' => $attCode['attribute_code'] ,
									);
							}

		        }
			}		

           /* if($id==1) {


			foreach($addresscollection as $addrCode) {

            if(!empty($addrCode['frontend_label'])) {
					$attr[] = array(
								'label' => $addrCode['frontend_label'] ,
								'value' => $addrCode['attribute_code'] ,
								);
						}
            }
            } */
			
			$fieldset->addField('attr_code', 'multiselect', array(
					  'label'     => Mage::helper('banner')->__("Attribute Code"),
					  'name'      => 'attr_code[]',
					  'required'  => true,
					  'values'     => $attr,
			)); 

		$fieldset->addField('status', 'select', array(
		  'name'      => 'status',
		  'style'     => 'display:none;',
		  'values'    => array(
			  array(
				  'value'     => 1,
				  'label'     => Mage::helper('banner')->__('Enabled'),
			  ),

			  array(
				  'value'     => 2,
				  'label'     => Mage::helper('banner')->__('Disabled'),
			  ),
		  ),
		));
		 if ( Mage::getSingleton('adminhtml/session')->getCustomerattributeData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getCustomerattributeData());
          Mage::getSingleton('adminhtml/session')->getCustomerattributeData(null);
      } elseif ( Mage::registry('customerattribute_data') ) {
          $form->setValues(Mage::registry('customerattribute_data')->getData());
      }

		return parent::_prepareForm();
	}
}
