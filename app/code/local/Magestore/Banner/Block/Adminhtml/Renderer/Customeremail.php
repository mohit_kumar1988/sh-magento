<?php
class Magestore_Banner_Block_Adminhtml_Renderer_Customeremail
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		
		if ($row->getCustomerId()) {
			return sprintf(
				'<a href="%s">%s</a>',
				$this->getUrl('adminhtml/customer/edit', array('_current'=>true,'id' => $row->getCustomerId())),
				$row->getEmail()
			);
		}
		else return sprintf ($row->getEmail());
	}
}
?>