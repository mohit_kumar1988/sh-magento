<?php
class Magestore_Banner_Block_Client extends Mage_Core_Block_Template
{
	
	public function _prepareLayout()
    {
		parent::_prepareLayout();
		$pager = $this->getLayout()->createBlock('page/html_pager', 'banner.index.index.pager')
            ->setCollection($this->getBanners());
        $this->setChild('clientpager', $pager);
        $this->getBanners()->load();
        return $this;
    }

    public function getPageHtml()
    {
        return $this->getChildHtml('clientpager');
    }
	
	public function addAffiliateLink()
	{
		$customerid = Mage::getSingleton('customer/session')->getData('id');
		$clientCustomerIds = Mage::helper('banner/client')->getClientCustomerIds();
		$customerNavigator = $this->getParentBlock();
		if($customerNavigator)
		if(in_array($customerid, $clientCustomerIds))
		$customerNavigator->addLink('bannerSystem','banner/index/index',
									$this->__('My Banners')
								);
	}
	
	public function getBanners()
	{
		if(!$this->hasData('listbanner')){
			$customerid = Mage::getSingleton('customer/session')->getData('id');
			$clientid = Mage::getResourceModel('banner/client_collection')
														->addFieldToFilter('customer_id', $customerid)
														->getFirstItem()
														->getData('id')
														;
			$banners = Mage::getResourceModel('banner/banner_collection')
													->addFieldToFilter('client_id', $clientid)
													->setOrder('id', 'desc')
													;
			$this->setData('listbanner',$banners);
		}
		return $this->getData('listbanner');
	}
	
	public function getCategory($banner)
	{
		$categorys = Mage::helper('banner')->getArrCategory();
		$categoryid = $banner->getCategoryId();
		return $categorys[$categoryid];
	}
	
	public function getStatus($banner)
	{
		$status =  array(
              1 => 'Enabled',
              2 => 'Disabled'
          );
		return $status[$banner->getStatus()];
	}
	
	public function getClicks($banner)
	{
		$clicks = "";
		if($banner->getImpmade() > 0)
		{
			$clicks = $banner->getClicks() . " - " . number_format(((100.0 * $banner->getClicks() / $banner->getImpmade())),2,'.','') . "%";
		}
		else
		{
			$clicks = "0 - 0%";
		}
		return $clicks;
	}
	
	public function getImpressions($banner)
	{
		$impressions = "";
		if($banner->getImptotal() == 0)
		{
			// $impressions = $banner->getImpmade() . " of unlimited";
			$impressions = $this->__("%s of unlimited", $banner->getImpmade());
		}
		else
		{
			// $impressions = $banner->getImpmade() . " of ". $banner->getImptotal();
			$impressions = $this->__("%s of %s", $banner->getImpmade(), $banner->getImptotal());
		}
		return $impressions;
	}
}