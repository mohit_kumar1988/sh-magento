<?php

class Magestore_Banner_Model_Condition extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('banner/condition');
    }
	
	public function getOptions()
	{
		$options = array();
		$conditionCollection = $this->getCollection();
		
		foreach($conditionCollection as $condition)
		{
			$option = array();
			$option['label'] = $condition->getName();
			$option['value'] = $condition->getId();
			$options[] = $option;
		}
		
		return $options;
	}
}
