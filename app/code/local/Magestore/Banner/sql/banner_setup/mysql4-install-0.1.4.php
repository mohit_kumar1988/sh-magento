<?php
$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('banner')};
CREATE TABLE {$this->getTable('banner')} (
  `id` int(11) NOT NULL auto_increment,
  `client_id` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `imptotal` int(11) NOT NULL default '0',
  `impmade` int(11) NOT NULL default '0',
  `clicks` int(11) NOT NULL default '0',
  `image` varchar(100) NOT NULL,
  `clickurl` varchar(200) NOT NULL default '',
  `status` tinyint(1) NOT NULL default '0',
  `custombannercode` text,
  `category_id` int(10) unsigned NOT NULL default '0',
  `description` text NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `publish_up` datetime NOT NULL default '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL default '0000-00-00 00:00:00',
  `height` int(11) default NULL,
  `width` int(11) default NULL,
  `startdate` date NOT NULL default '0000-00-00',
  `enddate` date NOT NULL default '0000-00-00',
  `color` varchar(10) NOT NULL default '',
  `image_text` varchar(255) NOT NULL,
  `image_position` smallint(6) NOT NULL,
  `link_cond` smallint(6) NOT NULL,
  `font_size` varchar(10) NOT NULL,
  `font_weight` varchar(10) NOT NULL,
  `text_width` int(11) NOT NULL,
  `text_height` int(11) NOT NULL,
  `show_cond` smallint(6) NOT NULL,
  `banner_cond` smallint(6) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `viewbanner` (`status`),
  KEY `idx_banner_catid` (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

DROP TABLE IF EXISTS {$this->getTable('bannerblock')};
CREATE TABLE {$this->getTable('bannerblock')} (
  `id` int(11) NOT NULL auto_increment,
  `alias` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL default '1',
  `show_title` tinyint(1) NOT NULL default '0',
  `display_method` tinyint(1) NOT NULL default '0',
  `width` smallint(6) NOT NULL default '300',
  `height` smallint(6) NOT NULL default '200',
  `speed` smallint(6) NOT NULL default '500',
  `show_lable` tinyint(1) NOT NULL default '0',
  `delay_time` int(11) NOT NULL default '0',
  `title` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
  `description` text character set utf8 collate utf8_unicode_ci,
  `sort_method` int(11) NOT NULL,
  `header_text` varchar(255) character set utf8 collate utf8_unicode_ci default NULL,
  `footer_text` varchar(255) character set utf8 collate utf8_unicode_ci default NULL,
  `num_banner` int(11) NOT NULL default '1',
  `target_method` int(11) NOT NULL default '0',
  `sort_order` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

DROP TABLE IF EXISTS {$this->getTable('bannercategory')};
CREATE TABLE {$this->getTable('bannercategory')} (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) character set utf8 collate utf8_unicode_ci NOT NULL,
  `description` text character set utf8 collate utf8_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `status` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

DROP TABLE IF EXISTS {$this->getTable('bannerclient')};
CREATE TABLE {$this->getTable('bannerclient')} (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `contact` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `extrainfo` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

DROP TABLE IF EXISTS {$this->getTable('bannerattributes')};
CREATE TABLE {$this->getTable('bannerattributes')} (
  `id` int(11) NOT NULL auto_increment,
  `attr_type` varchar(255) NOT NULL default '',
  `attr_code` varchar(255) NOT NULL default '',
  `status` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

INSERT INTO {$this->getTable('bannerattributes')} (`id`, `attr_type`, `attr_code`, `status`) VALUES
(1, 'Customer', 'dob,email,firstname,gender,lastname,middlename', '1'),
(2, 'CustomerAddress', 'city,firstname,lastname,postcode,telephone', '1'),
(3, 'Category', 'name', '1'),
(4, 'Product', 'name,price,sku,status,weight', '1');

DROP TABLE IF EXISTS {$this->getTable('bannercondition')};
CREATE TABLE {$this->getTable('bannercondition')} (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`banner_id` int( 11 ) NOT NULL ,
`category_id` int(11) NOT NULL,
`entity_type_id` int( 11 ) NOT NULL ,
`attribute_id` int( 11 ) NOT NULL ,
`attr_type` varchar( 255 ) NOT NULL DEFAULT '',
`attr_code` varchar( 255 ) NOT NULL DEFAULT '',
`relation` varchar( 255 ) NOT NULL DEFAULT '',
`attr_value` varchar( 255 ) NOT NULL DEFAULT '',
`attr_cond` varchar( 50 ) NOT NULL DEFAULT '',
`sort_order` int( 11 ) NOT NULL ,
`status` varchar( 255 ) NOT NULL DEFAULT '',
PRIMARY KEY ( `id` )
) ENGINE = MYISAM DEFAULT CHARSET = utf8;

    ");

$installer->endSetup(); 
