<?php
$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('bannerblock')}
	ADD `cookie_control` int(11),
	ADD `cookie_name` varchar(255),
	ADD `cookie_value` varchar(255) ;
	

");

$installer->endSetup(); 
