<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Sphill_Customization_Block_Tracking extends Mage_Core_Block_Template
{
    protected $_xmlPathEnabled = '';
    protected $_xmlPathCid = '';

    protected $_orders;

    /**
     * Checking enabled tracking code or not
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->_xmlPathEnabled || !Mage::getStoreConfigFlag($this->_xmlPathEnabled)) {
            return '';
        }
        /**
         * Display rate feature. Used in BizRate
         */
        if (!empty($this->_xmlPathDisplayRate)) {
            $rate = (int)Mage::getStoreConfig($this->_xmlPathDisplayRate);
            $rand = rand(0, 100);
            if ($rand > $rate) {
                return '';
            }
        }
        return parent::_toHtml();
    }

    /**
     * Get total for orders on success page
     *
     * @return float
     */
    public function getOrderTotal()
    {
        $orders = $this->getOrders();

        $total = 0;

        foreach ($orders as $order) {
            $total += round(Mage::getModel('sales/order')->load($order->getEntityId())->getGrandTotal(), 2);
        }
        return $total;
    }

    /**
     * Get orders on success page
     *
     * @return unknown
     */
    public function getOrders()
    {
        if (!$this->_orders) {
            if ($ids = Mage::getSingleton('core/session')->getLastMultishippingOrderIds(true)) {
                $orders = Mage::getResourceModel('sales/order_collection')
                    ->addAttributeToFilter('increment_id', array('in' => $ids))
                    ->load();
            } else {
                $quoteId = Mage::getSingleton('checkout/session')->getLastQuoteId();

                $orders = Mage::getResourceModel('sales/order_collection')
                    ->addAttributeToFilter('quote_id', $quoteId)
                    ->load();
            }

            $total = 0;
            $this->_orders = array();
            foreach ($orders as $order) {
                if (is_object($order)) {
                    $this->_orders[] = Mage::getModel('sales/order')->load($order->getEntityId());
                } else {
                    $this->_orders[] = Mage::getModel('sales/order')->load($order);
                }
            }
        }
        return $this->_orders;
    }

    /**
     * Retrieve company id
     *
     * @return mixed
     */
    public function getCid()
    {
        return Mage::getStoreConfig($this->_xmlPathCid);
    }
}
