<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Sphill
 * @package    Sphill_Customization
 * @copyright  Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

$installer = $this;

//NEW ONES
$installer->run("INSERT INTO `" . $installer->getTable('sphill_customization/country') . "` VALUES
(239, 'AX', 'Aland Islands', 1, 239, 1, 1220),
(240, 'BL', 'Saint Barthelemy', 1, 240, 1, 1221),
(241, 'CS', 'Serbia and Montenegro', 1, 241, 1, 1222),
(242, 'FX', 'Metropolitan France', 1, 242, 1, 1223),
(243, 'GG', 'Guernsey', 1, 243, 1, 1224),
(244, 'IM', 'Isle of Man', 1, 244, 1, 1225),
(245, 'JE', 'Jersey', 1, 245, 1, 1226),
(246, 'ME', 'Montenegro', 1, 246, 1, 1227),
(247, 'MF', 'Saint Martin', 1, 247, 1, 1228),
(248, 'PS', 'Palestinian Territory', 1, 248, 1, 1229),
(249, 'TF', 'French Southern Territories', 1, 249, 1, 1230),
(250, 'TL', 'East Timor', 1, 250, 1, 1231);");


//ALIASES FOR ALREADY EXISTING COUNTRIES BUT WITH DIFFERENT CODES
/*
LOCALE  EXISTING
'CD' => 'ZR',
'GB' => 'UK',
'DZ' => 'AG',
'RS' => 'SR'
 */

$installer->run("INSERT INTO `" . $installer->getTable('sphill_customization/country') . "` VALUES
(251, 'DZ', 'Algeria', 1, 4, 1, 1002),
(252, 'RS', 'Suriname', 1, 202, 1, 1176),
(253, 'GB', 'United Kingdom', 1, 224, 1, 1072),
(254, 'CD', 'Congo (Democratic Republic)', 1, 52, 1, 1042);");
