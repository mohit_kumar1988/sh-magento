<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Eastwood
 * @package     Eastwood_Adminhtml
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Springhills_Adminhtml_Block_Promo_Catalog_Edit_Tab_Main extends Mage_Adminhtml_Block_Promo_Catalog_Edit_Tab_Main
{

    protected function _prepareForm()
    {
        parent::_prepareForm();

        $model = Mage::registry('current_promo_catalog_rule');

        $form = $this->getForm();
        $fieldset = $form->getElement('base_fieldset');       

        $fieldset->addField('offer_code', 'text', array(
            'name'      => 'offer_code',
            'label'     => Mage::helper('salesrule')->__('Offer Code'),
            'title'     => Mage::helper('salesrule')->__('Offer Code'),
        ));

		$model->setOfferCode($model->getOfferCode());
		$form->setValues($model->getData());
        return $this;
    }

    
}
