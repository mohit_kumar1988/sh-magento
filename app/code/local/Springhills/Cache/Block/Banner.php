<?php
/**
 * Example View block
 *
 * @codepool   Local
 * @category   Fido
 * @package    Springhills_Cache
 * @module     Example
 */
class Springhills_Cache_Block_Banner extends Mage_Core_Block_Template
{
    private $message;
    private $att;
    private $actionName;
    private $routeName;
    private $moduleName;
    private $controllerName;
    private $catId;
    private $pageId;
    private $productId;

    public function homeBanner() {
        $message = '';
        $offer_code='';
        $offer_code = $this->getRequest()->getParam('sid',false);
        $bannerofferCode = Mage::getSingleton('core/session')->getData("offerCode");
        $resource = Mage::getResourceSingleton('checkout/cart');
        if($offer_code!= null){
            Mage::getSingleton('checkout/session')->setData("nocoupon",'');
            Mage::getSingleton('core/session')->setData("offerCodeMsg",'');
            Mage::getSingleton('core/session')->setData("offerCode",$offer_code);
            $bannerofferCode = $offer_code;
        }
        $bannerImageBlock = $resource->getBannerImage('home',$bannerofferCode);
        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $pageId = Mage::getSingleton('cms/page')->getIdentifier();
        $catId ='';

        $overlayBanner = '';
        $overlayBanner.= '<div id="cboxes">';
        $overlayBanner.= '<div class="window couponwinpop" id="cdialog" style="display: block;left:24.5%;top:25%;">';
        $overlayBanner.= '<div class="innerpop">';
        $overlayBanner.= '    <div class="popcontent">';
        $overlayBanner.= '        <div style = "height:330px;padding:10px 4px 14px 4px;background-image:none;text-align:center;" class="Instant">';
        $overlayBanner.= '           <div class="cclose" style="margin-right:0;" onclick="jQuery(\'.window\').hide();jQuery(\'#coupon-address-overlay\').hide();">Close<span>X</span></div>';
        $overlayBanner.= ' <span style="color:#C10000;font-size:14px;">That worked! Here is your offer for Keycode "' . $bannerofferCode .'":</span>';
        $overlayBanner .= $bannerImageBlock;
        $overlayBanner.= '       </div>';
        $overlayBanner.= '   </div>';
        $overlayBanner.= '</div>';
        $overlayBanner.= '</div>';
        $overlayBanner.= '</div>';

        if($routeName == 'enterprise_pagecache'){
            $action = $this->actionName ;
            $module = $this->moduleName ;
            $route  = $this->routeName ;
            $controller  = $this->controllerName ;
            if($controller=='category'){
                $catId = $this->catId ;
            }
            $pageId= $this->pageId;
        }else{
            $action = $actionName;
            $module = $moduleName;
            $route  = $routeName;

            $controller  = $controllerName;
            if($controller=='category'){
                $catId = Mage::registry('current_category')->getId();
            }
        }
        if($action=='index' && $module=='cms'){
           // Mage::app()->getLayout()->getMessagesBlock()->setMessages(Mage::getSingleton('core/session')->getMessages(true));
            $smessages = Mage::getSingleton('checkout/session')->getMessages()->getItems();
            $fg = 1;
            foreach ($smessages as $smessage) {
                $fg = 0;
                $message .= '<ul class="messages mesgerror"><li class="error-msg"><ul><li><span>'.$smessage->getText().'</span></li></ul></li></ul>';
            }
            $invalidCoupon = Mage::getSingleton('core/session')->getData("invalidCoupon");
            if($fg && $invalidCoupon!=null){
                $message .= '<ul class="messages mesgerror"><li class="error-msg"><ul><li><span>'.$invalidCoupon.'</span></li></ul></li></ul>';
                Mage::getSingleton('core/session')->setData("invalidCoupon",'');
            }

        }

        $isSecure = Mage::app()->getStore()->isCurrentlySecure();
        if($isSecure){
            $httpValue = "https";
        }else{
            $httpValue = "http";
        }
        //echo $action . "====". $module . "====" .$controller. "=====".$route."====".$pageId ;
        if($module!='checkout' && $module!='onestepcheckout'  && $module!='customer' ){
            $message .= '<script src="'.Mage::getResourceSingleton('checkout/cart')->getsecureurl().'js/iphone/jquery-1.7.1.min.js" ></script>';
        }
        //if(!empty($bannerofferCode)  && !empty($bannerImageBlock) && $action=='index' && $module=='cms'){
        if(!empty($bannerofferCode)  && !empty($bannerImageBlock)){// && $action=='index' && $module=='cms'){
            $bannerType = $resource->getBannerType($bannerofferCode);
            $blueArray = array(11,12,18,21);
            $script='';
            $script='
            var imgsrc = $("#bannerHomeVideo").attr("src");
            var parts = imgsrc.split(":");
            var newsrc = "'.$httpValue.':"+parts[1];
            $("#bannerHomeVideo").attr("src", newsrc);';
            $script ='';

            if(in_array($bannerType,$blueArray) && $bannerType!=null){
                $script ='
                   $(".enable-div span").show();
                   $(".enable-div .price").css("text-decoration","line-through");
			       $(".originalPricediv").hide();
                   ';
            }
            $yellowArray = array(7,13,16,19,20,26);
            $flg=0;
            $catIds='';
            $bannerDesc = $resource->getBannerDesc($bannerofferCode);
            if($bannerDesc!=null){
                $bannerArray = explode("^",$bannerDesc);
                if(in_array($bannerArray[0],$yellowArray)){
                    switch ($bannerArray[0]) {
                        case 7:
                            if(count($bannerArray) == 3 || count($bannerArray) == 4 ){
                                $catIds = explode("," ,$bannerArray[2] );
                                if(in_array($catId,$catIds)){
                                    $script .= '$(".enable-div span").show();';
                                    $script .= '$(".enable-div .price").css("text-decoration","line-through");
			                                    $(".originalPricediv").hide();';
                                    $flg=1;
                                }
                            }
                            break;
                        case 13:
                            if(count($bannerArray) == 3 || count($bannerArray) == 4 ){
                                $catIds = explode("," ,$bannerArray[2] );
                                if(in_array($catId,$catIds)){
                                    $script .= '$(".enable-div span").show();';
                                    $script .= '$(".enable-div .price").css("text-decoration","line-through");
			                                    $(".originalPricediv").hide();';
                                    $flg=1;
                                }
                            }
                            break;
                        case 16:
                            if(count($bannerArray) == 3 || count($bannerArray) == 4 ){
                                $skuIds = explode("," ,$bannerArray[2] );
                                //print_r($skuIds);
                                $script .= '$(".enable-div .price").css("text-decoration","line-through");';
                                foreach($skuIds as $sku){
                                    $script .= '$(".enable-div #'.$sku.' ").show();';
                                    $script .= '$(".originalPricediv #main-'.$sku.' ").hide();';
                                    $flg=1;
                                }
                            }
                            break;
                        case 19:
                            if(count($bannerArray) == 7 || count($bannerArray) == 8 ){
                                $skuIds = explode("," ,$bannerArray[6] );
                                //print_r($skuIds);
                                $script .= '$(".enable-div .price").css("text-decoration","line-through");';
                                foreach($skuIds as $sku){
                                    $script .= '$(".enable-div #'.$sku.' ").show();';
                                    $script .= '$(".originalPricediv #main-'.$sku.' ").hide();';
                                    $flg=1;
                                }
                            }
                            break;
                        case 20:
                            if(count($bannerArray) == 5 || count($bannerArray) == 6 ){
                                $skuIds = explode("," ,$bannerArray[4] );
                                //print_r($skuIds);
                                $script .= '$(".enable-div .price").css("text-decoration","line-through");';
                                foreach($skuIds as $sku){
                                    $script .= '$(".enable-div #'.$sku.' ").show();';
                                    $script .= '$(".originalPricediv #main-'.$sku.' ").hide();';
                                    $flg=1;
                                }
                            }
                            break;
                        case 26:
                            if(count($bannerArray) == 5 || count($bannerArray) == 6 ){
                                $catIds = explode("," ,$bannerArray[4] );
                                //print_r($skuIds);
                                if(in_array($catId,$catIds)){
                                    $script .= '$(".enable-div span").show();';
                                    $script .= '$(".enable-div .price").css("text-decoration","line-through");
			                                    $(".originalPricediv").hide();';
                                    $flg=1;
                                }
                            }
                            break;
                    }
                }

            }
            //$message .='<div style="text-align:center;">'.$bannerImageBlock.'</div>';
            $script .= '$(".catalog-category-view .recent-viewed").hide();';
            $script .= '$(".cms-index-index .recent-viewed").hide();';
            $value = Mage::getSingleton('core/session')->getDisplayCouponBanner();
            if($value==1 && $module!='checkout' && $module!='onestepcheckout' ){
                $script .= '$("#coupon-address-overlay").show()';
            }
            $message .= '<script>
			jQuery.noConflict();
			jQuery(document).ready(function($){
			    '.$script.'
			});
			</script>';
            if($module!='checkout' && $module!='onestepcheckout' && $value==1 ){
                $message .='<div style="text-align:center;">'.$overlayBanner.'</div>';
            }

        }else{
            $script='';

            $script .= '$(".catalog-category-view .recent-viewed").hide();';
            $script .= '$(".cms-index-index .recent-viewed").hide();';

            if(!empty($bannerofferCode) && !empty($bannerImageBlock) && $action=='index' && $module=='cms'){
                 $script ='$("#main-dynamic-banner").hide();';
            }else{
                if($_GET['sid']!='' && !empty($bannerImageBlock))
                    $script ='$("#main-dynamic-banner").hide();';
                else
                    $script ='$("#main-dynamic-banner").show();';
            }
            //echo $script;
            //echo $bannerofferCode;
            if(!empty($bannerofferCode)){
                $bannerType = $resource->getBannerType($bannerofferCode);

                $blueArray = array(11,12,18,21);
                if(in_array($bannerType,$blueArray) && $bannerType!=null ){

                    $message .='
                   <script>
                   jQuery.noConflict();
                   jQuery(document).ready(function($){
                       '.$script.'
                       $(".enable-div .price").css("text-decoration","line-through");
                       $(".enable-div span").show();
                       $(".originalPricediv").hide();
                   });
                   </script>';
                }

                $yellowArray = array(7,13,16,19,20,26);
                $flg=0;
                $catIds='';
                //echo $catId."=== Cat Id";
                if($catId!=null || 1){
                    $bannerDesc = $resource->getBannerDesc($bannerofferCode);
                    if($bannerDesc!=null){
                        $bannerArray = explode("^",$bannerDesc);
                        if(in_array($bannerArray[0],$yellowArray)){
                            switch ($bannerArray[0]) {
                                case 7:
                                    if(count($bannerArray) == 3 || count($bannerArray) == 4){
                                        $catIds = explode("," ,$bannerArray[2] );
                                        if(in_array($catId,$catIds)){
                                            $script .= '$(".enable-div span ").show();';
                                            $script .= '$(".enable-div .price").css("text-decoration","line-through");
			                                            $(".originalPricediv").hide();';
                                            $flg=1;
                                        }
                                    }
                                    break;
                                case 13:
                                    if(count($bannerArray) == 3 || count($bannerArray) == 4 ){
                                        $catIds = explode("," ,$bannerArray[2] );
                                        if(in_array($catId,$catIds)){
                                            $script .= '$(".enable-div span ").show();';
                                            $script .= '$(".enable-div .price").css("text-decoration","line-through");
			                                            $(".originalPricediv").hide();';
                                            $flg=1;
                                        }
                                    }
                                    break;
                                case 16:
                                    if(count($bannerArray) == 3 || count($bannerArray) == 4 ){
                                        $skuIds = explode("," ,$bannerArray[2] );
                                        //print_r($skuIds);
                                        $script .= '$(".enable-div .price").css("text-decoration","line-through");';
                                        foreach($skuIds as $sku){
                                            $script .= '$(".enable-div #'.$sku.' ").show();';
                                            $script .= '$(".originalPricediv #main-'.$sku.' ").hide();';
                                            $flg=1;
                                        }
                                    }
                                    break;
                                case 19:
                                    if(count($bannerArray) == 7 || count($bannerArray) == 8 ){
                                        $skuIds = explode("," ,$bannerArray[6] );
                                        //print_r($skuIds);
                                        $script .= '$(".enable-div .price").css("text-decoration","line-through");';
                                        foreach($skuIds as $sku){
                                            $script .= '$(".enable-div #'.$sku.' ").show();';
                                            $script .= '$(".originalPricediv #main-'.$sku.' ").hide();';
                                            $flg=1;
                                        }
                                    }
                                    break;
                                case 20:
                                    if(count($bannerArray) == 5 || count($bannerArray) == 6 ){
                                        $skuIds = explode("," ,$bannerArray[4] );
                                        //print_r($skuIds);
                                        $script .= '$(".enable-div .price").css("text-decoration","line-through");';
                                        foreach($skuIds as $sku){
                                            $script .= '$(".enable-div #'.$sku.' ").show();';
                                            $script .= '$(".originalPricediv #main-'.$sku.' ").hide();';
                                            $flg=1;
                                        }
                                    }
                                    break;
                                case 26:
                                    if(count($bannerArray) == 5 || count($bannerArray) == 6 ){
                                        $catIds = explode("," ,$bannerArray[4] );
                                        //print_r($skuIds);
                                        if(in_array($catId,$catIds)){
                                            $script .= '$(".enable-div span").show();';
                                            $script .= '$(".enable-div .price").css("text-decoration","line-through");
			                                    $(".originalPricediv").hide();';
                                            $flg=1;
                                        }
                                    }
                                    break;
                            }
                        }

                    }
                }

                //echo $script;
                //if($controller == 'category' && $_GET['sid']!='')
                    //$message .='<div style="text-align:center;">'.$overlayBanner.'</div>';
                    //$message .='<div style="text-align:center;">'.$bannerImageBlock.'</div>';

                $scriptEnable = 1;

                if($controller=='product'){
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $array = $this->getCacheKeyInfo();
                    $prdId = $array['product_id'];

                    if (empty($prdId))
                        $prdId = $this->productId;

                    $_product = Mage::getModel('catalog/product')->load($prdId);
                    $productSku = $_product->getSku();
                    $uom = $_product->getUnitOfMeasure();

                    $script .= '$("#tpriceblockdiv").show();';
                    $offerCode = 4; //For SH, the default offer code is always 4
                    if (!empty($bannerofferCode)){
                        $offerCode = $write->fetchOne("SELECT Offer_Code FROM keycodes where Keycode='".$bannerofferCode."' ");
                        if(empty($offerCode))
                            $offerCode = 4;
                        else
                            $defaultOfferProductPrice = $write->fetchAll("SELECT * FROM price where Product_Number='".$productSku."' and  Offer_Code='4' ");

                    }

                    $offerProductPrice= $write->fetchAll("SELECT * FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' ");
                    $priceBlock='';
                    if(count($defaultOfferProductPrice) > 1 && count($offerProductPrice) > 0 && count($offerProductPrice)!= count($defaultOfferProductPrice)){
                        $script .= '$("#tpriceblockdiv").hide();';
                        foreach($offerProductPrice as $tierInfo){
                            $finalPrice = substr( number_format(($tierInfo['Price']),4) , 0 , -2 );
                            if(empty($uom)) $uom = 1;
                            $priceBlock .= '<span class="price" style="font-weight:bold;font-size: 14px; margin-left: 60px; color:#222;">'.'Price: '.($uom*$tierInfo['Qty_Low']) . ' for $' .$finalPrice . '</span> \n';

                        }
                        $priceBlock = '<div class="pdisplay">'.$priceBlock.'</div>';
                        $script .='$(".offerfourprice").html(\''.$priceBlock.'\');';
                    }


                }
                //echo $script;
                if(in_array($bannerType,$yellowArray) && $flg){
                    $scriptEnable=0;
                    $message .='
                   <script>
                   jQuery.noConflict();
                   jQuery(document).ready(function($){
                       '.$script.'
                       $(".enable-div").show();
                   });
                   </script>';
                }
                if(empty($bannerImageBlock) && $action=='index' && $module=='cms'){
                    $message .='
                   <script>
                   jQuery.noConflict();
                   jQuery(document).ready(function($){
                       '.$script.'
                   });
                   </script>';
                }
                if($scriptEnable){
                    $message .='
                   <script>
                   jQuery.noConflict();
                   jQuery(document).ready(function($){
                       '.$script.'
                   });
                   </script>';
                }

            }else{
                $script .= '$(".catalog-category-view .recent-viewed").hide();';
                $script .= '$(".cms-index-index .recent-viewed").hide();';

                $message .='
               <script>
               jQuery.noConflict();
               jQuery(document).ready(function($){
                   '.$script.'
                   $(".enable-div").hide();
               });
               </script>';
            }
        }

        //*** Get the param called utm_campaign if exists set the cookie ***//

        $utm_campaign = $this->getRequest()->getParam('utm_campaign');
        $utm_source = $this->getRequest()->getParam('utm_source');
        if($utm_campaign == 'em' || $utm_source == 'em') {
            Mage::getModel('core/cookie')->set('Email_Exists', 'Y');
        }
        $message = $message ."<input type='hidden' name='currSessionId' id='currSessionId' value='".Mage::getModel('core/session')->getSessionId()."' />";
        return $message;
    }

    public function setProductId($id) {
        $this->productId = $id;
    }

    public function setParams($moduleName,$actionName,$routeName,$controllerName,$catId,$pageId){
        if($routeName != 'enterprise_pagecache'){
            $this->actionName = $actionName;
            $this->moduleName = $moduleName;
            $this->routeName = $routeName;
            $this->controllerName = $controllerName;
            $this->catId = $catId;
            $this->pageId = $pageId;
        }
    }


    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

    public function getCacheKeyInfo() {
        $info = parent::getCacheKeyInfo();

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $pageId = Mage::getSingleton('cms/page')->getIdentifier();
        if($routeName!='enterprise_pagecache'){
            $info['routeName'] = $routeName;
            $info['actionName'] = $actionName;
            $info['moduleName'] = $moduleName;
            $info['controllerName'] = $controllerName;
            $info['pageId'] = $pageId;
            if($controllerName=='category'){
                $info['category_id'] = Mage::registry('current_category')->getId();
            }
            if (Mage::registry('current_product'))
                $info['product_id'] = Mage::registry('current_product')->getId();
        }

        return $info;
    }


}