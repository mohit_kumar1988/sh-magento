<?php
/**
* Example View block
*
* @codepool   Local
* @category   Fido
* @package    Springhills_Cache
* @module     Example
*/
class Springhills_Cache_Block_Footersignup extends Mage_Core_Block_Template
{
    private $message;
    private $att;
    private $actionName;
    private $routeName;
    private $moduleName;
    private $controllerName;

    public function getSignupBlock() {
        $message = '';
        $bannerofferCode = Mage::getSingleton('core/session')->getData("offerCode");

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();

        if($routeName == 'enterprise_pagecache'){
           $action = $this->actionName ;
           $module = $this->moduleName ;
           $route  = $this->routeName ;
           $controller  = $this->controllerName ;
        }else{
           $action = $actionName;
           $module = $moduleName;
           $route  = $routeName;
           $controller  = $controllerName;
        }
        //echo $action . "====". $module . "====" .$controller. "=====".$route."====" ;

        if($controller=='index' && $route=='cms'){
           $pageType = 'homepage';
        }else if($controller=='category'){
           $pageType = 'category';
        }else if($controller=='product'){
           if($route =='review')
               $pageType = 'product review';
           else
               $pageType = 'product';

           $pageType = 'product';
        }else if($module=='catalogsearch' && $controller=='result'){
           $pageType = 'search';
        }else if($controller=='cart'){
           $pageType = 'cart';
        }else if($controller=='advanced'){
            $pageType = 'plantfinder:search';
        }else{
           $pageType = 'article';
        }


        $cookieEmailId = Mage::getModel('core/cookie')->get('email_id');

        if(empty($cookieEmailId)){

        $message .= '
        <form action="'.$this->getBaseUrl().'newsletter/subscriber/new/" method="post" id="newsletter-validate-detail2">

         <div class="FooterSignUpNow">
                   <span class="Sign-Up-Now">
                        <input type="text" class="input-text" name="email" value="Exclusive offers and updates - Enter email address" onfocus="if (this.value == \'Exclusive offers and updates - Enter email address\') { this.value = \'\'; }" onblur="if (this.value == \'\') { this.value = \'Exclusive offers and updates - Enter email address\'; }">
                        <input type="hidden"  name="signup-type" value="bottom">
                        <input type="hidden"  name="page_types" value="'.$pageType.'">
                        <p class="SignUpNowButton"> <input type="image" value="Go" src="'.$this->getSkinUrl('images/buttons/button-sign-up-now.gif').'"></p>
                    </span>

        </form>
        <script type="text/javascript">
        //<![CDATA[
            var newsletterSubscriberFormDetail = new VarienForm(\'newsletter-validate-detail2\');
        //]]>
        </script>
        ';

        }else{
            $message .= '<div style="height: 30px;">&nbsp;</div> ';
        }


        return $message;
    }

    public function setParams($moduleName,$actionName,$routeName,$controllerName,$catId,$pageId){
        if($routeName != 'enterprise_pagecache'){
            $this->actionName = $actionName;
            $this->moduleName = $moduleName;
            $this->routeName = $routeName;
            $this->controllerName = $controllerName;
            $this->catId = $catId;
            $this->pageId = $pageId;
        }
    }


    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

    public function getCacheKeyInfo() {
        $info = parent::getCacheKeyInfo();

        $cookieEmailId = Mage::getModel('core/cookie')->get('email_id');

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $pageId = Mage::getSingleton('cms/page')->getIdentifier();
        if($routeName!='enterprise_pagecache'){
            $info['routeName'] = $routeName;
            $info['actionName'] = $actionName;
            $info['moduleName'] = $moduleName;
            $info['controllerName'] = $controllerName;
            $info['pageId'] = $pageId;
            if($cookieEmailId!=''){
                $info['cookieEmailId'] = $cookieEmailId;
            }
        }
        return $info;
    }



}
