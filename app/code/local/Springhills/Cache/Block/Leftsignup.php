<?php
/**
 * Example View block
 *
 * @codepool   Local
 * @category   Fido
 * @package    Springhills_Cache
 * @module     Example
 */
class Springhills_Cache_Block_Leftsignup extends Mage_Core_Block_Template
{
    private $message;
    private $att;
    private $actionName;
    private $routeName;
    private $moduleName;
    private $controllerName;

    public function getSignupBlock() {
        $message = '';

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();

        if($routeName == 'enterprise_pagecache'){
            $action = $this->actionName ;
            $module = $this->moduleName ;
            $route  = $this->routeName ;
            $controller  = $this->controllerName ;
        }else{
            $action = $actionName;
            $module = $moduleName;
            $route  = $routeName;
            $controller  = $controllerName;
        }
        //echo $action . "====". $module . "====" .$controller. "=====".$route."====" ;

        if($controller=='index' && $route=='cms'){
           $pageType = 'homepage';
        }else if($controller=='category'){
            $pageType = 'category';
        }else if($controller=='product'){
            if($route =='review')
                $pageType = 'product review';
            else
                $pageType = 'product';

            $pageType = 'product';
        }else if($module=='catalogsearch' && $controller=='result'){
            $pageType = 'search';
        }else if($controller=='cart'){
            $pageType = 'cart';
        }else if($controller=='advanced'){
            $pageType = 'plantfinder:search';
        }else{
            $pageType = 'article';
        }


        $cookieEmailId = Mage::getModel('core/cookie')->get('email_id');

        if(empty($cookieEmailId)){

            $message .= '
		<div class="block signUp">
            <div id="left-emailblock" class="left-emailblock">
            <form action="'.$this->getBaseUrl().'newsletter/subscriber/new/" method="post" id="newsletter-validate-detail-left">
                <div class="block-content">
                  <p class="go-button">
                    <input type="text" class="form-fieldside" name="email" value="Enter email address" onfocus="if (this.value == \'Enter email address\') { this.value = \'\'; }" onblur="if (this.value == \'\') { this.value = \'Enter email address\'; }">
                    <input type="image" value="GO" src="'.$this->getSkinUrl('images/buttons/go.png').'" alt="Go">
                    <input type="hidden"  name="signup-type" value="left">
                    <input type="hidden"  name="page_types" value="'.$pageType.'">
                  </p>
                  <p class="a-center"><a href="'.$this->getBaseUrl().'emailsignup">Click for details</a></p>
                </div>
              </form>
 
            </div>
          </div> 

		<div class="clear"></div>
		<script type="text/javascript">
		//<![CDATA[
			var newsletterSubscriberFormDetail = new VarienForm(\'newsletter-validate-detail-left\');
		//]]>
		</script>';

        }



        return $message;
    }

    public function setCookieId($id){
        if($id != ''){
            $this->cookieId = $id;
        }
    }
    public function setParams($moduleName,$actionName,$routeName,$controllerName,$catId,$pageId){
        if($routeName != 'enterprise_pagecache'){
            $this->actionName = $actionName;
            $this->moduleName = $moduleName;
            $this->routeName = $routeName;
            $this->controllerName = $controllerName;
            $this->catId = $catId;
            $this->pageId = $pageId;
        }
    }


    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

    public function getCacheKeyInfo() {
        $info = parent::getCacheKeyInfo();

        $cookieEmailId = Mage::getModel('core/cookie')->get('email_id');

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $pageId = Mage::getSingleton('cms/page')->getIdentifier();
        if($routeName!='enterprise_pagecache'){
            $info['routeName'] = $routeName;
            $info['actionName'] = $actionName;
            $info['moduleName'] = $moduleName;
            $info['controllerName'] = $controllerName;
            $info['pageId'] = $pageId;
            if($cookieEmailId!=''){
                $info['cookieEmailId'] = $cookieEmailId;
            }
        }
        return $info;
    }


} ?>


