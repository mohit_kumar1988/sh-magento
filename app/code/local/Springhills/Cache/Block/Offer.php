<?php
/**
* Example View block
*
* @codepool   Local
* @category   Fido
* @package    Springhills_Cache
* @module     Example
*/
class Springhills_Cache_Block_Offer extends Mage_Core_Block_Template
{
    private $message;
    private $att;
	private $productId;

    protected function createMessage($msg) {
        $this->message = $msg;
    }

    public function getOfferBlock() {
        $array = $this->getCacheKeyInfo();
		$prdId = $array['product_id'];
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $keycode = Mage::getSingleton('core/session')->getData("offerCode");
        $offerCode = 4; //For SH, the default offer code is always 4

        if (!empty($keycode)) {
            $rule_id = $write->fetchOne("SELECT rule_id FROM  salesrule_coupon where code='".$keycode."' ");
            if(!empty($rule_id)){
                $current_date = date('Y-m-d');
                $offerCode = $write->fetchOne("SELECT Offer_Code  FROM  salesrule where rule_id='".$rule_id."' and '".$current_date."' BETWEEN  from_date AND to_date ");
                if(empty($offerCode)){
                    $offerCode = 4;
                }

            }else{
                $offerCode = 4;
            }
        }
        //echo $offerCode;

		if (empty($prdId))
			$prdId = $this->productId;
        $productid = $prdId;
        $_checkProduct= Mage::getModel('catalog/product')->load($prdId);

		// product name
		if($_checkProduct->getName()!='')
			$prodName =  stripslashes($_checkProduct->getName());
		else if($_checkProduct->getCommonName()!='')
			$prodName =   stripslashes($_checkProduct->getCommonName());
		else if( $_checkProduct->getBotanicalName()!='' )
			$prodName =   stripslashes($_checkProduct->getBotanicalName());
		else
			$prodName =   stripslashes($_checkProduct->getBreckName());
		$prodName = htmlspecialchars_decode(stripslashes($prodName));


        /***** Load the Free Item Banner to check the status *****/
        $_freeBanner = Mage::getModel("banner/banner")->load('63');
        $bannerStatus = $_freeBanner->getStatus();
        /***** Load the Free Item Banner to check the status *****/

        /***** Get the Free Item Product *****/
        $name = explode("-",$_freeBanner->getName());
        $productSku = trim($name[1]);

        if($_checkProduct->getRecommendedItems() !=null  && ($bannerStatus!=1 || ( $bannerStatus==1 && $productSku != $_checkProduct->getSku() ) )){
            $offer = array();
            $offer_products = explode(" ",$_checkProduct->getRecommendedItems());//array(16253,51540, 66041, 82052);   // This array value needs to change with the recommended items get from daily update product
            //echo "<pre>";
            //print_r($offer_products);
            if(count($offer_products)>0 ){

                $offer[$productid][]= $_checkProduct->getSku();
                foreach($offer_products as $prodSku){
                    $productId = Mage::getModel('catalog/product')->getIdBySku("$prodSku");
                    if($productId){
                        $_prod = Mage::getModel('catalog/product')->load($productId);
                        if($_prod && $_prod->isSaleable() && ($_prod->getFutureSeasonMessage() > 1 || $_prod->getFutureSeasonMessage()=='')){

                            $nsku = $prodSku."_VSPMAAC";
                            $OfferProductId = Mage::getModel('catalog/product')->getIdBySku("$nsku");
                            if($OfferProductId){
                                $_offerProd = Mage::getModel('catalog/product')->load($OfferProductId);
                                if($_offerProd->isSaleable()){
                                    $offer[$productid][]=$prodSku;
                                    break;
                                }
                            }


                        }
                    }
                }
                //print_r($offer);
                $price=0;

                //echo count($offer[$productid]);
                if(count($offer[$productid])==2) {

                    $message = '

                        <div class="box-collateral box-up-sell prod-offer">
                            <div class="box-title">
                                <h2>Save '.$prodName.' when you buy together:</h2>
                            </div>
	                        <span class="offer-msg">Customers frequently buy these items together:</span>
	                        <div id="cat" class="category-products">

		                    <div class="std">';
                            $i=1;
                            foreach ($offer[$productid] as $sku){

                                $_offerproduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                                $_product = Mage::getModel('catalog/product')->load($_offerproduct->getId());



                                if($_product->getName()!='')
                                    $prodName =  stripslashes($_product->getName());
                                else if($_product->getCommonName()!='')
                                    $prodName =   stripslashes($_product->getCommonName());
                                else if( $_product->getBotanicalName()!='' )
                                    $prodName =   stripslashes($_product->getBotanicalName());
                                else
                                    $prodName =   stripslashes($_product->getBreckName());
                                $prodName = strip_tags(htmlspecialchars_decode(stripslashes($prodName)));

			                    $message .='
			                    <div class="prod'.$i.'">
                                    <a href="'.Mage::getBaseUrl().$_product->getUrlKey().'.html" class="product-image" title="'.htmlspecialchars_decode(strip_tags($prodName)).'">
                                        <img src="'.Mage::helper('catalog/image')->init($_product, 'small_image')->resize(102).'" width="102" height="102" alt="'.htmlspecialchars_decode(strip_tags($prodName)).'" title="'.htmlspecialchars_decode(strip_tags($prodName)).'" />
                                    </a>
                                    <br/>
                                    <a href="'.Mage::getBaseUrl().$_product->getUrlKey().'.html">'.$prodName.'</a>
                                </div>';

                                $uom = 	$_product->getUnitOfMeasure();
                                $offerProductPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$sku."' and  Offer_Code='".$offerCode."' ");
                                $offerProductSalePrice = $write->fetchOne("SELECT Sale_Price FROM price where Product_Number='".$sku."' and  Offer_Code='".$offerCode."' ");
                                if( $offerProductSalePrice < $offerProductPrice && $offerProductSalePrice)
                                    $offerProductPrice = $offerProductSalePrice;

                                if($uom){
                                    $price =  $price + ($offerProductPrice);
                                }else{
                                    $price =  $price + $offerProductPrice;
                                }

                                if($i==1) {

                                    $fprice = $offerProductPrice;
                                    $fuom = $_product->getUnitOfMeasure();

                                    $message .='<div class="plus"><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/plus.png" border="0"></div>';

                                 } else {
                                    $suom = $_product->getUnitOfMeasure();
                                    $sprice = $offerProductPrice;

                                 }
                                $i++;

                            }
                            //echo $sprice.'----'.$fprice;
                            $offerprice = $sprice * 0.05;
                            $wdpriceactual = ($fprice+ ($sprice - $offerprice));
                            $priceactual =  $fprice+$sprice;
                            //echo '----';

                            $message .='
                            <div class="plus">
                                <img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/equal.png" border="0">
                            </div>
                            <div class="cart-btn">
                                <div class="prod-descr">
                                    <span class="offer-buy">Buy Them Both!</span><br>
                                    <span class="offer-regular">Regularly: '.Mage::helper('core')->currency($priceactual).'</span><br/>
                                    <span class="offer-nowonly">Now Only: '.Mage::helper('core')->currency($wdpriceactual) .'</span><br/>
                                    <span class="offer-yousave">YOU SAVE: '. Mage::helper('core')->currency(number_format(($offerprice),4)).'</span>
                                </div>
                                <div class="clear"></div>
                                <div class="offer-qty">
                                    <label for="qty"><b>Qty:</b></label>
                                    <input type="text" class="input-text qty" title="Qty" value="1" maxlength="12" id="buyqty" name="buyqty">
                                </div>
                                <button type="button" title="Buy Both Now" class="button btn-both-buynow" onclick="addskuproduct(\''.$offer[$productid][0].'\',\''. $nsku.'\',\''.$fuom.'\',\''.$suom.'\')">
                                    <span>
                                        <span class="btn-buy">Buy Both Now</span>
                                    </span>
                                </button>
                            </div>
                        </div>
	                </div>
                </div>';

                }
            }


		}

		return $message;
    }

 	public function setProductId($id) {
        $this->productId = $id;
    }

    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

	public function getCacheKeyInfo() {
		$info = parent::getCacheKeyInfo();

		if (Mage::registry('current_product'))
		    $info['product_id'] = Mage::registry('current_product')->getId();

		return $info;
	}

}
