<?php
/**
 * Example View block
 *
 * @codepool   Local
 * @category   Fido
 * @package    Springhills_Cache
 * @module     Example
 */
class Springhills_Cache_Block_Rightbanner extends Mage_Core_Block_Template
{
    private $message;
    private $att;
    private $actionName;
    private $routeName;
    private $moduleName;
    private $controllerName;

    public function rightBanner() {
        $message = '';
        $bannerofferCode = Mage::getSingleton('core/session')->getData("offerCode");
        $resource = Mage::getResourceSingleton('checkout/cart');
        $bannerImageBlock = $resource->getBannerImage('right',$bannerofferCode);
        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        if($routeName == 'enterprise_pagecache'){
            $action = $this->actionName ;
            $module = $this->moduleName ;
            $route  = $this->routeName ;
            $controller  = $this->controllerName ;
        }else{
            $action = $actionName;
            $module = $moduleName;
            $route  = $routeName;
            $controller  = $controllerName;
        }
        ///echo $action."###".$module."####".$route."##".$controller;
        /*
        if(!empty($bannerofferCode) && !empty($bannerImageBlock) ){

            if( $module!='onestepcheckout' && $module !='checkout' ){
                if($action!='index' && $module!='cms') {
                    if($controller == 'category' && $_GET['sid']!=''){
                        $message ='';
                    }else{
                        $message .='<div style="text-align:center;margin-top: 10px;">'.$bannerImageBlock.'</div>';
                    }
                }
            }
            if($message=='' && $route =='cms' && $controller =='page' ){
                $message .='<div style="text-align:center;margin-top: 10px;">'.$bannerImageBlock.'</div>';
            }
            if($message=='' && $route =='quickorder' && $controller =='index' ){
                $message .='<div style="text-align:center;margin-top: 10px;">'.$bannerImageBlock.'</div>';
            }

            if($message=='' && $moduleName =='checkout' && $controller =='cart' ){
                $message .='<div style="text-align:center;margin-top: 10px;">'.$bannerImageBlock.'</div>';
            }
        }*/

        if( $controller !='onepage' && $module !='onestepcheckout' )
            $message .='<div style="text-align:center;margin-top: 10px;">'.$bannerImageBlock.'</div>';

        return $message;
    }

    public function setParams($moduleName,$actionName,$routeName,$controllerName){
        if($routeName != 'enterprise_pagecache'){
            $this->actionName = $actionName;
            $this->moduleName = $moduleName;
            $this->routeName = $routeName;
            $this->controllerName = $controllerName;
        }
    }

    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

    public function getCacheKeyInfo() {
        $info = parent::getCacheKeyInfo();

        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        if($routeName!='enterprise_pagecache'){
            $info['routeName'] = $routeName;
            $info['actionName'] = $actionName;
            $info['moduleName'] = $moduleName;
            $info['controllerName'] = $controllerName;
        }

        return $info;
    }

}