<?php
/**
* Example View block
*
* @codepool   Local
* @category   Fido
* @package    Springhills_Cache
* @module     Example
*/
class Springhills_Cache_Block_View extends Mage_Core_Block_Template
{
    private $message;
    private $att;
	private $productId;

    protected function createMessage($msg) {
        $this->message = $msg;
    }

    public function receiveMessage() {
        $array = $this->getCacheKeyInfo();
		$prdId = $array['product_id'];

		if (empty($prdId))
			$prdId = $this->productId;

		$_product = Mage::getModel('catalog/product')->load($prdId);

		// product name
		if($_product->getName()!='') 
			$prodName =  stripslashes($_product->getName());
		else if($_product->getCommonName()!='') 
			$prodName =   stripslashes($_product->getCommonName()); 
		else if( $_product->getBotanicalName()!='' ) 
			$prodName =   stripslashes($_product->getBotanicalName()); 
		else 
			$prodName =   stripslashes($_product->getBreckName());
		$prodName = htmlspecialchars_decode(stripslashes($prodName));

		$futureSeasonMessage = $_product->getFutureSeasonMessage();
		$sellebleProdQty = $_product->getSellableQuantity();
		$inventoryQty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty(); 
		$displayFlag=1;
		$seasong_mess_flag=1;
		$message = '';

		// for Future Season Message 3,4,5
		if(($sellebleProdQty == '0' && ($futureSeasonMessage == '3' || $futureSeasonMessage == '4' || $futureSeasonMessage == '5'))) 
		{ 
			$message = '<span class="YouSave">Sorry, this product is currently SOLD OUT</span>';
		} 
		// for Future Season Message 1,2,6
		if (($futureSeasonMessage && $sellableProdQty !='0') || ($sellableProdQty == '0' && ($futureSeasonMessage == '1' || $futureSeasonMessage == '2' || $$futureSeasonMessage == '6')))
		{			
			$message = '<span class="YouSave">';		
			$sellableQty = (int)$inventoryQty;
			
			if($futureSeasonMessage ==1 && $displayFlag){
					 $displayFlag=0;
					 //$message .=  "We're sorry, ".$prodName." is currently unavailable.";
			}else if($futureSeasonMessage==2 && $displayFlag ){
					 $displayFlag=0;
					 $message .=  "We are out of inventory for Spring shipping, but we are taking orders for Fall shipping. See below for items that will ship in the Spring.";
			}else if((($futureSeasonMessage== 3 || $futureSeasonMessage==4) && ($sellableQty>0 && $sellableQty<30) ) && $displayFlag){
					$displayFlag=0;
					$message .= "<p style='font-weight:bold; color: #FF0000;font-size: 15px;text-transform:uppercase;text-align:center;'>Hurry! Limited Quantity</p>";

					$message .= '<div class="hurryup">Only '.$sellableQty.' in stock. BUY IT NOW!</div>';
			}
		    else if($futureSeasonMessage==5 && $displayFlag){
					 $displayFlag=0;
					 $message .=  "This items ships immediately upon receipt of order.";
			}
			else if($futureSeasonMessage==6 && $displayFlag){
					 $displayFlag=0;
					 $message .=  "<p style='font-size: 12px; color: #666; font-weight: normal;text-align: center;font-style: italic;'>This item will ship at the recommended planting time for your area.</p>";
			}		
			else{
				if($displayFlag && $_product->isSaleable())
					$message .= "In Stock Ships Today!";
			}
			$message .='</span>';	
		}

		return $message;
    }

 	public function setProductId($id) {
        $this->productId = $id;
    }

    protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

	public function getCacheKeyInfo() {
		$info = parent::getCacheKeyInfo();

		if (Mage::registry('current_product'))
		    $info['product_id'] = Mage::registry('current_product')->getId();

		return $info;
	}

}
