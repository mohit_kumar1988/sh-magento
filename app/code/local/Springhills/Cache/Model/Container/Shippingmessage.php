<?php

class Springhills_Cache_Model_Container_Shippingmessage extends Enterprise_PageCache_Model_Container_Abstract { 

    protected function _getCacheId()
    {
		if (Mage::registry('current_product'))
		    $productId = Mage::registry('current_product')->getId();
		
        return 'SHIPPING_MESSAGE^' . md5($this->_placeholder->getAttribute('cache_id') . ',' . $this->_placeholder->getAttribute('product_id') ) ;
    }

    protected function _renderBlock()
    {
        $blockClass = $this->_placeholder->getAttribute('block');
        $template = $this->_placeholder->getAttribute('template');

        $block = new $blockClass;
        $block->setTemplate($template);
		$moduleName = $this->_placeholder->getAttribute('moduleName');
		$actionName = $this->_placeholder->getAttribute('actionName');
		$routeName  = $this->_placeholder->getAttribute('routeName');
		$controllerName = $this->_placeholder->getAttribute('controllerName');
        $catId = $this->_placeholder->getAttribute('category_id');
        $pageId = $this->_placeholder->getAttribute('pageId');
		$productId = $this->_placeholder->getAttribute('product_id');
		$block->setProductId($productId);		
		$block->setParams($moduleName,$actionName,$routeName,$controllerName,$catId,$pageId);

        return $block->toHtml();
    }


	protected function _saveCache($data, $id, $tags = array(), $lifetime = null) { return false; }  

}
