<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Springhills_Catalog_Block_Product_List extends Mage_Catalog_Block_Product_List
{ 
    protected function _getProductCollection()
    {			
		$zoneCookie = Mage::getModel('core/cookie')->get('zone');
		$actual_zone = substr($zoneCookie,0,-1);	
		$zone = 'zone'.$actual_zone;
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }
            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }
            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
            // $this->_productCollection = $layer->getProductCollection();
			$order = $this->getRequest()->getParam('order');
			$sortByAttr = '';
			$sortFlag = 0;



			if( $order == "ordered_qty" ){
				$sortByAttr = 'ordered_qty'; // for future puppose when bestseller is based on sold quantity
			} elseif ( $order == "last_tendays_sale_price" ){
                $sortByAttr = 'last_tendays_sale_price';
            }elseif ( $order == "name" ){
				$sortByAttr = 'name';
			} elseif ($order == "sort_price" ){
                $sortByAttr = 'sort_price';
            } elseif ($order == "price" ){
				$sortByAttr = 'sort_price';
			} elseif ( $order == "till_date_revenue" ) {
				$sortByAttr = 'till_date_revenue'; // only for launch, used best seller revenue of product
			} elseif ( $order == "position" ) {
                $sortByAttr = 'position'; // only for launch, used best seller revenue of product
            } else {
				$sortFlag = 1;	
				$sortByAttr = $zone;
			}
            $controller = Mage::app()->getRequest()->getControllerName();


            if(Mage::registry('current_category')) {
                $categoryId = Mage::registry('current_category')->getId();
                $category = Mage::getModel('catalog/category')->load($categoryId);
                if($category->getSortByPosition() == 1 && $order=='' ) {
                    $order = "position";
                    $sortByAttr = 'position';
                }
            }
            //echo $zoneCookie;
			if( $zoneCookie == null ) {
				//echo "default Sort".$sortByAttr."####";
				if( $sortByAttr!='' && $sortByAttr!='zone'){
                    //echo $order;
					if( $order == "last_tendays_sale_price" ){
					//echo "Order qty Sort";
						$this->_productCollection = $layer->getProductCollection()
											//->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
                                            ->addAttributeToSort('last_tendays_sale_price','desc')
											->addAttributeToSort('inventory_in_stock','desc');
                                           // ->addAttributeToSort('future_season_message','desc');
                        if($controller == 'category'){
                            $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                        }
                        //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
					}else if( $order == "till_date_revenue" ){
					//echo "revenue Sort";
						$this->_productCollection = $layer->getProductCollection()
											//->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
											->addAttributeToSort('inventory_in_stock','desc')
                                            ->addAttributeToSort('future_season_message','desc')
											->addAttributeToSort($sortByAttr,'desc')
											->addAttributeToSort('name');
                        if($controller == 'category'){
                            $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                        }
                        //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
					}else if( $order == "position" ){
                        //echo "revenue Sort";
                        $this->_productCollection = $layer->getProductCollection()
                                            //->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
                                            ->addAttributeToFilter("price", array('gt'=> 0))
                                            ->addAttributeToSort('inventory_in_stock','desc')
                                            //->addAttributeToSort('future_season_message','desc')
                                            ->addAttributeToSort($sortByAttr,'asc')
                                            ->addAttributeToSort('name');
                        if($controller == 'category'){
                            $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                        }
                        //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
                    }else if($order == "name" ){
					//echo "name Sort";
						$this->_productCollection = $layer->getProductCollection()
											//->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
											->addAttributeToSort('inventory_in_stock','desc')
                                            ->addAttributeToSort('future_season_message','desc')
											->addAttributeToSort($sortByAttr);
                        if($controller == 'category'){
                            $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                        }
                        //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
					}else{
					//echo "sort_price Sort";
						$this->_productCollection = $layer->getProductCollection()
											//->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
                                            ->addAttributeToSort('last_tendays_sale_price','desc')
											->addAttributeToSort('inventory_in_stock','desc')
                                            ->addAttributeToSort('future_season_message','desc')
											->addAttributeToSort($sortByAttr)
											->addAttributeToSort('name');
                        if($controller == 'category'){
                            $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                        }
                       // $this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
					}
				}else {
					// Default Sort By Best Seller
					//echo "Default revenue Sort";
					$this->_productCollection = $layer->getProductCollection()
											//->joinField('in_stock_new', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
                                            ->addAttributeToSort('last_tendays_sale_price','desc')
											->addAttributeToSort('name', 'desc');
                    if($controller == 'category'){
                        $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                    }
                    //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
				}
			} else {				
				//echo "zone Sort";
				if( $order != "name" ){
					if($sortFlag){
						//echo "zone attr Sort";
						$this->_productCollection = $layer->getProductCollection()
										//->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
										->addAttributeToFilter("price", array('gt'=> 0))
										->addAttributeToSort($sortByAttr)
										->addAttributeToSort('inventory_in_stock','desc')
                                        ->addAttributeToSort('future_season_message','desc')
										->addAttributeToSort('name');
                        if($controller == 'category'){
                            $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                        }
                        //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
					}else{
						if( $order != "till_date_revenue" ){
						    //echo "price sort zone";
                            if( $order == "position" ){
                                //echo "revenue Sort";
                                $this->_productCollection = $layer->getProductCollection()
                                    //->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
                                    ->addAttributeToFilter("price", array('gt'=> 0))
                                    ->addAttributeToSort('inventory_in_stock','desc')
                                    //->addAttributeToSort('future_season_message','desc')
                                    ->addAttributeToSort($sortByAttr,'asc')
                                    ->addAttributeToSort('name');
                                if($controller == 'category'){
                                    $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                                }
                                //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
                            }else if( $order == "last_tendays_sale_price" ){
                                //echo "best Sort";
                                $this->_productCollection = $layer->getProductCollection()
                                    ->addAttributeToSort($sortByAttr,'desc')
                                    ->addAttributeToFilter("price", array('gt'=> 0))
                                    ->addAttributeToSort('last_tendays_sale_price','desc')
                                    ->addAttributeToSort('inventory_in_stock','desc')
                                    ->addAttributeToSort('name');
                                if($controller == 'category'){
                                    $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                                }
                                //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
                            }else {
                                $this->_productCollection = $layer->getProductCollection()
                                    //->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
                                    ->addAttributeToFilter("price", array('gt'=> 0))
                                    ->addAttributeToSort('inventory_in_stock','desc')
                                    ->addAttributeToSort('future_season_message','desc')
                                    ->addAttributeToSort($sortByAttr)
                                    ->addAttributeToSort('name');
                                if($controller == 'category'){
                                    $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                                }
                            }

                            //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
						}else{
						    //echo "zone revenue Sort";
						    $this->_productCollection = $layer->getProductCollection()
										//->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
										->addAttributeToFilter("price", array('gt'=> 0))
										->addAttributeToSort('inventory_in_stock','desc')
                                        ->addAttributeToSort('future_season_message','desc')
										->addAttributeToSort($sortByAttr,'desc')
										->addAttributeToSort('name');
                            if($controller == 'category'){
                                $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                            }
                            //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
						}
					}
				}else {
				    //echo "zone name Sort";
				    $this->_productCollection = $layer->getProductCollection()
											//->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
											->addAttributeToSort('inventory_in_stock','desc')
                                            ->addAttributeToSort('future_season_message','desc')
											->addAttributeToSort($sortByAttr ,'ASC');
                    if($controller == 'category'){
                        $this->_productCollection->addAttributeToFilter('default_category', array('eq' => 1));
                    }
                    //$this->_productCollection->addAttributeToFilter('future_season_message', array('IN' => array('','2','3','4','5','6')));
				}
			}
			$this->prepareSortableFieldsByCategory($layer->getCurrentCategory());		
		    if ($origCategory) {
		        $layer->setCurrentCategory($origCategory);
		    }			
		}
        return $this->_productCollection;
    }
}
