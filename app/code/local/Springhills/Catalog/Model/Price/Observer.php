<?php
class Springhills_Catalog_Model_Price_Observer
{
    public function __construct()
    {
    }
    /**
     * Applies the special price percentage discount
     * @param   Varien_Event_Observer $observer
     * @return  Xyz_Catalog_Model_Price_Observer
     */
    public function apply_discount_percent($observer)
    {
        $moduleName = Mage::app()->getRequest()->getModuleName();
        $actionName = Mage::app()->getRequest()->getActionName();
        $routeName = Mage::app()->getRequest()->getRouteName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        //echo $actionName . "====". $moduleName . "====" .$routeName. "=====".$controllerName ;
        $identifier =  Mage::getSingleton('cms/page')->getIdentifier();
		$event = $observer->getEvent();
		$product = $event->getProduct();
		// process percentage discounts only for simple products  
		$quoteid=Mage::getSingleton("checkout/session")->getQuote()->getId();   
		if ($product->getSuperProduct() && $product->getSuperProduct()->isConfigurable()) {
		
		}
		else {
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$keycode = Mage::getSingleton('core/session')->getData("offerCode");
			$offerCode = 4; //For SH, the default offer code is always 4

			if (!empty($keycode)) {
				$rule_id = $write->fetchOne("SELECT rule_id FROM  salesrule_coupon where code='".$keycode."' ");
				if(!empty($rule_id)){
						$current_date = date('Y-m-d');
						$offerCode = $write->fetchOne("SELECT Offer_Code  FROM  salesrule where rule_id='".$rule_id."' and '".$current_date."' BETWEEN  from_date AND to_date ");
						if(empty($offerCode)){
							$offerCode = 4;
						}
					
				}else{
					$offerCode = 4;
				}
			}


			//echo $offerCode;
			$sku = $product->getSku();

			$offerProductPrice = $product->getPrice();
			if($moduleName=='checkout' || $moduleName=='onestepcheckout' || $moduleName=='quickorder') {
				//in case of checkout determine the quantity in the cart to determine the price to be used
				$session = Mage::getSingleton('checkout/session');
				foreach ($session->getQuote()->getAllItems() as $item) {
					if($item->getProductId() == $product->getId()) {
						$_product = Mage::getModel('catalog/product')->load($item->getProductId());
						$uom  = $_product->getUnitOfMeasure();
						$qty = $item->getQty()/$uom ;
                        $origSku = explode("_",$item->getSku());
						$offerProductPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$origSku[0]."' and  Offer_Code='".$offerCode."' and  '".$qty."' between Qty_Low AND Qty_High ");

                        //echo $offerProductSalePrice = $write->fetchOne("SELECT Sale_Price FROM price where Product_Number='".$item->getSku()."' and  Offer_Code='4' and  '".$qty."' between Qty_Low AND Qty_High ");

                        //if( $offerCode == 4  && $offerProductSalePrice )
                            //$offerProductPrice = $offerProductSalePrice;

						if(empty($offerProductPrice)){
                            $offerProductPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$origSku[0]."' and  Offer_Code='4' and  '".$qty."' between Qty_Low AND Qty_High ");
                            //if($offerProductSalePrice){
                                //$offerProductPrice = $offerProductSalePrice;
                            //}
                        }

						$offerProductPrice = floor(($offerProductPrice/$uom)*10000)/10000;

						break;
					}
				}
			}
			else {
                if($controllerName!='product'){
                    $offerCode = 4;
                }
                //echo $offerCode;
				//try to see if the product has a price for the offer code.  If not, use the default of 4
				$offerProductPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$sku."' and  Offer_Code='".$offerCode."' ");
                $offerProductSalePrice=0;
                if($identifier!='addproduct'){
                    $offerProductSalePrice = $write->fetchOne("SELECT Sale_Price FROM price where Product_Number='".$sku."' and  Offer_Code='".$offerCode."' ");
                }

                if(empty($offerProductPrice))
                    $offerProductPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$sku."' and  Offer_Code='4' ");

                if($offerProductSalePrice && $offerCode == 4 ){
                    if($controllerName=='product'){
                        if( $offerProductSalePrice < $offerProductPrice )
                            $offerProductPrice = $offerProductSalePrice;
                    }else{
                        $offerProductPrice = $offerProductSalePrice;
                    }
                }
                //echo $offerProductPrice;
				if ($offerProductPrice == 0)
						$offerProductPrice = $product->getPrice();

				$uom  = $product->getUnitOfMeasure();
				$offerProductPrice = floor(($offerProductPrice/$uom)*10000)/10000;//number_format(($offerProductPrice/$uom), 4);
			}
            //echo "====<b>".$offerProductPrice."</b>";
			if ($offerProductPrice != 0) {
				$product->setFinalPrice($offerProductPrice);
			}
		}         
    }
}
