<?php
    /**
     * Magento
     *
     * NOTICE OF LICENSE
     *
     * This source file is subject to the Open Software License (OSL 3.0)
     * that is bundled with this package in the file LICENSE.txt.
     * It is also available through the world-wide-web at this URL:
     * http://opensource.org/licenses/osl-3.0.php
     * If you did not receive a copy of the license and are unable to
     * obtain it through the world-wide-web, please send an email
     * to license@magentocommerce.com so we can send you a copy immediately.
     *
     * DISCLAIMER
     *
     * Do not edit or add to this file if you wish to upgrade Magento to newer
     * versions in the future. If you wish to customize Magento for your
     * needs please refer to http://www.magentocommerce.com for more information.
     *
     * @category    Mage
     * @package     Mage_CatalogSearch
     * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
     * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
     */


    /**
     * CatalogSearch Mysql resource helper model
     *
     * @category    Mage
     * @package     Mage_Catalog
     * @author      Magento Core Team <core@magentocommerce.com>
     */
class Springhills_CatalogSearch_Model_Resource_Helper_Mysql4 extends Mage_CatalogSearch_Model_Resource_Helper_Mysql4
{
    public function chooseFulltextWhere($table, $alias, $select, $queryText)
    {
        $queryText = "'".$queryText."*'";
        $field = new Zend_Db_Expr('MATCH ('.$alias.'.data_index) AGAINST ('.$queryText.' IN BOOLEAN MODE)');
        $select->columns(array('relevance' => $field));
        return $field;
    }
}