<?php

include_once('Mage/CatalogSearch/controllers/ResultController.php');

class Springhills_CatalogSearch_ResultController extends Mage_CatalogSearch_ResultController {

    /**
     * Display search result
     */
    public function indexAction()
    {
        $query = Mage::helper('catalogsearch')->getQuery();
        /* @var $query Mage_CatalogSearch_Model_Query */

        $query->setStoreId(Mage::app()->getStore()->getId());

        if ($query->getQueryText() != '') {
            
            //If we search using a SKU number, got the product page.
            $productSkuSearch = Mage::getModel('catalog/product')
                       ->getCollection()
                       ->addAttributeToFilter('sku',$query->getQueryText());
            if(count($productSkuSearch) == 1 ){
                $productUrl = $productSkuSearch->getFirstItem()->getProductUrl();
                $this->_redirectUrl($productUrl);
                return;                
            }
            
            if (Mage::helper('catalogsearch')->isMinQueryLength()) {
                $query->setId(0)
                    ->setIsActive(1)
                    ->setIsProcessed(1);
            }
            else {
                if ($query->getId()) {
                    $query->setPopularity($query->getPopularity()+1);
                }
                else {
                    $query->setPopularity(1);
                }

                if ($query->getRedirect()){
                    $query->save();
                    $this->getResponse()->setRedirect($query->getRedirect());
                    return;
                }
                else {
                    $query->prepare();
                }
            }

            Mage::helper('catalogsearch')->checkNotes();

            $this->loadLayout();
            $this->_initLayoutMessages('catalog/session');
            $this->_initLayoutMessages('checkout/session');
            $this->renderLayout();

            if (!Mage::helper('catalogsearch')->isMinQueryLength()) {
                $query->save();
            }
        }
        else {
            $this->_redirectReferer();
        }
    }

  
}
