<?php


class Springhills_Catalogoffer_Block_Adminhtml_Catalogoffer extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

		$this->_controller = "adminhtml_catalogoffer";
		$this->_blockGroup = "catalogoffer";
		$this->_headerText = Mage::helper("catalogoffer")->__("Catalog Offer Manager");
		$this->_addButtonLabel = Mage::helper("catalogoffer")->__("Add New Offer");
		parent::__construct();
		//$this->removeButton('add');

	}

}
