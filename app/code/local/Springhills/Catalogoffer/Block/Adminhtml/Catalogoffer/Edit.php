<?php
	
class Springhills_Catalogoffer_Block_Adminhtml_Catalogoffer_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "catalogoffer_id";
				$this->_blockGroup = "catalogoffer";
				$this->_controller = "adminhtml_catalogoffer";
				$this->_updateButton("save", "label", Mage::helper("catalogoffer")->__("Save Offer"));
				$this->_updateButton("delete", "label", Mage::helper("catalogoffer")->__("Delete Item"));
                $this->removeButton('delete');
                $this->_addButton("save and continue", array(
					"label"     => Mage::helper("catalogoffer")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("catalogoffer_data") && Mage::registry("catalogoffer_data")->getId() ){

				    return Mage::helper("catalogoffer")->__("Edit Catalog Offer", $this->getFirstname());

				} 
				else{

				     return Mage::helper("catalogoffer")->__("Add Catalog Offer");

				}
		}
}
