<?php
class Springhills_Catalogoffer_Block_Adminhtml_Catalogoffer_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{
				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("catalogoffer_form", array("legend"=>Mage::helper("catalogoffer")->__("Offer information")));

                $postData = Mage::registry("catalogoffer_data")->getData();
                //Readonly for Offer_Code in Edit action
                if(count($postData)> 0){
                   $fieldset->addField("Offer_Code", "text", array(
                        "label" => Mage::helper("catalogoffer")->__("Offer Code"),
                        "class" => "required-entry",
                        "required" => true,
                        "name" => "Offer_Code",
                        "readonly"=>true,
                    ));
                }else{
                   $fieldset->addField("Offer_Code", "text", array(
                        "label" => Mage::helper("catalogoffer")->__("Offer Code"),
                        "class" => "required-entry",
                        "required" => true,
                        "name" => "Offer_Code",
                    ));
                }

				$fieldset->addField("Offer_Date", "text", array(
				"label" => Mage::helper("catalogoffer")->__("Offer Date"),
				"class" => "required-entry",
				"required" => true,
				"name" => "Offer_Date",
				));

				$fieldset->addField("Offer_Exp_Date", "text", array(
				"label" => Mage::helper("catalogoffer")->__("Offer Exp Date"),
				"class" => "required-entry",
				"required" => true,
				"name" => "Offer_Exp_Date",
				));
				$fieldset->addField("Offer", "textarea", array(
				"label" => Mage::helper("catalogoffer")->__("Offer"),
                "required" => true,
				"name" => "Offer",
				));
				$fieldset->addField("Offer_Text", "textarea", array(
				"label" => Mage::helper("catalogoffer")->__("Offer Text"),
				"class" => "required-entry",
				"name" => "Offer_Text",
				));
                $fieldset->addField("Bonus_Item_1", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Bonus Item 1"),
                "class" => "validate-number",
                "name" => "Bonus_Item_1",
                ));
                $fieldset->addField("Bonus_Amount_1", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Bonus Amount 1"),
                "class" => "validate-number",
                "name" => "Bonus_Amount_1",
                 ));
                $fieldset->addField("Bonus_Item_2", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Bonus Item 2"),
                "name" => "Bonus_Item_2",
                ));
                $fieldset->addField("Bonus_Amount_2", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Bonus Amount 2"),
                "class" => "validate-number",
                "name" => "Bonus_Amount_2",
                ));
                $fieldset->addField("Bonus_Item_3", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Bonus Item 3"),
                "name" => "Bonus_Item_3",
                ));
                $fieldset->addField("Bonus_Amount_3", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Bonus Amount 3"),
                "class" => "validate-number",
                "name" => "Bonus_Amount_3",
                 ));
                $fieldset->addField("Bonus_Item_4", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Bonus Item 4"),
                "name" => "Bonus_Item_4",
                ));
                $fieldset->addField("Bonus_Amount_4", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Bonus Amount 4"),
                "class" => "validate-number",
                "name" => "Bonus_Amount_4",
                ));
                $fieldset->addField("Coupon_Amt_1", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Coupon Amt 1"),
                "class" => "validate-number",
                "name" => "Coupon_Amt_1",
                ));
                $fieldset->addField("Coupon_Min_Order_1", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Coupon Min Order 1"),
                "name" => "Coupon_Min_Order_1",
                ));
                $fieldset->addField("Coupon_Amt_2", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Coupon Amt 2"),
                "class" => "validate-number",
                "name" => "Coupon_Amt_2",
                ));
                $fieldset->addField("Coupon_Min_Order_2", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Coupon Min Order 2"),
                "name" => "Coupon_Min_Order_2",
                ));
                $fieldset->addField("Coupon_Amt_3", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Coupon Amt 3"),
                "class" => "validate-number",
                "name" => "Coupon_Amt_3",
                 ));
                $fieldset->addField("Coupon_Min_Order_3", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Coupon Min Order 3"),
                "name" => "Coupon_Min_Order_3",
                 ));
                $fieldset->addField("Coupon_Amt_4", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Coupon Amt 4"),
                "class" => "validate-number",
                "name" => "Coupon_Amt_4",
                 ));
                $fieldset->addField("Coupon_Min_Order_4", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Coupon Min Order 4"),
                "name" => "Coupon_Min_Order_4",
                ));

				$fieldset->addField("One_Use", "text", array(
                "label" => Mage::helper("catalogoffer")->__("One Use"),
                "name" => "One_Use",
				));
				$fieldset->addField("Affiliate", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Affiliate"),
                "name" => "Affiliate",
				));
                $fieldset->addField("Free_Ship_Upgrade", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Free Ship Upgrade"),
                "name" => "Free_Ship_Upgrade",
                ));
                $fieldset->addField("Coupon_Type", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Coupon Type"),
                "name" => "Coupon_Type",
                ));
                $fieldset->addField("Affiliate_Prog", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Affiliate_Prog"),
                "name" => "Affiliate_Prog",
                ));
                $fieldset->addField("Custnbr_Required", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Customer Number Required"),
                "name" => "Custnbr_Required",
                'description' => 'True or False',
                ));
                $fieldset->addField("Last_Update", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Last Update"),
                "name" => "Last_Update",
                ));
                $fieldset->addField("Catalog_Type", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Catalog Type"),
                "name" => "Catalog_Type",
                ));
                $fieldset->addField("House_Credit", "text", array(
                "label" => Mage::helper("catalogoffer")->__("House Credit"),
                "name" => "House_Credit",
                ));
                $fieldset->addField("OverWeight", "text", array(
                "label" => Mage::helper("catalogoffer")->__("OverWeight"),
                "name" => "OverWeight",
                ));
                $fieldset->addField("Mailed", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Mailed"),
                "name" => "Mailed",
                ));
                $fieldset->addField("Media_Code", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Media Code"),
                "name" => "Media_Code",
                ));
                $fieldset->addField("List_Type", "text", array(
                "label" => Mage::helper("catalogoffer")->__("List Type"),
                "name" => "List_Type",
                ));
                $fieldset->addField("Media_Type", "text", array(
                "label" => Mage::helper("catalogoffer")->__("Media Type"),
                "name" => "Media_Type",
                ));


				if (Mage::getSingleton("adminhtml/session")->getCatalogofferData())
				{
                    $form->setValues(Mage::getSingleton("adminhtml/session")->getCatalogofferData());
					Mage::getSingleton("adminhtml/session")->setCatalogofferData(null);
				} 
				elseif(Mage::registry("catalogoffer_data")) {
                    $form->setValues(Mage::registry("catalogoffer_data")->getData());
				}
				return parent::_prepareForm();
		}
}
