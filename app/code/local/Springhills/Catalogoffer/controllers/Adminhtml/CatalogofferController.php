<?php

class Springhills_Catalogoffer_Adminhtml_CatalogofferController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("catalogoffer/catalogoffer")->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogoffer  Manager"),Mage::helper("adminhtml")->__("Catalogoffer Manager"));
				return $this;
		}
		public function indexAction() 
		{
				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{
				$brandsId = $this->getRequest()->getParam("id");
				$brandsModel = Mage::getModel("catalogoffer/catalogoffer")->load($brandsId);
				if ($brandsModel->getId() || $brandsId == 0) {
					Mage::register("catalogoffer_data", $brandsModel);
					$this->loadLayout();
					$this->_setActiveMenu("catalogoffer/catalogoffer");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogoffer Manager"), Mage::helper("adminhtml")->__("Catalogoffer Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogoffer Description"), Mage::helper("adminhtml")->__("Catalogoffer Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("catalogoffer/adminhtml_catalogoffer_edit"))->_addLeft($this->getLayout()->createBlock("catalogoffer/adminhtml_catalogoffer_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("catalogoffer")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("catalogoffer/catalogoffer")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("catalogoffer_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("catalogoffer/catalogoffer");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogoffer Manager"), Mage::helper("adminhtml")->__("Catalogoffer Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogoffer Description"), Mage::helper("adminhtml")->__("Catalogoffer Description"));


		$this->_addContent($this->getLayout()->createBlock("catalogoffer/adminhtml_catalogoffer_edit"))->_addLeft($this->getLayout()->createBlock("catalogoffer/adminhtml_catalogoffer_edit_tabs"));

		$this->renderLayout();

		       // $this->_forward("edit");
		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');

				if ($post_data) {

					try {

						$post_data['updated_at']=date('Y-m-d H:i:s');
						$brandsModel = Mage::getModel("catalogoffer/catalogoffer")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

                        $offer_code_data= $brandsModel->getData();
                        $offer_code =$offer_code_data['Offer_Code'];
                        $sql = "SELECT * FROM salesrule WHERE Offer_Code<>4 and Offer_Code='$offer_code'";
                        $arrSalesRule = $write->fetchAll($sql);
                        //Get The Sales Rule Details for Updating with latest info
                        foreach($arrSalesRule as $rule){
                            $keyCode = array();
                            $i = 0;
                            $coupon = Mage::getModel('salesrule/rule')->load($rule['rule_id']);
                            $saleOfferData = $coupon->getData();
                            $offerKeycode = $saleOfferData['coupon_code'];
                            if (!empty($offer_code_data['Coupon_Amt_1']) && !empty($offer_code_data['Coupon_Min_Order_1'])) {
                                $keyCode[$i]['keycode'] = $offerKeycode;
                                $keyCode[$i]['amount'] = $offer_code_data['Coupon_Amt_1'];
                                $keyCode[$i]['min_order'] = $offer_code_data['Coupon_Min_Order_1'];
                                $keyCode[$i]['type'] = 'general';
                                $i++;
                            } if (!empty($offer_code_data['Coupon_Amt_2']) && !empty($offer_code_data['Coupon_Min_Order_2'])) {
                                $keyCode[$i]['keycode'] = $offerKeycode . "_" . $offer_code_data['Coupon_Min_Order_2'];
                                $keyCode[$i]['amount'] = $offer_code_data['Coupon_Amt_2'];
                                $keyCode[$i]['min_order'] = $offer_code_data['Coupon_Min_Order_2'];
                                $keyCode[$i]['type'] = 'general';
                                $i++;
                            } if (!empty($offer_code_data['Coupon_Amt_3']) && !empty($offer_code_data['Coupon_Min_Order_3'])) {
                                $keyCode[$i]['keycode'] = $offerKeycode . "_" . $offer_code_data['Coupon_Min_Order_3'];
                                $keyCode[$i]['amount'] = $offer_code_data['Coupon_Amt_3'];
                                $keyCode[$i]['min_order'] = $offer_code_data['Coupon_Min_Order_3'];
                                $keyCode[$i]['type'] = 'general';
                                $i++;
                            }if (!empty($offer_code_data['Coupon_Amt_4']) && !empty($offer_code_data['Coupon_Min_Order_4'])) {
                                $keyCode[$i]['keycode'] = $offerKeycode . "_" .$offer_code_data['Coupon_Min_Order_4'];
                                $keyCode[$i]['amount'] = $offer_code_data['Coupon_Amt_4'];
                                $keyCode[$i]['min_order'] = $offer_code_data['Coupon_Min_Order_4'];
                                $keyCode[$i]['type'] = 'general';
                                $i++;
                            }
                            if (!empty($offer_code_data['Bonus_Item_1']) && !empty($offer_code_data['Bonus_Amount_1'])) {
                                $keyCode[$i]['keycode'] = $offerKeycode;
                                $keyCode[$i]['product'] = $offer_code_data['Bonus_Item_1'];
                                $keyCode[$i]['min_order'] = $offer_code_data['Bonus_Amount_1'];
                                $keyCode[$i]['type'] = 'freegift';
                                $i++;
                            }if ((empty($offer_code_data['Coupon_Amt_1']) && empty($offer_code_data['Coupon_Min_Order_1'])) && (empty($offer_code_data['Bonus_Item_1']) && empty($offer_code_data['Bonus_Amount_1']))) {
                                $keyCode[$i]['keycode'] = $offerKeycode;
                                $keyCode[$i]['amount'] = $offer_code_data['Coupon_Amt_1'];
                                $keyCode[$i]['min_order'] = $offer_code_data['Coupon_Min_Order_1'];
                                $keyCode[$i]['type'] = 'general';
                            }
                            $usesPerCoupon = "";
                            $usesPerCustomer = "";
                            if ($offer_code_data['Custnbr_Required'] == "True" && $offer_code_data['One_Use'] == 'True') {
                                $usesPerCoupon = 1;
                                $usesPerCustomer = 1;
                            } else if ($offer_code_data['Custnbr_Required'] == "True" && ($offer_code_data['One_Use'] == 'False' || empty($offer_code_data['One_Use']))) {
                                $usesPerCoupon = "";
                                $usesPerCustomer = 1;
                            } else if (($offer_code_data['Custnbr_Required'] == "False" || empty($offer_code_data['Custnbr_Required'])) && $offer_code_data['One_Use'] == 'True') {
                                $usesPerCoupon = 1;
                                $usesPerCustomer = "";
                            } else if (($offer_code_data['Custnbr_Required'] == "False" || empty($offer_code_data['Custnbr_Required'])) &&
                                ($offer_code_data['One_Use'] == 'False' || empty($offer_code_data['One_Use']))) {
                                $usesPerCoupon = "";
                                $usesPerCustomer = "";
                            }

                            if ($offer_code_data['One_Use'] == 'True')
                                $onetime = 1;
                            else
                                $onetime=0;
                            $j = 0;

                            /**UpDating Coupon Information Magento**/
                            foreach ($keyCode as $rec)
                            {
                                $conditionsArray[0] = array("type" => 'salesrule/rule_condition_address',
                                    "attribute" => 'base_subtotal',
                                    "operator" => '>=',
                                    "value" => $keyCode[$j]['min_order'],
                                    "is_value_processed" => 0);
                                $array = array("type" => 'salesrule/rule_condition_combine',
                                    "attribute" => '',
                                    "operator" => '',
                                    "value" => 1,
                                    "is_value_processed" => '',
                                    "aggregator" => 'all',
                                    "conditions" => $conditionsArray);
                                $conditionSerialized = serialize($array);
                                $ruleDetails = array("name" => $offer_code_data['Offer'],
                                    "description" => $offer_code_data['Offer_Text'],
                                    "Offer_Date" => $offer_code_data['Offer_Date'],
                                    "Offer_Exp_Date" => $offer_code_data['Offer_Exp_Date'],
                                    "couponType" => 2,
                                    "usesPerCoupon" => $usesPerCoupon,
                                    "couponcode" => $keyCode[$j]['keycode'],
                                    "usesPerCustomer" => $usesPerCustomer,
                                    "Offer_Code" => $saleOfferData['offer_code'],
                                    "discountAmount" => $keyCode[$j]['amount']);
                                //Saving Of Offer_Code
                                $this->generalCouponCode($conditionSerialized, $ruleDetails,$write);
                            }
                        }

                        /**End of UpDating Coupon Information Magento**/
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Catalogoffer was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setCatalogofferData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $brandsModel->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					}
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setCatalogofferData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



        public function deleteAction()
        {
                if( $this->getRequest()->getParam("id") > 0 ) {
                    try {
                        $brandsModel = Mage::getModel("catalogoffer/catalogoffer");
                        $brandsModel->setId($this->getRequest()->getParam("id"))->delete();
                        Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                        $this->_redirect("*/*/");
                    }
                    catch (Exception $e) {
                        Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                        $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                    }
                }
                $this->_redirect("*/*/");
        }
        public function generalCouponCode($conditionSerialized, $ruleDetails, $write) {

            $checkCode = Mage::getModel('salesrule/coupon')->load($ruleDetails['couponcode'], 'code');
            $ruleId = $checkCode->getRuleId();

            if (!empty($ruleId)) {
                $coupon = Mage::getModel('salesrule/rule')->load($checkCode->getRuleId());
                $mode = 'update';
            } else {

                $coupon = Mage::getModel('salesrule/rule');
                $mode = 'insert';
            }

            $coupon->setName($ruleDetails['name'])
                ->setDescription($ruleDetails['description'])
                ->setFromDate($ruleDetails['Offer_Date'])
                ->setToDate($ruleDetails['Offer_Exp_Date'])
                ->setCouponType(2)
                ->setUsesPerCoupon($ruleDetails['usesPerCoupon'])
                ->setUsesPerCustomer($ruleDetails['usesPerCustomer'])
                ->setCouponCode($ruleDetails['couponcode'])
                ->setConditionsSerialized($conditionSerialized)
                ->setCustomerGroupIds(array(0, 1, 2, 3)) //an array of customer grou pids
                ->setIsActive(1)
                ->setActionsSerialized('a:6:{s:4:"type";s:40:"salesrule/rule_condition_product_combine";s:9:"attribute";N;s:8:"operator";N;s:5:"value";s:1:"1";s:18:"is_value_processed";N;s:10:"aggregator";s:3:"all";}')
                ->setStopRulesProcessing(0)
                ->setIsAdvanced(1)
                ->setProductIds('')
                ->setSortOrder(0)
                ->setSimpleAction('cart_fixed')
                ->setDiscountQty(null)
                ->setDiscountStep('0')
                ->setSimpleFreeShipping('0')
                ->setApplyToShipping('0')
                ->setIsRss(0)
                ->setOfferCode($ruleDetails['Offer_Code'])
                ->setWebsiteIds(array(1))
                ->setDiscountAmount($ruleDetails['discountAmount']);
            try {
                $coupon->save();
                //$updateCatalogOffer = $write->query("update catalog_offers set Flag=1 where Offer_Code='" . $ruleDetails['Offer_Code'] . "'");
                //$updateKeyCode = $write->query("update Keycodes set Flag=1 where Keycode='" . $ruleDetails['couponcode'] . "'");
                if ($mode == 'insert'){
                    //echo '---Created:: OfferCode:' . $ruleDetails['Offer_Code'] . '---Keycode:' . $ruleDetails['couponcode'] . "\n";
                }
                if ($mode == 'update'){
                    //echo '---Updated:: OfferCode:' . $ruleDetails['Offer_Code'] . '---Keycode:' . $ruleDetails['couponcode'] . "\n";
                }
            } catch (Except $e) {
                $result = 'Failed:-KeyCode creation:' . $ruleDetails['keycode'] . "\n" . $e->getMessage . "\n";

                $log_array[]['Message'] = $e->getMessage();
                $log_array[]['Trace'] = $e->getTraceAsString();
                $log_array[]['Info'] = "Unable to create couponcode:" . $ruleDetails['keycode'];
            }
            return $result;
        }

}
