<?php
class Springhills_Catalogprice_Block_Adminhtml_Catalogprice_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("catalogprice_form", array("legend"=>Mage::helper("catalogprice")->__("Price information")));

                $postData = Mage::registry("catalogprice_data")->getData();
                //Readonly for Product_Number in Edit action
                if(count($postData)> 0){
                    $fieldset->addField("Product_Number", "text", array(
                        "label" => Mage::helper("catalogprice")->__("Sku"),
                        "class" => "required-entry",
                        "required" => true,
                        "name" => "Product_Number",
                        "readonly"=>true,
                    ));
                }else{
                    $fieldset->addField("Product_Number", "text", array(
                        "label" => Mage::helper("catalogprice")->__("Sku"),
                        "class" => "required-entry",
                        "required" => true,
                        "name" => "Product_Number",
                    ));
                }

				$fieldset->addField("Offer_Code", "text", array(
				"label" => Mage::helper("catalogprice")->__("Offer Code"),
				"class" => "required-entry",
				"required" => true,
				"name" => "Offer_Code",
				));

				$fieldset->addField("Qty_Low", "text", array(
				"label" => Mage::helper("catalogprice")->__("Qty Low"),
				"class" => "required-entry validate-number",
				"required" => true,
				"name" => "Qty_Low",
				));
				$fieldset->addField("Qty_High", "text", array(
				"label" => Mage::helper("catalogprice")->__("Qty High"),
                "class" => "required-entry validate-number",
				"required" => true,
				"name" => "Qty_High",
				));
				$fieldset->addField("Price", "text", array(
				"label" => Mage::helper("catalogprice")->__("Price"),
				"class" => "required-entry validate-number",
				"required" => true,
				"name" => "Price",
				));

                $fieldset->addField("Whlsl_Price", "text", array(
                    "label" => Mage::helper("catalogprice")->__("Whlsl Price"),
                    "name" => "Whlsl_Price",
                ));
                $fieldset->addField("Sale_Price", "text", array(
                    "label" => Mage::helper("catalogprice")->__("Sale Price"),
                    "name" => "Sale_Price",
                ));
                $fieldset->addField("Last_Update", "text", array(
                    "label" => Mage::helper("catalogprice")->__("Last Update"),
                    "name" => "Last_Update",
                ));
                $fieldset->addField("Catalog_Type", "text", array(
                    "label" => Mage::helper("catalogprice")->__("Catalog Type"),
                    "name" => "Catalog_Type",
                ));
                $fieldset->addField("FreeItm", "text", array(
                    "label" => Mage::helper("catalogprice")->__("FreeItm"),
                    "name" => "FreeItm",
                ));
                $fieldset->addField("FreeQty", "text", array(
                    "label" => Mage::helper("catalogprice")->__("FreeQty"),
                    "name" => "FreeQty",
                ));
				if (Mage::getSingleton("adminhtml/session")->getCatalogpriceData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getCatalogpriceData());
					Mage::getSingleton("adminhtml/session")->setCatalogpriceData(null);
				} 
				elseif(Mage::registry("catalogprice_data")) {
				    $form->setValues(Mage::registry("catalogprice_data")->getData());
				}
				return parent::_prepareForm();
		}
}
