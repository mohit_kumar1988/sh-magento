<?php
$installer = $this;
$installer->startSetup();
$installer->run("CREATE TABLE IF NOT EXISTS `price` (
  `catalogprice_id` int(11) NOT NULL AUTO_INCREMENT,
  `Product_Number` varchar(255) NOT NULL,
  `Offer_Code` varchar(255) NOT NULL,
  `Qty_Low` bigint(20) NOT NULL,
  `Qty_High` bigint(20) NOT NULL,
  `Price` varchar(255) NOT NULL,
  `Whlsl_Price` varchar(255) NOT NULL,
  `Sale_Price` varchar(255) NOT NULL,
  `Last_Update` varchar(100) NOT NULL,
  `Catalog_Type` varchar(255) NOT NULL,
  `FreeItm` varchar(255) NOT NULL,
  `FreeQty` varchar(255) NOT NULL,
  PRIMARY KEY (`catalogprice_id`)
) ;
		");
//demo 
Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 
