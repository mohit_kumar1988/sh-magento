<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Flat sales order resource
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */


require_once 'redback/RedbackServices.php';
class Springhills_Checkout_Model_Mysql4_Cart extends  Mage_Checkout_Model_Resource_Cart
{

    public function getsecureurl(){
        if ($_SERVER["HTTPS"] == 'on') {
            $gcbaseurl =  Mage::getUrl('', array('_forced_secure' => true) );
        }else {
            $gcbaseurl = Mage::getBaseUrl();
        }
        return $gcbaseurl;
    }


    public function insertCustomerCreditCardInfo($data,$quoteId){
        ////Connect to window database for credit card table
        $write_window = Mage::getSingleton('core/resource')->getConnection('wp_write');
        // month is single digit then prepend 0
        $ccExpiryMonth = $data['cc_exp_month'];
        if(strlen($data['cc_exp_month']) == 1)
            $ccExpiryMonth = '0'.$data['cc_exp_month'];
        $ccExpiryDate = $ccExpiryMonth.$data['cc_exp_year'];
        /*ENCODE THE CARD DETAILS*/
        $encust_ccType= base64_encode($data['cc_type']);
        $encust_ccNo=base64_encode($data['cc_number']);
        $encust_ccCid=base64_encode($data['cc_cid']);
        $encust_ccExpDate=base64_encode($ccExpiryDate);
        //check for the existence of quote_id
        $chkForQuote = $write_window->FetchOne("SELECT COUNT(*) AS COUNT FROM customer_card_details WHERE quote_id=".$quoteId);

        /*END OF ENCODE*/
        if($chkForQuote > 0){
            $write_window->query("UPDATE customer_card_details SET card_type='".$encust_ccType."',card_number='".$encust_ccNo."',card_cvv='".$encust_ccCid."',card_expirydate='".$encust_ccExpDate."' WHERE quote_id=".$quoteId);
        }else{
            $write_window->query("INSERT INTO customer_card_details (entity_id,quote_id,card_type,card_number,card_cvv,card_expirydate) VALUES (NULL,".$quoteId.",'".$encust_ccType."','".$encust_ccNo."','".$encust_ccCid."','".$encust_ccExpDate."')");
        }
        //End of saving credit card details
    }

    public function  createRedbackOrder(){
        $session = Mage::getSingleton('checkout/session');
        $quoteId= $session->getQuote()->getId();
        //$quoteId= $session->getLastOrderId();
        $write =Mage::getSingleton('core/resource')->getConnection('core_write');
        //Connect to window database for credit card table
        $write_window = Mage::getSingleton('core/resource')->getConnection('wp_write');
        /***GET THE CARD DETAILS and Customer Number***/
        $creditCardDetails = $write_window ->fetchAll("SELECT * FROM customer_card_details WHERE quote_id=".$quoteId);
        /******/
        /** GET SHIPPING INFORMATION**/
        $shippingAddress = $write->fetchAll('SELECT * FROM sales_flat_quote_address WHERE quote_id='.$quoteId.' and address_type="shipping"');

        /** GET SHIPPING INFORMATION**/
        $billingAddress = $write->fetchAll('SELECT * FROM sales_flat_quote_address WHERE quote_id='.$quoteId.' and address_type="billing"');
        //Getting gift discount price
        $FetchOffer = $write->fetchAll("SELECT redback_certificate_number,redback_certificate_amount,redback_replacecert_number,redback_replacecert_amount FROM sales_flat_quote WHERE entity_id=".$quoteId);
        $billingRegionInfo = $this->getRegionInfo($billingAddress[0]['region_id']);
        $ShippingRegionInfo = $this->getRegionInfo($shippingAddress[0]['region_id']);
        //get the customer Number
        $coresession = Mage::getSingleton("core/session");
        $arrySession=$coresession->getData();
        /*$session->getCustKeycode($data['hdnOfferCatalog']);
        $session->getCustGiftAmounts($data['hdnGiftAmount']);
        $session->getCustGiftNumbers($data['hdnGiftNumber']);*/
        $custNumber = '';
        $custPin =$billingAddress[0]['postcode'];
        $revCreditPlan='';
        $enableRevCreditPlan=0;
        if(!empty($arrySession['cust_number'])){
            $custNumber = $arrySession['cust_number'];
            $revCreditPlan = $arrySession['rev_credit_plan']; // for revolving credit plan
            $enableRevCreditPlan = $arrySession['enable_rev_credit_plan'];
        }
        $pin = $billingAddress[0]['postcode'];
        $pinhint = 'zipcode';
        /*if(empty($arrySession['cust_pin'])){
            $pin = $billingAddress[0]['postcode'];
            $pinhint = 'zipcode';
        }*/

        $custPin = $billingAddress[0]['postcode'];

        $title='4';
        $unitPrice=array();
        $sku=array();
        $qty=array();
        $itemDate = array();
        foreach ($session->getQuote()->getAllItems() as $item) {
            $arraySku='';
            $arraySku = explode("_",$item->getSku());
            $sku[] = $arraySku[0];
            $qty[]=$item->getQty();
            $unitPrice[]=$item->getPrice();
            $itemDate[] = $item->getCreatedAt();
        }
        $strSku='';
        $strQty='';
        $strUnitPrice = '';
        $strItemDate	='';
        if(count($sku) > 0 ){
            $strSku= implode('ý',$sku);
        }
        if(count($qty) > 0 ){
            $strQty= implode('ý',$qty);
        }
        if(count($unitPrice) > 0 ){
            $strUnitPrice= implode('ý',$unitPrice);
        }
        if(count($itemDate) > 0 ){
            $strItemDate= implode('ý',$itemDate);
        }
        $arrPromo = explode('_', $session->getQuote()->getCouponCode() );
        $promocode = $arrPromo[0];
        
        if($promocode == "")
            $promocode='';
        /*if($arrySession['offer']=='no')
            $promocode='';
        */
        //Gift Replacments for 1 2 3
        $giftReplacementCertNum1='';
        $giftReplacementCertAmt1='';
        $giftReplacementCertNum2='';
        $giftReplacementCertAmt2='';
        $giftReplacementCertNum3='';
        $giftReplacementCertAmt3='';
        $strGiftNum= '';
        $strGiftAmt='' ;
        if(!empty($FetchOffer[0]['redback_certificate_number'])){
            $gifttArray1 = explode(",",$FetchOffer[0]['redback_certificate_number']);
            $giftReplacementCertNum1=$gifttArray1[0];
            $strGiftNum =$gifttArray1[0];
            if(isset($gifttArray1[1])){
                $giftReplacementCertNum2=$gifttArray1[1];
                $strGiftNum .= ','.$giftReplacementCertNum2;
            }
        }
        if(!empty($FetchOffer[0]['redback_certificate_amount'])){
            $gifttArray2 = explode(",",$FetchOffer[0]['redback_certificate_amount']);
            $giftReplacementCertAmt1=$gifttArray2[0];
            $strGiftAmt = $giftReplacementCertAmt1;
            if(isset($gifttArray2[1])){
                $giftReplacementCertAmt2=$gifttArray2[1];
                $strGiftAmt .= ','.$giftReplacementCertAmt2;
            }
        }
        $strReplaceGiftNum= '';
        $strReplaceGiftAmt='' ;
        if(!empty($FetchOffer[0]['redback_replacecert_number'])){
            $strReplaceGiftNum= $FetchOffer[0]['redback_replacecert_number'];
            $strReplaceGiftAmt= $FetchOffer[0]['redback_replacecert_amount'];
        }



        //Add two fields for Mobile
        $is_Mobilephone='';
        $is_Mobileoptin='';
        if($arrySession['is_mobilephone']==1){
            $is_Mobilephone= '1';
        }
        if($arrySession['is_mobileoptin']==1){
            $is_Mobileoptin= '1';
        }


        $session->setCustGiftReplacementOne($payment['hdnGiftreplacment1']);
        $session->setCustGiftReplacementTwo($payment['hdnGiftreplacment2']);
        $session->setCustGiftReplacementThree($payment['hdnGiftreplacment3']);

        $orderId= $session->getLastOrderId();
        $objorder = Mage::getModel('sales/order')->load($orderId);
        $couponAmount = $session->getQuote()->getBaseSubtotal() - $session->getQuote()->getBaseSubtotalWithDiscount();
        if(!empty($couponAmount)){
            if($couponAmount < 0)
                $couponAmount = -1*$couponAmount;
        }else{
            $couponAmount =0;
        }
        $paymentMethod='';
        if(empty($revCreditPlan) || $enableRevCreditPlan==0 )
            $paymentMethod = trim(base64_decode($creditCardDetails[0]['card_type']));
        else
            $paymentMethod = $revCreditPlan;

        $data =array(	"OrderNumber"=>'',
            "CustFirstName"=>trim($billingAddress[0]['firstname']),
            "CustLastName"=>trim($billingAddress[0]['lastname']),
            "CustAdd1"=>trim($billingAddress[0]['street']),
            "CustAdd2"=>'',
            "CustAdd3"=>'',
            "CustCity"=>trim($billingAddress[0]['city']),
            //"CustState"=>trim($billingAddress[0]['region']),
            "CustState"=>$billingRegionInfo[0]['code'],
            "CustZip"=>trim($billingAddress[0]['postcode']),
            "CustCountry"=>trim($billingAddress[0]['country_id']),
            "CustPhoneDay"=>trim($billingAddress[0]['telephone']),
            "CustPhoneEve"=>'',
            "CustEmail1"=>trim($billingAddress[0]['email']),
            "CustEmail2"=>'',
            "CustPin"=>$custPin,
            "ShipFirstName"=>trim($shippingAddress[0]['firstname']),
            "ShipLastName"=>trim($shippingAddress[0]['lastname']),
            "ShipAdd1"=>trim($shippingAddress[0]['street']),
            "ShipAdd2"=>'',
            "ShipAdd3"=>'',
            "ShipCity"=>trim($shippingAddress[0]['city']),
            //"ShipState"=>trim($shippingAddress[0]['region']),
            "ShipState"=>$ShippingRegionInfo[0]['code'],
            "ShipZip"=>trim($shippingAddress[0]['postcode']),
            "ShipCountry"=>trim($shippingAddress[0]['country_id']),
            "ShipPhone"=>trim($shippingAddress[0]['telephone']),
            "ShipEmail"=>trim($shippingAddress[0]['email']),
            "ShipAttention"=>'',
            "CustNumber"=>$custNumber,
            "WebReference"=>$session->getLastRealOrderId(),
            "PromoCode"=>$promocode,
            "Title"=>$title,
            "ShipMethod"=>'',
            "PaymentMethod"=> "$paymentMethod",
            "CreditCardNumber"=>trim(base64_decode($creditCardDetails[0]['card_number'])),
            "CardExpDate"=>trim(base64_decode($creditCardDetails[0]['card_expirydate'])),
            "CardCVV"=>trim(base64_decode($creditCardDetails[0]['card_cvv'])),
            "CardAddress"=>'',
            "CardZip"=>'',
            "MerchAmount"=>trim($session->getQuote()->getBaseSubtotal()),
            "CouponAmount"=>$couponAmount, //base_discount_amount
            "DiscAmount"=>'',
            "ShipAmount"=>$objorder->getBaseShippingAmount(),
            "PremShipAmount"=>'',
            "OverweightAmount"=>'',
            "TaxAmount"=>$objorder->getBaseTaxAmount(),
            "TotalAmount"=>trim($session->getQuote()->getBaseGrandTotal()),//base_subtotal 	base_grand_total
            "Comments"=>'',
            "Items"=>utf8_decode($strSku), // comma separated
            "QtyOrdered"=>utf8_decode($strQty),// comma separated
            "UnitPrice"=>utf8_decode($strUnitPrice),// comma separated
            "ReleaseDate"=>utf8_decode($strItemDate),
            "FreeFlag"=>'',
            "PersonalizationCode"=>'',
            "PersonalizationDetail1"=>'',
            "PersonalizationDetail2"=>'',
            "PersonalizationDetail3"=>'',
            "PersonalizationDetail4"=>'',
            "PersonalizationDetail5"=>'',
            "PIN"=>$pin,
            "PINHint"=>$pinhint,
            "OptInFlag"=>'',
            "OptInDate"=>'',
            "OptOutDate"=>'',
            "Etrack"=>'',
            "IPSource"=>$_SERVER["REMOTE_ADDR"],
            "GVCFlag"=>'',
            "GVCDate"=>'',
            "ValidErrCode"=>'',
            "ValidErrMsg"=>'',
            "OrderErrCode"=>'',
            "OrderErrMsg"=>'',
            "Cert1_Number"=>$giftReplacementCertNum1,
            "Cert1_Amount"=>$giftReplacementCertAmt1,
            "Cert2_Number"=>$giftReplacementCertNum2,
            "Cert2_Amount"=>$giftReplacementCertAmt2,
            "Cert3_Number"=>$giftReplacementCertNum3,
            "Cert3_Amount"=>$giftReplacementCertAmt3,
            "Repc1_Number"=>$strReplaceGiftNum,
            "Repc1_Amount"=>$strReplaceGiftAmt,
            "Repc2_Number"=>'',
            "Repc2_Amount"=>'',
            "Repc3_Number"=>'',
            "Repc3_Amount"=>'',
            "Web_Date"=>'',
            "Web_Time"=>'',
            "MobilePhone"=>$is_Mobilephone,
            "MobileOptin"=>$is_Mobileoptin
        );

        $defaultKeycode = Mage::getSingleton('core/session')->getData('offerCode');
        $resource = Mage::getResourceSingleton('newsletter/subscriber');
        $existenceCount = $resource->checkSubscriberEmail($billingAddress[0]['email']);
        if ($existenceCount == 0 || ($existenceCount>0 && !empty($defaultKeycode) )) {
            Mage::getModel('newsletter/subscriber')->subscribe($billingAddress[0]['email']);
        }

        // For UCX logContact
        Mage::getSingleton('core/session')->setData("cookie_type","center");
        Mage::getSingleton('core/session')->setData("pageType" , "order status");
        Mage::getSingleton('core/session')->setData("requestType" , "email checkout");
        Mage::getSingleton('core/session')->setData("requestEmail" , $billingAddress[0]['email']);
        // UCX code ends here /////


        $objRedBackApi = new RedbackServices();
        $responseRedback = $objRedBackApi->createOrderService($data);
        $orderId= $session->getLastOrderId();
        //If redback is not availble else
        if(!$responseRedback){
            $write_window->query("UPDATE customer_card_details SET entity_id= ".$orderId." WHERE quote_id=".$quoteId);
            $write->query("Update sales_flat_order SET redback_status='0',redback_keycode='".$promocode."',redback_certificate_number='".$strGiftNum."',redback_certificate_amount='".$strGiftAmt."',redback_mobilephone='".$is_Mobilephone."',redback_mobileoptin='".$is_Mobileoptin."' , redback_replacecert_amount='".$strReplaceGiftAmt."',redback_replacecert_number='".$strReplaceGiftNum."'  WHERE quote_id=".$quoteId);
        }else{
            if($responseRedback['OrderErrCode']=='0' || empty($responseRedback['OrderErrCode']) ){

                $write_window->query("DELETE FROM customer_card_details WHERE quote_id=".$quoteId);
                $write->query("Update sales_flat_order SET redback_order_number='".$responseRedback['OrderNumber']."', redback_status='1',redback_keycode='".$promocode."',redback_certificate_number='".$strGiftNum."',redback_certificate_amount='".$strGiftAmt."',redback_mobilephone='".$is_Mobilephone."',redback_mobileoptin='".$is_Mobileoptin."',redback_cust_num='".$responseRedback['CustNumber']."',redback_cust_zip='".$responseRedback['CustZip']."', redback_replacecert_amount='".$strReplaceGiftAmt."',redback_replacecert_number='".$strReplaceGiftNum."'  WHERE quote_id=".$quoteId);
                //Set Session for new customer
                if(empty($custNumber)){
                    $this->setSessionNewCustomer($responseRedback);
                }
            }else{
                $write_window->query("UPDATE customer_card_details SET entity_id= ".$orderId." WHERE quote_id=".$quoteId);
                $write->query("Update sales_flat_order SET redback_status='0',redback_keycode='".$promocode."',redback_certificate_number='".$strGiftNum."',redback_certificate_amount='".$strGiftAmt."',redback_mobilephone='".$is_Mobilephone."',redback_mobileoptin='".$is_Mobileoptin."' , redback_replacecert_amount='".$strReplaceGiftAmt."',redback_replacecert_number='".$strReplaceGiftNum."'  WHERE quote_id=".$quoteId);
            }
        }


    }


    //http://www.dreamincode.net/forums/topic/10862-zip-code-to-city-state/
    public function get_zip_info($zip) {

        //Function to retrieve the contents of a webpage and put it into $pgdata
        $pgdata =""; //initialize $pgdata
        $fd = fopen("http://zipinfo.com/cgi-local/zipsrch.exe?zip=$zip","r"); //open the url based on the user input and put the data into $fd
        while(!feof($fd)) {//while loop to keep reading data into $pgdata till its all gone
            $pgdata .= fread($fd, 1024); //read 1024 bytes at a time
        }
        fclose($fd); //close the connection
        if (preg_match("/is not currently assigned/", $pgdata)) {
            $city = "N/A";
            $state = "N/A";
        }
        else {
            $citystart = strpos($pgdata, "Code</th></tr><tr><td align=center>");
            $citystart = $citystart + 35;
            $pgdata = substr($pgdata, $citystart);
            $cityend = strpos($pgdata, "</font></td><td align=center>");
            $city = substr($pgdata, 0, $cityend);

            $statestart = strpos($pgdata, "</font></td><td align=center>");
            $statestart = $statestart + 29;
            $pgdata = substr($pgdata, $statestart);
            $stateend = strpos($pgdata, "</font></td><td align=center>");
            $state = substr($pgdata, 0, $stateend);
        }
        $zipinfo[zip] = $zip;
        $zipinfo[city] = $city;
        $zipinfo[state] = $state;
        return $zipinfo;
    }


    public function getRegionInfo($region){
        //Saving Credit card details temporarliy till the redback status is success
        $write =Mage::getSingleton('core/resource')->getConnection('core_write');
        if(is_numeric($region))
            $regionInfo = $write->FetchAll("SELECT * FROM directory_country_region WHERE region_id=".$region." and country_id='US'");
        else
            $regionInfo = $write->FetchAll("SELECT * FROM directory_country_region WHERE code='".$region."' and country_id='US'");
        return $regionInfo;
    }

    public function setSessionNewCustomer($responseRedback){
        /*********IF customer comes as guest register then set the session bases on the new customer info**********/
        //REDBACK API CALL FOR AUTHENTICATION OF CUSTOMER  60606    '49150029'
        $objRedBackApi = new RedbackServices();
        $params =array(	'CustNumber'	=>	$responseRedback['CustNumber'],
            'Title'	=>	'4'	,
            'CustName'	=>	''	,
            'CustAdd1'	=>	''	,
            'CustAdd2'	=>	''	,
            'CustState'	=>	''	,
            'CustCity'	=>	''	,
            'CustZip'	=>	$responseRedback['CustZip'],
            'CustCountry'	=>	''	,
            'CustPhone'	=>	''	,
            'CustFax'	=>	''	,
            'CustEmail'	=>	'',
            'CustPin'	=>	''	,
            'CustClubFlag'	=>	''	,
            'CustClubDisc'	=>	''	,
            'CustClubDate'	=>	''	,
            'CustPinHint'	=>	''	,
            'CustOptInFlag'	=>	''	,
            'CustError' => '',
            'CustMessage' => '',
            'CustOrder'	=>	''	,
            'CustFirstName'	=>	''	,
            'CustLastName'	=>	''	,
            'ClubNumber'	=>	'');

        $responseCustomer = $objRedBackApi->CustMasterAccessEmailZip($params);

        //If Customer error code is zero then successfully logged
        if($responseCustomer['CustError']=='0'){
            $customerNumer= $responseCustomer['CustNumber'];
            $customerArray =array();
            $customerArray[$customerNumer]['CustNumber']=$responseCustomer['CustNumber'];
            $customerArray[$customerNumer]['CustAdd1']=$responseCustomer['CustAdd1'];
            $customerArray[$customerNumer]['CustAdd2']=$responseCustomer['CustAdd2'];
            $customerArray[$customerNumer]['State']=$responseCustomer['CustState'];
            if(empty($responseCustomer['CustState'])){
                $stateCode = $this->get_zip_info($responseCustomer['CustZip']);
                $customerArray[$customerNumer]['State']=$stateCode['state'];
            }
            $customerArray[$customerNumer]['City']=$responseCustomer['CustCity'];
            $customerArray[$customerNumer]['Country']=$responseCustomer['CustCountry'];
            $customerArray[$customerNumer]['Phone']=str_replace(".","",$responseCustomer['CustPhone']);
            $customerArray[$customerNumer]['CustFax']=$responseCustomer['CustFax'];
            $customerArray[$customerNumer]['Email']=$responseCustomer['CustEmail'];
            $customerArray[$customerNumer]['CustPin']=$responseCustomer['CustPin'];
            $customerArray[$customerNumer]['FirstName']=$responseCustomer['CustFirstName'];
            $customerArray[$customerNumer]['LastName']=$responseCustomer['CustLastName'];
            $customerArray[$customerNumer]['CustZip']=$responseCustomer['CustZip'];
            $session = Mage::getSingleton("core/session");
            $session->setCustNumber($customerArray[$customerNumer]['CustNumber']);
            $session->setCustAdd1($customerArray[$customerNumer]['CustAdd1']);
            $session->setCustAdd2($customerArray[$customerNumer]['CustAdd2']);
            $session->setCustState($customerArray[$customerNumer]['State']);
            $session->setCustCity($customerArray[$customerNumer]['City']);
            $session->setCustCountry($customerArray[$customerNumer]['Country']);
            $session->setCustPhone($customerArray[$customerNumer]['Phone']);
            $session->setCustFax($customerArray[$customerNumer]['CustFax']);
            $session->setCustEmail($customerArray[$customerNumer]['Email']);
            $session->setCustPin($customerArray[$customerNumer]['CustPin']);
            $session->setCustFirstName($customerArray[$customerNumer]['FirstName']);
            $session->setCustLastName($customerArray[$customerNumer]['LastName']);
            $session->setCustZip($customerArray[$customerNumer]['CustZip']);
            //exit;
        }
    }
//amitsharma@somemailll.com
    /*******************/

    public function getOfferPriceBlock($productSku , $keycode, $uom)	{
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $defaultOfferProductPrice='';
        $offerCode = 4; //For SH, the default offer code is always 4
        $k=0;
        if (!empty($keycode)){
            $offerCode = $write->fetchOne("SELECT Offer_Code FROM keycodes where Keycode='".$keycode."' ");
            if(empty($offerCode))
                $offerCode = 4;
            else
                $defaultOfferProductPrice = $write->fetchAll("SELECT * FROM price where Product_Number='".$productSku."' and  Offer_Code='4' ");
        }

        $offerProductPrice= $write->fetchAll("SELECT * FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' ");
        $offercount= $write->FetchOne("SELECT count(*) FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' ");
        if(empty($offerProductPrice)){
            $offerProductPrice = $write->fetchAll("SELECT * FROM price where Product_Number='".$productSku."' and  Offer_Code='4' ");
            $offercount= $write->FetchOne("SELECT count(*) FROM price where Product_Number='".$productSku."' and  Offer_Code='4' ");
            $offerCode=4;
        }
        //echo $offerCode;
        $block = '';
        $i=0;
        $priceFlag=0;
        if(count($defaultOfferProductPrice) > 1 && $offercount!= count($defaultOfferProductPrice)){
            $priceFlag=1;
        }
        //echo count($offerProductPrice);
        if(count($offerProductPrice)>0){
            if($offercount>1){
                $block .="<div class='Discounts' id='discounts'>";
                $block .="<p class='discount' style='color: #C03;font-family: Arial;font-size: 14px;font-weight:bold;text-align:center;'>Save with Volume Discounts!</p>";
                $block .="<table border='0' align='center' width='90%' cellspacing='0' cellpadding='0' style='margin:0 auto;'>
                <tbody style='color:#222;'><tr>
                <th  width='33%'>Qty (per Item)</th>
                <th align='center' width='33%' class='a-center'>Price per</th>
                <th align='center' width='33%' class='a-center' style='color: #C03;font-family: Arial;font-size: 13px;font-weight:bold;font-style:Italic;'>YOU SAVE</th></tr>";

            }
            foreach($offerProductPrice as $tierInfo){
                $i++;
                if(empty($uom)) $uom = 1;
                if($uom){

                    if($i==1){ $startingprice = substr( number_format(($tierInfo['Price']),4) , 0 , -2 ); }
                    $finalPrice = substr( number_format(($tierInfo['Price']),4) , 0 , -2 );
                    $tdiff=(($startingprice-$finalPrice)/$startingprice)*100;
                    $tnewp = number_format($tdiff);
                    if($offerCode==4){
                        $withOutOfferProductPrice = $write->fetchOne("SELECT Price FROM price where Qty_Low='".$tierInfo['Qty_Low']."' AND Product_Number='".$productSku."' and  Offer_Code='4' ");
                        $withOutOfferFinalPrice = substr( number_format(($withOutOfferProductPrice*$tierInfo['Qty_Low']),4) , 0 , -2 );
                        $withOutOfferProductSalePrice = $write->fetchOne("SELECT Sale_Price FROM price where Qty_Low='".$tierInfo['Qty_Low']."' AND Product_Number='".$productSku."' and  Offer_Code='4' ");
                        $withOutOfferSaleFinalPrice = substr( number_format(($withOutOfferProductSalePrice*$tierInfo['Qty_Low']),4) , 0 , -2 );
                        $finalPrice = substr( number_format(($tierInfo['Price']*$tierInfo['Qty_Low']),4) , 0 , -2 );
                        if($tierInfo['Sale_Price']){

                            //echo $withOutOfferFinalPrice."++++++".$withOutOfferSaleFinalPrice;
                            if($withOutOfferFinalPrice<$withOutOfferSaleFinalPrice){
                                if($i==1)

                                    $block .= "<span class='price' style='font-size: 14px; margin-left: 60px; color:#222;' >"."Price: ".($uom*$tierInfo['Qty_Low']) . " for $" ."<span style='text-decoration: line-through;'>".$withOutOfferSaleFinalPrice ."</span>". "</span> \n";
                                $block .="<br>";
                                $diff=(($withOutOfferSaleFinalPrice - $withOutOfferFinalPrice)/$withOutOfferSaleFinalPrice)*100;
                                $newp = number_format(floor($diff));

                                $block .= "<span class='price' style='color:#990000;font-size: 16px; ' >"."Sale Price: ".($uom*$tierInfo['Qty_Low']) . " for $" .$withOutOfferFinalPrice ."<span style='color:#336633;font-size:16px;margin-left:10px;'>"."SAVE "."$newp"."%!"."</span>". "</span> \n";


                                /*$block .= "<span class='price' style='text-decoration: line-through;' >".($uom*$tierInfo['Qty_Low']) . " for 123  $" .$withOutOfferSaleFinalPrice. "</span> \n";
                                $block .= "<span class='price' style='color:#FF0000;' >".($uom*$tierInfo['Qty_Low']) . " for 456 $" .$withOutOfferFinalPrice . "</span> \n";*/




                            }else{
                                $block .= "<span class='price'  >".($uom*$tierInfo['Qty_Low']) . " for $" .$withOutOfferFinalPrice . "</span> \n";
                            }


                        }else{
                            //echo $tierInfo['Price']."===".$withOutOfferProductPrice;
                            if($tierInfo['Price']!=$withOutOfferProductPrice){
                                $withOutOfferFinalPrice = substr( number_format(($withOutOfferProductPrice*$tierInfo['Qty_Low']),4) , 0 , -2 );
                                if($i==1)
                                    $block .= "<span class='price' style='text-decoration: line-through;' >".($uom*$tierInfo['Qty_Low']) . " for $" .$withOutOfferFinalPrice . "</span> \n";

                                $block .= "<span class='price' style='color:#FF0000;' >".($uom*$tierInfo['Qty_Low']) . " for $" .$finalPrice . "</span> \n";
                            }else{
                                //echo 'TEst'.$offercount;
                                if($offercount>1 ){
                                    $block .= "<tr>";

                                    if($i==count($offerProductPrice)){
                                        $block .= "<td align='center' class='Qty' style='font-weight:normal' >".($uom*$tierInfo['Qty_Low'])."+"."</td>";
                                    }else{
                                        $block .= "<td align='center' class='Qty' style='font-weight:normal' >".($uom*$tierInfo['Qty_Low'])."-".($uom*$tierInfo['Qty_High'])."</td>";
                                    }
                                    //$block .="<td>$" .$finalPrice . "</td> \n";
                                    //echo number_format($tierInfo['Price'],4);
                                    $block .= "<td style='text-align:center;' class='Priceper'><span class='price' style='color: #C03;font-weight:normal;'><span class='price'>$" .(floor($tierInfo['Price'] *100)/100). "</span></span></td>";



                                    if($i!=1){
                                        $block .= "<td style='text-align:center;' class='Priceper'><span  style='color: #C03;'>" .$tnewp."%</span></td>";
                                    }else{
                                        $block .= "<td style='text-align:center;' class='Priceper'><span  style='color: #C03;'></span></td>";
                                    }

                                    $block .="</tr>";


                                }

                                else{
                                    $block .= "<span class='price' style='font-size: 14px; margin-left: 60px; color:#222;'>"."Price: ".($uom*$tierInfo['Qty_Low']) . " for  $" .$finalPrice . "</span> \n";

                                }


                            }


                        }

                    }else{
                        $withOutOfferProductPrice = $write->fetchOne("SELECT Price FROM price where Qty_Low='".$tierInfo['Qty_Low']."' AND Product_Number='".$productSku."' and  Offer_Code='4' ");
                        $finalPrice = substr( number_format(($tierInfo['Price']*$tierInfo['Qty_Low']),4) , 0 , -2 );
                        if($tierInfo['Price']!=$withOutOfferProductPrice){
                            $withOutOfferFinalPrice = substr( number_format(($withOutOfferProductPrice*$tierInfo['Qty_Low']),4) , 0 , -2 );


                            if($offercount>1){

                            }
                            else{


                                if($i==1)
                                    $block .= "<span class='price' style='font-size: 14px; margin-left: 60px; color:#222;' >"."Price: ".($uom*$tierInfo['Qty_Low']) . " for $" ."<span style='text-decoration: line-through;'>".$withOutOfferFinalPrice ."</span>". "</span> \n";
                                $block .="<br>";
                                $diff=(($withOutOfferFinalPrice - $finalPrice)/$withOutOfferFinalPrice)*100;
                                $newp = number_format(floor($diff));

                                $block .= "<span class='price' style='color:#990000;font-size: 16px; ' >"."Sale Price: ".($uom*$tierInfo['Qty_Low']) . " for $" .$finalPrice ."<span style='color:#336633;font-size:16px;margin-left:10px;'>"."SAVE "."$newp"."%!"."</span>". "</span> \n";
                            }
                            if($offercount>1 ){

                                if($k==0)
                                {
                                    $baseprice=$withOutOfferProductPrice;
                                }
                                $finalPrice = substr( number_format(($tierInfo['Price']),4) , 0 , -2 );
                                $offerdiff=(($baseprice-$finalPrice)/$baseprice)*100;
                                $offernewp = number_format($offerdiff);
                                $k++;
                                $block .= "<tr>";
                                if($i==count($offerProductPrice)){
                                    $block .= "<td align='center' class='Qty' style='font-weight:normal' >".($uom*$tierInfo['Qty_Low'])."+"."</td>";
                                }else{

                                    $block .= "<td align='center' class='Qty' style='font-weight:normal' >".($uom*$tierInfo['Qty_Low'])."-".($uom*$tierInfo['Qty_High'])."</td>";


                                }
                                //$block .="<td>$" .$finalPrice . "</td> \n";



                                $block .= "<td style='text-align:center;' class='Priceper'><span class='price' style='color: #222;font-weight:normal;'>";
                                if($withOutOfferProductPrice)
                                    $block .= "<span style='text-decoration: line-through;color: #222;'>"."$".(floor($withOutOfferProductPrice *100)/100)."</span> ";
                                $block .= "<span class='price' >$".(floor($tierInfo['Price'] *100)/100) . "</span></span></td>";



                                //if($i!=1){
                                $block .= "<td style='text-align:center;' class='Priceper'><span  style='color: #C03;'>" .$offernewp."%</span></td>";
                                //}else{
                                //$block .= "<td style='text-align:center;' class='Priceper'><span  style='color: #C03;'></span></td>";
                                //}

                                $block .="</tr>";


                            }


                        }else{
                            if($tierInfo['Sale_Price']!=null && $tierInfo['Sale_Price'] > $tierInfo['Price'] ){
                                if($i==1)
                                    /* $block .= "<span class='price' style='text-decoration: line-through;' >".($uom*$tierInfo['Qty_Low']) . " 111 for $" .$tierInfo['Sale_Price'] . "</span> \n";

                                 $block .= "<span class='price' style='color:#FF0000;' >".($uom*$tierInfo['Qty_Low']) . " 222 for $" .substr( number_format(($tierInfo['Price']*$tierInfo['Qty_Low']),4) , 0 , -2 ) . "</span> \n";*/


                                    $block .= "<span class='price' style='font-size: 14px; margin-left: 60px; color:#222;' >"."Price: ".($uom*$tierInfo['Qty_Low']) . " for $" ."<span style='text-decoration: line-through;'>".$tierInfo['Sale_Price'] ."</span>". "</span> \n";
                                $block .="<br>";
                                $diff=(($tierInfo['Sale_Price'] - substr( number_format(($tierInfo['Price']*$tierInfo['Qty_Low']),4) , 0 , -2 ))/$tierInfo['Sale_Price'])*100;
                                $newp = number_format(floor($diff));

                                $block .= "<span class='price' style='color:#990000;font-size: 16px; ' >"."Sale Price: ".($uom*$tierInfo['Qty_Low']) . " for $" .substr( number_format(($tierInfo['Price']*$tierInfo['Qty_Low']),4) , 0 , -2 )."<span style='color:#336633;font-size:16px;margin-left:10px;'>"."SAVE "."$newp"."%!"."</span>". "</span> \n";





                            }else{
                                if($priceFlag){
                                    $priceBlock = "<span class='price' style='font-size: 14px; margin-left: 60px; color:#222;'>"."Price: ".($uom*$tierInfo['Qty_Low']) . " for $" .$finalPrice . "</span> \n";

                                }else{

                                    if($offercount>1 ){

                                        if($k==0)
                                        {
                                            $baseprice=$withOutOfferProductPrice;
                                        }
                                        //$finalPrice = substr( number_format(($tierInfo['Price']),4) , 0 , -2 );
                                        $offerPrice = $tierInfo['Price'];
                                        $mainParts = explode('.', $offerPrice);
                                        if (strlen($mainParts[1]) > 2) {
                                            $next = substr($mainParts[1],0,2);
                                            $finalPrice = $mainParts[0].'.'.$next;
                                        }else{
                                            $finalPrice = $offerPrice;
                                        }
                                        $offerdiff=(($baseprice-$finalPrice)/$baseprice)*100;
                                        $offernewp = number_format($offerdiff);
                                        $k++;
                                        $block .= "<tr>";
                                        if($i==count($offerProductPrice)){
                                            $block .= "<td align='center' class='Qty' style='font-weight:normal' >".($uom*$tierInfo['Qty_Low'])."+"."</td>";
                                        }else{

                                            $block .= "<td align='center' class='Qty' style='font-weight:normal' >".($uom*$tierInfo['Qty_Low'])."-".($uom*$tierInfo['Qty_High'])."</td>";


                                        }
                                        //$block .="<td>$" .$finalPrice . "</td> \n";


                                        $block .= "<td style='text-align:center;' class='Priceper'><span class='price' style='color: #222;font-weight:normal;'><span class='price' >$" .$finalPrice . "</span></span></td>";



                                        //if($i!=1){
                                        $block .= "<td style='text-align:center;' class='Priceper'><span  style='color: #C03;'>" .$offernewp."%</span></td>";
                                        //}else{
                                        //$block .= "<td style='text-align:center;' class='Priceper'><span  style='color: #C03;'></span></td>";
                                        //}

                                        $block .="</tr>";


                                    }
                                    else{
                                        $block .= "<span class='price' style='font-size: 14px; margin-left: 60px; color:#222;'>"."Price: ".($uom*$tierInfo['Qty_Low']) . " for $" .$finalPrice . "</span> \n";
                                    }

                                }



                            }
                        }
                    }
                }else{
                    $block .= "<span class='price'>".$tierInfo['Qty_Low'] . " for </span>" . Mage::helper('core')->currency($tierInfo['Price']). "\n";
                }
            }
            if($offercount>1){
                $block .='</tbody></table></div>';
            }
        }

        return $block;
    }



    public function getSalePriceBlock($productSku, $uom){
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $offerCode = 4; //For SH, the default offer code is always 4

        $block='';
        $offerProductPrice= $write->fetchOne("SELECT Sale_Price FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' order by Qty_Low ");
        if(empty($offerProductPrice)){
            $offerPrice= $write->fetchOne("SELECT Price FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' order by Qty_Low");
            $mainPrice = substr( number_format(($offerPrice),4) , 0 , -2 );
            $block .= "<span class='price' style='color: #336633;font-size: 11px;font-weight: bold;'> Today's Low Price: ".$uom . " for $" .$mainPrice . "</span> \n";
        }else{
            $offerPrice= $write->fetchOne("SELECT Price FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' order by Qty_Low");
            $mainPrice = substr( number_format(($offerProductPrice),4) , 0 , -2 );
            $salePrice = substr( number_format(($offerPrice),4) , 0 , -2 );
            $color='color:#336633;';
            if($mainPrice > $salePrice){
                $block .= "<span class='price' style='color: #336633;font-size: 11px;font-weight: bold;' >Price :&nbsp;</span><span class='price' style='text-decoration: line-through;color: #336633;font-size: 11px;font-weight: bold;' >".$uom . " for $" .$mainPrice . "</span> \n";
                $color = 'color:#FF0000;';
            }
            $block .= "<span class='price' style='".$color."font-weight: bold;' >Today's Low Price: ". $uom . " for  $" .$salePrice . "</span> \n";
        }

        return $block;
    }
    public function getSalePriceCartBlock($productSku, $uom , $qty ,$defaultPrice){
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $offerCode = 4; //For SH, the default offer code is always 4
        $bannerofferCode = Mage::getSingleton('core/session')->getData("offerCode");
        if(!empty($bannerofferCode)){
            $offerCode = $write->fetchOne("SELECT Offer_Code FROM keycodes where Keycode='".$bannerofferCode."' ");
            if(empty($offerCode))
                $offerCode = 4;
        }
        $qty = $qty/$uom;
        $block='';
        if($offerCode==4){
            $offerProductPrice= $write->fetchOne("SELECT Sale_Price FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' and '".$qty."' BETWEEN Qty_Low AND Qty_High order by Qty_Low ");
            if(empty($offerProductPrice)){
                $offerPrice= $write->fetchOne("SELECT Price FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' and '".$qty."' BETWEEN Qty_Low AND Qty_High order by Qty_Low");
                //$offerPrice=floor($offerPrice *100)/100;
                $mainParts = explode('.', $offerPrice);
                if (strlen($mainParts[1]) > 2) {
                    $next = substr($mainParts[1],0,2);
                    $offerPrice = $mainParts[0].'.'.$next;
                }else{
                    $offerPrice = $offerPrice;
                }
                $mainPrice = Mage::helper('core')->currency(number_format(($offerPrice),4));
                $block .= "<span class='price' style='color: #666666;font-size: 11px;font-weight: bold;'>".$uom . " for <span class='price'>$" .$offerPrice . "</span></span> \n";
            }else{
                $offerPrice= $write->fetchOne("SELECT Price FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' and '".$qty."' BETWEEN Qty_Low AND Qty_High order by Qty_Low");
                $mainPrice = Mage::helper('core')->currency(number_format(($offerProductPrice),4));
                //$mainPrice=floor($offerProductPrice *100)/100;
                $mainParts = explode('.', $offerProductPrice);
                if (strlen($mainParts[1]) > 2) {
                    $next = substr($mainParts[1],0,2);
                    $mainPrice = $mainParts[0].'.'.$next;
                }else{
                    $mainPrice = $offerProductPrice;
                }
                //$salePrice = Mage::helper('core')->currency(number_format(($offerPrice),4) );
                //$salePrice=floor($offerPrice *100)/100;

                $saleParts = explode('.', $offerPrice);
                if (strlen($saleParts[1]) > 2) {
                    $next = substr($saleParts[1],0,2);
                    $salePrice = $saleParts[0].'.'.$next;
                }else{
                    $salePrice = $offerPrice;
                }
                if($salePrice!=$mainPrice){
                    $block .= "<span class='price strikethrough' style='text-decoration: line-through;color: #666666;font-size: 11px;font-weight: bold;' >".$uom . " for <span class='price' >$" .$mainPrice . "</span></span> \n";
                    $block .= "<span class='price redcolor' style='color:#FF0000;font-weight: bold;' >". $uom . " for  <span class='price'>$" .$salePrice . "</span></span> \n";
                }else{
                    $block .= "<span class='price' style='color: #666666;font-size: 11px;font-weight: bold;' >".$uom . " for <span class='price' >$" .$mainPrice . "</span></span> \n";
                }

            }
        }else{
            $offerProductSalePrice='';
            $offerProductPrice= $offerProductTestPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' and '".$qty."' BETWEEN Qty_Low AND Qty_High  ");
            if(empty($offerProductPrice)){
                $offerProductPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$productSku."' and  Offer_Code='4' and '".$qty."' BETWEEN Qty_Low AND Qty_High  ");
                $offerProductSalePrice = $write->fetchOne("SELECT Sale_Price FROM price where Product_Number='".$productSku."' and  Offer_Code='4' and '".$qty."' BETWEEN Qty_Low AND Qty_High  ");

            }

            if (empty($offerProductPrice))
                $offerProductPrice = $defaultPrice;

            //$offerProductPrice = $offerProductPrice;
            //$price = Mage::helper('core')->currency($offerProductPrice);
            $offerPrice= $write->fetchOne("SELECT Price FROM price where Product_Number='".$productSku."' and  Offer_Code='4' and '".$qty."' BETWEEN Qty_Low AND Qty_High order by Qty_Low");
            //$mainPrice = Mage::helper('core')->currency($offerPrice);

            $mainParts = explode('.', $offerPrice);
            if (strlen($mainParts[1]) > 2) {
                $next = substr($mainParts[1],0,2);
                $mainPrice = $mainParts[0].'.'.$next;
            }else{
                $mainPrice = $offerPrice;
            }

            $offerParts = explode('.', $offerProductPrice);
            if (strlen($offerParts[1]) > 2) {
                $next = substr($offerParts[1],0,2);
                $price = $offerParts[0].'.'.$next;
            }else{
                $price = $offerProductPrice;
            }

            $color = 'color: #666666;';
            $cls ='';
            if($offerProductSalePrice!=null && $offerProductSalePrice > $offerProductPrice ){
                $color ='color:#FF0000;';
                $cls = 'redcolor';
                //$mainSalePrice = Mage::helper('core')->currency($offerProductSalePrice);
                $mainSalePrice=floor($offerProductSalePrice *100)/100;
                $block .= "<span class='price strikethrough' style='text-decoration: line-through;color: #666666;font-size: 11px;font-weight: bold;'>".$uom . " for <span class='price'>$" .$mainSalePrice . "</span></span> \n";
            }else{
                if($offerProductTestPrice!=null && $offerProductPrice< $offerPrice){
                    $color ='color:#FF0000;';
                    $cls = 'redcolor';
                    $block .= "<span class='price strikethrough' style='text-decoration: line-through;color: #666666;font-size: 11px;font-weight: bold;'>".$uom . " for <span class='price' >$ " .$mainPrice . "</span></span> \n";
                }
            }

            $block .= "<span class='price ".$cls."' style='".$color."font-weight: bold;' >". $uom . " for <span class='price' >$" .$price . "</span></span> \n";

        }


        return $block;
    }

    public function getOfferCartPrice($productSku, $keycode, $qty, $uom, $defaultPrice){
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $offerCode = 4; //For SH, the default offer code is always 4
        if (!empty($keycode)){
            $offerCode = $write->fetchOne("SELECT Offer_Code FROM keycodes where Keycode='".$keycode."' ");
            if(empty($offerCode))
                $offerCode = 4;
        }

        $qty = $qty/$uom;

        $offerProductPrice= $write->fetchOne("SELECT Price FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' and '".$qty."' BETWEEN Qty_Low AND Qty_High  ");
        if(empty($offerProductPrice))
            $offerProductPrice = $write->fetchOne("SELECT Price FROM price where Product_Number='".$productSku."' and  Offer_Code='4' and '".$qty."' BETWEEN Qty_Low AND Qty_High  ");

        if (empty($offerProductPrice))
            $offerProductPrice = $defaultPrice;

        $offerProductPrice = number_format((($offerProductPrice)*$qty) , 4);
        $price = Mage::helper('core')->currency($offerProductPrice);

        return $price;
    }

    public function updateCart($itemId,$productId,$productQty,$action){
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $session = Mage::getSingleton('checkout/session');
        $quoteId= $session->getQuote()->getId();
        if($action=='remove'){
            $cartHelper = Mage::helper('checkout/cart');
            $cartHelper->getCart()->removeItem($itemId)->save();
            return true;
        }
        if($action=='update'){
            $cartHelper = Mage::helper('checkout/cart');
            $items = $cartHelper->getCart()->getItems();
            foreach ($items as $item) {
                if($item->getId()==$itemId){
                    $item->setQty($productQty);
                    $cartHelper->getCart()->save();
                    return true;
                }
            }
        }
    }

    /**** get item id from cart ****/
    public function getItemId($productid, $quoteid){
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        //echo "Update sales_flat_quote_item set offer_product_id = ".$offerproductid." where quote_id=".$quoteid." and product_id=".$productId; die();
        $itemId = $write->fetchOne("select item_id from sales_flat_quote_item where product_id = '".$productid."'  and quote_id='".$quoteid."' ");
        return $itemId;
    }

    /**** Update for Offer Product in cart ****/
    public function updateOfferProduct($offerproductid, $productId){

        $quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        //echo "Update sales_flat_quote_item set offer_product_id = ".$offerproductid." where quote_id=".$quoteid." and product_id=".$productId; die();
        $write->query("Update sales_flat_quote_item set offer_product_id = ".$offerproductid." where quote_id=".$quoteid." and product_id=".$productId);

    }

    /**** get for Offer Product in cart ****/
    public function checkOfferProduct($id){
        $quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
        $quoteItem = Mage::getModel('sales/quote_item')->load($id);
        $quote = Mage::getModel('sales/quote')->load($quoteItem->getQuoteId());
        $offerProductId = $quoteItem->getOfferProductId();
        $quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $checkForOffer = $write->fetchOne("select item_id from sales_flat_quote_item where quote_id='".$quoteid."' and product_id='".$offerProductId."' ");
        return $checkForOffer;
    }

    public function getCouponValidity($couponCode){

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $trimmedCode = explode('_', $couponCode);
        $mainCouponCode = $trimmedCode[0];
        $current_date = date('Y-m-d');
        //$OfferCode = $write->fetchOne("select Offer_Code from keycodes where Keycode ='".$mainCouponCode."' ");
        $rule_id = $write->fetchOne("select rule_id from salesrule_coupon where code ='".$mainCouponCode."' ");
        if(empty($rule_id)){
            return 'Notexists';
        }else{
            $records = $write->fetchAll("select * from salesrule where rule_id ='".$rule_id."' AND '".$current_date."' BETWEEN from_date AND to_date ");
            if(!empty($records)){

                foreach($records as $record){
                    $oneUse = ''; // Magento will take care of this
                    if($record['uses_per_coupon']){
                        $oneUse='True';
                    }else{
                        $oneUse='False';
                    }

                    if($record['uses_per_customer']){
                        $custNumRequired='True';
                    }else{
                        $custNumRequired='False';
                    }

                }
                $session = Mage::getSingleton("core/session");
                $arrySession=$session->getData();
                if($custNumRequired == 'True'){
                    if(empty($arrySession['cust_number']) ){
                        return 'Invalid';
                    } else {
                        // Redback call
                        $custNumber = $arrySession['cust_number'];
                        $custZip = $arrySession['cust_zip'];
                        $objRedBackApi = new RedbackServices();
                        $data =array(
                            "Keycode"=>$mainCouponCode,
                            "Title"=>'4',
                            "Catalog"=>'',
                            "CustNumber" => $custNumber,
                            "CustName" => '',
                            "CustFirstName"=>'',
                            "CustLastName"=>'',
                            "CustCompany"=>'',
                            "CustAdd1" => '',
                            "CustAdd2" => '',
                            "CustCity"=>'',
                            "CustState"=>'',
                            "CustZip"=>$custZip,
                            "CustPhone" => '',
                            "CustEmail" => '',
                            "CustClubFlag"=>'',
                            "CustClubDisc"=>'',
                            "CustClubDate"=>'',
                            "CustError" => '',
                            "CustMessage" => '',
                            "debug_id"=>''
                        );

                        $responseCustomer = $objRedBackApi->keyCodeLoginService($data);

                        if($responseCustomer['Message']=='Success' && empty($responseCustomer['CustError']) && !empty($responseCustomer['CustName']) ){
                            return true;
                        }else{
                            return $responseCustomer['CustMessage'];
                        }

                    }
                } else {
                    // If Customer Number required is false
                    return true;
                }
            }else {
                if($rule_id){
                    return 'Coupon code applied "'.$mainCouponCode.'" is expired.';
                }else
                    return 'Invalid';
            }
        }

    }

    public function getCouponMessage(){
        $quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $couponCode = $write->fetchOne("select coupon_code from sales_flat_quote where entity_id ='".$quoteid."' ");

        if($couponCode)
            return 'no';
        else
            return 'yes';
    }


    public function updateCouponCode($ccode){
        $quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $quote = Mage::getModel('sales/quote')->load($quoteid);
        $coupon_code = $quote->getCouponCode();
        $bannerofferCode = Mage::getSingleton('core/session')->getData("offerCode");
        if($bannerofferCode==null && $ccode!=null){
            $bannerofferCode = $ccode;
        }
        if($coupon_code=='' && $bannerofferCode!=null ){
            $coupon_code = $bannerofferCode;
        }
        //echo $coupon_code;
        $trimmedCode = explode('_', $coupon_code);
        $subtotal = $quote->getSubtotal();

        if($coupon_code!=null ||  $ccode!=null  ){
            echo "\n";
            if($coupon_code==null &&  $ccode!=null){
                $trimmedCode = explode('_', $ccode);
            }
            $records = $write->fetchAll("select * from salesrule_coupon where code like '".$trimmedCode[0]."%' order by coupon_id desc ");
            $count =  count($records);
            $noCoupon = Mage::getModel("checkout/session")->getData("nocoupon");
            $sessionQuote = Mage::getModel('checkout/cart')->getQuote();
            if($count>1 && $noCoupon!='Yes'){
                $applyFlag = 0 ;

                foreach($records as $coupon){
                    $couponArray = explode("_",$coupon['code']);

                    if(count($couponArray)==1){
                        $conditions_serialized = $write->fetchOne("select conditions_serialized from salesrule where rule_id = '".$coupon['rule_id']."' ");
                        $dataArray = unserialize($conditions_serialized);
                        foreach($dataArray as $skey=> $svalue){
                            if($skey=='conditions'){
                                foreach($svalue as $sv){
                                    if($sv['attribute']=='base_subtotal'){
                                        $rule_base_subtotal=$sv['value'];
                                    }
                                }
                            }
                        }
                    }

                    if(count($couponArray)>1){

                        $amount = $couponArray[1];
                        //echo "####";
                        if($subtotal >= $amount){
                            //echo $coupon['code'];
                            // remove the coupon without "_" and applying the new with "_"
                            $applyFlag = 1 ;
                            // Applying the new coupon
                            Mage::getModel("checkout/session")->setData("coupon_code",$coupon['code']);
                            $sessionQuote->setCouponCode($coupon['code'])->save();
                            $sessionQuote->setTotalsCollectedFlag(false)->collectTotals()->save();
                            //Mage::log("_applyCoupon : Set coupon to quote:".$quote->getCouponCode());a

                            break;
                        }
                    }
                }
                if(!$applyFlag ){
                    //echo $trimmedCode['0']."######".$subtotal."######".$rule_base_subtotal;
                    if($subtotal >= $rule_base_subtotal ){
                        // Applying the new coupon
                        Mage::getModel("checkout/session")->setData("coupon_code",$trimmedCode['0']);
                        $sessionQuote->setCouponCode($trimmedCode['0'])->save();
                        $sessionQuote->setTotalsCollectedFlag(false)->collectTotals()->save();
                    }
                }
            } else{

                if( $bannerofferCode!=null && $noCoupon!='Yes' ){
                    if($count==1){
                        Mage::getModel("checkout/session")->setData("coupon_code",$trimmedCode['0']);
                        $sessionQuote->setCouponCode($trimmedCode['0'])->save();
                        $sessionQuote->setTotalsCollectedFlag(false)->collectTotals()->save();
                    }
                }
            }

        }else{
            //echo 'No Coupon';
        }

    }

    public function getBannerType($bannerCode){
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $rule_id = $write->FetchOne("select rule_id from salesrule_coupon where code ='".$bannerCode."' ");
        $current_date = date('Y-m-d');
        $bannerImage='';
        $bannerCount = $write->fetchOne("SELECT count(Offer_Code) as count  FROM  salesrule where rule_id='".$rule_id."' and '".$current_date."' BETWEEN  from_date AND to_date ");
        if($rule_id && $bannerCount){
            $rule = Mage::getModel('salesrule/rule')->load($rule_id);
            $bannerImage = $rule->getDescription();
            $type = explode("^",$bannerImage);
            return $type[0];
        }else{
            return null;
        }
    }

    public function getBannerDesc($bannerCode){
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $rule_id = $write->FetchOne("select rule_id from salesrule_coupon where code ='".$bannerCode."' ");
        $current_date = date('Y-m-d');
        $bannerImage='';
        $bannerCount = $write->fetchOne("SELECT count(Offer_Code) as count  FROM  salesrule where rule_id='".$rule_id."' and '".$current_date."' BETWEEN  from_date AND to_date ");
        if($rule_id && $bannerCount){
            $rule = Mage::getModel('salesrule/rule')->load($rule_id);
            $bannerImage = $rule->getDescription();
            return $bannerImage;
        }else{
            return null;
        }
    }

    public function getBannerImage( $position, $bannerCode){

        if($bannerCode!=4){
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $rule_id = $write->FetchOne("select rule_id from salesrule_coupon where code ='".$bannerCode."' ");
            $current_date = date('Y-m-d');
            $bannerImage='';
            $bannerCount = $write->fetchOne("SELECT count(Offer_Code) as count  FROM  salesrule where rule_id='".$rule_id."' and '".$current_date."' BETWEEN  from_date AND to_date ");
            if($bannerCount){
                $rule = Mage::getModel('salesrule/rule')->load($rule_id);
                $bannerImage = $rule->getDescription();
            }
            $imageArray = explode("^", $bannerImage);
            $expiredate = $imageArray[1];
            $bannerStyle='';

            switch($imageArray[0]){
                Case 1:
                Case 2:
                Case 3:
                Case 4:
                Case 5:
                Case 6:
                Case 8:
                Case 9:
                Case 10:
                Case 11:
                Case 12:
                Case 14:
                Case 15:
                Case 17:
                Case 18:
                    if($imageArray[2])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[2].'\'" ';
                    break;
                Case 7:
                Case 13:
                    if($imageArray[3])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[3].'\'" ';
                    break;
                Case 16:
                    if($imageArray[3])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[3].'\'" ';
                    break;
                Case 19:
                    if($imageArray[7])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[7].'\'" ';
                    break;
                Case 20:
                    if($imageArray[5])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[5].'\'" ';
                    break;
                Case 21:
                    if($imageArray[3])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[3].'\'" ';
                    break;
                Case 23:
                    if($imageArray[4])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[4].'\'" ';
                    break;
                Case 24:
                    if($imageArray[3])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[3].'\'" ';
                    break;
                Case 25:
                    if($imageArray[3])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[3].'\'" ';
                    break;
                Case 26:
                    if($imageArray[3])
                        $bannerStyle = 'style="cursor:pointer;"  onclick="window.location.href=\''.$imageArray[3].'\'" ';
                    break;

            }


            if($position =='home'){
                if($imageArray[0]==1){
                    /*$block ='
					<div class="bc-One" '.$bannerStyle.' >
						<span class="TextOne">Limited Time Special</span>
						<span class="TextTwo">Take $100 OFF<span>your order of $200 or more</span></span>
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-Twentyfive mainhomebanner" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-01.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==2 ){
                    /*$block ='
					<div class="bc-One" '.$bannerStyle.' >
						<span class="TextOne">Limited Time Special</span>
						<span class="TextTwo">Take $20 OFF <span>your order of $50 or more</span></span>
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires">	<span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-Twentyfive mainhomebanner" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-02.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==3 ){
                    /*$block ='
					<div class="bc-One" '.$bannerStyle.' >
						<span class="TextOne">Limited Time Special</span>
						<span class="TextTwo">Take $25 OFF<span>your order of $50 or more</span></span>
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-Twentyfive mainhomebanner" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-03.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==4){
                    /*$block ='
					<div class="bc-One" '.$bannerStyle.' >
						<span class="TextOne">Limited Time Special</span>
						<span class="TextTwo">Take $25 OFF<span>your order of $75 or more</span></span>
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-Twentyfive mainhomebanner" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-04.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==5 ){
                    /*$block ='
					<div class="bc-One" '.$bannerStyle.' >
						<span class="TextOne">Limited Time Special</span>
						<span class="TextTwo">Take $50 OFF<span>your order of $100 or more</span></span>
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-Twentyfive mainhomebanner" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-05.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==6 ){
                    /*$block ='
					<div class="bc-One" '.$bannerStyle.' >
						<span class="TextOne">Limited Time Special</span>
						<span class="TextTwo">Take $50 OFF<span>your order of $200 or more</span></span>
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-Twentyfivee mainhomebanner" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-06.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==7){

                    $block ='
					<div class="bc-Seven" '.$bannerStyle.' >
						<span class="TextOne">Hurry! <br />Take advantage of our</span>
						<span class="TextTwo">1¢ Sale</span>
						<span class="TextThree"> Buy one item at regular price get the second for only a penny! </span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==8 ){

                    $block ='
					<div class="bc-Eight" '.$bannerStyle.' >
						<span class="TextOne">Limited -Time ONLY!</span>
						<span class="TextTwo">FREE
						<span>Shipping & Handling</span></span>
						<span class="TextThree"> Order by the deadline below and you\'ll get FREE Shipping Handling on your next order!</span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==9 ){
                    $block ='
					<div class="bc-Eight" '.$bannerStyle.'>
						<span class="TextOne">Limited -Time ONLY!</span>
						<span class="TextTwo">FREE
						<span>Shipping & Handling</span></span>
						<span class="TextThree">Order by the deadline below and you\'ll get FREE Shipping Handling on your next order of $25 or more!</span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==10 ){
                    $block ='
					<div class="bc-Eight" '.$bannerStyle.' >
						<span class="TextOne">Limited -Time ONLY!</span>
						<span class="TextTwo">FREE
						<span>Shipping & Handling</span></span>
						<span class="TextThree">Order by the deadline below and you\'ll get FREE Shipping Handling on your next order of $75 or more!</span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==11 ){
                    $block ='
					<div class="bc-Eleven" '.$bannerStyle.' >
						<span class="TextOne">Sitewide Sale!</span>
						<span class="TextTwo">Save 10% Off<span>your total order.</span></span>
						<span class="TextThree">Order by the deadline below and Save 10% off your next order!</span>
						<span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==12 ){
                    $block ='
					<div class="bc-Twelve" '.$bannerStyle.' >
						<span class="TextOne">Sitewide Sale!</span>
						<span class="TextTwo">Save 15% Off<span><em>Plus Free Shipping & Handling</em></span></span>
						<span class="TextThree">Order by the deadline below and Save 15% off and get Free Shipping and Handling with $25 Order!</span>
						<span class="TextFour">Start Saving Now!</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==13 ){
                    $block ='';
                    return $block;

                }else if($imageArray[0]==14 ){
                    $block ='
					<div class="bc-Fourteen" '.$bannerStyle.' >
						<span class="TextOne">Take Our Survey and</span>
						<span class="TextTwo">Get A FREE<br  />$5 Coupon</span>
						<span class="TextThree">After you receive your order we\'ll email you a 5 question survey. Just answer the question and you\'ll receive a $5 savings coupon for your next purchase. It\'s that simple!</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==15 ){
                    $block ='
					<div class="bc-Fifteen" '.$bannerStyle.'>
						<span class="TextOne">For A Limited-Time ONLY!</span>
						<span class="TextTwo">SAVE $25<span>when you spend $50</span></span>
						<span class="TextTwo Green">SAVE $50<span>when you spend $100</span></span>
						<span class="TextTwo DarkGreen" >SAVE $100<span>when you spend $200</span></span>


						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==16 ){
                    $block ='
					<div class="bc-Sixteen" '.$bannerStyle.' >
						<span class="TextOne">For A Limited-Time ONLY!</span>
						<span class="TextTwo">Save up to 75% Off<span>Select Items</span></span>
						<span class="TextThree">Take advantage of special saving on limited<br />
						qualities of some of our popular items!</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;
                }
                else if($imageArray[0]==18 ){
                    $block ='
					<div class="bc-Eighteen" '.$bannerStyle.' >
						<span class="TextOne">Special Offer</span>
						<span class="TextTwo">Save up to '.$imageArray[2].'% <span>on Selected Items</span></span>
						<span class="TextThree">Order by the deadline below and Save up to '.$imageArray[2].'%!</span>
						<span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
                    return $block;
                }
                else if($imageArray[0]==19 ){
                    /*$img = Mage::getModel('catalog/product')->loadBySku('7992'); */

                    $block =
                        '<div class="bc-Nineteen" '.$bannerStyle.' >
						<div style=" position:absolute; left:0px; top:0px;"><img src="'.$imageArray[1].'" width="320" height="320" /></div>
						<span class="TextOne">'.$imageArray[2].'</span>
						<span class="TextTwo">'.$imageArray[3].' <span>Save '.$imageArray[4].'%</span></span>
						<span class="TextFour">&nbsp;</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires<br />'.$imageArray[5].'</span>
					</div>';

                    return $block;
                }
                else if($imageArray[0]==20 ){
                    $block ='
				<div class="bc-Twenty" '.$bannerStyle.' > <span class="TextOne">'.$imageArray[1].'</span>
					<span class="TextTwo">SAVE<span>UP<br />TO</span>&nbsp;&nbsp;'.$imageArray[2].'%!</span>
					<span class="TextThree">Hurry! Offer Expires '.$imageArray[3].'</span>
					<span class="TextExpires">&nbsp;</span>
				</div>';

                    return $block;
                }
                else if($imageArray[0]==21 ){
                    $block ='
					<div class="bc-Twentyone" '.$bannerStyle.' > <span class="TextOne">Sitewide Sale!</span>
					<span class="TextTwo">Save '.$imageArray[1].'% Off<span>your total order</span></span>
					<span class="TextThree">Order by the deadline below and Save '.$imageArray[1].'% off your next order!</span>
					<span class="TextFour">&nbsp;</span>
					<span class="TextExpires"><span style="color:#fff;">Hurry!</span> Offer Expires<br /> '.$imageArray[2].'</span>
					</div>';
                    return $block;
                }
                // Not using type 22, because that is used in Myseasons, so if client want to use that in future for springhill that is reserved.
                else if($imageArray[0]==23 ){
                    $block =
                        '<div class="bc-Twentythree" '.$bannerStyle.' >
						<div style=" position:absolute; left:0px; top:0px;"><img src="'.$imageArray[1].'" width="325" height="320" /></div>
						<span class="TextOne">Take $'.$imageArray[2].' Off</span>
						<span class="TextTwo">your next order of $'.$imageArray[3].' or more.</span></span>
						<div style="text-align:center;margin-top:5px;" ><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/start-shopping-big.png"  /></div>
					</div>';

                    return $block;
                }
                else if($imageArray[0]==24 ){
                    $block =
                        '<div class="bc-Twentyfour" '.$bannerStyle.' >
						<span class="TextOne">Take $'.$imageArray[1].' Off</span>
						<span class="TextTwo">your next order of $'.$imageArray[2].' or more.</span>
						<div style="text-align:center;margin-top:5px;" ><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/start-shopping-big24.png"  /></div>
					</div>';

                    return $block;
                }
                else if($imageArray[0]==25 ){
                    $block =
                        '<div class="bc-Twentyfive" '.$bannerStyle.' >
						<img src="'.$imageArray[1].'"  border="0" style="margin-bottom:10px;" />
					</div>';

                    return $block;
                }
                else if($imageArray[0]==26 ){
                    $block =
                        '<div class="bc-Twentyfive" '.$bannerStyle.' >
						<img src="'.$imageArray[1].'"  border="0" style="margin-bottom:10px;" />
					</div>';

                    return $block;
                }

            }
            if($position =='right'){  // Banner for Right Position

                if($imageArray[0]==1){
                    /*$block ='
					<div class="bc-right-one" '.$bannerStyle.' >
						<span class="bc-right-text-one">Take $100 OFF</span>
						<span class="bc-right-text-two">your order of $200 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-right-twentyfive" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/right-bc-01.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==2 ){
                    /*$block ='
					<div class="bc-right-two" '.$bannerStyle.' >
						<span class="bc-right-text-one">Take $20 OFF</span>
						<span class="bc-right-text-two">your order of $50 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-right-twentyfive" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/right-bc-02.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==3 ){
                    /*$block ='
					<div class="bc-right-three" '.$bannerStyle.' >
						<span class="bc-right-text-one">Take $25 OFF</span>
						<span class="bc-right-text-two">your order of $50 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-right-twentyfive" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/right-bc-03.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==4){
                    /*$block ='
					<div class="bc-right-four" '.$bannerStyle.' >
						<span class="bc-right-text-one">Take $25 OFF</span>
						<span class="bc-right-text-two">your order of $75 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-right-twentyfive" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/right-bc-04.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==5 ){
                    /*$block ='
					<div class="bc-right-five" '.$bannerStyle.' >
						<span class="bc-right-text-one">Take $50 OFF</span>
						<span class="bc-right-text-two">your order of $100 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-right-twentyfive" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/right-bc-05.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==6 ){
                    /*$block ='
					<div class="bc-right-six" '.$bannerStyle.' >
						<span class="bc-right-text-one">Take $50 OFF</span>
						<span class="bc-right-text-two">your order of $200 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';*/
                    $block ='';
                    $block =
                        '<div class="bc-right-twentyfive" '.$bannerStyle.' >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/right-bc-06.jpg"  border="0" style="margin-bottom:10px;" />
					</div>';
                    return $block;

                }else if($imageArray[0]==7){
                    $block ='
					<div class="bc-right-seven" '.$bannerStyle.' >
						<span class="bc-right-text-one">1¢ SALE</span>
						<span class="bc-right-text-two">Going on NOW!</span>
						<span class="bc-right-text-four">Get your second item for only a penny!</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==8 ){
                    $block ='
					<div class="bc-right-eight" '.$bannerStyle.' >
						<span class="bc-right-text-one">FREE</span>
						<span class="bc-right-text-two">Shipping & Handling</span>
						<span class="bc-right-text-four">Limited -Time ONLY!</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==9 ){
                    $block ='
					<div class="bc-right-nine" '.$bannerStyle.' >
						<span class="bc-right-text-one">FREE</span>
						<span class="bc-right-text-two">Shipping & Handling</span>
						<span class="bc-right-text-four">On orders $25 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==10 ){
                    $block ='
					<div class="bc-right-ten" '.$bannerStyle.' >
						<span class="bc-right-text-one">FREE</span>
						<span class="bc-right-text-two">Shipping & Handling</span>
						<span class="bc-right-text-four">On orders $75 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==11 ){
                    $block ='
					<div class="bc-right-eleven" '.$bannerStyle.' >
						<span class="bc-right-text-one">Sitewide Sale!</span>
						<span class="bc-right-text-two">Save 10% Off</span>
						<span class="bc-right-text-four">your total purchase</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==12 ){
                    $block ='
					<div class="bc-right-twelve" '.$bannerStyle.' >
						<span class="bc-right-text-one">Sitewide Sale!</span>
						<span class="bc-right-text-two">Save 15% Off</span>
						<span class="bc-right-text-four">plus Free Shipping with any $25+ Order</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
                    return $block;

                }else if($imageArray[0]==13 ){
                    $block ='';
                    return $block;

                }else if($imageArray[0]==14 ){
                    $block ='
					<div class="bc-right-fourteen" '.$bannerStyle.' >
						<span class="bc-right-text-one">Take Our Survey</span>
						<span class="bc-right-text-two">Get A FREE $5 Coupon</span>
						<span class="bc-right-text-four">Make a purchase and we\'ll email survey. Answer it and you\'ll receive a $5 coupon for your next purchase.</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';

                    return $block;

                }else if($imageArray[0]==15 ){
                    /* $block ='
                     <div class="bc-right-fifteen" '.$bannerStyle.' >
                         <span class="bc-right-text-one"><span class="save">SAVE $25</span><span>when you spend $50</span></span>
                         <span class="bc-right-text-two"><span class="save">SAVE $50</span><span>when you spend $100</span></span>
                         <span class="bc-right-text-four"><span class="save">SAVE $100</span><span>when you spend $200</span></span>
                         <span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
                     </div>';*/
                    $block ='
					<div class="bc-right-fifteen" '.$bannerStyle.' >
						<span class="bc-right-text-one"><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/save-25.jpg" /></span>
						<span class="bc-right-text-two"><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/save-50.jpg" /></span>
						<span class="bc-right-text-four"><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/save-100.jpg"/></span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';

                    return $block;

                }else if($imageArray[0]==16 ){
                    $block ='
					<div class="bc-right-sixteen" '.$bannerStyle.' >
						<span class="bc-right-text-one">Save</span>
						<span class="bc-right-text-two">up to</span>
						<span class="bc-right-text-four">75<sup>%</sup></span>
						<span class="bc-right-text-five">On Select Items</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
                    return $block;
                }
                else if($imageArray[0]==18 ){
                    $block ='
					<div class="bc-right-eighteen" '.$bannerStyle.' >
						<span class="bc-right-text-one">Special Offer</span>
						<span class="bc-right-text-two">Save '.$imageArray[2].'% Off</span>
						<span class="bc-right-text-four">on selected items</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
                    return $block;
                }
                else if($imageArray[0]==19 ){
                    $block =
                        '
                     <div class="bc-right-nineteen" '.$bannerStyle.' >
                        <span class="bc-right-text-one"><i>Bumper Crop Special</i></span>
                        <span class="bc-right-text-two"><span>Save '.$imageArray[4].'%</span> on '.$imageArray[3].' </span>
						<div><img src="'.$imageArray[1].'" width="90" height="90" /></div>
						<span class="TextExpires"><span>Hurry!</span> Offer<br />Ends '.$imageArray[5].'</span>
					</div>';

                    return $block;
                }
                else if($imageArray[0]==20 ){
                    $block ='
                    <div class="bc-right-twenty" '.$bannerStyle.' > <span class="bc-right-text-one">'.$imageArray[1].'</span>
                        <span class="bc-right-text-two">SAVE<span>UP<br />TO</span></span>
                        <span class="bc-right-text-three">'.$imageArray[2].'<sup>%</sup></span>
                        <span class="TextExpires"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$imageArray[3].'</span>
                    </div>';

                    return $block;
                }

                else if($imageArray[0]==21 ){
                    $block ='
					<div class="bc-right-twentyone" '.$bannerStyle.' >
						<span class="bc-right-text-one">Sitewide Sale!</span>
						<span class="bc-right-text-two">Save '.$imageArray[1].'% Off</span>
						<span class="bc-right-text-four">your total order</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$imageArray[2].'</span>
					</div>';
                    return $block;

                }
                // Same for right side banner, 22 type is reserved for Myseasons type banner-22
                else if($imageArray[0]==23 ){
                    $block =
                        '
                     <div class="bc-right-twentythree" '.$bannerStyle.' >
                        <span class="bc-right-text-one">Take $'.$imageArray[2].' Off</span>
                        <span class="bc-right-text-two">your order of $'.$imageArray[3].' or more.</span>
						<div style="text-align:center;" ><img src="'.$imageArray[1].'" width="145" height="145" /></div>
						<div style="text-align:center;margin-top:5px;" ><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/start-shopping.png"  /></div>
					</div>';

                    return $block;
                }
                else if($imageArray[0]==24 ){
                    $block =
                        '
                     <div class="bc-right-twentyfour" '.$bannerStyle.' >
                        <span class="bc-right-text-two">Take <br /> $'.$imageArray[1].'&nbsp;Off</span>
						<span class="bc-right-text-three">your order of $'.$imageArray[2].' or more.</span>
						<div style="text-align:center;margin-top:0px;" ><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/start-shopping.png"  /></div>
					</div>';

                    return $block;
                }
                else if($imageArray[0]==25 ){
                    $block =
                        '<div class="bc-right-twentyfive" '.$bannerStyle.' >
						<img src="'.$imageArray[2].'"  border="0" style="margin-bottom:10px;" />
					</div>';

                    return $block;
                }
                else if($imageArray[0]==26 ){
                    $block =
                        '<div class="bc-right-twentyfive" '.$bannerStyle.' >
						<img src="'.$imageArray[2].'"  border="0" style="margin-bottom:10px;" />
					</div>';

                    return $block;
                }
            }
        }else{
            if($position =='home'){
                $block ='
						<div class="bc-One">
							<span class="TextOne">Limited Time Special</span>
							<span class="TextTwo">Take $20 OFF<span>your order of $50 or more</span></span>
							<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span>

							<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
						</div>';
                return $block;

            }
            if($position =='right'){
                $block = '
				<div class="bc-right-one">
					<span class="bc-right-text-one">Take $100 OFF</span>
					<span class="bc-right-text-two">your order of $200 or more</span>
					<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
				</div>';
                return $block;
            }

        }

    }
    public function deleteCoupon($ruleId){
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $write->query("DELETE from salesrule_coupon where rule_id = '".$ruleId."' ");
    }

    public function getShippingAmount(){
        $session = Mage::getSingleton('checkout/session');
        $cartBaseSubtotal = 0;
        foreach ($session->getQuote()->getAllItems() as $item) {
            $cartBaseSubtotal = $cartBaseSubtotal+ $item->getRowTotal();
        }

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $quoteid=Mage::getSingleton("checkout/session")->getQuote()->getId();

// Need this to check whether to use offer_code of 4 or other offer code
        echo $coupon_code = $write->fetchOne("select coupon_code from sales_flat_quote  where entity_id='".$quoteid."' ");

        if(!empty($coupon_code) ){

            // getting the rule id will be used for fetching offer_code
            $rule_id = $write->fetchOne("select rule_id from salesrule_coupon  where code='".$coupon_code."' ");
            $offer_code = $write->fetchOne("select offer_code from salesrule  where rule_id='".$rule_id."' ");
            if(empty($offer_code))
                $offer_code=4;

            // Getting Shipping Amount on the basis of order base total and offer code
            $shipping_amount = $write->fetchOne("select Shipping from shippingtables  where Order_Low <= ".$cartBaseSubtotal." AND Order_High >= ".$cartBaseSubtotal."  and Offer_Code='".$offer_code."' ");
        }
        else{
            //Getting Shipping Amount on the basis of order base total and offer code
            $shipping_amount = $write->fetchOne("select Shipping from shippingtables  where Order_Low <= ".$cartBaseSubtotal." AND Order_High >= ".$cartBaseSubtotal."  and Offer_Code='4' ");


        }
        return $shipping_amount;
    }

    public function addFreeItem(){

        $session= Mage::getSingleton('checkout/session');
        $_freeBanner = Mage::getModel("banner/banner")->load('63');
        $bannerStatus = $_freeBanner->getStatus();
        $name = explode("-",$_freeBanner->getName());
        $productSku = trim($name[1]);
        if($bannerStatus==1){
            $freeItemFlag = 1;
            $itemCount = count($session->getQuote()->getAllItems());
            foreach($session->getQuote()->getAllItems() as $item)
            {
                //echo $item->getSku();
                if ($item->getSku() == $productSku){
                    $freeItemFlag = 0;
                }
            }
            if($freeItemFlag && $itemCount>0){
                $cart = Mage::helper('checkout/cart')->getCart();
                $productId = Mage::getModel('catalog/product')->getIdBySku("$productSku");
                $_product = Mage::getModel('catalog/product')->load($productId);
                $cart->addProduct($_product, array('qty' => 1));
                $cart->save();
            }
        }
    }
}
