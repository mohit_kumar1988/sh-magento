<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Multishipping checkout controller
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once("app/code/core/Mage/Checkout/controllers/CartController.php");
class Springhills_Checkout_CartController extends Mage_Checkout_CartController
{

	/**
     * Adding multiple products to shopping cart action
     * based on Mage_Checkout_CartController::addAction()
     * see also http://www.magentocommerce.com/boards/viewthread/8610/
     * and http://www.magentocommerce.com/wiki/how_to_overload_a_controller
     */
    public function addmultipleAction()
    {
        $Ids = $this->getRequest()->getParam('products',false);		
		$qty = $this->getRequest()->getParam('qty',false);
		//echo $qty; die(); 
		if($qty=="" || $qty==0){
			$qty=1;
		} 
		else {
			$qty = explode(",", $qty );
		}
		
        if($Ids=="") { 
            $this->_goBack();
            return;
        }else{
			$productIds = explode(",", $Ids );
		}
		$cart = Mage::helper('checkout/cart')->getCart();
		$i=0;
		$j=0;
        foreach( $productIds as $skuId) {
            try {
                //$qty = $this->getRequest()->getParam('qty' . $productId, 0);
                if ($qty <= 0) continue; // nothing to add

                $productId = Mage::getModel('catalog/product')->getIdBySku("$skuId");
	 			$i++;
                $product = Mage::getModel('catalog/product')->load($productId); 
				$cart->addProduct($product, array('qty' => $qty[$j]));  
   				$cart->save();
                $prodName=strip_tags(htmlspecialchars_decode(stripslashes($product->getName())));
                $message = $this->__('%s was successfully added to your shopping cart.', $prodName );    
                Mage::getSingleton('checkout/session')->addSuccess($message);
		if($i==1){
			$mainProductId = $productId;
		}else{
			$offerProductId = $productId;
		}	
		$j++;
            }
            catch (Mage_Core_Exception $e) {
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice($prodName . ': ' . $e->getMessage());
                }
                else {
                    Mage::getSingleton('checkout/session')->addError($prodName . ': ' . $e->getMessage());
                }
            }
            catch (Exception $e) {
                Mage::getSingleton('checkout/session')->addException($e, $this->__('Can not add item to shopping cart'));
            }
        }
	try{
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
					 					
		/**** Update the Offer product id to main product ***/
		$checkResource =  Mage::getResourceSingleton('checkout/cart');
		$checkResource->updateOfferProduct($offerProductId,$mainProductId);
		/**** Update the Offer product id to main product ***/									
		
		
	}catch (Exception $e){
		Mage::getSingleton('checkout/session')->addError($prodName . ': ' . $e->getMessage());
	}	

		
        $this->_goBack();
    }
    /**
     * Add product to shopping cart action
     */
    public function addAction()
    {
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml(strip_tags(htmlspecialchars_decode(stripslashes($product->getName())))));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
                
    }
    
    
}

