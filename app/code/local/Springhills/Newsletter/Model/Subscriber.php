<?php
require_once 'redback/RedbackServices.php';
class  Springhills_Newsletter_Model_Subscriber extends Mage_Newsletter_Model_Subscriber
{

    public function subscribe($email)
    {
        $this->loadByEmail($email);
        $customerSession = Mage::getSingleton('customer/session');

        if(!$this->getId()) {
            $this->setSubscriberConfirmCode($this->randomSequence());
        }

        $isConfirmNeed   = (Mage::getStoreConfig(self::XML_PATH_CONFIRMATION_FLAG) == 1) ? true : false;
        $isOwnSubscribes = false;
        $ownerId = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email)
            ->getId();
        $isSubscribeOwnEmail = $customerSession->isLoggedIn() && $ownerId == $customerSession->getId();

        if (!$this->getId() || $this->getStatus() == self::STATUS_UNSUBSCRIBED
            || $this->getStatus() == self::STATUS_NOT_ACTIVE
        ) {
            if ($isConfirmNeed === true) {
                // if user subscribes own login email - confirmation is not needed
                $isOwnSubscribes = $isSubscribeOwnEmail;
                if ($isOwnSubscribes == true){
                    $this->setStatus(self::STATUS_SUBSCRIBED);
                } else {
                    $this->setStatus(self::STATUS_NOT_ACTIVE);
                }
            } else {
                $this->setStatus(self::STATUS_SUBSCRIBED);
            }
            $this->setSubscriberEmail($email);
        }

        if ($isSubscribeOwnEmail) {
            $this->setStoreId($customerSession->getCustomer()->getStoreId());
            $this->setCustomerId($customerSession->getCustomerId());
        } else {
            $this->setStoreId(Mage::app()->getStore()->getId());
            $this->setCustomerId(0);
        }

        $this->setIsStatusChanged(true);

        try {
            $this->save();

            $defaultKeycode = Mage::getSingleton('core/session')->getData('offerCode');
            //REDBACK API CALL TO SEND THE EMAIL ID
            $objRedBackApi = new RedbackServices();
            $data =array(
                'Title'	=>'4',
                'CustNumber'=>'',
                'CustFirstName'=>'',
                'CustLastName'=>'',
                'CustCompany'=>'',
                'CustAddr1'=>'',
                'CustAddr2'=>'',
                'CustCity'=>'',
                'CustState'=>'',
                'CustZip'=>'',
                'CustEmail'=>$email,
                'CustEmIP'=>'',
                'CustPhone'=>'',
                'PromoCode'=>$defaultKeycode,
                'OptOutFlag'=>'',
                'ContestCode'=>'',
                'CatErrCode'=>'',
                'CatErrMsg'=>'',
                'Bonus'=>'',
                'debug'=>'',
                'debug_id'=>'',
                'BirthMonth'=>'',
                'BirthDay'=>'',
                'ConfirmEmail'=>'',
                'IPaddress'=>$_SERVER["REMOTE_ADDR"],
                'TimeStamp'=>''
            );
            //print_r($data);die();
            $redbackStatus = $objRedBackApi->newsletterService($data);
            //If Errorcode is empty or Redback dint return false
            $iStatus=0;
            if($redbackStatus)
                $iStatus=1;
            $mysqlResource = Mage::getResourceSingleton('newsletter/subscriber');

            $mysqlResource->updateRedBackStatus($this->getId(),$iStatus,$_SERVER["REMOTE_ADDR"]);

            //END OF REDBACK API
            if ($isConfirmNeed === true
                && $isOwnSubscribes === false
            ) {
                //$this->sendConfirmationRequestEmail();
            } else {
                // $this->sendConfirmationSuccessEmail();
            }
            Mage::getSingleton('core/session')->setData('defaultkeycode','');

            return $this->getStatus();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}
