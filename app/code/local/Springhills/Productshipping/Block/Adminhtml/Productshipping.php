<?php


class Springhills_Productshipping_Block_Adminhtml_Productshipping extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

		$this->_controller = "adminhtml_productshipping";
		$this->_blockGroup = "productshipping";
		$this->_headerText = Mage::helper("productshipping")->__("Product Shipping Manager");
		$this->_addButtonLabel = Mage::helper("productshipping")->__("Add New Product Shipping");
		parent::__construct();
		//$this->removeButton('add');

	}

}
