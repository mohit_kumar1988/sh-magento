<?php
	
class Springhills_Productshipping_Block_Adminhtml_Productshipping_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "productshipping_id";
				$this->_blockGroup = "productshipping";
				$this->_controller = "adminhtml_productshipping";
				$this->_updateButton("save", "label", Mage::helper("productshipping")->__("Save Product Shipping"));
				$this->_updateButton("delete", "label", Mage::helper("productshipping")->__("Delete Product Shipping"));
                $this->removeButton('delete');
				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("productshipping")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("productshipping_data") && Mage::registry("productshipping_data")->getId() ){

				    return Mage::helper("productshipping")->__("Edit Catalog Request", $this->getFirstname());

				} 
				else{

				     return Mage::helper("productshipping")->__("Add Item");

				}
		}
}
