<?php
class Springhills_Productshipping_Block_Adminhtml_Productshipping_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("productshipping_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("productshipping")->__("Product Shipping Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("productshipping")->__("Product Shipping Information"),
				"title" => Mage::helper("productshipping")->__("Product Shipping Information"),
				"content" => $this->getLayout()->createBlock("productshipping/adminhtml_productshipping_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
