<?php
$installer = $this;
$installer->startSetup();
$installer->run("CREATE TABLE IF NOT EXISTS `shippingtables` (
  `shipping_id` int(11) NOT NULL AUTO_INCREMENT,
  `Offer_Code` varchar(255) NOT NULL,
  `Order_Low` decimal(12,4) NOT NULL,
  `Order_High` decimal(12,4) NOT NULL,
  `Shipping` varchar(255) NOT NULL,
  `Last_Update` varchar(255) NOT NULL,
  `Catalog_Type` varchar(255) NOT NULL,
  `Type` varchar(255) NOT NULL,
  `Percent` varchar(255) NOT NULL,
  PRIMARY KEY (`shipping_id`)
);
   
		");
//demo 
Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 
