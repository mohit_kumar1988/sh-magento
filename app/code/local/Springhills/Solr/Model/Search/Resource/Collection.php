<?php

class Springhills_Solr_Model_Search_Resource_Collection 
    extends Enterprise_Search_Model_Resource_Collection
{

    /**
     * Set product visibility filter for enabled products
     *
     * @param   array $visibility
     * @return  Mage_Catalog_Model_Resource_Product_Collection
     */
    public function setVisibility($visibility)
    {
        //Add solr filter: Don't get products with price == 0
        $this->addFqFilter(array('-'.Mage::getModel('enterprise_search/adapter_httpStream')->getPriceFieldName() => 0));
        
        if (is_array($visibility)) {
            $this->addFqFilter(array('visibility' => $visibility));
        }

        return $this;
    }


}
