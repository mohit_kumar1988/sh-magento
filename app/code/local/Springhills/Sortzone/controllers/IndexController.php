<?php
class Springhills_Sortzone_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function removeAction(){
        $cookieValue = $this->getRequest()->getParam('cookieValue',false);
        $pageLimit = $this->getRequest()->getParam('pageLimit',false);
        $limit='';
        if($pageLimit !=null){
            $limit = '&limit='.$pageLimit ;
        }
        if(!empty($cookieValue)){
            Mage::getModel('core/cookie')->set('zone',null,0);

            $pageAction = $this->getRequest()->getParam('currentPage',false);
            $pageUrl = $this->getRequest()->getParam('pageUrl',false);
            if($pageAction  == 'catalog_category_view'){
                if($pageUrl):
                    $redirectUrl = Mage::getBaseUrl().$pageUrl.'?order=zone0&dir=asc'.$limit;
                else:
                    $redirectUrl = Mage::getBaseUrl().$pageUrl.'?order=zone0&dir=asc'.$limit;
                endif;
            }
            if($pageAction  == 'catalogsearch_result_index'){
                $q = $this->getRequest()->getParam('ss');
                if($q!=null)
                    $redirectUrl = Mage::getBaseUrl().$pageUrl.'/?ss='.$q.'&dir=asc'.$limit;
                else
                    $redirectUrl = Mage::getBaseUrl().$pageUrl.'/?dir=asc'.$limit;
            }
            if($pageAction == 'catalogsearch_advanced_result'){
                $category = $this->getRequest()->getParam('category',false);
                $web_search = $this->getRequest()->getParam('web_search',false);
                $full = $this->getRequest()->getParam('full_sun',false);
                $shade = $this->getRequest()->getParam('shade',false);
                $partial = $this->getRequest()->getParam('partial_shade',false);

                $redirectUrl =  Mage::getBaseUrl()."catalogsearch/advanced/result/?plantfinder=yes&dir=asc".$limit;

                if($category)
                    $redirectUrl .= '&category='.$category;
                if($web_search)
                    $redirectUrl .= '&web_search_zone='.$web_search;
                if($shade)
                    $redirectUrl .= '&shade='.$shade;
                if($full)
                    $redirectUrl .= '&full_sun='.$full;
                if($partial)
                    $redirectUrl .= '&partial_shade='.$partial;
            }

            $this->_redirectUrl($redirectUrl);

        }
    }

    public function postAction()
    {
        /**** Cookie Value from Toolbar ***/
        $cookieValue = $this->getRequest()->getParam('cookieValue',false);
        $pageLimit = $this->getRequest()->getParam('pageLimit',false);
        $post = $this->getRequest()->getPost();
        if ($post || $cookieValue!='') {

            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $quertSp = '&';
                $limit='';
                if($pageLimit !=null){
                    $limit = $quertSp.'limit='.$pageLimit ;
                }
                if(empty($cookieValue)){
                    $postObject = new Varien_Object();
                    $postObject->setData($post);
                    $error = false;
                    if (!Zend_Validate::is(trim($post['sort-zip-code']) , 'NotEmpty')) {
                        $error = true;
                    }


                    /* Retrieving Post Values */
                    $data['sort-zip-code']=trim($post['sort-zip-code']);
                    $data['currpageAction']=trim($post['currpageAction']);
                    $mainUrl = explode("?", $post['currpageUrl'] );
                    $data['currpageUrl']= Mage::getBaseUrl().trim($mainUrl['0']);
                    $pageUrl = strstr($data['currpageUrl'], '?', true);

                    /* Fetch data based on Zipcode */
                    $write = Mage::getSingleton("core/resource")->getConnection("core_write");
                    $value = $write->query("SELECT * from hardiness_zone where zipcode='".$data['sort-zip-code']."' ");
                    $row = $value->fetch();



                    if($row) {
                        $related_zone = $row['zone'];
                        $actual_zone = substr_replace($related_zone,"",-1);
                        $name = 'zone';
                        $value = $related_zone;

                        /*** Redirection for different pages with params conditions ***/
                        if($data['currpageAction'] == 'catalog_category_view'):
                            Mage::getModel('core/cookie')->set('zone', $value);
                            if($pageUrl):
                                $redirectUrl = $data['currpageUrl'].$quertSp.'dir=asc'.$quertSp.'order=zone'.$actual_zone.$limit;
                            else:
                                $redirectUrl = $data['currpageUrl'].'?dir=asc&order=zone'.$actual_zone.$limit;
                                //echo $redirectUrl; exit;
                            endif;

                        elseif($data['currpageAction'] == 'catalogsearch_result_index'):
                            Mage::getModel('core/cookie')->set('zone', $value);
                            $q = $this->getRequest()->getParam('ss');
                            if($q!=null)
                                $redirectUrl = $data['currpageUrl'].'?ss='.$q.$quertSp.'dir=asc'.$quertSp.'order=zone'.$actual_zone.$limit;
                            else
                                $redirectUrl = $data['currpageUrl'].'?dir=asc'.$quertSp.'order=zone'.$actual_zone.$limit;

                        elseif($data['currpageAction'] == 'catalogsearch_advanced_result'):
                            Mage::getModel('core/cookie')->set('zone', $value);
                            $redirectUrl = $data['currpageUrl']."?dir=asc".$limit;
                            if($actual_zone)
                                $redirectUrl .= $quertSp.'order=zone'.$actual_zone;
                            if($post['category'])
                                $redirectUrl .= $quertSp.'category='.$post['category'];
                            if($post['web_search_zone'])
                                $redirectUrl .= $quertSp.'web_search_zone='.$post['web_search_zone'];
                            if(empty ($post['web_search_zone']))
                                $redirectUrl .= $quertSp.'web_search_zone='.$actual_zone;
                            if($post['plantfinder'])
                                $redirectUrl .= $quertSp.'plantfinder='.$post['plantfinder'];
                            if($post['shade'])
                                $redirectUrl .= $quertSp.'shade='.$post['shade'];
                            if($post['full_sun'])
                                $redirectUrl .= $quertSp.'full_sun='.$post['full_sun'];
                            if($post['partial_shade'])
                                $redirectUrl .= $quertSp.'partial_shade='.$post['partial_shade'];
                        endif;

                        $email = $post['hzoneemail'];
                        if($email != null && $email != 'Email Address' ){
                            Mage::getModel('core/cookie')->set('email_id', $email);
                        }

                        //echo $redirectUrl;die();
                        if(!empty($email)){
                            $redirectUrl .= '&lflg=1&hzip='.$post['sort-zip-code'];

                            $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
                            // Transactional Email Template's ID Use template id of template created for newsletter
                            $templateId = 3;
                            // Set sender information
                            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
                            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
                            // $senderName ="Amit";
                            // $senderEmail = "amits@kensium.com";
                            $sender = array('name' => $senderName,
                                'email' => $senderEmail);
                            // Set recepient information
                            $recepientEmail = $email;
                            $recepientName = $email;
                            // Get Store ID
                            $storeId = Mage::app()->getStore()->getId();
                            // Set variables that can be used in email template
                            $vars = array('customerEmail' => $email);
                            $translate = Mage::getSingleton('core/translate');

                            // Send Transactional Email
                            Mage::getModel('core/email_template')
                                ->sendTransactional($templateId, $sender, $recepientEmail, $recepientName, $vars, $storeId);
                            $translate->setTranslateInline(true);
                        }
                        $this->_redirectUrl($redirectUrl);
                    }else {
                        $redirectUrl = Mage::getBaseUrl().$post['currpageUrl'];
                        Mage::getSingleton('core/session')->addError('Zone is not available for the entered zip code');
                        $this->_redirectUrl($redirectUrl);
                    }
                }else{
                    /**** If Toolbar cookie exists *****/
                    if(!empty($cookieValue)){

                        $pageAction = $this->getRequest()->getParam('currentPage',false);
                        $pageUrl = $this->getRequest()->getParam('pageUrl',false);
                        $actual_zone = substr_replace($cookieValue,"",-1);
                        if($pageAction  == 'catalog_category_view'){
                            Mage::getModel('core/cookie')->set('zone', $cookieValue);
                            if($pageUrl):
                                $redirectUrl = Mage::getBaseUrl().$pageUrl.'?dir=asc'.$quertSp.'order=zone'.$actual_zone.$limit;
                            else:
                                $redirectUrl = Mage::getBaseUrl().$pageUrl.'?dir=asc'.$quertSp.'order=zone'.$actual_zone.$limit;
                            endif;
                        }
                        if($pageAction  == 'catalogsearch_result_index'){
                            Mage::getModel('core/cookie')->set('zone', $cookieValue);
                            $q = $this->getRequest()->getParam('ss');
                            if($q!=null)
                                $redirectUrl = Mage::getBaseUrl().$pageUrl.'/result/?ss='.$q.$quertSp.'dir=asc'.$quertSp.'order=zone'.$actual_zone.$limit;
                            else
                                $redirectUrl = Mage::getBaseUrl().$pageUrl.'/result/?dir=asc'.$quertSp.'order=zone'.$actual_zone.$limit;
                        }
                        if($pageAction == 'catalogsearch_advanced_result'){
                            Mage::getModel('core/cookie')->set('zone', $cookieValue);

                            $category = $this->getRequest()->getParam('category',false);
                            $web_search = $this->getRequest()->getParam('web_search',false);
                            $full = $this->getRequest()->getParam('full_sun',false);
                            $shade = $this->getRequest()->getParam('shade',false);
                            $partial = $this->getRequest()->getParam('partial_shade',false);

                            $redirectUrl =  Mage::getBaseUrl()."catalogsearch/advanced/result/?plantfinder=yes".$quertSp."dir=asc".$limit;

                            if($actual_zone)
                                $redirectUrl .= $quertSp.'order=zone'.$actual_zone;
                            if($category)
                                $redirectUrl .= $quertSp.'category='.$category;
                            if($web_search)
                                $redirectUrl .= $quertSp.'web_search_zone='.$actual_zone;
                            if($shade)
                                $redirectUrl .= $quertSp.'shade='.$shade;
                            if($full)
                                $redirectUrl .= $quertSp.'full_sun='.$full;
                            if($partial)
                                $redirectUrl .= $quertSp.'partial_shade='.$partial;
                        }
                        //echo $web_search.$redirectUrl;die;
                        $this->_redirectUrl($redirectUrl);

                    }
                }

            } catch (Exception $e) {
                $translate->setTranslateInline(true);
                $this->_redirect('*/*/');
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
    }
}
?>