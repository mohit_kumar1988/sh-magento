<?php

//$log_array = array();
//require_once '../app/Mage.php';
//umask(0);
//Mage::app('default');
//include 'web_conf.php';

function inventoryFeedCron($start=0) {
    $log_array = array();
    $argv[1] = $start;
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

    $InventoryStatus = $write->fetchAll("SELECT status FROM import_notification_history 
    WHERE import_type='inventory' or import_type='price' or import_type='catalogOffers' or import_type='productDescription'  or  import_type='deleteKeycodes' or import_type='deletePrice' or import_type='deleteShipping' ");

    if ($InventoryStatus[0]['status'] != "inProgress" &&
            $InventoryStatus[1]['status'] != "inProgress" &&
            $InventoryStatus[2]['status'] != "inProgress" &&
            $InventoryStatus[3]['status'] != "inProgress" && $InventoryStatus[4]['status'] !="inProgress" && $InventoryStatus[5]['status'] !="inProgress" && $InventoryStatus[6]['status'] !="inProgress") {
//Mail to be sent for
        //$arrTo[] = array("toEmail" => "amitblues123@gmail.com", "toName" => "imsupport");
        $arrTo[] = array("toEmail" => "mohansaip@kensium.com", "toName" => "Janesh");
//This URL IS USED FOR BATCH PROCESSING

        $CronURL = '../dailyfeed/InventoryFeedUpdateCron.php';

//Semaphore Flag
        $txtFlagFile = '/home/springhill/UploadComplete.txt';

//Directory path where import files are located
        $dir_import = "/home/springhill/";

//Directory Path where Import files to be moved after successful import
        $dir_Archive = $GLOBALS["global_dir_Archive"];

        $txFileName = "InventoryFeed.txt";

        $timestamp = date("YmdHis") . "_";

        $txtArchiveInventoryFeed = $dir_Archive . $timestamp . $txFileName;

        $tableName = "inventoryfeed_temp";

        $txtInventoryFeedFile = $dir_import . $txFileName;

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $InventoryId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='inventoryFeed' and status='notStarted'");

//Checking flag file and then proceeding for Inventory Update
        if (file_exists($txtFlagFile) && file_exists($txtInventoryFeedFile) && ($argv[1] == 0 && !empty($InventoryId) || ($argv[1] != 0 && empty($InventoryId)))) {

//Saving the lastupdated time in database for 36 hours email notification
            $InventoryFeedId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='inventoryFeed'");
            if (empty($InventoryFeedId)) {
                $write->query("INSERT INTO import_notification_history(import_type,status) VALUES('inventoryFeed','inProgress')");
            } else {
                $write->query("UPDATE import_notification_history SET status='inProgress',result_count='" . $argv[1] . "' WHERE id=" . $InventoryFeedId);
            }

            //Get the limit to be imported by default 100        
            /*
             * Update Inventory values with Product info
             */
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

            $totalResults = $connection->select()
                    ->from('inventoryfeed_temp as i', array('count(*) as count'))
                    ->join("catalog_product_entity AS cpe", "cpe.sku = i.Product_Number", array());
            $totalResultsCount = $connection->fetchAll($totalResults);


            if (file_exists($txtInventoryFeedFile) && $totalResultsCount[0]['count'] == 0) {
                try {
                    $write->query("LOAD DATA LOCAL INFILE '" . $txtInventoryFeedFile . "' INTO TABLE " . $tableName . "
                FIELDS TERMINATED BY '|' ENCLOSED BY '' 
                LINES TERMINATED BY '\n' STARTING BY ''");
                    $write->exec("call spSkuNotFoundInventoryFeed()");
                    $write->exec("call spCompareInventoryFeed()");
                    $write->query("delete FROM inventoryfeed_temp where Available='Available'");
                    $write->query("UPDATE import_notification_history SET start_date=NOW(),email_notification='No',result_count='" . $argv[1] . "' WHERE id=" . $InventoryFeedId);
                } catch (Exception $e) {
                    $log_array[0]['Message'] = $e->getMessage();
                    $log_array[0]['Trace'] = $e->getTraceAsString();
                    $log_array[0]['Info'] = "Something went wrong while importing from " . $txFileName . " file.";
                }
            }

            $limit = 100;
//            $start = 0;
//            //Get the Start Index for Import
//            if (!empty($argv[1])) {
//                $start = $argv[1];
//            } else {
//                $start = 0;
//            }
            $totalResults1 = $connection->select()
                    ->from('inventoryfeed_temp as i', array('count(*) as count'))
                    ->join("catalog_product_entity AS cpe", "cpe.sku = i.Product_Number", array());
            //->where('i.Product_Number IN (16329,16386,16394,16444,16543,16550,16626,17947,20107,21493,21501)');
            $totalResultsCount1 = $connection->fetchAll($totalResults1);

            if ($start < $totalResultsCount1[0]['count']) {
                try {
                    $inFeedSelect = "";
                    if ($start == 0) {
                        $inFeedSelect = $connection->select()
                                ->from('inventoryfeed_temp as i', array('i.Product_Number', 'i.Active', 'i.Obsolete',
                                    'i.Available', 'i.Sellable_Quantity', 'i.Season', 'i.Cross_Sell_Items',
                                    'i.Future_Season_Indicator', 'i.Future_Season_Message', 'i.Recommended_Items'))
                                ->join("catalog_product_entity AS cpe", "cpe.sku = i.Product_Number", array())
                                //->where('i.Product_Number IN (16329,16386,16394,16444,16543,16550,16626,17947,20107,21493,21501)')
                                ->order("i.Product_Number")
                                ->limit($limit);
                    } else {
                        $inFeedSelect = $connection->select()
                                ->from('inventoryfeed_temp as i', array('i.Product_Number', 'i.Active', 'i.Obsolete',
                                    'i.Available', 'i.Sellable_Quantity', 'i.Season', 'i.Cross_Sell_Items',
                                    'i.Future_Season_Indicator', 'i.Future_Season_Message', 'i.Recommended_Items'))
                                ->join("catalog_product_entity AS cpe", "cpe.sku = i.Product_Number", array())
                                //->where('i.Product_Number IN (16329,16386,16394,16444,16543,16550,16626,17947,20107,21493,21501)')
                                ->order("i.Product_Number")
                                ->limit($limit, $start);
                    }

                    $inRowsArray = $connection->fetchAll($inFeedSelect);
                } catch (Exception $e) {
                    $log_array[1]['Message'] = $e->getMessage();
                    $log_array[1]['Trace'] = $e->getTraceAsString();
                    $log_array[1]['Info'] = "Something went wrong while importing from " . $tableName . " table.";
                }
                $error_inc = 2;
                $res_cnt = count($inRowsArray);
                $inc_val = 0;
                foreach ($inRowsArray as $infKey => $infValue) {
                    $sku = $infValue['Product_Number'];
                    // $_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                    $product_id = Mage::getModel('catalog/product')->getIdBySku("$sku");
                    $_Pdetails = Mage::getModel('catalog/product')->load($product_id);

                    $offersku = $sku . "_VSPMAAC";
                    //$_Pdetails_offer = Mage::getModel('catalog/product')->loadByAttribute('sku', $offersku);

                    $offer_product_id = Mage::getModel('catalog/product')->getIdBySku("$offersku");
                    $_Pdetails_offer = Mage::getModel('catalog/product')->load($offer_product_id);


                    if (!empty($_Pdetails['sku'])) {
                        //$product_id = $_Pdetails->getId();

                        /* $prod_details = Mage::getModel('catalog/product');
                          $prod_details->load($product_id); */

                        /* if (!empty($_Pdetails_offer)) {
                          $offer_product_id = $_Pdetails_offer->getId();
                          $offer_productdataVal = Mage::getModel('catalog/product')->load($offer_product_id);
                          //$offer_UomQty = $offer_productdataVal->getUnitOfMeasure();
                          } */



                        $cross_data = explode(" ", $infValue['Cross_Sell_Items']);
                        $cross_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
                        $param = array();

                        try {
                            //$prod_details->setCrossSellLinkData();
//                        $prod_details->setCrossSellLinkData($param);
//                        $prod_details->save();
//                        if ($_Pdetails_offer && $offer_product_id) {
//                            $offer_productdataVal->setCrossSellLinkData($param);
//                            $offer_productdataVal->save();
//                        }
                            $total_crossel = $write->fetchAll("select cpl.link_id FROM catalog_product_link as cpl, catalog_product_link_attribute_int as cplai 
    WHERE cpl.link_id=cplai.link_id  and cpl.link_type_id = 5 and cpl.product_id=" . $product_id);
                            $list = array();
                            if (count($total_crossel) > 0) {
                                foreach ($total_crossel as $name => $id) {
                                    $list[] = $id['link_id'];
                                }
                                $select_list = implode(",", $list);
                                if (!empty($select_list)) {
                                    $write->query("DELETE FROM catalog_product_link WHERE link_type_id = 5 and product_id = $product_id");
                                    $write->query("DELETE FROM catalog_product_link_attribute_int WHERE link_id in (" . $select_list . ")");
                                    if ($_Pdetails_offer && $offer_product_id) {
                                        $write->query("DELETE FROM catalog_product_link WHERE link_type_id = 5 and product_id = $offer_product_id");
                                        $write->query("DELETE FROM catalog_product_link_attribute_int WHERE link_id in (" . $select_list . ")");
                                    }
                                }
                            }
                        } catch (Exception $e) {
                            $log_array[$error_inc]['Message'] = $e->getMessage();
                            $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc]['Info'] = "Something went wrong while updating Product Cross-sell data SKU: " . $sku . " and Product Id:" . $product_id;
                        }
                        $inc = 1;
                        foreach ($cross_data as $k => $v) {
                            $pDetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $v);
                            $pid = '';
                            if (!empty($pDetails['sku'])) {
                                $pid = $pDetails->getId();
                                try {
                                    $param[$pid] = array('position' => $inc);
                                    $inc++;
                                } catch (Exception $e) {
                                    $log_array[$error_inc]['Message'] = $e->getMessage();
                                    $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                                    $log_array[$error_inc]['Info'] = "Something went wrong while updating Product Cross-sell data SKU: " . $sku . " and Product Id:" . $product_id;
                                }
                            }
                        }
                        //print "\n--->SKU: " . $sku . " and Product Id:" . $product_id;
//                    print_r($param);
                        //$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                        try {
                            $flag = 0;
                            $flag1 = 0;
                            if ($_Pdetails) {
                                $status = ($infValue['Obsolete'] == "N") ? 1 : 2;
//                            $prod_details->setStatus("");
//                            $prod_details->setSeason("");
//                            $prod_details->setRecommendedItems("");
//                            $prod_details->setFutureSeasonIndicator("");
//                            $prod_details->setSellableQuantity("");
//                            $prod_details->setFutureSeasonMessage("");
//                            $prod_details->save();

                                if ($_Pdetails->getStatus() != $status) {
                                   // $_Pdetails->setStatus($status);
                                    //$flag = 1;
                                }if ($_Pdetails->getSeason() != $infValue['Season']) {
                                    $_Pdetails->setSeason($infValue['Season']);
                                    $flag = 1;
                                }if ($_Pdetails->getRecommendedItems() != $infValue['Recommended_Items']) {
                                    $_Pdetails->setRecommendedItems($infValue['Recommended_Items']);
                                    $flag = 1;
                                }if ($_Pdetails->getFutureSeasonIndicator() != $infValue['Future_Season_Indicator']) {
                                    $_Pdetails->setFutureSeasonIndicator($infValue['Future_Season_Indicator']);
                                    $flag = 1;
                                }if ($_Pdetails->getSellableQuantity() != $infValue['Sellable_Quantity']) {
                                    $_Pdetails->setSellableQuantity($infValue['Sellable_Quantity']);
                                    $flag = 1;
                                }if ($_Pdetails->getFutureSeasonMessage() != $infValue['Future_Season_Message']) {
                                    //$_Pdetails->setFutureSeasonMessage($infValue['Future_Season_Message']);
                                    //$flag = 1;
                                }
                                $webId = array(1);
                                if (!empty($param)) {
                                    $_Pdetails->setCrossSellLinkData($param);
                                    $flag = 1;
                                }
                                if ($flag == 1) {
                                    $_Pdetails->setWebsiteIds($webId);
                                    $_Pdetails->save();
                                }
                            }

                            if ($_Pdetails_offer && $offer_product_id) {
                                $status = ($infValue['Obsolete'] == "N") ? 1 : 2;
//                            $offer_productdataVal->setStatus("");
//                            $offer_productdataVal->setSeason("");
//                            $offer_productdataVal->setRecommendedItems("");
//                            $offer_productdataVal->setFutureSeasonIndicator("");
//                            $offer_productdataVal->setSellableQuantity("");
//                            $offer_productdataVal->setFutureSeasonMessage("");
//                            $offer_productdataVal->save();

                                if ($_Pdetails_offer->getStatus() != $status) {
                                    //$_Pdetails_offer->setStatus($status);
                                    //$flag1 = 1;
                                }if ($_Pdetails_offer->getSeason() != $infValue['Season']) {
                                    $_Pdetails_offer->setSeason($infValue['Season']);
                                    $flag1 = 1;
                                }if ($_Pdetails_offer->getRecommendedItems() != $infValue['Recommended_Items']) {
                                    $_Pdetails_offer->setRecommendedItems($infValue['Recommended_Items']);
                                    $flag1 = 1;
                                }if ($_Pdetails_offer->getFutureSeasonIndicator() != $infValue['Future_Season_Indicator']) {
                                    $_Pdetails_offer->setFutureSeasonIndicator($infValue['Future_Season_Indicator']);
                                    $flag1 = 1;
                                }if ($_Pdetails_offer->getSellableQuantity() != $infValue['Sellable_Quantity']) {
                                    $_Pdetails_offer->setSellableQuantity($infValue['Sellable_Quantity']);
                                    $flag1 = 1;
                                }if ($_Pdetails_offer->getFutureSeasonMessage() != $infValue['Future_Season_Message']) {
                                    //$_Pdetails_offer->setFutureSeasonMessage($infValue['Future_Season_Message']);
                                    //$flag1 = 1;
                                }
                                if (!empty($param)) {
                                    if ($_Pdetails_offer && $offer_product_id) {
                                        $_Pdetails_offer->setCrossSellLinkData($param);
                                        $flag1 = 1;
                                    }
                                }
                                if ($flag1 == 1) {
                                    $webId = array(1);
                                    $_Pdetails_offer->setWebsiteIds($webId);
                                    $_Pdetails_offer->save();
                                }
                            }
                        } catch (Exception $e) {
                            echo "Exception 2: " . $product_id . "==>" . $e . "\n";

                            $log_array[$error_inc]['Message'] = $e->getMessage();
                            $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc]['Info'] = "Something went wrong while updating Product data SKU: " . $sku . " and Product Id:" . $product_id;
                        }

                        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_id);
                        if ($_Pdetails_offer && $offer_product_id) {
                            $offer_stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($offer_product_id);
                        }
                        $is_stock = "";
                        $is_stock = ($infValue['Available'] > 0) ? '1' : '0';
                        try {
                            $flag2 = 0;
                            $flag3 = 0;
                            
                            if ($stockItem->getManageStock() != 1) {
                              //  $stockItem->setManageStock(1);
                               // $flag2 = 1;
                            }
                           
                            //$productdata = Mage::getModel('catalog/product')->load($product_id);
                            if ($_Pdetails->getUnitOfMeasure() > 1) {
                                if ($stockItem->getMinSaleQty() != $_Pdetails->getUnitOfMeasure()) {
                                    $stockItem->setMinSaleQty($_Pdetails->getUnitOfMeasure());
                                    $flag2 = 1;
                                }if ($stockItem->getUseConfigMinSaleQty() != 0) {
                                    $stockItem->setUseConfigMinSaleQty("0");
                                    $flag2 = 1;
                                }
                            }
                            $stockItem->setQty(0);
                            if ($stockItem->getQty() != $infValue['Available'] && !empty($infValue['Available'])) {
                                $stockItem->setQty($infValue['Available']);
                                $flag2 = 1;
                            }
                            if ($stockItem->getIsInStock() != $is_stock) {
                                $stockItem->setIsInStock($is_stock);
                                $flag2 = 1;
                            }
                            if ($flag2 == 1) {
                                $stockItem->save();
                            }

                            if ($_Pdetails_offer && $offer_product_id) {
                                if ($offer_stockItem->getManageStock() != 0) {
                                 //   $offer_stockItem->setManageStock(0);
                                   // $flag3 = 1;
                                }
                                /* if ($offer_stockItem->getManageStock() != 1) {
                                  $offer_stockItem->setManageStock("1");
                                  $flag3 = 1;
                                  } */
                                //$productdata = Mage::getModel('catalog/product')->load($product_id);
                                if ($_Pdetails_offer->getUnitOfMeasure() > 1) {
                                    if ($offer_stockItem->getMinSaleQty() != $_Pdetails_offer->getUnitOfMeasure()) {
                                        $offer_stockItem->setMinSaleQty($_Pdetails_offer->getUnitOfMeasure());
                                        $flag3 = 1;
                                    }if ($offer_stockItem->getUseConfigMinSaleQty() != 0) {
                                        $offer_stockItem->setUseConfigMinSaleQty("0");
                                        $flag3 = 1;
                                    }
                                }
                                $offer_stockItem->setQty(0);
                                if ($offer_stockItem->getQty() != $infValue['Available'] && !empty($infValue['Available'])) {
                                    $offer_stockItem->setQty($infValue['Available']);
                                    $flag3 = 1;
                                }
                                if ($offer_stockItem->getIsInStock() != $is_stock) {
                                    $offer_stockItem->setIsInStock($is_stock);
                                    $flag3 = 1;
                                }
                                if ($flag3 == 1) {
                                    $offer_stockItem->save();
                                }
                            }
                            $dbc = Mage::getSingleton('core/resource')->getConnection('core_write');
                            $resource = Mage::getSingleton('core/resource');
                            $table = $resource->getTableName('catalog/product') . '_tier_price';

                            $dbc->query("DELETE FROM $table WHERE all_groups=1 and entity_id = $product_id");
                            if ($_Pdetails_offer && $offer_product_id) {
                                $dbc->query("DELETE FROM $table WHERE all_groups=1 and entity_id = $offer_product_id");
                            }
                        } catch (Exception $e) {
//                    echo "Exception 3: " . $product_id . "==>" . $e . "\n";
                            $log_array[$error_inc]['Message'] = $e->getMessage();
                            $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc]['Info'] = "Something went wrong while updating stock data SKU: " . $sku . " and Product Id:" . $product_id;
                        }
                    }
                    print $inc_val . "===>" . $sku . "\n";
                    $error_inc++;
                    $inc_val++;
                }
                $start = $start + $limit;
                //exec('php InventoryFeedUpdateCron.php ' . $start . ' ' . $limit);
                inventoryFeedCron($start);
            } else {
                $error_inc1 = $res_cnt + 2;
                try {
                    if (copy($txtInventoryFeedFile, $txtArchiveInventoryFeed)) {
                        unlink($txtInventoryFeedFile);
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1]['Message'] = $e->getMessage();
                    $log_array[$error_inc1]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1]['Info'] = "Something went wrong while archive the " . $tableName . " file.";
                }

                try {
                    $produ_desc_file = $dir_import . "ProductDescription.txt";
                    $inventory_file = $dir_import . "Inventory.txt";
                    $inventory_feed_file = $dir_import . "InventoryFeed.txt";
                    $price_file = $dir_import . "Price.txt";
                    $catalog_offer_file = $dir_import . "CatalogOffers.txt";
                    $keycode_file = $dir_import . "KeyCodes.txt";
                    $shipping_table_file = $dir_import . "ShippingTables.txt";
                    $delete_Price = $dir_import . "DeletePrice.txt";
                    $delete_keycodes = $dir_import . "DeleteKeyCodes.txt";
                    $delete_Shipping = $dir_import . "DeleteShippingDetails.txt";

                    if (!file_exists($produ_desc_file) &&
                            !file_exists($inventory_file) &&
                            !file_exists($inventory_feed_file) &&
                            !file_exists($price_file) &&
                            !file_exists($catalog_offer_file) &&
                            !file_exists($keycode_file) &&
                            !file_exists($shipping_table_file) &&
                            !file_exists($delete_Price)&&
                            !file_exists($delete_keycodes)&&
                            !file_exists($delete_Shipping)) {
                        unlink($txtFlagFile);
                        /* for ($i = 1; $i < 9; $i++) {
                          $process = Mage::getModel('index/process')->load($i);
                          $process->reindexAll();
                          } */
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 1]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 1]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 1]['Info'] = "Something went wrong while removing flag file.";
                }
                try {
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->delete($tableName);
                    $write->query("UPDATE import_notification_history SET status='notStarted', end_date=NOW(),result_count='" . $argv[1] . "' WHERE import_type='inventoryFeed' and status='inProgress'");
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 2]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 2]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 2]['Info'] = "Something went wrong while deleting the records from " . $tableName . " table.";
                }
            }
            //DELETE THE ARCHIVE FILES OLDER THAN 30 Days
            if ($handle = opendir($dir_Archive)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $arrFiles = explode("_", $entry);

                        $fileTimeStamp = $arrFiles[0];
                        $date1 = date('YmdHis');
                        $date2 = $fileTimeStamp;

                        $ts1 = strtotime($date1);
                        $ts2 = strtotime($date2);
                        $seconds_diff = $ts1 - $ts2;
                        $days = floor($seconds_diff / 3600 / 24);
                        try {
                            //Delete the files older than 30 days
                            if ($days > 30) {
                                unlink($dir_Archive . $entry);
                            }
                        } catch (Exception $e) {
                            $log_array[$error_inc1 + 3]['Message'] = $e->getMessage();
                            $log_array[$error_inc1 + 3]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc1 + 3]['Info'] = "Something went wrong while deleting 30 days old archive files.";
                        }
                    }
                }
                closedir($handle);
            }
            //ERROR LOG
            //ERROR LOG
            //print_r($log_array);exit;
            if (count($log_array) > 0) {
                $errorMsg = '';
                foreach ($log_array as $log) {
                    $errorMsg .= '
					- - - - - - - -
					ERROR
                                        SUBJECT: ' . $log['Info'] . '
					Message: ' . $log['Message'] . '
					Trace: ' . $log['Trace'] . '					
					Time: ' . date('Y m d H:i:s') . '
					';
                    //echo "\r\n" . $errorMsg;
                }
                file_put_contents($dir_Archive . '/logs/log_' . time() . '.txt', $errorMsg);
                //Getting admin Information
                $adminInfo = $write->fetchAll("SELECT * from admin_user");
                $userFirstname = $adminInfo[0]['firstname'];
                $userLastname = $adminInfo[0]['lastname'];
                $userEmail = $adminInfo[0]['email'];
                //$arrTo[] = array("toEmail" => $userEmail, "toName" => $userFirstname . ' ' . $userLastname);

                $mail = new Zend_Mail();
                $mail->setType(Zend_Mime::MULTIPART_RELATED);
                $mail->setBodyHtml('There were errors updating the Inventoryfeed for Spring Hill. Please review the log files at: ' . $_SERVER['REMOTE_ADDR'] . '/dailyfeed/archive/logs/log_' . time() . '.txt');
                //$mail->setFrom($userEmail, $userFirstname . ' ' . $userLastname);
                //SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
                /* foreach ($arrTo as $to) {
                  $mail->addTo($to['toEmail'], $to['toName']);
                  } */
                $mail->setFrom('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('sudhakark@kensium.com', 'Sudhakar');
                $mail->setSubject('Error updating SH product database');
//                $fileContents = file_get_contents($dir_Archive . '/logs/log_' . time() . '.txt');
//                $file = $mail->createAttachment($fileContents);
//                $file->filename = "log_" . time() . ".txt";
                try {
                    if ($mail->send()) {
                        Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
                    }
                } catch (Exception $ex) {
                    Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
                }
            }
        }
        return TRUE;
    } else {
        return FALSE;
    }
}

?>
