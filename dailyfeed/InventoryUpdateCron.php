<?php

//$log_array = array();
//require_once '../app/Mage.php';
//umask(0);
//Mage::app('default');
//include 'web_conf.php';
function inventoryCron($start=0) {
    $log_array = array();
    $argv[1] = $start;
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

    $InventoryStatus = $write->fetchAll("SELECT status FROM import_notification_history 
    WHERE import_type='inventoryFeed' or import_type='price' or import_type='catalogOffers' or import_type='productDescription' or  import_type='deleteKeycodes' or import_type='deletePrice' or import_type='deleteShipping' ");

    if ($InventoryStatus[0]['status'] != "inProgress" &&
            $InventoryStatus[1]['status'] != "inProgress" &&
            $InventoryStatus[2]['status'] != "inProgress" &&
            $InventoryStatus[3]['status'] != "inProgress"  && $InventoryStatus[4]['status'] !="inProgress" && $InventoryStatus[5]['status'] !="inProgress" && $InventoryStatus[6]['status'] !="inProgress") {

//Mail to be sent for
        //$arrTo[] = array("toEmail" => "amitblues123@gmail.com", "toName" => "imsupport");
        $arrTo[] = array("toEmail" => "mohansaip@kensium.com", "toName" => "Janesh");

//This URL IS USED FOR BATCH PROCESSING
        $CronURL = '../dailyfeed/InventoryUpdateCron.php';

//Semaphore Flag
        $txtFlagFile = '/home/springhill/UploadComplete.txt';

//Directory path where import files are located
        $dir_import = "/home/springhill/";

//Directory Path where Import files to be moved after successful import
        $dir_Archive = $GLOBALS["global_dir_Archive"];

        $timestamp = date("YmdHis") . "_";

        $txtArchiveInventory = $dir_Archive . $timestamp . "Inventory.txt";


//Directory path for Inventory.txt file
        $txtInventoryFile = $dir_import . "Inventory.txt";

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $InventoryId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='inventory' and status='notStarted'");

// && empty($InventoryId)
//Checking flag file and then proceeding for Inventory Update

        if (file_exists($txtFlagFile) && file_exists($txtInventoryFile) && ($argv[1] == 0 && !empty($InventoryId) || ($argv[1] != 0 && empty($InventoryId)))) {

            //Saving the lastupdated time in database for 36 hours email notification
            $InventoryDataId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='inventory'");
            if (empty($InventoryDataId)) {
                $write->query("INSERT INTO import_notification_history(import_type,status) VALUES('inventory','inProgress')");
            } else {
                $write->query("UPDATE import_notification_history SET status='inProgress',result_count='" . $argv[1] . "' WHERE id=" . $InventoryDataId);
            }


            //Get the limit to be imported by default 100
//            $start = 0;
//             //2600;
//            //Get the Start Index for Import
//            if (!empty($argv[1]))
//                $start = $argv[1];
//            else
//                $start = 0;

            $limit = 100;
            /*
             * Update inventory values with Product info
             */
            $connection = Mage::getSingleton('core/resource')
                    ->getConnection('core_read');

            $totalResults = $connection->select()
                    ->from('inventory_temp as i', array('count(*) as count'))
                    ->join("catalog_product_entity AS cpe", "cpe.sku = i.Item_Num", array());
            $totalResultsCount = $connection->fetchAll($totalResults);

            if (file_exists($txtInventoryFile) && $totalResultsCount[0]['count'] == 0) {
                try {
                    $write->query("LOAD DATA LOCAL INFILE '" . $txtInventoryFile . "' INTO TABLE inventory_temp
FIELDS TERMINATED BY '|' ENCLOSED BY '' 
LINES TERMINATED BY '\n' STARTING BY ''");
                    $write->exec("call spSkuNotFoundInventory()");
                    $write->exec("call spCompareInventory()");
                    $write->query("DELETE FROM inventory_temp where Item_Num='Item_Num'");
                    $write->query("UPDATE import_notification_history SET start_date=NOW(),email_notification='No',result_count='" . $argv[1] . "' WHERE id=" . $InventoryDataId);
                } catch (Exception $e) {
                    $log_array[0]['Message'] = $e->getMessage();
                    $log_array[0]['Trace'] = $e->getTraceAsString();
                    $log_array[0]['Info'] = "Something went wrong while importing from Inventory.txt file.";
                }
            }

            $totalResults1 = $connection->select()
                    ->from('inventory_temp as i', array('count(*) as count'))
                    ->join("catalog_product_entity AS cpe", "cpe.sku = i.Item_Num", array());
            $totalResultsCount1 = $connection->fetchAll($totalResults1);


            if (count($totalResultsCount1) > 0 && $start < $totalResultsCount1[0]['count']) {

                try {
                    $inSelect = "";
                    if ($start == 0) {
                        $inSelect = $connection->select()
                                ->from('inventory_temp as i', array('i.Item_Num', 'i.Unit_Of_Measurement', 'i.Restricted_States', 'i.Botanical'))
                                ->join("catalog_product_entity AS cpe", "cpe.sku = i.Item_Num", array())
                                ->order("i.Item_Num")
                                ->limit($limit);
                    } else {
                        $inSelect = $connection->select()
                                ->from('inventory_temp as i', array('i.Item_Num', 'i.Unit_Of_Measurement', 'i.Restricted_States', 'i.Botanical'))
                                ->join("catalog_product_entity AS cpe", "cpe.sku = i.Item_Num", array())
                                ->order("i.Item_Num")
                                ->limit($limit, $start);
                    }
                    $inRowsArray = $connection->fetchAll($inSelect);
                } catch (Exception $e) {
                    $log_array[1]['Message'] = $e->getMessage();
                    $log_array[1]['Trace'] = $e->getTraceAsString();
                    $log_array[1]['Info'] = "Something went wrong while importing from Inventory table.";
                }
                $i = 1;

                $error_inc = 2;
                $inc_val = 0;
                $res_cnt = count($inRowsArray);

                foreach ($inRowsArray as $key => $value) {
                    $sku = $value['Item_Num'];
                    //$_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                    $product_id = Mage::getModel('catalog/product')->getIdBySku("$sku");
                    $offer_sku = $sku."_VSPMAAC";
                    $offer_product_id = Mage::getModel('catalog/product')->getIdBySku("$offer_sku");
                    $_Pdetails = Mage::getModel('catalog/product')->load($product_id);
                    if($offer_product_id){
                        $_OfferPdetails = Mage::getModel('catalog/product')->load($offer_product_id);
                    }
                    if (!empty($_Pdetails['sku'])) {
                        //$product_id = $_Pdetails->getId();
                        try {
                            $flag = 0;
                            /* $prod_details = Mage::getModel('catalog/product');
                              $prod_details->load($product_id); */
                            if ($_Pdetails->getUnitOfMeasure() != $value['Unit_Of_Measurement']) {
                                $_Pdetails->setUnitOfMeasure($value['Unit_Of_Measurement']);
                                $flag = 1;
                            }
                            if ($_Pdetails->getRestrictedStates() != $value['Restricted_States']) {
                                $_Pdetails->setRestrictedStates($value['Restricted_States']);
                                $flag = 1;
                            }
                            if ($_Pdetails->getBotanicalName() != addslashes($value['Botanical'])) {
                                $_Pdetails->setBotanicalName(addslashes($value['Botanical']));
                                $flag = 1;
                            }
                            if($offer_product_id){
                                $_OfferPdetails->setUnitOfMeasure($value['Unit_Of_Measurement']);
                                $_OfferPdetails->setRestrictedStates($value['Restricted_States']);
                                $_OfferPdetails->setBotanicalName($value['Botanical']);
                                $flag = 1;
                            }
                            $webId = array(1);

                            if ($flag == 1){
                                $_Pdetails->setWebsiteIds($webId);
                                $_Pdetails->save();
                            }
                            if($offer_product_id && $flag){
                                $_OfferPdetails->setWebsiteIds($webId);
                                $_OfferPdetails->save();
                            }

                        } catch (Exception $e) {
                            $log_array[$error_inc]['Message'] = $e->getMessage();
                            $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc]['sku'] = $sku;
                            $log_array[$error_inc]['Info'] = "Something went wrong while updating product information.";
                        }
                    }
                    print $inc_val . "===>" . $sku . "\n";
                    $i++;
                    $error_inc++;
                    $inc_val++;
                }

                $start = $start + $limit;
                //exec('php InventoryUpdateCron.php ' . $start . ' ' . $limit);
                inventoryCron($start);
            } else {
                $error_inc1 = $res_cnt + 2;
                try {
                    if (copy($txtInventoryFile, $txtArchiveInventory)) {
                        unlink($txtInventoryFile);
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1]['Message'] = $e->getMessage();
                    $log_array[$error_inc1]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1]['Info'] = "Something went wrong while archive the Inventory file.";
                }

                try {
                    $produ_desc_file = $dir_import . "ProductDescription.txt";
                    $inventory_file = $dir_import . "Inventory.txt";
                    $inventory_feed_file = $dir_import . "InventoryFeed.txt";
                    $price_file = $dir_import . "Price.txt";
                    $catalog_offer_file = $dir_import . "CatalogOffers.txt";
                    $keycode_file = $dir_import . "KeyCodes.txt";
                    $shipping_table_file = $dir_import . "ShippingTables.txt";
                    $delete_Price = $dir_import . "DeletePrice.txt";
                    $delete_keycodes = $dir_import . "DeleteKeyCodes.txt";
                    $delete_Shipping = $dir_import . "DeleteShippingDetails.txt";

                    if (!file_exists($produ_desc_file) &&
                            !file_exists($inventory_file) &&
                            !file_exists($inventory_feed_file) &&
                            !file_exists($price_file) &&
                            !file_exists($catalog_offer_file) &&
                            !file_exists($keycode_file) &&
                            !file_exists($shipping_table_file) &&
                            !file_exists($delete_Price)&&
                            !file_exists($delete_keycodes)&&
                            !file_exists($delete_Shipping)) {
                        unlink($txtFlagFile);
                        /* for ($i = 1; $i < 9; $i++) {
                          $process = Mage::getModel('index/process')->load($i);
                          $process->reindexAll();
                          } */
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 1]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 1]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 1]['Info'] = "Something went wrong while removing flag file.";
                }
                try {
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->delete("inventory_temp");

                    $write->query("UPDATE import_notification_history SET status='notStarted',result_count='" . $argv[1] . "',end_date=NOW() WHERE import_type='inventory' and status='inProgress'");
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 2]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 2]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 2]['Info'] = "Something went wrong while deleting the records from Inventory table.";
                }
            }
            //echo "i am in 196\n";
            //DELETE THE ARCHIVE FILES OLDER THAN 30 Days
            if ($handle = opendir($dir_Archive)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $arrFiles = explode("_", $entry);

                        $fileTimeStamp = $arrFiles[0];
                        $date1 = date('YmdHis');
                        $date2 = $fileTimeStamp;

                        $ts1 = strtotime($date1);
                        $ts2 = strtotime($date2);
                        $seconds_diff = $ts1 - $ts2;
                        $days = floor($seconds_diff / 3600 / 24);
                        try {
                            //Delete the files older than 30 days
                            if ($days > 30) {
                                unlink($dir_Archive . $entry);
                            }
                        } catch (Exception $e) {
                            $log_array[$error_inc1 + 3]['Message'] = $e->getMessage();
                            $log_array[$error_inc1 + 3]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc1 + 3]['Info'] = "Something went wrong while deleting 30 days old archive files.";
                        }
                    }
                }
                closedir($handle);
            }
            //ERROR LOG
            //echo "i am in 226\n";
            if (count($log_array) > 0) {
                $errorMsg = '';
                foreach ($log_array as $log) {
                    $errorMsg .= '
					- - - - - - - -
					ERROR
                                        SUBJECT: ' . $log['Info'] . '
					Message: ' . $log['Message'] . '
					Trace: ' . $log['Trace'] . '
					ProductId: ' . $log['sku'] . '
					Time: ' . date('Y m d H:i:s') . '
					';
                    echo "\r\n" . $errorMsg;
                }
                file_put_contents($dir_Archive . '/logs/log_' . time() . '.txt', $errorMsg);
                //Getting admin Information
                $adminInfo = $write->fetchAll("SELECT * from admin_user");
                $userFirstname = $adminInfo[0]['firstname'];
                $userLastname = $adminInfo[0]['lastname'];
                $userEmail = $adminInfo[0]['email'];
                //$arrTo[] = array("toEmail" => $userEmail, "toName" => $userFirstname . ' ' . $userLastname);

                $mail = new Zend_Mail();
                $mail->setType(Zend_Mime::MULTIPART_RELATED);
                $mail->setBodyHtml('There were errors updating the Inventory for Spring Hill. Please review the log files at: ' . $_SERVER['REMOTE_ADDR'] . '/dailyfeed/archive/logs/log_' . time() . '.txt');
                //$mail->setFrom($userEmail, $userFirstname . ' ' . $userLastname);
                //SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
                /* foreach ($arrTo as $to) {
                  $mail->addTo($to['toEmail'], $to['toName']);
                  } */
                $mail->setFrom('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('mohansaip@kensium.com', 'Mohan sai');
                $mail->addTo('sudhakark@kensium.com', 'Sudhakar');
                $mail->setSubject('Error updating SH product database');
//            $fileContents = file_get_contents($dir_Archive . '/logs/log_' . time() . '.txt');
//            $file = $mail->createAttachment($fileContents);
//            $file->filename = "log_" . time() . ".txt";
                try {
                    if ($mail->send()) {
                        Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
                    }
                } catch (Exception $ex) {
                    Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
                }
            }
        }
        //print "completed";
        return TRUE;
    } else {
        return FALSE;
    }
}

?>
