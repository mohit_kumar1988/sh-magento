<?php

//$log_array = array();
//require_once '../app/Mage.php';
//umask(0);
//Mage::app('default');



function productDescriptionCron($start=0) {


    $log_array = array();
    $argv[1] = $start;
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

    $ProductDescStatus = $write->fetchAll("SELECT status FROM import_notification_history 
    WHERE import_type='inventoryFeed' or import_type='price' or import_type='catalogOffers' or import_type='inventory' or  import_type='deleteKeycodes' or import_type='deletePrice' or import_type='deleteShipping' ");


    if ($ProductDescStatus[0]['status'] != "inProgress" &&
        $ProductDescStatus[1]['status'] != "inProgress" &&
        $ProductDescStatus[2]['status'] != "inProgress" &&
        $ProductDescStatus[3]['status'] != "inProgress" &&
        $ProductDescStatus[4]['status'] !="inProgress" && $ProductDescStatus[5]['status'] !="inProgress" && $ProductDescStatus[6]['status'] !="inProgress") {

        //Mail to be sent for
        //$arrTo[] = array("toEmail" => "amitblues123@gmail.com", "toName" => "imsupport");
        $arrTo[] = array("toEmail" => "sudhakark@kensium.com", "toName" => "Sudhakar");

        //This URL IS USED FOR BATCH PROCESSING
        $CronURL = '../dailyfeed/ProductDescriptionUpdateCron.php';

        //Semaphore Flag
        $txtFlagFile = '/home/springhill/UploadComplete.txt';

        //Directory path where import files are located
        $dir_import = "/home/springhill/";

        //Directory Path where Import files to be moved after successful import
        $dir_Archive = $GLOBALS["global_dir_Archive"];

        $timestamp = date("YmdHis") . "_";

        $txtArchiveProductDesc = $dir_Archive . $timestamp . "ProductDescription.txt";


        //Directory path for Inventory.txt file
        $txtProductDescFile = $dir_import . "ProductDescription.txt";

        $ProductDescId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='productDescription' and status='notStarted'");


        //Checking flag file and then proceeding for Inventory Update
        //print $argv[1]."===>".$ProductDescId;exit;
        if (file_exists($txtFlagFile) && file_exists($txtProductDescFile) && ($argv[1] == 0 && !empty($ProductDescId) || ($argv[1] != 0 && empty($ProductDescId)))) {

            //Saving the lastupdated time in database for 36 hours email notification
            $ProductDescDataId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='productDescription'");
            if (empty($ProductDescDataId)) {
                $write->query("INSERT INTO import_notification_history(import_type,status) VALUES('productDescription','inProgress')");
            } else {
                $write->query("UPDATE import_notification_history SET status='inProgress',result_count='" . $argv[1] . "' WHERE id=" . $ProductDescDataId);
            }


            //Get the limit to be imported by default 100
            $limit = 100;

            $write = Mage::getSingleton('core/resource')->getConnection('core_write');

            //$info = $write->fetchAll("SELECT * FROM product_description_temp where item_number != '' order by item_number");
            //$info = $write->fetchAll("SELECT * FROM product_description_temp WHERE Item_Number !=0 LIMIT 100");
            $totalResultsCount = $write->fetchAll("SELECT count(*) as count FROM product_description_temp WHERE Item_Number!=0 and item_number != '' ORDER BY Item_Number ASC");
            if (file_exists($txtProductDescFile) && $totalResultsCount[0]['count'] == 0) {
                try {

                    $write1 = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write1->delete("product_description_temp");

                    /***Modified for compare file with previous table***/

                    $write1->delete("product_description_temp1");
                    $write->query("LOAD DATA LOCAL INFILE '" . $txtProductDescFile . "' INTO TABLE product_description_temp1
FIELDS TERMINATED BY '|' ENCLOSED BY '' 
LINES TERMINATED BY '\n' STARTING BY ''");
                    $write->exec("call compareTables('product_description_temp1','product_description_temp2')");
                    $write1->delete("product_description_insert_temp");

                    /******END OF MODIFICATION*******/

                    $write->query("delete FROM product_description_temp where Item_Number='Item Number'");
                    $write->query("UPDATE import_notification_history SET start_date=NOW(),email_notification='No',result_count='" . $argv[1] . "' WHERE id=" . $ProductDescId);
                } catch (Exception $e) {
                    $log_array[0]['Message'] = $e->getMessage();
                    $log_array[0]['Trace'] = $e->getTraceAsString();
                    $log_array[0]['Info'] = "Something went wrong while importing from ProductDescription.txt file.";
                }
            }

            $totalResultsCount1 = $write->fetchAll("SELECT count(*) as count FROM product_description_temp WHERE Item_Number!=0 and item_number != '' ORDER BY Item_Number ASC");
            if (count($totalResultsCount1) > 0 && $start < $totalResultsCount1[0]['count']) {
                try {
                    $inSelect = "";
                    if ($start == 0) {
                        //echo "SELECT * FROM product_description_temp WHERE Item_Number!=0 and item_number != '' ORDER BY Item_Number ASC LIMIT " . $limit . "\n";
                        $info = $write->fetchAll("SELECT * FROM product_description_temp WHERE Item_Number!=0 and item_number != '' ORDER BY Item_Number ASC LIMIT " . $limit);
                    } else {
                        //echo "SELECT * FROM product_description_temp WHERE Item_Number!=0 and item_number != '' ORDER BY Item_Number ASC LIMIT " . $start . "," . $limit . "\n";
                        $info = $write->fetchAll("SELECT * FROM product_description_temp WHERE Item_Number!=0 and item_number != '' ORDER BY Item_Number ASC LIMIT " . $start . "," . $limit);
                    }
                } catch (Exception $e) {
                    $log_array[1]['Message'] = $e->getMessage();
                    $log_array[1]['Trace'] = $e->getTraceAsString();
                    $log_array[1]['Info'] = "Something went wrong while importing from product_description_temp table.";
                }
                $i = 1;

                $error_inc = 2;
                $inc_val = 0;
                $res_cnt = count($info);

                //$info = $write->fetchAll("SELECT * FROM product_description_temp WHERE Item_Number!=0 and item_number != '' ORDER BY Item_Number ASC");      
                $store = "admin";
                $websites = array(1);
                $has_options = 0;
                $page_layout = 'No Layout updates';
                $options_container = 'container2';
                $msrp = '';
                $msrp_enabled = '2';
                $msrp_display_actual_price_type = '4';
                $is_returnable = '2';
                $is_recurring = '0';
                $visibility = '4';
                $enable_googlecheckout = 1;
                $tax_class_id = '2';
                $meta_keyword = '';
                $meta_title = "";
                $weight = '1.0';

                $recommendation_url = '';
                $a = 0;
                $i = 0;
                $sku_array = array(73182, 73183, 64844, 64850, 76506, 79989);

                foreach ($info as $row) {
                    $product_skus = explode(",", $row['Item_Number']);

                    if (count($product_skus) > 0) {
                        $config = 1;
                        foreach ($product_skus as $sku) {
                            try {
                                if ($config == 0) {
                                    $config_sku = $sku;
                                    $type = 'configurable';
                                }
                                else
                                    $type = 'simple';

                                if (!empty($sku)) {
                                    try {
                                        $sku_val = trim($sku);
                                        $product_id = Mage::getModel('catalog/product')->getIdBySku($sku_val); //$sku;21501
                                        $flag = 0;
                                        $_Pdetails = Mage::getModel('catalog/product')->load($product_id);

                                        if ($_Pdetails->getBotanicalName() != htmlspecialchars(addslashes($row['Registered_or_Botanical_Name']))) {
                                            $_Pdetails->setBotanicalName(htmlspecialchars(addslashes($row['Registered_or_Botanical_Name'])));
                                            $flag = 1;
                                        }
                                        if ($_Pdetails->getHardinessZones() != addslashes($row['hardiness_zone'])) {
                                            $_Pdetails->setHardinessZones(addslashes($row['hardiness_zone']));
                                            $flag = 1;
                                        }
                                        if ($_Pdetails->getCommonName() != addslashes($row['COMMON_NAME'])) {
                                            $_Pdetails->setCommonName(addslashes($row['COMMON_NAME']));
                                            $flag = 1;
                                        }
                                        if ($_Pdetails->getBreckName() != addslashes($row['Spring_Hill_Breck_Name'])) {
                                            $_Pdetails->setBreckName(addslashes($row['Spring_Hill_Breck_Name']));
                                            $flag = 1;
                                        }
                                        if ($_Pdetails->getPartialShade() != $row['PARTIAL_SHADE']) {
                                            $_Pdetails->setPartialShade($row['PARTIAL_SHADE']);
                                            $flag = 1;
                                        }

                                        if ($_Pdetails->getFullSun() != $row['FULL_SUN']) {
                                            $_Pdetails->setFullSun($row['FULL_SUN']);
                                            $flag = 1;
                                        }
                                        if ($_Pdetails->getShade() != $row['SHADE']) {
                                            $_Pdetails->setShade($row['SHADE']);
                                            $flag = 1;
                                        }

                                        if ($_Pdetails->getWebSearchZone() == "" && $row['Web_Search_Zone'] == "") {
                                            for ($j = 1; $j <= 13; $j++) {
                                                $value = 14;
                                                $attribute_code = "zone" . $j;
                                                $attribute = $_Pdetails->getResource()->getAttribute($attribute_code);
                                                $source = $attribute->getSource();
                                                $optionId = getSourceOptionId($source, $value);
                                                $_Pdetails->setData($attribute_code, $optionId)->save();
                                            }
                                        } else {
                                            $zone_data = "";

                                            if (($_Pdetails->getWebSearchZone() != $row['Web_Search_Zone']) && $row['Web_Search_Zone'] != "")
                                                $zone_data = $row['Web_Search_Zone'];
                                            else
                                                $zone_data = $_Pdetails->getWebSearchZone();

                                            $totalZones = 13;
                                            $zoneArray = explode(",", $zone_data);
                                            $zoneMinValue = min($zoneArray);
                                            $zoneMaxValue = max($zoneArray);
                                            if (count($zoneArray) > 0 && $zone_data != "") {
                                                $k = 1;
                                                for ($j = 1; $j <= 13; $j++) {
                                                    if ($j > $zoneMaxValue && $j <= 13) {
                                                        $attribute_code = "zone" . $j; //
                                                        $value = ($j - $zoneMaxValue) + 1;
                                                        $attribute = $_Pdetails->getResource()->getAttribute($attribute_code);
                                                        $source = $attribute->getSource();
                                                        $optionId = getSourceOptionId($source, $value);
                                                        $_Pdetails->setData($attribute_code, $optionId);
                                                    } else if ($j < $zoneMinValue) {
                                                        $value = ($totalZones - $zoneMaxValue) + 1 + $k;
                                                        $attribute_code = "zone" . $j; //
                                                        $attribute = $_Pdetails->getResource()->getAttribute($attribute_code);
                                                        $source = $attribute->getSource();
                                                        $optionId = getSourceOptionId($source, $value);
                                                        $_Pdetails->setData($attribute_code, $optionId);
                                                        $k++;
                                                    } else {
                                                        $value = 1;
                                                        $attribute_code = "zone" . $j; //
                                                        $attribute = $_Pdetails->getResource()->getAttribute($attribute_code);
                                                        $source = $attribute->getSource();
                                                        $optionId = getSourceOptionId($source, $value);
                                                        $_Pdetails->setData($attribute_code, $optionId);
                                                    }
                                                }
                                            }
                                        }

                                        //For the name of the product
                                        $name = "";
                                        $breck_name = addslashes($row['Spring_Hill_Breck_Name']);
                                        $common_name = addslashes($row['COMMON_NAME']);
                                        $botanical_name = htmlspecialchars(addslashes($row['Registered_or_Botanical_Name']));

                                        if (!empty($breck_name)) {
                                            $name = $breck_name;
                                        } else if (!empty($common_name)) {
                                            $name = $common_name; //$start value is less than the number of rows in the product description else show common_name;
                                        } else if (!empty($botanical_name)) {
                                            $name = $botanical_name;
                                        }

                                        if ($_Pdetails->getName() != $name) {
                                            //$_Pdetails->setName($name);
                                            //$flag = 1;
                                        }

                                        /**** Update All Attributes to Product Details attribute ***/

                                        $botanical_name = $_Pdetails->getBotanicalName();
                                        $form = $row['FORM'];
                                        $sun_exposure = $_Pdetails->getSunExposure();
                                        $height_habit  = addslashes($row['Height_Habit']);
                                        $spread = addslashes($row['SPREAD']);
                                        $spacing  = addslashes($row['Spacing']);
                                        $hardiness_zones = addslashes($row['hardiness_zone']);
                                        $foliage_type  = addslashes($row['TYPE_OF_FOLIAGE']);
                                        $flower_form  = addslashes($row['Flower_Form']);
                                        $flower_color  = addslashes($row['FLOWER_COLOR']);
                                        $flowering_date = addslashes($row['FLOWERING_DATE']);
                                        $planting_requirements  = $row['Unique'];
                                        $soil_requirements  = addslashes($row['SOIL_REQUIREMENTS']);
                                        $growth_rate = addslashes($row['GROWTH_RATE']);
                                        $unique_characteristics  = addslashes($row['UNIQUE_CHARACTERISTICS']);
                                        $additional_information = $row['ADDITIONAL_INFORMATION'];
                                        $shipped  = addslashes($row['Shipped']);
                                        $restricted_states = trim($_Pdetails->getRestrictedStates());
                                        $common_name = addslashes($row['COMMON_NAME']);
                                        $breck_name = addslashes($row['Spring_Hill_Breck_Name']);
                                        $display_hardiness_zone = addslashes($row['hardiness_zone']);
                                        $container = $row['CONTAINER'];
                                        $discontinued = $_Pdetails->getDiscontinued();
                                        $borders = $row['BORDERS'];
                                        $deer_resistant = $row['Deer_Resistant'];
                                        $dried_flowers = $row['DRIED_FLOWERS'];
                                        $cut_flowers = $row['CUT_FLOWERS'];
                                        $shade  = $row['SHADE'];
                                        $partial_shade  = $row['PARTIAL_SHADE'];
                                        $indoors_only  = $row['Indoors_only'];
                                        $io_in_warm_temp = $row['Indoors_Outdoors_in_warm_temperatures'];
                                        $ground_covers = $row['GROUND_COVERS'];
                                        $rock_gardens = $row['ROCK_GARDENS'];
                                        $full_sun  = $row['FULL_SUN'];
                                        $multiplies_annually  = $row['Multiplies_Annually'];
                                        $season_shipped_spring = $row['Season_Shipped_Spring'];
                                        $season_shipped_fall  = $row['Season_Shipped_Fall'];
                                        $permantly_planted_garden  = $row['Can_be_permantly_planted_in_garden'];
                                        $good_for_forcing  = $row['Good_for_Forcing'];
                                        $good_for_naturalizing = $row['Good_for_Naturalizing'];
                                        $long_lasting_cut = $row['Long-Lasting_cut'];
                                        $long_lasting_in_garden  = $row['Long_Lasting_in_Garden'];
                                        $blooms_width  = addslashes($row['BLOOMS_WITH']);
                                        $average_number_blooms = addslashes($row['Average_Number_of_Blooms']);
                                        $awards  = $row['Awards'];
                                        $family  = addslashes($row['Family']);
                                        $duration   = addslashes($row['DURATION']);
                                        $light   = $row['Light_Needed_Indoors'];
                                        $bloom_time   = $_Pdetails->getBloomTime();
                                        $pruning   = $row['PRUNING'];
                                        $time_of_pruning  = $row['TIME_OF_PRUNING'];
                                        $special_care   = $row['Special_Care'];
                                        $special_care_instructions   = addslashes($row['Special_care_or_planting_instructions']);
                                        $type_of_fruit   = addslashes($row['TYPE_OF_FRUIT']);
                                        $web_search_flower_color   = addslashes($row['Web_Search_Flower_Color']);
                                        $web_search_zone   = $row['Web_Search_Zone'];
                                        $web_height_search    = addslashes($row['Web_Height_Search']);
                                        $web_flowering_time_search  = addslashes($row['Web_Flowering_Time_Search']);
                                        $light_needed_indoors    = $row['Light_Needed_Indoors'];
                                        $key_difference_similar_bulbs    = $row['Key_difference_from_similar_bulbs'];
                                        $root_system    = addslashes($row['ROOT_SYSTEM']);
                                        $resistance  = $row['RESISTANCE'];
                                        $foliage   = addslashes($row['TYPE_OF_FOLIAGE']);
                                        $growth_rate     = addslashes($row['GROWTH_RATE']);
                                        $good_for_other       = addslashes($row['Good_For_Other']);
                                        $fragrance      = addslashes($row['FRAGRANCE']);
                                        $usage     = $_Pdetails->getUsage();
                                        $season      = $_Pdetails->getSeason();
                                        $shipping_schedule     = $_Pdetails->getShippingSchedule();
                                        $planting_time      = addslashes($row['Planting_Time']);
                                        $watering_requirements     = $row['Watering_Requirements'];
                                        $temperature_requirements     = $row['Temperature_Requirements'];
                                        $fertilization_requirements      = $row['Fertilization_Requirements'];

                                        $_ProductDetails='';

                                        if ($botanical_name){
                                            $_ProductDetails .= '<p><strong>Botanical Name: </strong>'.stripslashes($botanical_name).'</p>';
                                        }

                                        if ($breck_name){
                                            $_ProductDetails .= '<p><strong>Spring Hill Name: </strong>'.stripslashes($breck_name).'</p>';
                                        }

                                        if ($common_name){
                                            $_ProductDetails .= '<p><strong>Common Name: </strong>'.stripslashes($common_name).'</p>';
                                        }

                                        if ($family){
                                            $_ProductDetails .= '<p><strong>Family: </strong>'.stripslashes($family).'</p>';
                                        }

                                        if ($form){
                                            $_ProductDetails .= '<p><strong>Form: </strong>'.stripslashes($form).'</p>';
                                        }

                                        if ($root_system){
                                            $_ProductDetails .= '<p><strong>Root System: </strong>'.stripslashes($root_system).'</p>';
                                        }

                                        if ($sun_exposure ){
                                            $_ProductDetails .= '<p><strong>Sun Exposure : </strong>';

                                            if($sun_exposure  == 1) {
                                                $_ProductDetails .='Yes';
                                            } else {
                                                $_ProductDetails .= 'No';
                                            }
                                            $_ProductDetails .= '</p>';
                                        }
                                        if ($light){
                                            $_ProductDetails .='<p><strong>Light: </strong>'.stripslashes($light).'</p>';
                                        }

                                        if($full_sun || $partial_shade){
                                            $_ProductDetails .='<p><strong>Sun Exposure: </strong>';
                                            if($full_sun && $partial_shade):
                                                $_ProductDetails .='Partial Shade/Full Sun';
                                            elseif($full_sun):
                                                $_ProductDetails .='Full Sun';
                                            elseif($partial_shade):
                                                $_ProductDetails .='Partial Shade';
                                            endif;
                                            $_ProductDetails .='</p>';
                                        }

                                        if ($shade){
                                            $_ProductDetails .='<p><strong>Shade : </strong>';
                                            if($shade == 1)
                                                $_ProductDetails .='Yes';
                                            else
                                                $_ProductDetails .='No';
                                            $_ProductDetails .='</p>';
                                        }


                                        if ($height_habit){
                                            $_ProductDetails .='<p><strong>Height/Habit: </strong>'.stripslashes($height_habit).'</p>';
                                        }

                                        if ($spread){
                                            $_ProductDetails .='<p><strong>Spread: </strong>'.stripslashes($spread).'</p>';
                                        }

                                        if ($spacing): $_ProductDetails .='<p><strong>Spacing: </strong>'.stripslashes($spacing).'</p>'; endif;

                                        if ($growth_rate): $_ProductDetails .='<p><strong>Growth Rate: </strong>'.stripslashes($growth_rate).'</p>'; endif;

                                        if ($hardiness_zones): $_ProductDetails .='<p><strong>Hardiness Zones: </strong>'.stripslashes($hardiness_zones).'</p>'; endif;

                                        if ($permantly_planted_garden):
                                            $_ProductDetails .='<p><strong>Can be permantly planted in garden: </strong>';
                                            if($permantly_planted_garden  == 1) { $_ProductDetails .='Yes'; } else { $_ProductDetails .='No'; }
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($indoors_only): $_ProductDetails .='<p><strong>Indoors only : </strong>';
                                            if($indoors_only == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($io_in_warm_temp): $_ProductDetails .='<p><strong>Indoors/Outdoors in warm temperatures : </strong>';
                                            if($io_in_warm_temp == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($light_needed_indoors):  $_ProductDetails .='<p><strong>Light Needed Indoors: </strong>'.stripslashes($light_needed_indoors).'</p>'; endif;

                                        if ($planting_time): $_ProductDetails .='<p><strong>Planting Time: </strong>'.stripslashes($planting_time).'</p>'; endif;

                                        if ($bloom_time): $_ProductDetails .='<p><strong>Bloom Time: </strong>'.stripslashes($bloom_time).'</p>'; endif;

                                        if ($flowering_date): $_ProductDetails .='<p><strong>Flowering Date: </strong>'.stripslashes($flowering_date).'</p>'; endif;

                                        if ($duration): $_ProductDetails .='<p><strong>Duration: </strong>'.stripslashes($duration).'</p>'; endif;


                                        if ($blooms_width): $_ProductDetails .='<p><strong>Blooms With: </strong>'.stripslashes($blooms_width).'</p>'; endif;

                                        if ($flower_color): $_ProductDetails .='<p><strong>Flower Color: </strong>'.stripslashes($flower_color).'</p>'; endif;

                                        if ($flower_form): $_ProductDetails .='<p><strong>Flower Form: </strong>'.stripslashes($flower_form).'</p>'; endif;

                                        if ($average_number_blooms): $_ProductDetails .='<p><strong>Average Number of Blooms: </strong>'.stripslashes($average_number_blooms).'</p>'; endif;

                                        if ($foliage): $_ProductDetails .='<p><strong>Foliage: </strong>'.stripslashes($foliage).'</p>'; endif;

                                        if ($foliage_type): $_ProductDetails .='<p><strong>Foliage Type: </strong>'.stripslashes($foliage_type).'</p>'; endif;


                                        if ($type_of_fruit): $_ProductDetails .='<p><strong>Type Of Fruit: </strong>'.stripslashes($type_of_fruit).'</p>'; endif;

                                        if ($usage): $_ProductDetails .='<p><strong>Usage: </strong>'.stripslashes($usage).'</p>'; endif;

                                        if ($ground_covers): $_ProductDetails .='<p><strong>Ground Covers: </strong>';
                                            if($ground_covers == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No'; }
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($borders): $_ProductDetails .='<p><strong>Borders : </strong>';
                                            if($borders == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($rock_gardens): $_ProductDetails .='<p><strong>Rock Gardens : </strong>';
                                            if($rock_gardens  == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($container): $_ProductDetails .='<p><strong>Container: </strong>';
                                            if($container == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No'; }
                                            $_ProductDetails .='</p>';
                                        endif;


                                        if ($cut_flowers): $_ProductDetails .='<p><strong>Cut Flowers : </strong>';
                                            if($cut_flowers == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($dried_flowers): $_ProductDetails .='<p><strong>Dried Flowers : </strong>';
                                            if($dried_flowers == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($good_for_naturalizing): $_ProductDetails .='<p><strong>Good For Naturalizing: </strong>';
                                            if($good_for_naturalizing == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;


                                        if ($good_for_forcing): $_ProductDetails .='<p><strong>Good For Forcing: </strong>';
                                            if($good_for_forcing == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;


                                        if ($good_for_other): $_ProductDetails .='<p><strong>Good For Other: </strong>'.stripslashes($good_for_other).'</p>'; endif;

                                        if ($fragrance): $_ProductDetails .='<p><strong>Fragrance: </strong>'.stripslashes($fragrance).'</p>'; endif;

                                        if ($resistance): $_ProductDetails .='<p><strong>Resistance: </strong>'.stripslashes($resistance).'</p>'; endif;

                                        if ($multiplies_annually): $_ProductDetails .='<p><strong>Multiplies Annually: </strong>';
                                            if($multiplies_annually == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No'; }
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($long_lasting_in_garden): $_ProductDetails .='<p><strong>Long Lasting In Garden: </strong>';
                                            if($long_lasting_in_garden == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;


                                        if ($long_lasting_cut): $_ProductDetails .='<p><strong>Long Lasting Cut: </strong>';
                                            if($long_lasting_cut == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';} $_ProductDetails .='</p>';
                                        endif;

                                        if ($planting_requirements): $_ProductDetails .='<p><strong>Planting Requirements: </strong>'.stripslashes($planting_requirements).'</p>'; endif;

                                        if ($soil_requirements): $_ProductDetails .='<p><strong>Soil Requirements: </strong>'.stripslashes($soil_requirements).'</p>'; endif;

                                        if ($fertilization_requirements): $_ProductDetails .='<p><strong>Fertilization Requirements: </strong>'.stripslashes($fertilization_requirements).'</p>'; endif;

                                        if ($watering_requirements): $_ProductDetails .='<p><strong>Watering Requirements: </strong>'.stripslashes($watering_requirements).'</p>'; endif;


                                        if ($temperature_requirements): $_ProductDetails .='<p><strong>Temperature Requirements: </strong>'.stripslashes($temperature_requirements).'</p>'; endif;

                                        // if ($winter_requirement): $_ProductDetails .='<p><strong>Winter Requirements: </strong>'.stripslashes($winter_requirement).'</p>'; endif;

                                        if ($special_care_instructions): $_ProductDetails .='<p><strong>Special care or planting instructions: </strong>'.stripslashes($special_care_instructions).'</p>'; endif;

                                        if ($special_care): $_ProductDetails .='<p><strong>Special Care: </strong>'.stripslashes($special_care).'</p>'; endif;

                                        if ($pruning): $_ProductDetails .='<p><strong>Pruning: </strong>'.stripslashes($pruning).'</p>'; endif;

                                        if ($time_of_pruning): $_ProductDetails .='<p><strong>Time Of Pruning: </strong>'.stripslashes($time_of_pruning).'</p>'; endif;

                                        // if ($two_unique_characteristic): $_ProductDetails .='<p><strong>2unique Characteristics: </strong>'.stripslashes($two_unique_characteristic).'</p>'; endif;

                                        if ($additional_information): $_ProductDetails .='<p><strong>Additional Information: </strong>'.stripslashes($additional_information).'</p>'; endif;

                                        if ($shipped): $_ProductDetails .='<p><strong>Shipped: </strong>'.stripslashes($shipped).'</p>'; endif;
                                        $_ProductDetails .='<p> <strong> Shipping: </strong> <a href="'.Mage::getBaseUrl().'"shipping-schedule">View Shipping Schedule</a>';
                                        if($season_shipped_fall || $season_shipped_spring):
                                            $_ProductDetails .='<br>';
                                            if($season_shipped_fall == '1' && $season_shipped_spring == '1'):
                                                $_ProductDetails .='This item ships in both Spring and Fall';
                                            elseif($season_shipped_fall == '1' && $season_shipped_spring == '0'):
                                                $_ProductDetails .='This item ships in Fall';
                                            elseif($season_shipped_fall == '0' && $season_shipped_spring == '1'):
                                                $_ProductDetails .='This item ships in Spring';
                                            endif;
                                        endif;
                                        $_ProductDetails .='</p>';

                                        if (trim($restricted_states)): $_ProductDetails .='<p><strong>Restricted States: </strong>'.stripslashes($restricted_states).'</p>'; endif;

                                        if ($awards): $_ProductDetails .='<p><strong>Awards: </strong>'.stripslashes($awards).'</p>'; endif;


                                        if ($web_flowering_time_search && 0): $_ProductDetails .='<p><strong>Web Flowering Time Search: </strong>'.stripslashes($web_flowering_time_search).'</p>'; endif;

                                        if ($web_height_search && 0): $_ProductDetails .='<p><strong>Web Height Search: </strong>'.stripslashes($web_height_search).'</p>'; endif;

                                        if ($web_search_flower_color && 0): $_ProductDetails .='<p><strong>Web Search Flower Color: </strong>'.stripslashes($web_search_flower_color).'</p>'; endif;

                                        /*if ($lifetime_guarantee): $_ProductDetails .='<p><strong>Lifetime Guarantee : </strong>';
                                            if($lifetime_guarantee == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No' ;}
                                            $_ProductDetails .='</p>';
                                        endif;*/

                                        if ($discontinued): $_ProductDetails .='<p><strong>Discontinued/Obsolete : </strong>';
                                            if($discontinued == 1) { $_ProductDetails .='Yes'; }
                                            else { $_ProductDetails .='No';}
                                            $_ProductDetails .='</p>';
                                        endif;

                                        if ($sun_exposure): $_ProductDetails .='<p><strong>Sun Exposure: </strong>'.stripslashes($sun_exposure).'</p>'; endif;

                                        if ($key_difference_similar_bulbs): $_ProductDetails .='<p><strong>Key difference from similar bulbs: </strong>'.stripslashes($key_difference_similar_bulbs).'</p>'; endif;

                                        if ($shipping_schedule): $_ProductDetails .='<p><strong>Shipping Schedule: </strong>'.stripslashes($shipping_schedule).'</p>'; endif;

                                        $_ProductDetails .='<span itemprop="brand" style="display:none;">Springhill</span>';
                                        if(!empty($strCategories)) {
                                            $_ProductDetails .='<span itemprop="category" style="display:none;">'.$strCategories.'</span>';
                                        }
                                        $_ProductDetails .='<div style="float:left; clear:left;">';
                                        if ($unique_characteristics):
                                            $_ProductDetails .='<p><strong>Unique Characteristics: </strong>'.stripslashes($unique_characteristics).'</p>';
                                        endif;
                                        $_ProductDetails .='</div>';
                                        
                                        if ($_ProductDetails) {
                                            $_Pdetails->setProductDetails($_ProductDetails);
                                            $flag = 1;
                                        } 
                                        /**** Update All Attributes to Product Details attribute ***/ 
                                        //$_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                                        if (!empty($product_id)) {
                                            try {
                                                print "flag---->" . $flag;
                                                if ($flag == 1) {
                                                    $_Pdetails->save();
                                                }
                                            } catch (Exception $e) {
                                                echo 'Exception1---->: ' . $e->getMessage() . "\n";
                                            }
                                        } else {

                                            $_Pdetails->setSku($sku_val);
                                            $nameWithOutSpecialChars = '';
                                            if (!empty($name)) {
                                                $_Pdetails->setName($name);
                                                $nameWithOutSpecialChars = preg_replace("/[^[:alnum:]]/", "-", $name);
                                            }
                                            $urlKey = $nameWithOutSpecialChars . "-" . str_replace("_", "-", $sku_val);
                                            $_Pdetails->setUrlKey($urlKey);
                                            $_Pdetails->setStore($store);
                                            $_Pdetails->setWebsites($websites);
                                            $_Pdetails->setHasOptions($has_options);
                                            $_Pdetails->setPageLayout($page_layout);
                                            $_Pdetails->setMsrp($msrp);
                                            $_Pdetails->setMsrpEnabled($msrp_enabled);
                                            $_Pdetails->setMsrpDisplayActualPriceType($msrp_display_actual_price_type);
                                            $_Pdetails->setIsReturnable($is_returnable);
                                            $_Pdetails->setIsRecurring($is_recurring);
                                            $_Pdetails->setVisibility($visibility);
                                            $_Pdetails->setEnableGooglecheckout($enable_googlecheckout);
                                            $_Pdetails->setTaxClass_id($tax_class_id);
                                            $_Pdetails->setWeight($weight);
                                            $_Pdetails->setRecommendationUrl($recommendation_url);
                                            $_Pdetails->setMetaKeyword($meta_keyword);
                                            $_Pdetails->setMetaTitle($meta_title);
                                            $_Pdetails->setMetaDescription('');
                                            $_Pdetails->setNewsFromDate('');
                                            $_Pdetails->setNewsToDate('');
                                            $_Pdetails->setRequiredOptions('0');
                                            $_Pdetails->setRelatedTgtrPositionLimit('');
                                            $_Pdetails->setRelatedTgtrPositionBehavior('');
                                            $_Pdetails->setUpsellTgtrPositionLimit('');
                                            $_Pdetails->setUpsellTgtrPositionBehavior('');
                                            $_Pdetails->setIsNewPlant('0');
                                            $_Pdetails->setIsWebOnlyPlant('0');
                                            $_Pdetails->setIsOnClearance('0');
                                            $_Pdetails->setBestSellerIndicator('0');
                                            $_Pdetails->setLifetimeGuarantee('0');
                                            $_Pdetails->setSpecialPrice('');
                                            $_Pdetails->setSpecialFromDate('');
                                            $_Pdetails->setSpecialToDate('');
                                            $_Pdetails->setMinimalPrice('');
                                            $_Pdetails->setTierPrice(array());
                                            $_Pdetails->setRecurringProfile('');
                                            $_Pdetails->setCustomDesign('');
                                            $_Pdetails->setCustomDesignFrom('');
                                            $_Pdetails->setCustomDesignTo('');
                                            $_Pdetails->setCustomLayoutUpdate('');
                                            $_Pdetails->setOptionsContainer($options_container);
                                            $_Pdetails->setGiftMessageAvailable('0');
                                            $_Pdetails->setGiftWrappingAvailable('0');
                                            $_Pdetails->setGiftWrappingPrice('');
                                            $_Pdetails->setStatus('1');
                                            $_Pdetails->setPrice(0);
                                            $_Pdetails->setTypeId("simple");
                                            // $attributeSets = $client->call($session_id, 'product_attribute_set.list');
                                            // $attributeSet = $attributeSets[1];
                                            $_Pdetails->setAttributeSetId(10);
                                            if ($flag == 1) {
                                                $_Pdetails->save();
                                            }
                                            print "---->product sku===>" . $sku . "===>" . $sku_val;
                                        }
                                        /****create duplicate products ****/

                                        $mainProductStatus = $_Pdetails->getStatus();
                                        if ($mainProductStatus == 1) {
                                            $offerSku = $sku_val . '_VSPMAAC';
                                            $duplicate_productid = Mage::getModel('catalog/product')->getIdBySku($offerSku);
                                            if (empty($duplicate_productid)) {
                                                $newProduct = $_Pdetails->duplicate();
                                                $offer_product_id = $newProduct->getId();
                                                $offer = Mage::getModel('catalog/product')->load($offer_product_id);
                                                $offer->setStatus(1);
                                                $offer->setSku($offerSku);
                                                $offer->setVisibility("1");
                                                $offerurlKey = $nameWithOutSpecialChars . "-" . str_replace("_", "-", $offerSku);
                                                $offer->setUrlKey($offerurlKey);
                                                $offer->setCategoryIds(array(243));
                                                $offer->save();

                                                // Offer Product Inventory update
                                                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($offer_product_id);
                                                $stockItem->setIsInStock("1");
                                                $stockItem->setUseConfigManageStock(0);
                                                $stockItem->setManageStock("0");
                                                $stockItem->save();
                                            } else {
                                                $offer = Mage::getModel('catalog/product')->load($duplicate_productid);
                                                $offer->setStatus(1);
                                                $offer->setVisibility("1");
                                                $offerurlKey = $nameWithOutSpecialChars . "-" . str_replace("_", "-", $offerSku);
                                                $offer->setUrlKey($offerurlKey);
                                                $offer->setCategoryIds(array(243));
                                                $offer->save();

                                                // Offer Product Inventory update
                                                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($duplicate_productid);
                                                $stockItem->setIsInStock("1");
                                                $stockItem->setUseConfigManageStock(0);
                                                $stockItem->setManageStock("0");
                                                $stockItem->save();
                                            }
                                        }
                                    } catch (Exception $e) {
                                        $log_array[$error_inc]['Message'] = $e->getMessage();
                                        $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                                        $log_array[$error_inc]['sku'] = $sku;
                                        $log_array[$error_inc]['Info'] = "Something went wrong while updating product description information.";
                                        //print_r($arrayProduct);
                                    }
                                }
                                echo "----->" . $a . "#####" . $sku . "####" . $sku_val . "\n";
                                $config++;
                                $a++;
                            } catch (Exception $e) {
                                $log_array[$error_inc]['Message'] = $e->getMessage();
                                $log_array[$error_inc]['Trace'] = $e->getTraceAsString();
                                $log_array[$error_inc]['sku'] = $sku;
                                $log_array[$error_inc]['Info'] = "Something went wrong while updating product description information.";
                                //print_r($arrayProduct);
                            }
                        }
                    }
                }
                $start = $start + $limit;
                //exec('php ProductDescriptionCron.php ' . $start . ' ' . $limit);
                productDescriptionCron($start);
            } else {
                $error_inc1 = $res_cnt + 2;
                try {
                    if (copy($txtProductDescFile, $txtArchiveProductDesc)) {
                        unlink($txtProductDescFile);
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1]['Message'] = $e->getMessage();
                    $log_array[$error_inc1]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1]['Info'] = "Something went wrong while archive the product description file.";
                }

                try {
                    $produ_desc_file = $dir_import . "ProductDescription.txt";
                    $inventory_file = $dir_import . "Inventory.txt";
                    $inventory_feed_file = $dir_import . "InventoryFeed.txt";
                    $price_file = $dir_import . "Price.txt";
                    $catalog_offer_file = $dir_import . "CatalogOffers.txt";
                    $keycode_file = $dir_import . "KeyCodes.txt";
                    $shipping_table_file = $dir_import . "ShippingTables.txt";
                    $delete_Price = $dir_import . "DeletePrice.txt";
                    $delete_keycodes = $dir_import . "DeleteKeyCodes.txt";
                    $delete_Shipping = $dir_import . "DeleteShippingDetails.txt";

                    if (!file_exists($produ_desc_file) &&
                        !file_exists($inventory_file) &&
                        !file_exists($inventory_feed_file) &&
                        !file_exists($price_file) &&
                        !file_exists($catalog_offer_file) &&
                        !file_exists($keycode_file) &&
                        !file_exists($shipping_table_file) &&
                        !file_exists($delete_Price)&&
                        !file_exists($delete_keycodes)&&
                        !file_exists($delete_Shipping)) {

                        unlink($txtFlagFile);
                        /* for ($i = 1; $i < 9; $i++) {
                          $process = Mage::getModel('index/process')->load($i);
                          $process->reindexAll();
                          } */
                    }
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 1]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 1]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 1]['Info'] = "Something went wrong while removing flag file.";
                }
                try {
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $write->delete("product_description_temp");

                    $write->query("UPDATE import_notification_history SET status='notStarted', end_date=NOW(),result_count='" . $argv[1] . "' WHERE import_type='productDescription' and status='inProgress'");
                } catch (Exception $e) {
                    $log_array[$error_inc1 + 2]['Message'] = $e->getMessage();
                    $log_array[$error_inc1 + 2]['Trace'] = $e->getTraceAsString();
                    $log_array[$error_inc1 + 2]['Info'] = "Something went wrong while deleting the records from product description table.";
                }
            }
            //DELETE THE ARCHIVE FILES OLDER THAN 30 Days
            if ($handle = opendir($dir_Archive)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $arrFiles = explode("_", $entry);

                        $fileTimeStamp = $arrFiles[0];
                        $date1 = date('YmdHis');
                        $date2 = $fileTimeStamp;

                        $ts1 = strtotime($date1);
                        $ts2 = strtotime($date2);
                        $seconds_diff = $ts1 - $ts2;
                        $days = floor($seconds_diff / 3600 / 24);
                        try {
                            //Delete the files older than 30 days
                            if ($days > 30) {
                                unlink($dir_Archive . $entry);
                            }
                        } catch (Exception $e) {
                            $log_array[$error_inc1 + 3]['Message'] = $e->getMessage();
                            $log_array[$error_inc1 + 3]['Trace'] = $e->getTraceAsString();
                            $log_array[$error_inc1 + 3]['Info'] = "Something went wrong while deleting 30 days old archive files.";
                        }
                    }
                }
                closedir($handle);
            }
            //ERROR LOG
            if (count($log_array) > 0) {
                $errorMsg = '';
                foreach ($log_array as $log) {
                    $errorMsg .= '
					- - - - - - - -
					ERROR
                                        SUBJECT: ' . $log['Info'] . '
					Message: ' . $log['Message'] . '
					Trace: ' . $log['Trace'] . '
					ProductId: ' . $log['sku'] . '
					Time: ' . date('Y m d H:i:s') . '
					';
                    echo "\r\n" . $errorMsg;
                }
                file_put_contents($dir_Archive . '/logs/log_' . time() . '.txt', $errorMsg);
                //Getting admin Information
                $adminInfo = $write->fetchAll("SELECT * from admin_user");
                $userFirstname = $adminInfo[0]['firstname'];
                $userLastname = $adminInfo[0]['lastname'];
                $userEmail = $adminInfo[0]['email'];
                //$arrTo[] = array("toEmail" => $userEmail, "toName" => $userFirstname . ' ' . $userLastname);

                $mail = new Zend_Mail();
                $mail->setType(Zend_Mime::MULTIPART_RELATED);
                $mail->setBodyHtml('There were errors updating the product description for Spring Hill. Please review the log files at: ' . $_SERVER['REMOTE_ADDR'] . '/dailyfeed/archive/logs/log_' . time() . '.txt');
                //$mail->setFrom($userEmail, $userFirstname . ' ' . $userLastname);
                //SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
                /* foreach ($arrTo as $to) {
                  $mail->addTo($to['toEmail'], $to['toName']);
                  } */
                $mail->setFrom('sudhakark@kensium.com', 'Sudhakar');
                $mail->addTo('sudhakark@kensium.com', 'Sudhakar');
                $mail->addTo('sudhakark@kensium.com', 'Sudhakar');
                $mail->setSubject('Error updating SH product database');
                //$fileContents = file_get_contents($dir_Archive . '/logs/log_' . time() . '.txt');
                //$file = $mail->createAttachment($fileContents);
                //$file->filename = "log_" . time() . ".txt";
                try {
                    if ($mail->send()) {
                        Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
                    }
                } catch (Exception $ex) {
                    Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
                }
            }
        }
        return TRUE;
    } else {
        return FALSE;
    }
}

function getSourceOptionId(Mage_Eav_Model_Entity_Attribute_Source_Interface $source, $value) {
    foreach ($source->getAllOptions() as $option) {
        if (strcasecmp($option['label'], $value) == 0) {
            return $option['value'];
        }
    }
    return null;
}
?>
