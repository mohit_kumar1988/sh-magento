<?php

require_once '/var/www/html/springhill/app/Mage.php';
umask(0);
Mage::app('default');

global $global_dir_Archive;
$global_dir_Archive = "/var/www/html/springhill/dailyfeed/archive/";

$dbConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
$reindexStatus = $dbConnection->fetchAll("SELECT count(*) as count FROM import_notification_history WHERE cron_running=1");

if ( $reindexStatus[0]['count'] > 0 ){

    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
    $updatedDate = $write->fetchOne("SELECT end_date FROM  `import_notification_history` where cron_running=1 ORDER BY end_date DESC LIMIT 0 , 1");
    $date1 = date('YmdHis');
    $date2 = $updatedDate;
    $ts1 = strtotime($date1);
    $ts2 = strtotime($date2);
    $seconds_diff = $ts1 - $ts2;
    $hours = intval(intval($seconds_diff) / 3600);
    if($hours>12)
    {
        $userFirstname = "Sudhakar";
        $userLastname = "kodali";
        $userEmail = "sudhakark@kensium.com";
        $mail = new Zend_Mail();
        $mail->setType(Zend_Mime::MULTIPART_RELATED);
        $mail->setBodyHtml('There were was problem in excecution of Cron Files....');
        $mail->addTo('sudhakark@kensium.com', 'Sudhakar');
        //$mail->addTo('rgedupudi@kensium.com', 'Rahul');
        $mail->setSubject('Problem in excecution of Cron Files');
        try {
            if ($mail->send()) {
                //Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
            }
        } catch (Exception $ex) {
            //Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
        }
    }

    exit;
}


//Update flag to indicate cron in-progress
$dbConnection->query("UPDATE import_notification_history SET cron_running = 1");

// Set index to manual mode
//$processes = Mage::getSingleton('index/indexer')->getProcessesCollection();
//$processes->walk('setMode', array(Mage_Index_Model_Process::MODE_MANUAL));
//$processes->walk('save');

include_once("ProductDescriptionCron.php");
include_once("InventoryUpdateCron.php");
include_once("InventoryFeedUpdateCron.php");
include_once("PriceUpdateCron.php");
include_once("couponOfferCron.php");

/**Delete Files**/
include_once("PriceDeleteCron.php");
include_once("DeleteKeycodesCron.php");
include_once("DeleteShippingCron.php");

priceDeleteCron(0);
KeycodeDeleteCron(0);
ShippingDeleteCron(0);
/****/
productDescriptionCron(0);
inventoryCron(0);
inventoryFeedCron(0);
priceCron(0);
couponOfferCron(0);

// Daily update completed. reindex all tables.
//$processes = Mage::getSingleton('index/indexer')->getProcessesCollection();
//$processes->walk('reindexAll');

//wait for 10 min
//sleep(600);

// Set index to update on save 
//$processes->walk('setMode', array(Mage_Index_Model_Process::MODE_REAL_TIME));
//$processes->walk('save');

//Update flag to indicate cron completion
$dbConnection->query("UPDATE import_notification_history SET cron_running = 0");
?>

