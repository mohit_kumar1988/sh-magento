<?php
require_once '/var/www/html/springhill/app/Mage.php';
umask(0);
Mage::app('default');
global $global_dir_Archive;
global $global_server_path, $global_server_image_path;
$global_server_path = "http://www.springhillnursery.com/";
$global_dir_Archive = "/var/www/html/springhill/dailyfeed/singlefeed/";
$global_server_image_path = $global_server_path."media/catalog/product";
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$_categories = Mage::getModel('catalog/category')
    ->getCollection()
    ->addAttributeToSelect('*')
    ->addIsActiveFilter();
$i=0;
$CategoryInfo =array();
//echo count($_categories);exit;
foreach ($_categories  as $_category) {
    $category = Mage::getModel('catalog/category')->load($_category->getId());
    $entityId = $category->getId();
    if($entityId==2){
        continue;
    }
    /*if($i==5){
        break;
    }*/
    $productCollection = $category->getProductCollection();
    $strCategories='';
    foreach ($productCollection as $product) {
        $pId=$product->getId();
        $_uPdetails = Mage::getModel('catalog/product')->load($pId);
        $categoriesIds = $_uPdetails->getCategoryIds();
        if(in_array($entityId,$categoriesIds))
        {
            if(empty($strCategories))
                $strCategories = $_uPdetails->getSku();
            else
                $strCategories .= ','.$_uPdetails->getSku();
        }
    }

    $CategoryInfo[$i]['Category_Name']=$category->getName();
    if($category->getIsActive()==0){
        $CategoryInfo[$i]['Status']='No';
    }else{
        $CategoryInfo[$i]['Status']='Yes';
    }
    $CategoryInfo[$i]['Url_Key']=$GLOBALS["global_server_path"] .$category->getUrlKey().".html";
    $CategoryInfo[$i]['Description']=trim(preg_replace('/\s+/', ' ', html_entity_decode($category->getDescription())));
    $cImage = $category->getImage();
    if(empty($cImage))
        $CategoryInfo[$i]['Image']='';
    else
        $CategoryInfo[$i]['Image']=$GLOBALS["global_server_image_path"].$cImage;
    $CategoryInfo[$i]['Meta_Keywords']=$category->getMetaKeywords();
    $CategoryInfo[$i]['Meta_Description']=$category->getMetaDescription();
    if($category->getIncludeInMenu()==0){
        $CategoryInfo[$i]['Include_in_navigation_menu']='No';
    }else{
        $CategoryInfo[$i]['Include_in_navigation_menu']='Yes';
    }
    if($category->getIsFeatured()==0){
        $CategoryInfo[$i]['Is_Featured']='No';
    }else{
        $CategoryInfo[$i]['Is_Featured']='Yes';
    }
    if($category->getIsLeftMenu()==0){
        $CategoryInfo[$i]['For_Plant_Finder']='No';
    }else{
        $CategoryInfo[$i]['For_Plant_Finder']='Yes';
    }
    if($category->getIsLeftMenu()==0){
        $CategoryInfo[$i]['Is_Left_Menu']='No';
    }else{
        $CategoryInfo[$i]['Is_Left_Menu']='Yes';
    }
    if($category->getIsSitemap()==0){
        $CategoryInfo[$i]['Show_In_Sitemap']='No';
    }else{
        $CategoryInfo[$i]['Show_In_Sitemap']='Yes';
    }
    $CategoryInfo[$i]['Sort_Order']=$category->getSortOrder();
    $CategoryInfo[$i]['Category Products']=$strCategories;
    echo "count===".$i."==Id===".$entityId."\n";
    $i++;

}

if(count($CategoryInfo)>0){
    try {
        $txFileName = "CategoryExport.txt";
        $timestamp = date("Ymd") . "_";
        $dir_Archive = $GLOBALS["global_dir_Archive"];
        $CategoryFile = $dir_Archive . $timestamp . $txFileName;
        if(file_exists($CategoryFile)){
            unlink($CategoryFile);
        }
        $file = $timestamp . $txFileName;
        $fh = fopen($CategoryFile, 'a+') or die("can't open file");
        $header ="Category Name|Is Active|URL Key|Description|Image|Meta Keywords|Meta Description|Include in Navigation Menu|Is Featured|For Plant Finder|Is Left Menu|Show In Sitemap|Sort Order|Category Products\r\n";
        fwrite($fh, $header);
        foreach ($CategoryInfo as $id => $info) {
            $stringData = implode("|", $info) . "\n";
            fwrite($fh, $stringData);
        }
        fclose($fh);
        removeOldFiles($dir_Archive);
        //echo $CategoryFile."file is exported with all category details";
        echo '<a href="'.$global_server_path.'dailyfeed/archive/'.$file.'">'.$file."</a>file is exported with all category details";
    } catch (Exception $e) {
        $log_array[0]['Message'] = $e->getMessage();
        $log_array[0]['Trace'] = $e->getTraceAsString();
        $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
    }
}
function removeOldFiles($dir_Archive){
    //Reading all the files from the archive directory for finding latest price file
    if ($handle = opendir($dir_Archive)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                //Check for price.txt extension
                if(strpos($entry, 'CategoryExport.txt')){
                    $timestamp = str_replace('_CategoryExport.txt','',$entry);
                    // get today and last 1 days time
                    $today = time();
                    $OneMonth = $today - (60*60*24*30);
                    $from = date("Ymd", $OneMonth);
                    if($from > $timestamp){
                        unlink($dir_Archive.$entry);
                    }

                }
            }
        }
        closedir($handle);
    }
}
