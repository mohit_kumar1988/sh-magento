<?php
    require_once '/var/www/html/springhill/app/Mage.php';
    umask(0);
    Mage::app('default');
    global $global_dir_Archive;
    global $global_server_path, $global_server_image_path;
    $global_server_path = "http://www.springhillnursery.com/";
    $global_dir_Archive = "/var/www/html/springhill/dailyfeed/singlefeed/";
    $global_server_image_path = $global_server_path."media/catalog/product";
    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
    $_categories = Mage::getModel('catalog/category')
        ->getCollection()
        ->addAttributeToSelect('entity_id');
        //->addIsActiveFilter();
    $i=0;
    $CategoryInfo =array();
    $j=0;
    foreach ($_categories  as $_category) {
        $category = Mage::getModel('catalog/category')->load($_category->getId());
        $entityId = $category->getId();
        if($entityId==2){
            continue;
        }
        $productCollection = $category->getProductCollection();
        $strCategories='';
        foreach ($productCollection as $product) {
            $pId=$product->getId();
            $_uPdetails = Mage::getModel('catalog/product')->load($pId);
            $categoriesIds = $_uPdetails->getCategoryIds();
            $productSku =  $_uPdetails->getSku();
            if(!strpos($productSku,'_VSPMAAC')){
                if(in_array($entityId,$categoriesIds))
                {
                    $CategoryInfo[$j]['Category_Id']= $category->getId();
                    $CategoryInfo[$j]['Category_Name']= $category->getName();
                    $CategoryInfo[$j]['sku'] = $productSku;
                    $CategoryInfo[$j]['Product_Name'] = $_uPdetails->getName();
                    echo "Id===>".$category->getId()."====category===>".$category->getName()."====SKU====>".$_uPdetails->getSku();
                    echo "\n";
                }
                $j++;
            }
        }

        $i++;
    }
if(count($CategoryInfo)>0){
    try {
        $txFileName = "CategoryProductExport.txt";
        $timestamp = date("Ymd") . "_";
        $dir_Archive = $GLOBALS["global_dir_Archive"];
        $CategoryFile = $dir_Archive . $timestamp . $txFileName;
        $file = $timestamp . $txFileName;
        if(file_exists($CategoryFile)){
            unlink($CategoryFile);
        }
        $fh = fopen($CategoryFile, 'a+') or die("can't open file");
        $header ="Id|Category Name|Sku|Product Name\n";
        fwrite($fh, $header);
        foreach ($CategoryInfo as $id => $info) {
            $stringData = implode("|", $info) . "\n";
            fwrite($fh, $stringData);
        }
        fclose($fh);
        removeOldFiles($dir_Archive);
        //echo $CategoryFile."file is exported with all category details";
        echo '<a href="'.$global_server_path.'dailyfeed/singlefeed/'.$file.'">'.$file."</a>file is exported with all category details";
    } catch (Exception $e) {
        $log_array[0]['Message'] = $e->getMessage();
        $log_array[0]['Trace'] = $e->getTraceAsString();
        $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
    }
}
function removeOldFiles($dir_Archive){
    //Reading all the files from the archive directory for finding latest price file
    if ($handle = opendir($dir_Archive)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                //Check for price.txt extension
                if(strpos($entry, 'CategoryProductExport.txt')){
                    $timestamp = str_replace('_CategoryProductExport.txt','',$entry);
                    // get today and last 1 days time
                    $today = time();
                    $OneMonth = $today - (60*60*24*30);
                    $from = date("Ymd", $OneMonth);
                    if($from > $timestamp){
                        unlink($dir_Archive.$entry);
                    }

                }
            }
        }
        closedir($handle);
    }
}
