<?php
require_once '/var/www/html/springhill/app/Mage.php';
umask(0);
Mage::app('default');
global $global_dir_Archive;
global $global_server_path, $global_server_image_path;
$global_server_path = "http://www.springhillnursery.com/";
$global_dir_Archive = "/var/www/html/springhill/dailyfeed/singlefeed/";
$global_server_image_path = $global_server_path."media/catalog/product";
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
//$query = 'SELECT entity_id,sku,type_id FROM catalog_product_entity WHERE sku!=""';
$query = 'SELECT entity_id,sku,type_id FROM catalog_product_entity WHERE sku!="" and sku not like "%_VSPMAAC"';
$fetchAllProducts = $write->fetchAll($query);
$i=0;
$ProductInfo =array();
//echo count($fetchAllProducts);exit;
foreach($fetchAllProducts as $product){
    $prodId = $product['entity_id'];
    $_Pdetails =Mage::getModel('catalog/product')->load($prodId);
    $productSku = $_Pdetails->getSku();
    if(!empty($productSku)){
        //If Image is empty dont include in feed
        $prodImage= $_Pdetails->getImage();
        if($prodImage=="/" || $prodImage=="no_selection" || $prodImage=="")
            $image ='';
        else
            $image = $GLOBALS["global_server_image_path"] . $_Pdetails->getImage();

        //Get categories
        $categories_list = $_Pdetails->getCategoryIds();
        $strCategories='';
        foreach($categories_list as $catvalue) {
            $catagory_model = Mage::getModel('catalog/category');
            $categories = $catagory_model->load($catvalue);
            if(empty($strCategories))
                $strCategories = $categories->getName();
            else
                $strCategories .= ','.$categories->getName();
        }
         $upsellProduct='';
         $crossProduct='';
         if( $_Pdetails->getRecommendedItems() ){
             $offer_products = explode(" ",$_Pdetails->getRecommendedItems());
             if(count($offer_products)>0 ){
                 foreach($offer_products as $prodSku){
                     $productId = Mage::getModel('catalog/product')->getIdBySku("$prodSku");
                     $_prod = Mage::getModel('catalog/product')->load($productId);
                     if($_prod && $_prod->isSaleable() && ($_prod->getFutureSeasonMessage() > 1 || $_prod->getFutureSeasonMessage()=='')){
                            if(empty($upsellProduct)){
                                $upsellProduct=$prodSku;
                            }else{
                                if(empty($crossProduct)){
                                    $crossProduct=$prodSku;
                                }else{
                                    $crossProduct .=','.$prodSku;
                                }
                            }
                     }
                 }
             }
         }
        $tierPrices = $_Pdetails->getTierPrice();
        $strTPrice='';
        $strTgroup='';
        $strTQty='';
        foreach($tierPrices as $tprice){
            if(empty($strTPrice)){
                $strTPrice = $tprice['price'];
                $strTgroup = 'ALL GROUPS';
                $strTQty = $tprice['price_qty'];
            }else{
                $strTPrice .= ','.$tprice['price'];
                $strTgroup .= ','.'ALL GROUPS';
                $strTQty .= ','.$tprice['price_qty'];
            }

        }
        //get stock data
        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_Pdetails->getId());
        $manageQty = $stockItem->getManageStock();
        $qty='';
        $strManageQty="No";
        if($manageQty==1){
            $qty = $stockItem->getQty();
            $strManageQty="Yes";
        }
        //$backorders = $stockItem->getBackorders();
        $backorders = 'No BackOrder';

        $ProductInfo[$i]['SKU']=$productSku;
       // $ProductInfo[$i]['Status']=$_Pdetails->getStatus();
	if($_Pdetails->isSaleable() && ($_Pdetails->getFutureSeasonMessage() > 1 || $_Pdetails->getFutureSeasonMessage()=='')){
            $ProductInfo[$i]['Status']=1;
        }else{
            $ProductInfo[$i]['Status']=2;
        }
        $ProductInfo[$i]['Short_Description']=trim(preg_replace('/\s+/', ' ', $_Pdetails->getShortDescription()));
        $ProductInfo[$i]['Description']=trim(preg_replace('/\s+/', ' ', $_Pdetails->getDescription()));
//        $ProductInfo[$i]['Short_Description']=preg_replace("/(\r?\n){2,}/", "", $_Pdetails->getShortDescription());
//        $ProductInfo[$i]['Description']=preg_replace("/(\r?\n){2,}/", "", $_Pdetails->getDescription());
        $ProductInfo[$i]['URL_Key']=$GLOBALS["global_server_path"] .$_Pdetails->getUrlPath();
        $ProductInfo[$i]['Name']=$_Pdetails->getName();
        $ProductInfo[$i]['Visibility']=$_Pdetails->getVisibility();
        $ProductInfo[$i]['Price']=$_Pdetails->getPrice();
        $ProductInfo[$i]['Zones']=$_Pdetails->getWebSearchZone();
        $ProductInfo[$i]['Botanical_Name']=$_Pdetails->getBotanicalName();
        $ProductInfo[$i]['Deer_resistant']=$_Pdetails->getDeerResistant();
        $ProductInfo[$i]['Related_Video_Url']=$_Pdetails->getRelatedVideoUrl();
        $ProductInfo[$i]['SEO_Block']=$_Pdetails->getSeoCopyBlock();
        $ProductInfo[$i]['Tier_Price']=$strTPrice;
        $ProductInfo[$i]['Tier_Price_Customer_Group']=$strTgroup;
        $ProductInfo[$i]['Tier_Price_Qty']=$strTQty;
        $ProductInfo[$i]['Meta_Title']=$_Pdetails->getMetaTitle();
        $ProductInfo[$i]['Meta_Keywords']=$_Pdetails->getMetaKeywords();
        $ProductInfo[$i]['Meta_Description']=$_Pdetails->getMetaDescription();
        $ProductInfo[$i]['Image']=$image;
        $ProductInfo[$i]['Manage_Qty']=$strManageQty;
        $ProductInfo[$i]['Qty']=$qty;
        $ProductInfo[$i]['BackOrder']=$backorders;
        $ProductInfo[$i]['Category']=$strCategories;
        $ProductInfo[$i]['Related_Products']='';
        $ProductInfo[$i]['Up_Sell']= $upsellProduct;
        $ProductInfo[$i]['Cross_Sells']=$crossProduct;
        echo "count===".$i."==SKU===".$productSku."\n";
    }
    $i++;
    /*if($i==10){
        break;
    }*/
}


if(count($ProductInfo)>0){
    try {
        $txFileName = "ProductExport.txt";
        $timestamp = date("Ymd") . "_";
        $dir_Archive = $GLOBALS["global_dir_Archive"];
        $ProductFile = $dir_Archive . $timestamp . $txFileName;
        $file =$timestamp . $txFileName;
        if(file_exists($ProductFile)){
            unlink($ProductFile);
        }
        $fh = fopen($ProductFile, 'a+') or die("can't open file");
        $header ="SKU^Status^Short Description^Description^URL Key^Name^Visibility^Price^Zones^Botanical Name^Deer resistant^Related Video Url^SEO Block^Tier Price^Tier Price Customer Group^Tier Price Qty^Meta Title^Meta Keywords^Meta Description^Image^Manage Qty^Qty^Backorders^Categories^Related Products^Up Sell^Cross Sells\n";
        fwrite($fh, $header);
        foreach ($ProductInfo as $id => $info) {
             $stringData = implode("^", $info) . "\n";
             fwrite($fh, $stringData);
        }
        fclose($fh);
        removeOldFiles($dir_Archive);
        //echo $ProductFile."file is exported with all product details";
        echo '<a href="'.$global_server_path.'dailyfeed/archive/'.$file.'">'.$file."</a>file is exported with all product details";
    } catch (Exception $e) {
        $log_array[0]['Message'] = $e->getMessage();
        $log_array[0]['Trace'] = $e->getTraceAsString();
        $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
    }
}

function removeOldFiles($dir_Archive){
    //Reading all the files from the archive directory for finding latest price file
    if ($handle = opendir($dir_Archive)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                //Check for price.txt extension
                if(strpos($entry, 'ProductExport.txt')){
                    $timestamp = str_replace('_ProductExport.txt','',$entry);
                    // get today and last 1 days time
                    $today = time();
                    $OneMonth = $today - (60*60*24*30);
                    $from = date("Ymd", $OneMonth);
                    if($from > $timestamp){
                        unlink($dir_Archive.$entry);
                    }

                }
            }
        }
        closedir($handle);
    }
}


