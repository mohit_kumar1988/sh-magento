<?php
require_once '/var/www/html/springhill/app/Mage.php';
umask(0);
Mage::app('default');
global $global_dir_Archive;
global $global_server_path, $global_server_image_path;
$global_server_path = "http://www.springhillnursery.com/";
$global_dir_Archive = "/var/www/html/springhill/dailyfeed/singlefeed/";
$global_server_image_path = $global_server_path."media/catalog/product";
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$query = 'SELECT rule_id FROM `salesrule` WHERE name!=""';
$fetchAllSalesRules = $write->fetchAll($query);
$ruleOffers= array();
$i=0;
foreach($fetchAllSalesRules as $rule){
    $ruleId=$rule['rule_id'];
    $objRule = Mage::getModel('salesrule/rule')->load($ruleId);
    //print_r(unserialize($objRule->getConditionsSerialized()));exit;//base_subtotal value   //attribute //value
    $ruleOffers[$i]['Rule_Name']=$objRule->getName();
    $ruleOffers[$i]['Description']=trim(preg_replace('/\s+/', ' ', $objRule->getDescription()));
    $ruleOffers[$i]['Status']=$objRule->getIsActive();
    $ruleOffers[$i]['Coupon_Code']=$objRule->getCouponCode();
    $ruleOffers[$i]['Uses_per_Coupon']=$objRule->getUsesPerCoupon();
    $ruleOffers[$i]['Uses_per_Customer']=$objRule->getUsesPerCustomer();
    $ruleOffers[$i]['From_Date']=$objRule->getFromDate();
    $ruleOffers[$i]['To_Date']=$objRule->getToDate();
    $ruleOffers[$i]['Offer_Code']=$objRule->getOfferCode();
    $arrCondition =unserialize($objRule->getConditionsSerialized());
    $strCondition='';
    if(!empty($arrCondition['conditions'][0]['attribute']))
        $strCondition = $arrCondition['conditions'][0]['attribute'].','.$arrCondition['conditions'][0]['value'];
    $ruleOffers[$i]['Conditions']=$strCondition;
    $ruleOffers[$i]['Action_Apply']=$objRule->getSimpleAction();
    $ruleOffers[$i]['Discount_Amount']=$objRule->getDiscountAmount();
    $ruleOffers[$i]['Discount_Qty']=$objRule->getDiscountQty();
    $ruleOffers[$i]['Apply_To_Shipping']=$objRule->getApplyToShipping();
    $ruleOffers[$i]['Free_Shipping']=$objRule->getSimpleFreeShipping();
    $arrActionCondition =unserialize($objRule->getActionsSerialized());
    $strActionCondition='';
    if(!empty($arrActionCondition['conditions'][0]['attribute']))
        $strActionCondition=$arrActionCondition['conditions'][0]['attribute'].','.$arrActionCondition['conditions'][0]['value'];
    $ruleOffers[$i]['Actions']=$strActionCondition;
    echo "count===".$i."==Id===".$ruleId."\n";
    $i++;
}

if(count($ruleOffers)>0){
    try {
        $txFileName = "ShoppingRules.txt";
        $timestamp = date("Ymd") . "_";
        $dir_Archive = $GLOBALS["global_dir_Archive"];
        $SalesRuleFile = $dir_Archive . $timestamp . $txFileName;
        $file = $timestamp . $txFileName;
        if(file_exists($SalesRuleFile)){
            unlink($SalesRuleFile);
        }
        $fh = fopen($SalesRuleFile, 'a+') or die("can't open file");
        $header ="Rule Name|Description|Status|Coupon Code|Uses per Coupon|Uses per Customer|From Date|To Date|Offer Code|Conditions|Actions Apply|Discount Amount|Maximum Qty Discount|Apply to Shipping|Free Shipping|Action Conditions\n";
        fwrite($fh, $header);
        foreach ($ruleOffers as $id => $info) {
            $stringData = implode("|", $info) . "\n";
            fwrite($fh, $stringData);
        }
        fclose($fh);
        removeOldFiles($dir_Archive);
        echo '<a href="'.$global_server_path.'dailyfeed/archive/'.$file.'">'.$file."</a>file is exported with all shopping rule details";
        //echo $SalesRuleFile."file is exported with all shopping rule details";
    } catch (Exception $e) {
        $log_array[0]['Message'] = $e->getMessage();
        $log_array[0]['Trace'] = $e->getTraceAsString();
        $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
    }
}
function removeOldFiles($dir_Archive){
    //Reading all the files from the archive directory for finding latest price file
    if ($handle = opendir($dir_Archive)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                //Check for price.txt extension
                if(strpos($entry, 'ShoppingRules.txt')){
                    $timestamp = str_replace('_ShoppingRules.txt','',$entry);
                    // get today and last 1 days time
                    $today = time();
                    $OneMonth = $today - (60*60*24*30);
                    $from = date("Ymd", $OneMonth);
                    if($from > $timestamp){
                        unlink($dir_Archive.$entry);
                    }

                }
            }
        }
        closedir($handle);
    }
}
