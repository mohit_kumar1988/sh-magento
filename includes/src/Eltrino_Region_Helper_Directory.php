<?php
/**
 * Remove or Change Displayed States and Regions
 *
 * LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE_OSL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@eltrino.com so we can send you a copy immediately.
 *
 * @category   Eltrino
 * @package    Eltrino_Region
 * @copyright  Copyright (c) 2012 Eltrino LLC. (http://www.eltrino.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Helper to obtain directory information
 *
 * @category   Eltrino
 * @package    Eltrino_Region
 * @copyright  Copyright (c) 2012 Eltrino LLC. (http://www.eltrino.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Eltrino_Region_Helper_Directory extends Mage_Directory_Helper_Data
{
    public function getRegionJson()
    {
        Varien_Profiler::start('TEST: '.__METHOD__);
        if (!$this->_regionJson) {
            $cacheKey = 'DIRECTORY_REGIONS_JSON_STORE'.Mage::app()->getStore()->getId();
            if (Mage::app()->useCache('config')) {
                $json = Mage::app()->loadCache($cacheKey);
            }
            if (empty($json)) {
                $countryIds = array();
                foreach ($this->getCountryCollection() as $country) {
                    $countryIds[] = $country->getCountryId();
                }
                $disabledRegions = array();
                $disabledRegionsCollection = Mage::getResourceModel('eltrino_region/entity_collection')->load();
                foreach ($disabledRegionsCollection as $item) {
                    $disabledRegions[] = $item->getRegionId();
                }
                $collection = Mage::getModel('directory/region')->getResourceCollection()
                    ->addCountryFilter($countryIds);
				//$disabledRegions[]= 1;
				$action = Mage::app()->getRequest()->getControllerName();
				$write = Mage::getSingleton('core/resource')->getConnection('core_write'); 
				if($action == 'onepage'){
					$quoteid=Mage::getSingleton("checkout/session")->getQuote()->getId();
					$session = Mage::getSingleton('checkout/session');
					foreach ($session->getQuote()->getAllItems() as $item) {
						$product_id = $item->getProductId();
						$_product = Mage::getModel('catalog/product')->load($product_id);
						if($_product->getRestrictedStates() !='' ){
							$regionsValue[$product_id] = explode(" ", $_product->getRestrictedStates());
							//Mage::log('Hello'.$_product->getRestrictedStates(), null, 'mylogfile.log'); 
						}
					}
					//Mage::log('Hello'.count($regionsValue) , null, 'mylogfile.log'); 
					if(count($regionsValue) > 0 ){
						foreach($regionsValue as $regionval){
							foreach($regionval as $val){
								//Mage::log("#####".$val, null, 'mylogfile.log'); 
								$regionId = $write->fetchOne("SELECT region_id FROM directory_country_region where code='".$val."' ");
							    //$disabledRegions[] = $regionId;
								//Mage::log($regionId.'Hello', null, 'mylogfile.log'); 
							}
						}
					}
				}
			
                if (!empty($disabledRegions)) {
                    $collection->addFieldToFilter($this->getRegionTableAlias() . ".region_id", array('nin' => $disabledRegions));
                }

                $regions = array();
                foreach ($collection as $region) {
                    if (!$region->getRegionId()) {
                        continue;
                    }
                    $regions[$region->getCountryId()][$region->getRegionId()] = array(
                        'code' => $region->getCode(),
                        'name' => $this->__($region->getName())
                    );
                }
                $json = Mage::helper('core')->jsonEncode($regions);

                if (Mage::app()->useCache('config')) {
                    Mage::app()->saveCache($json, $cacheKey, array('config'));
                }
            }
            $this->_regionJson = $json;
        }

        Varien_Profiler::stop('TEST: '.__METHOD__);
        return $this->_regionJson;
    }

    /**
     * Retrieve table alias for region table depending
     * on Magento version. If Magento version lower
     * then 1.6 result will 'region' and in other case 'main_table'.
     *
     * @return string
     */
    public function getRegionTableAlias()
    {
        if (version_compare('1.6', Mage::getVersion()) == 1) {
            return 'region';
        }
        return 'main_table';
    }
}
