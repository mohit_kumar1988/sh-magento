<?php
class Kanavan_Searchautocomplete_Block_Suggest extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getSearchautocomplete()     
     { 
        if (!$this->hasData('searchautocomplete')) {
            $this->setData('searchautocomplete', Mage::registry('searchautocomplete'));
        }
        return $this->getData('searchautocomplete');
     }

     public function getSuggestProducts()     
     {


		$query = Mage::helper('catalogsearch')->getQuery();
		$query->setStoreId(Mage::app()->getStore()->getId());

		if ($query->getRedirect()){
			$query->save();
		}
		else {
			$query->prepare();
		}

		Mage::helper('catalogsearch')->checkNotes();
		$results=$query->getResultCollection();//->setPageSize(5);
		$results->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>0', 'left');
		//$results->getSelect()->joinLeft('sales_flat_order_item AS sfoi','e.entity_id = sfoi.product_id','SUM(sfoi.qty_ordered) AS ordered_qty')->group('e.entity_id')->order('ordered_qty desc');			
		$results->addAttributeToFilter('sku', array('neq' => 'f1'));
		$results->addAttributeToFilter("price", array('gt'=> 0));
		$results->setVisibility(array(3,4));
		//$results->addAttributeToSort('ordered_qty','desc'); 
		$results->addAttributeToSort('till_date_revenue','desc'); 
		$results->addAttributeToSort('inventory_in_stock','desc'); 
		//$results->printlogquery(true)."\n\n";

        if(Mage::getStoreConfig('searchautocomplete/preview/number_product'))
        {
            $results->setPageSize(Mage::getStoreConfig('searchautocomplete/preview/number_product'));
        }
        else
        {
            $results->setPageSize(5);
        }
        $results->addAttributeToSelect('short_description');
        $results->addAttributeToSelect('name');
        $results->addAttributeToSelect('thumbnail');
        $results->addAttributeToSelect('small_image');
        $results->addAttributeToSelect('url_key');


        return $results;
    }
     public function enabledSuggest()     
     {
        return Mage::getStoreConfig('searchautocomplete/suggest/enable');
      }

     public function enabledPreview()     
     {
        return Mage::getStoreConfig('searchautocomplete/preview/enable');
     }

     public function getImageWidth()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/image_width');
     }

     public function getImageHeight()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/image_height');
     }
     public function getEffect()
     {
        return Mage::getStoreConfig('searchautocomplete/settings/effect');
     }

     public function getPreviewBackground()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/background');
     }

     public function getSuggestBackground()
     {
        return Mage::getStoreConfig('searchautocomplete/suggest/background');
     }

     public function getSuggestColor()
     {
        return Mage::getStoreConfig('searchautocomplete/suggest/suggest_color');
     }

     public function getSuggestCountColor()
     {
        return Mage::getStoreConfig('searchautocomplete/suggest/count_color');
     }

     public function getBorderColor()
     {
        return Mage::getStoreConfig('searchautocomplete/settings/border_color');
     }

     public function getBorderWidth()
     {
        return Mage::getStoreConfig('searchautocomplete/settings/border_width');
     }

     public function isShowImage()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/show_image');
     }

     public function isShowName()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/show_name');
     }
     public function getProductNameColor()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/pro_title_color');
     }

     public function getProductDescriptionColor()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/pro_description_color');
     }


     public function isShowDescription()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/show_description');
     }

     public function getNumDescriptionChar()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/num_char_description');
     }


     public function getImageBorderWidth()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/image_border_width');
     }
     public function getImageBorderColor()
     {
        return Mage::getStoreConfig('searchautocomplete/preview/image_border_color');
     }

     public function getHoverBackground()
     {
        return Mage::getStoreConfig('searchautocomplete/settings/hover_background');
     }

}
