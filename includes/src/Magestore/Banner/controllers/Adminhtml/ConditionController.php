<?php

class Magestore_Banner_Adminhtml_ConditionController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('banner/condition')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Condition Manager'), Mage::helper('adminhtml')->__('Condition Manager'));

		
		
		return $this;
	}   
 
	public function indexAction() {
		$this->banner = $this->getRequest()->getParam('banner_id');
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('banner/condition')->load($id);


		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('condition_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('banner/conditions');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Condition Manager'), Mage::helper('adminhtml')->__('Condition Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Condition News'), Mage::helper('adminhtml')->__('Condition News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('banner/adminhtml_condition_edit'))
				->_addLeft($this->getLayout()->createBlock('banner/adminhtml_condition_edit_tabs'));

			$this->renderLayout();
		} else {

			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('banner')->__('Condition does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {

		if ($data = $this->getRequest()->getPost()) {

			$resource = new Mage_Core_Model_Resource();
			$read = $resource->getConnection('core_read');
			$write = $resource->getConnection('core_write');

            if($data['attr_type'] == "Customer") {
            $filtterId = 1;
            } else if($data['attr_type'] == "CustomerAddress") {
            $filtterId = 2;
            } else if($data['attr_type'] == "Category" ) 
            { $filtterId = 3;
            }else if($data['attr_type'] == "Product" ) 
            { $filtterId = 4;
            }

			$category_id =  $write -> fetchOne( "SELECT category_id  FROM banner  where  id='".$data['banner_id']."'");
            $attributeDetails = Mage::getResourceModel('eav/entity_attribute_collection')
                                    ->setEntityTypeFilter($filtterId)
						            ->setCodeFilter($data['attr_code'])
						            ->load()
						            ->getFirstItem(); 

			//print_r($attributeDetails); exit;

			$data['entity_type_id'] = $attributeDetails['entity_type_id'];
			$data['attribute_id']= $attributeDetails['attribute_id'];
			$banner_id=$data['banner_id'];
			$data['category_id']=$category_id;
			if($data['relation']!='Between'){
				$data['attr_value2']='';
			}

			$data['attr_code'] =implode(",",$data['attr_code']);
			$model = Mage::getModel('banner/condition');

			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
					
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('banner')->__('Condition was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {

					$this->_redirect('*/*/edit/', array('banner_id'=>$banner_id,'id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/index/banner_id/'.$banner_id);
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('banner')->__('Unable to find condition to save'));
        $this->_redirect('*/*/banner_id');
	}
 
	public function deleteAction() {
		
		$banner_id     = $this->getRequest()->getParam('banner_id');
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				
				$model = Mage::getModel('banner/condition');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Condition was successfully deleted'));
				$this->_redirect('*/*/index/banner_id/'.$banner_id );
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/index/banner_id/'.$banner_id );
	}

    public function massDeleteAction() {

        $conditionIds = $this->getRequest()->getParam('condition');
		$banner_id     = $this->getRequest()->getParam('banner_id');
        if(!is_array($conditionIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($conditionIds as $conditionId) {
                    $condition = Mage::getModel('banner/condition')->load($conditionId);
                    $condition->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($conditionIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index', array('banner_id'=>$banner_id ));
    }
	
    public function massStatusAction()
    {
        $conditionIds = $this->getRequest()->getParam('condition');
		$banner_id     = $this->getRequest()->getParam('banner_id');
        if(!is_array($conditionIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($conditionIds as $conditionId) {
                    $condition = Mage::getSingleton('banner/condition')
                        ->load($conditionId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($conditionIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index',array('banner_id'=>$banner_id ));
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'condition.csv';
        $content    = $this->getLayout()->createBlock('banner/adminhtml_condition_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'condition.xml';
        $content    = $this->getLayout()->createBlock('banner/adminhtml_condition_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
