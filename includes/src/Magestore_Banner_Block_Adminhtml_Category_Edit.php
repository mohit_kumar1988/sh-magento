<?php

class Magestore_Banner_Block_Adminhtml_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'banner';
        $this->_controller = 'adminhtml_category';
        
        $this->_updateButton('save', 'label', Mage::helper('banner')->__('Save Category'));
        $this->_updateButton('delete', 'label', Mage::helper('banner')->__('Delete Category'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('category_description') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'category_description');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'category_description');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('category_data') && Mage::registry('category_data')->getId() ) {
			
		   return Mage::helper('banner')->__("Edit Category '%s'", $this->htmlEscape(Mage::registry('category_data')->getName()));
        } else {
            
			return Mage::helper('banner')->__('Add Category');
        }
    }
}