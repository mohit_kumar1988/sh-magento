<?php

class Magestore_Banner_Block_Adminhtml_Client_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('client_form', array('legend'=>Mage::helper('banner')->__('Client information')));
     
      $fieldset->addField('customer_id', 'text', array(
          'label'     => Mage::helper('banner')->__('Customer Id'),
          'name'      => 'customer_id',
		  'readonly'  => 'readonly',
          // 'class'     => 'required-entry',
          'required'  => false,		  
      ));	 
	  
	  
	  $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('banner')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));

	  $fieldset->addField('contact', 'text', array(
          'label'     => Mage::helper('banner')->__('Contact'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'contact',
      ));
      		
	  $fieldset->addField('email', 'text', array(
          'label'     => Mage::helper('banner')->__('Email'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'email',
      ));		
      
     
      $fieldset->addField('extrainfo', 'editor', array(
          'name'      => 'extrainfo',
          'label'     => Mage::helper('banner')->__('Extra Information'),
          'title'     => Mage::helper('banner')->__('Extra Information'),
          'style'     => 'width:700px; height:150px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getClientData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getClientData());
          Mage::getSingleton('adminhtml/session')->getClientData(null);
      } elseif ( Mage::registry('client_data') ) {
          $form->setValues(Mage::registry('client_data')->getData());
      }
      return parent::_prepareForm();
  }
}