<?php

class Magestore_Banner_Block_Adminhtml_Client_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('client_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('banner')->__('Client Information'));
  }

  protected function _beforeToHtml()
  {
	if(!$this->getRequest()->getParam('id'))
	{	
	  $this->addTab('customer_section', array(
          'label'     => Mage::helper('banner')->__('Import Customer Info'),
          'title'     => Mage::helper('banner')->__('Import Customer Info'),
		  'url'		  => $this->getUrl('*/*/customer',array('_current'=>true)),
		  'class'     => 'ajax',      
	  ));        
	}	
	
      $this->addTab('form_section', array(
          'label'     => Mage::helper('banner')->__('Client Information'),
          'title'     => Mage::helper('banner')->__('Client Information'),
          'content'   => $this->getLayout()->createBlock('banner/adminhtml_client_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}