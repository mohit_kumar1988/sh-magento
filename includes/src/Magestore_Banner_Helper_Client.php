<?php

class Magestore_Banner_Helper_Client extends Mage_Core_Helper_Abstract
{
	public function getClientCustomerIds()
	{
		$customerIds = array(0);
		$collection = Mage::getResourceModel('banner/client_collection');
		if(count($collection))
		foreach($collection as $client)
		{
			$customerIds[] = $client->getCustomerId();
		}
		return $customerIds;
	}
}