<?php

class Magestore_Banner_Helper_Data extends Mage_Core_Helper_Abstract
{
	public static function uploadBannerImage()
	{
		Mage::helper('banner')->createImageFolder();
		
		$banner_image_path = Mage::getBaseDir('media') . DS .'banners'.DS;
		
		$image = "";
		if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
			try {	
				/* Starting upload */	
				$uploader = new Varien_File_Uploader('image');
				
				// Any extention would work
				$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','bmp'));
				$uploader->setAllowRenameFiles(false);
									
				$uploader->setFilesDispersion(false);
																
				$uploader->save($banner_image_path, $_FILES['image']['name'] );
				
			} catch (Exception $e) 
			{
		  
			}
	        			
			$image = str_replace(" ","_",$_FILES['image']['name']);
		}
		return $image;
	}
	
	public static function createImageFolder()
	{
		$banner_image_path = Mage::getBaseDir('media') . DS .'banners';
		if(!is_dir($banner_image_path))
		{
			try{
				
				mkdir($banner_image_path);
				chmod($banner_image_path,0777);
			
			} catch(Exception $e) {
			
			}
		}
	}
	
	public static function deleteImageFile($image)
	{
		
		if(!$image)
		{
			return;
		}
		$banner_image_path = Mage::getBaseDir('media') . DS .'banners'.DS.$image;
		
		if(!file_exists($banner_image_path))
		{
			return;
		}
		try{
			
			unlink($banner_image_path);
			
		} catch (Exception $e) {
		
		}
	}
	
	public static function getArrClient()
	{
		$arrClient = array();
		$collection = Mage::getModel('banner/client')->getCollection();	
		foreach($collection as $client)
		{
			$arrClient[$client->getId()] = $client->getName();
		}
		return $arrClient;
	}

	public static function getArrCategory()
	{
		$arrCategory = array();
		$collection = Mage::getModel('banner/category')->getCollection();	
		foreach($collection as $category)
		{
			$arrCategory[$category->getId()] = $category->getName();
		}
		return $arrCategory;
	}	
	
	
	/*
	* new new new
	*/
	
	/*
     * Needed to display block position select in backend
     * @return array Returns multi-level array containin block groups and key => value pairs with 'label' and 'value' keys
     */
    public function getBlockIdsToOptionsArray()
    {   
		return array(
			/*array(
                'label' => $this->__('Position Sidebar Right Top & Bottom will not display on Home Page & Category Page'),
                'value' => array(
                    array('value' => '', 'label' => ''),
                )),	*/
			array(
                'label' => $this->__('General (will be displayed on all pages)'),
                'value' => array(
                    array('value' => 'content-top',         'label' => $this->__('Content Top')),
                    array('value' => 'sidebar-right-top',   'label' => $this->__('Sidebar Right Top')),
                    array('value' => 'sidebar-right-bottom','label' => $this->__('Sidebar Right Bottom')),
                    array('value' => 'sidebar-left-top',    'label' => $this->__('Sidebar Left Top')),
                    array('value' => 'sidebar-left-bottom', 'label' => $this->__('Sidebar Left Bottom')),
                   // array('value' => 'menu-top',            'label' => $this->__('Menu top')),
                    //array('value' => 'menu-bottom',         'label' => $this->__('Menu bottom')),
                    array('value' => 'page-bottom',         'label' => $this->__('Page Bottom')),
                )),
            array(
                'label' => $this->__('Home Page'),
                'value' => array(
					 array('value' => 'cms-page-content-top', 	'label' => $this->__('Homepage Content Top')),
                	 array('value' => 'cms-page-right-top', 	'label' => $this->__('Homepage Sidebar Right Top')),
					 array('value' => 'cms-page-right-bottom', 	'label' => $this->__('Homepage Sidebar Right Bottom')),
					 array('value' => 'cms-page-left-top', 		'label' => $this->__('Homepage Sidebar Left Top')),
					 array('value' => 'cms-page-left-bottom', 	'label' => $this->__('Homepage Sidebar Left Bottom')),
					 array('value' => 'cms-page-bottom', 		'label' => $this->__('Homepage Bottom')),
                )),			
           /* array(
                'label' => $this->__('Default for using in CMS page template'),
                'value' => array(
                    array('value' => 'custom',               'label' => $this->__('Custom')),
                )),*/
           
            array(
                'label' => $this->__('Catalog and Product'),
                'value' => array(
                    array('value' => 'catalog-content-top',         'label' => $this->__('Catalog Content Top')),
                    array('value' => 'catalog-sidebar-right-top',   'label' => $this->__('Catalog Sidebar Right Top')),
                    array('value' => 'catalog-sidebar-right-bottom','label' => $this->__('Catalog Sidebar Right Bottom')),
                    array('value' => 'catalog-sidebar-left-top',    'label' => $this->__('Catalog Sidebar Left Top')),
                    array('value' => 'catalog-sidebar-left-bottom', 'label' => $this->__('Catalog Sidebar Left Bottom')),
                    //array('value' => 'catalog-menu-top',            'label' => $this->__('Catalog Menu Top')),
                   // array('value' => 'catalog-menu-bottom',         'label' => $this->__('Catalog Menu Bottom')),
                    array('value' => 'catalog-page-bottom',         'label' => $this->__('Catalog Page Bottom')),
                )),
			array(
                'label' => $this->__('Category Only'),
                'value' => array(
                    array('value' => 'category-content-top',         'label' => $this->__('Category Content Top')),
                    array('value' => 'category-sidebar-right-top',   'label' => $this->__('Category Sidebar Right Top')),
                    array('value' => 'category-sidebar-right-bottom','label' => $this->__('Category Sidebar Right Bottom')),
                    array('value' => 'category-sidebar-left-top',    'label' => $this->__('Category Sidebar Left Top')),
                    array('value' => 'category-sidebar-left-bottom', 'label' => $this->__('Category Sidebar Left Bottom')),
                    //array('value' => 'product-menu-top',            'label' => $this->__('Product Menu Top')),
                    //array('value' => 'product-menu-bottom',         'label' => $this->__('Product Menu Bottom')),
                    array('value' => 'category-page-bottom',         'label' => $this->__('Category Page Bottom')),
                )),
            array(
                'label' => $this->__('Product Only'),
                'value' => array(
                    array('value' => 'product-content-top',         'label' => $this->__('Product Content Top')),
                    array('value' => 'product-sidebar-right-top',   'label' => $this->__('Product Sidebar Right Top')),
                    array('value' => 'product-sidebar-right-bottom','label' => $this->__('Product Sidebar Right Bottom')),
                    array('value' => 'product-sidebar-left-top',    'label' => $this->__('Product Sidebar Left Top')),
                    array('value' => 'product-sidebar-left-bottom', 'label' => $this->__('Product Sidebar Left Bottom')),
                    //array('value' => 'product-menu-top',            'label' => $this->__('Product Menu Top')),
                    //array('value' => 'product-menu-bottom',         'label' => $this->__('Product Menu Bottom')),
                    array('value' => 'product-page-bottom',         'label' => $this->__('Product Page Bottom')),
                )),
			array(
                'label' => $this->__('Cart Only'),
                'value' => array(
                    array('value' => 'cart-content-top',            'label' => $this->__('Cart Content Top')),
                    array('value' => 'cart-right-top',        		'label' => $this->__('Cart Sidebar Right Top')),
					array('value' => 'cart-right-bottom',           'label' => $this->__('Cart Sidebar Right Bottom')),
                    array('value' => 'cart-left-top',        		'label' => $this->__('Cart Sidebar Left Top')),
					array('value' => 'cart-left-bottom',            'label' => $this->__('Cart Sidebar Left Bottom')),
                	array('value' => 'cart-page-bottom',        	'label' => $this->__('Cart Page Bottom')),
                )),
			array(
                'label' => $this->__('Checkout Only'),
                'value' => array(
                    array('value' => 'checkout-content-top',        'label' => $this->__('Checkout Content Top')),
                    array('value' => 'checkout-right-top',        	'label' => $this->__('Checkout Sidebar Right Top')),
					array('value' => 'checkout-right-bottom',       'label' => $this->__('Checkout Sidebar Right Bottom')),
                    array('value' => 'checkout-page-bottom',       	'label' => $this->__('Checkout Page Bottom')),
                )), 
            array(
                'label' => $this->__('Customer Only'),
                'value' => array(
                    array('value' => 'customer-content-top',        'label' => $this->__('Customer Content Top')),
					array('value' => 'customer-right-top',        	'label' => $this->__('Customer Sidebar Right Top')),
					array('value' => 'customer-right-bottom',       'label' => $this->__('Customer Sidebar Right Bottom')),
					array('value' => 'customer-left-top',        	'label' => $this->__('Customer Sidebar Left Top')),
					array('value' => 'customer-left-bottom',        'label' => $this->__('Customer Sidebar Left Bottom')),
					array('value' => 'customer-page-bottom',        'label' => $this->__('Customer Page Bottom')),
                )),
			 array(
                'label' => $this->__('CMS Only'),
                'value' => array(
                    array('value' => 'cms-content-top',        	'label' => $this->__('CMS Content Top')),
					array('value' => 'cms-right-top',        	'label' => $this->__('CMS Sidebar Right Top')),
					array('value' => 'cms-right-bottom',      	'label' => $this->__('CMS Sidebar Right Bottom')),
					array('value' => 'cms-left-top',        	'label' => $this->__('CMS Sidebar Left Top')),
					array('value' => 'cms-left-bottom',        	'label' => $this->__('CMS Sidebar Left Bottom')),
					array('value' => 'cms-bottom',        		'label' => $this->__('CMS Page Bottom')),
                )),
           
        );
    }
	public static function getUrl($route='', $params=array())
    {
        return Mage::getModel('adminhtml/url')->getUrl($route, $params);
    }
}
