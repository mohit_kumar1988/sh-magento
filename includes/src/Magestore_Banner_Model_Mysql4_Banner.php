<?php

class Magestore_Banner_Model_Mysql4_Banner extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the banner_id refers to the key field in your database table.
        $this->_init('banner/banner', 'id');
    }
	
	public function getListBannerOfBlock($block)
	{		
		try{
			$randomise = $block['sort_method']?false:true;
		
			$today = date("Y-m-d");		
			// Fetching the block to display
			$select = $this->_getReadAdapter()->select()
					->from($this->getTable('banner'), array('*',$randomise?'Rand() as ordering':''))
					->where('client_id=?',$block['client_id'])
					->where('category_id=?',$block['category_id'])
					->where('status=?',1)
					->where('startdate <= ?',$today)
					->where('enddate >= ?',$today)
					->limit($block['num_banner'],0)
					->order("ordering","ASC")
					;
		
			$items = $this->_getReadAdapter()->fetchAll($select);	
			// echo '<pre>';
			//print_r($items);

			// getting block banner & category id  
			$bannerid = $items[0]['id'];
			$show_banner = $items[0]['banner_cond'];
			$category_id = $items[0]['category_id'];
			
			// Fetching all the banner on the basis of category id, that we are using to in checking banner condition
			$selectCategory = $this->_getReadAdapter()->select()
					->from($this->getTable('banner'), array('*',$randomise?'Rand() as ordering':''))
					->where('category_id=?',$category_id)
					->where('status=?',1);
			$_itemsCat = $this->_getReadAdapter()->fetchAll($selectCategory);	
			
			foreach($_itemsCat as $values){ 
				$_bannerId[] = $values['id']; // all banner ids
			}

			// fetching and checking  banner conditions 
			//$ccheck = $this->bannerCondition($bannerid);
			$counT = count($_bannerId);
			
			$ccheck = $this->bannerCondition( $category_id , $counT ,$show_banner);
			
			// display check for banner
			if($ccheck) {
				$this->impress($items);
				return $items;
			}
		} catch (Exception $e){
			return null;	
		}
  	}

    // Function for getting Attribute Name but not used till now
    public function getAttributeNameForBanner($attr_id) {
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $banner_condition_attrid = $write->query("select entity_type_code from eav_entity_type where entity_type_id=$attr_id");
        $banner_condition_attrid_row = $banner_condition_attrid->fetch();
        return $banner_condition_attrid_row['entity_type_code'];
    } 
    
    // Function for getting attribute code
    public function getAttributeCodeNameForBanner($attr_codeid) {
      $write = Mage::getSingleton('core/resource')->getConnection('core_write');
      $banner_condition_attrcodeid = $write->query("select attribute_code from eav_attribute where attribute_id=$attr_codeid");
      $banner_condition_attrcodeid_row = $banner_condition_attrcodeid->fetch();
      return $banner_condition_attrcodeid_row['attribute_code'];
    } 


     // Function for checking banner conditions and return true or false
     public function bannerCondition($cid , $_counT, $show_banner) { 
		
		// Checking for banner having condtion or not, other banner will not display
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$_banner_condition = $write->query("select * from bannercondition where category_id=$cid AND status =1 group by banner_id order by sort_order ");
		$banner_condition_count = $_banner_condition ->rowCount();
		if( $banner_condition_count == $_counT ){
			$_count = true;
		}
		// Fetching all the banner condition 
		$banner_condition = $write->query("select * from bannercondition where category_id=$cid AND status =1 order by sort_order ");
		$condition_count = $banner_condition->rowCount();
		
		// Whether to check the condition, if yes then it will check the condition

		$theme = Mage::getSingleton('core/design_package')->getTheme('frontend');
		$pageId = Mage::getSingleton('cms/page')->getId();
		$curTheme ='';
		if($pageId){
			$_page = Mage::getModel('cms/page')->load($pageId);
			$curTheme =  $_page->getCustomTheme();

		}

		if($theme !='iphone' && $curTheme != 'curvecommerce/collectorsclub'){
		
			if($show_banner){		
			// Condition Starts Here
				if($condition_count && $_count ) {  
				    $final_check = false;
				    $tcount = 0;
				    $rcount = 0; 
				    $cond_str = "";  
				    $cond_value = Array();
				    $cond_op = Array();
					while ( $row = $banner_condition->fetch()) {
						//echo "<br>";
						//echo "<pre>";
						//print_r( $row); 

						//get tablenames
						//get attrbute code names;

						$check_condition = $this->getConditionResult($row); 
						if($check_condition) {
							$check_condition=1;
						}else { 
							$check_condition=0; 
						}
						$cond_value[]=$check_condition; 

						if($row['attr_cond']=='OR') {
							$cond_op[] = '||';
						}
						if($row['attr_cond']=='AND') {
							$cond_op[] = '&&';
						}


				    }         

					for($i=0 ; $i<count($cond_value); $i++){

						if($i==0) {
							$cond_str =   'var_export('.$cond_value[$i].', TRUE)'; 
						} 
						else {

							$cond_str .=$cond_op[$i-1].'var_export('.$cond_value[$i].', TRUE)';  ;
						}      
					} 
				  
					if(count($cond_value)==1){
						if($cond_value[0]==1){ 
							$final_check = true;
						} 
						else {
							$final_check = false;
						} 
					} else if(count($cond_value)>1) {
				       
						if(eval('return (' . $cond_str . ');')) {

							$final_check = true;
						}
						else {
							$final_check = false;

						}
				    }     
					//echo $final_check;
					return $final_check;
				}  // end of condition_count
				else {
					return false;
				}
			}else{
				return true;
			}
		}else{
				return false;
		}
    }

    // Function for checking between result and returns true or false
    public function checkBtwCondition($var1, $var2, $var3) {
        if($var1>$var2 && $var3>$var1) {
         	return true;
        }else {
        	return false;
        }
    }
    

    function checkDateTimeFormat($dateTime)
    {
            if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $dateTime, $matches) || preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $dateTime, $matches) )
                    { 
                                if (checkdate($matches[2], $matches[3], $matches[1]))
                                            { 
                                                            return true; 
                                                                    } 
                                    } 
                return false;
    }  

    // Function for checking condition relations and returns true or false
    public function num_cond ($var1=NULL, $op, $var2=NULL, $var3=NULL) {


        if($this->checkDateTimeFormat($var1)){

            $var1 = strtotime($var1);
            $var2 = strtotime($var2);
            $var3 = strtotime($var3);
        
        }


		switch ($op) {
			case "==":  return $var1 == $var2;
			case "!=":  return $var1 != $var2;
			case ">=":  return $var1 >= $var2;
			case "<=":  return $var1 <= $var2;
			case ">":   return $var1 >  $var2;
			case "<":   return $var1 <  $var2;
			case "btw": return $this->checkBtwCondition($var1, $var2, $var3);
			default:    return true;
		}   
    }

    // Function to check for attribute exists and returns model name
    public function getModelNameGivenAttrId($attr_cid) {
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$banner_attrcodeid = $write->query("select t.entity_model as model from eav_attribute as a, eav_entity_type as t   where a.entity_type_id =     t.entity_type_id and  a.attribute_id='$attr_cid'");
		$banner_attrcodeid_row = $banner_attrcodeid->fetch();
		return $banner_attrcodeid_row['model'];
    }

    // Function to check condition and returns reuslt as true or false
    public function getConditionResult($condition) {
        
		$attr_codename = $this->getAttributeCodeNameForBanner($condition['attribute_id']) ;
		$attr_code_id = $condition['attribute_id'] ;

		$modelname_condition_attrid =  $this->getModelNameGivenAttrId($attr_code_id) ;

		$attrtypeid = $condition['entity_type_id'];
		// Condition for checking customer
		$cus_details='';

		if($attrtypeid==1 ) { // Customer Attribute
		   	$logged_customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
			// if customer exists
			if($logged_customer_id) {
				$cus_details = Mage::getModel($modelname_condition_attrid)->load($logged_customer_id)->getData();
			 	//print_r($cus_details);
			}              
		}

		if( $attrtypeid==2) { // Customer Address Attribute
			$logged_customer_id = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling();
			// if customer exists
			if($logged_customer_id) {
				$cus_details = Mage::getModel($modelname_condition_attrid)->load($logged_customer_id)->getData();
				// print_r($cus_details);
			}              
		}

		// Condition for checking catalog  category
		if($attrtypeid==3) { // Category Attribute
			if( Mage::registry('current_category') ) {
				$current_category_id =   Mage::registry('current_category')->getId()   ;
			}	
			$cus_details = Mage::getModel($modelname_condition_attrid)->load($current_category_id)->getData();
			//print_r($cus_details);
		}

		// Condition for checking catalog product
		if( $attrtypeid==4) { // Products Attribute
			if( Mage::registry('current_product') ) {
				$current_product_id =   Mage::registry('current_product')->getId()   ;
			}
			$cus_details = Mage::getModel($modelname_condition_attrid)->load($current_product_id)->getData();
			//print_r($cus_details);
		}

		// echo $condition['attrcodeid'] ;
		$attr_codeidvalue =  $condition['attr_value'] ;
		$attr_codeidvalue2 =  $condition['attr_value2'] ;
		$cus_val =  $cus_details[$attr_codename];
		$attr_relation =  $condition['relation'] ;

		$rel_syb = '';
		if($attr_relation=='Greater than') {  $rel_syb = ">" ; }
		if($attr_relation=='Equals') {  $rel_syb = "==" ; }
		if($attr_relation=='Not Equals') {  $rel_syb = "!=" ; }
		if($attr_relation=='Lesser than') {  $rel_syb = "<" ; }
		if($attr_relation=='Greater than Equals') {  $rel_syb = ">=" ; }
		if($attr_relation=='Lesser than Equals') {  $rel_syb = "<=" ; }
		// if($attr_relation=='Like') {  $rel_syb = "like" ; }
		if($attr_relation=='Between') {  $rel_syb = "btw" ; }

		$con_final =  $this->num_cond(  $cus_val  , $rel_syb,  $attr_codeidvalue, $attr_codeidvalue2);

		if($con_final) {
			return true;
		}
		else {
			return false;
		}
    }

	
	public function impress($list)
	{		
		for($i = 0; $i < count($list); $i++)
		{
			$banner = $list[$i];
			$banner['impmade']++;
			$expire = ($banner['impmade'] >= $banner['imptotal']) && ($banner['imptotal'] != 0);
			$write = $this->_getWriteAdapter();
			$write->beginTransaction();	
			$data['impmade'] = $banner['impmade'];
			if($expire)
			{
				$data['status'] = '2';
			}
			$write->update($this->getTable("banner"),$data,"id=" . $banner['id']);
			$write->commit();				
		}
	}
}
