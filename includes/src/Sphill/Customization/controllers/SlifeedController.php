<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */


class Sphill_Customization_SlifeedController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $fileName = Mage::getModel('sphill_customization/slifeed')->getFilename();
        $content = file_get_contents($fileName);
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'text/plain', true)
            ->setHeader('Content-Length', strlen($content))
            ->setHeader('Content-Disposition', 'attachment; filename=' . basename($fileName))
            ->setHeader('Last-Modified', date('r'));
        if (!is_null($content)) {
            $this->getResponse()->setBody($content);
        }
    }

    /**
     * FOR TEST ONLY
     *
     */
    public function testAction()
    {
        Mage::getModel('sphill_customization/observer')->generateSliFeed();
    }

    /**
     * FOR TEST ONLY
     *
     */
    public function exceptionAction()
    {
        $name = $email;
    }
}
