<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Sphill_Customization_Block_Product_SpecialPrice extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        if ($this->_getData('sku')) {
            $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->_getData('sku'));
            if ($_product) {
                $_specialPrice = $_product->getData('special_price');
                if (!is_null($_specialPrice)) {
                    if ($this->_IsStoreDateInInterval(Mage::app()->getStore(), $_product->getData('special_from_date'), $_product->getData('special_to_date'))) {
                        $_prefix = $this->_getData('prefix') ? $this->_getData('prefix') : '';
                        $_sufix  = $this->_getData('sufix') ? $this->_getData('sufix') : '';
                        return $_prefix . Mage::app()->getStore()->formatPrice($_specialPrice) . $_sufix;
                    }
                }
            }
        }
        return '';
    }

    protected function _IsStoreDateInInterval($store, $dateFrom = null, $dateTo = null)
    {
        if (!$store instanceof Mage_Core_Model_Store) {
            $store = Mage::app()->getStore($store);
        }

        $storeTimeStamp = strtotime(Mage::app()->getLocale()->storeDate($store, null, true));
        $fromTimeStamp  = strtotime($dateFrom);
        $toTimeStamp    = strtotime($dateTo);

        if ($dateTo) {
            // fix date YYYY-MM-DD 00:00:00 to YYYY-MM-DD 23:59:59
            $toTimeStamp += 86400;
        }

        $result = false;
        if (!is_empty_date($dateFrom) && $storeTimeStamp < $fromTimeStamp) {
        }
        elseif (!is_empty_date($dateTo) && $storeTimeStamp > $toTimeStamp) {
        }
        else {
            $result = true;
        }

        return $result;
    }
}
