<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once('lib/Varien/Convert/Parser/Xml/Excel.php');

class Sphill_Customization_Helper_Excel extends Varien_Convert_Parser_Xml_Excel
{

    public function unparse()
    {
        if ($wsName = $this->getVar('single_sheet')) {
            $data = array($wsName => $this->getData());
        } else {
            $data = $this->getData();
        }

        $this->validateDataGrid();

        $xml = '<'.'?xml version="1.0"?'.'><'.'?mso-application progid="Excel.Sheet"?'.'>
<Workbook xmlns:x="urn:schemas-microsoft-com:office:excel"
  xmlns="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">';

        if (is_array($data)) {
            foreach ($data as $wsName=>$wsData) {
                if (!is_array($wsData)) {
                    continue;
                }
                $fields = $this->getGridFields($wsData);

                $xml .= '<Worksheet ss:Name="'.$wsName.'"><Table>';
                if ($this->getVar('fieldnames')) {
                    $xml .= '<Row>';
                    foreach ($fields as $fieldName) {
                        $xml .= '<Cell><Data ss:Type="String">'.$fieldName.'</Data></Cell>';
                    }
                    $xml .= '</Row>';
                }
                foreach ($wsData as $i=>$row) {
                    if (!is_array($row)) {
                        continue;
                    }
                    $xml .= '<Row>';
                    $urlPattern = '{
                      \\b
                      # Match the leading part (proto://hostname, or just hostname)
                      (
                        # http://, or https:// leading part
                        (https?)://[-\\w]+(\\.\\w[-\\w]*)+
                      )

                      # Allow an optional port number
                      ( : \\d+ )?

                      # The rest of the URL is optional, and begins with /
                      (
                        /
                        # The rest are heuristics for what seems to work well
                        [^.!,?;"\\\'<>()\[\]\{\}\s\x7F-\\xFF]*
                        (
                          [.!,?]+ [^.!,?;"\\\'<>()\\[\\]\{\\}\s\\x7F-\\xFF]+
                        )*
                      )?
                    }ix';
                    $aPattern = '{<a[^>]*>(.*?)</a>}ix';
                    foreach ($fields as $fieldName) {
                        $data = isset($row[$fieldName]) ? $row[$fieldName] : '';
                        $cellHref = '';
                        if (preg_match($urlPattern, $data, $regs)) {
                            $hrefArr = explode('<br/>',$data);
                            foreach ($hrefArr as $href) {
                                if (preg_match($urlPattern, $href, $regs)) {
                                    $_data = $regs[0];
                                    if (preg_match($aPattern, $href, $res)) {
                                        $_data = $res[1];
                                    }
                                    $cellHref = ' ss:HRef="'.$regs[0].'"';
                                    $xml .= '<Cell'.$cellHref.'><Data ss:Type="String">'.$_data.'</Data></Cell>';
                                }
                            }
                        } else {
                            $xml .= '<Cell'.$cellHref.'><Data ss:Type="String">'.$data.'</Data></Cell>';
                        }
                    }
                    $xml .= '</Row>';
                }
                $xml .= '</Table></Worksheet>';
            }
        }

        $xml .= '</Workbook>';

        $this->setData($xml);

        return $this;
    }
}
