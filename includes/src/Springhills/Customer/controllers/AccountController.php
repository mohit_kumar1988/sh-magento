<?php
require_once 'app/code/core/Mage/Customer/controllers/AccountController.php';
require_once 'redback/RedbackServices.php';
class Springhills_Customer_AccountController extends Mage_Customer_AccountController
{


   /**
     * Default customer account page
     */
    public function indexAction()
    {
	//If session is exists then redirect to onestepcheckout
        $session = Mage::getSingleton("core/session");
	$arrySession=$session->getData();
	if( !empty($arrySession['cust_number'])){
		$redirectUrl = Mage::getBaseUrl().'onestepcheckout';
		$this->_redirectUrl($redirectUrl);
	}
        $this->getLayout()->getBlock('head')->setTitle($this->__('My Account'));
        $this->renderLayout();
    }

   /**
     * Customer login form page
     */
    public function loginAction()
    { 
	//If session is exists then redirect to myorder
        $session = Mage::getSingleton("core/session");
	$arrySession=$session->getData();
	if( !empty($arrySession['cust_number'])){
		$redirectUrl = Mage::getBaseUrl().'myorder';
		$this->_redirectUrl($redirectUrl);
	} 
        $this->getResponse()->setHeader('Login-Required', 'true');

        $this->loadLayout();

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
  
 }
   /**
     * Login post action
     */
    public function loginPostAction()
    {
       
       if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session = Mage::getSingleton("core/session");
        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
		//REDBACK API CALL FOR AUTHENTICATION OF CUSTOMER  60606    '49150029'
	       $objRedBackApi = new RedbackServices();
		$email=$login['username'];
		$pin =$login['password'];
		$zip =$login['password'];
		$custNo =$login['accno'];
		//giving prority to custnumber than email
	       if(!empty($custNo)){
	       		$pin='';
			$email='';	
		}else{
			$custNo='';
			$zip='';
		}
	       $params =array(	'CustNumber'	=>	$custNo,
				'Title'	=>	'4'	,
				'CustName'	=>	''	,
				'CustAdd1'	=>	''	,
				'CustAdd2'	=>	''	,
				'CustState'	=>	''	,
				'CustCity'	=>	''	,
				'CustZip'	=>	$zip,
				'CustCountry'	=>	''	,
				'CustPhone'	=>	''	,
				'CustFax'	=>	''	,
				'CustEmail'	=>	$email,
				'CustPin'	=>	$pin,
				'CustClubFlag'	=>	''	,
				'CustClubDisc'	=>	''	,
				'CustClubDate'	=>	''	,
				'CustPinHint'	=>	''	,
				'CustOptInFlag'	=>	''	,
				'CustError' => '',
				'CustMessage' => '',
				'CustOrder'	=>	''	,
				'CustFirstName'	=>	''	,
				'CustLastName'	=>	''	,
				'ClubNumber'	=>	'');

		$responseCustomer = $objRedBackApi->CustMasterAccessEmailZip($params);
		//If Customer error code is zero then successfully logged
		if($responseCustomer['CustError']=='0'){
			// set cookie for email subscription
	                Mage::getModel('core/cookie')->set('email_id', $login['username']);
			$customerNumer= $responseCustomer['CustNumber'];
			$customerArray =array();
			$customerArray[$customerNumer]['CustNumber']=$responseCustomer['CustNumber'];
			$customerArray[$customerNumer]['CustAdd1']=$responseCustomer['CustAdd1'];
			$customerArray[$customerNumer]['CustAdd2']=$responseCustomer['CustAdd2'];
			$customerArray[$customerNumer]['State']=$responseCustomer['CustState'];	
			if(empty($responseCustomer['CustState'])){
				$checkResource =  Mage::getResourceSingleton('checkout/cart');
				$stateCode = $checkResource->get_zip_info($responseCustomer['CustZip']);
				$customerArray[$customerNumer]['State']=$stateCode['state'];	
			}
			$customerArray[$customerNumer]['City']=$responseCustomer['CustCity'];
			$customerArray[$customerNumer]['Country']=$responseCustomer['CustCountry'];
			$customerArray[$customerNumer]['Phone']=str_replace(".","",$responseCustomer['CustPhone']);
			$customerArray[$customerNumer]['CustFax']=$responseCustomer['CustFax'];
			$customerArray[$customerNumer]['Email']=$responseCustomer['CustEmail'];
			if(empty($responseCustomer['CustEmail']))
				$customerArray[$customerNumer]['Email']=$login['username'];
			$customerArray[$customerNumer]['CustPin']=$responseCustomer['CustZip'];
			$customerArray[$customerNumer]['FirstName']=$responseCustomer['CustFirstName'];
			$customerArray[$customerNumer]['LastName']=$responseCustomer['CustLastName'];
			$customerArray[$customerNumer]['CustZip']=$responseCustomer['CustZip'];
			$session->setCustNumber($customerArray[$customerNumer]['CustNumber']);
			$session->setCustAdd1($customerArray[$customerNumer]['CustAdd1']);
			$session->setCustAdd2($customerArray[$customerNumer]['CustAdd2']);
			$session->setCustState($customerArray[$customerNumer]['State']);
			$session->setCustCity($customerArray[$customerNumer]['City']);
			$session->setCustCountry($customerArray[$customerNumer]['Country']);
			$session->setCustPhone($customerArray[$customerNumer]['Phone']);
			$session->setCustFax($customerArray[$customerNumer]['CustFax']);
			$session->setCustEmail($customerArray[$customerNumer]['Email']);
			$session->setCustPin($customerArray[$customerNumer]['CustPin']);
			$session->setCustFirstName($customerArray[$customerNumer]['FirstName']);
			$session->setCustLastName($customerArray[$customerNumer]['LastName']);
			$session->setCustZip($customerArray[$customerNumer]['CustZip']);
			Mage::getSingleton('core/session')->addSuccess('Welcome ! You are successfully logged in.');
			if(isset($login['action']) && $login['action']=='login'){
				$redirectUrl = Mage::getBaseUrl().'myorder';
			}else{
				$redirectUrl = Mage::getBaseUrl().'onestepcheckout';
			}
			$this->_redirectUrl($redirectUrl);
		}else {
			if(!$responseCustomer){
				if(isset($login['action']) && $login['action']=='login'){
				Mage::getSingleton('core/session')->addError("We're sorry. It looks like there was a problem finding your information at this time.Please try again later.");
			}else{
				Mage::getSingleton('core/session')->addError("We're sorry. It looks like there was a problem finding your information at this time. Please feel free to continue your purchase by selecting \"click here to purchase as a guest\" and continuing through the normal checkout process.  We will add this order to your existing account order history.  If you have any questions, feel free to call our customer service at 513-354-1509");
			}
				
			}else{
				Mage::getSingleton('core/session')->addError($responseCustomer['CustMessage']);
			}
			if(isset($login['action']) && $login['action']=='login'){
				$redirectUrl = Mage::getBaseUrl().'customer/account/login';
			}else{
				$redirectUrl = Mage::getBaseUrl().'checkout/onepage/';
			}
			$this->_redirectUrl($redirectUrl);
            }
		
	}

    }

   

   
}
