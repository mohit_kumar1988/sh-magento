<?php

class Springhills_Freecatalog_Adminhtml_FreecatalogController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("freecatalog/freecatalog")->_addBreadcrumb(Mage::helper("adminhtml")->__("Freecatalog  Manager"),Mage::helper("adminhtml")->__("Freecatalog Manager"));
				return $this;
		}
		public function indexAction() 
		{
				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{
				$brandsId = $this->getRequest()->getParam("id");
				$brandsModel = Mage::getModel("freecatalog/freecatalog")->load($brandsId);
				if ($brandsModel->getId() || $brandsId == 0) {
					Mage::register("freecatalog_data", $brandsModel);
					$this->loadLayout();
					$this->_setActiveMenu("freecatalog/freecatalog");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Freecatalog Manager"), Mage::helper("adminhtml")->__("Freecatalog Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Freecatalog Description"), Mage::helper("adminhtml")->__("Freecatalog Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("freecatalog/adminhtml_freecatalog_edit"))->_addLeft($this->getLayout()->createBlock("freecatalog/adminhtml_freecatalog_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("freecatalog")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("freecatalog/freecatalog")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("freecatalog_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("freecatalog/freecatalog");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Freecatalog Manager"), Mage::helper("adminhtml")->__("Freecatalog Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Freecatalog Description"), Mage::helper("adminhtml")->__("Freecatalog Description"));


		$this->_addContent($this->getLayout()->createBlock("freecatalog/adminhtml_freecatalog_edit"))->_addLeft($this->getLayout()->createBlock("freecatalog/adminhtml_freecatalog_edit_tabs"));

		$this->renderLayout();

		       // $this->_forward("edit");
		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {
			
						$post_data['updated_at']=date('Y-m-d H:i:s');
						$brandsModel = Mage::getModel("freecatalog/freecatalog")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Freecatalog was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setFreecatalogData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $brandsModel->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setFreecatalogData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$brandsModel = Mage::getModel("freecatalog/freecatalog");
						$brandsModel->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}
}
