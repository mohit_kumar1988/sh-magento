<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Springhills_Catalog_Block_Product_List extends Mage_Catalog_Block_Product_List
{ 
    protected function _getProductCollection()
    {			
		$zoneCookie = Mage::getModel('core/cookie')->get('zone');
		$actual_zone = substr($zoneCookie,0,-1);	
		$zone = 'zone'.$actual_zone;
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }
            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }
            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
            // $this->_productCollection = $layer->getProductCollection();
			$order = $this->getRequest()->getParam('order');
			$sortByAttr = '';
			$sortFlag = 0;				

			if( $order == "ordered_qty" ){
				$sortByAttr = 'ordered_qty'; // for future puppose when bestseller is based on sold quantity
			} elseif ( $order == "name" ){
				$sortByAttr = 'name';
			} elseif ($order == "price" ){
				$sortByAttr = 'sort_price';
			} elseif ( $order == "till_date_revenue" ) {
				$sortByAttr = 'till_date_revenue'; // only for launch, used best seller revenue of product
			} else {
				$sortFlag = 1;	
				$sortByAttr = $zone;
			}
			if( $zoneCookie ==null ) {				
				//echo "default Sort".$sortByAttr."####";
				if( $sortByAttr!='' && $sortByAttr!='zone'){
					if( $order == "ordered_qty" ){
					//echo "Order qty Sort";
						$this->_productCollection = $layer->getProductCollection()
											->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
											->addAttributeToSort('inventory_in_stock','desc')	
											->addAttributeToSort('future_season_message', 'desc')
											->addAttributeToSort('name');
					}else if( $order == "till_date_revenue" ){
					//echo "revenue Sort";
						$this->_productCollection = $layer->getProductCollection()
											->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
											->addAttributeToSort('inventory_in_stock','desc')
											->addAttributeToSort('future_season_message', 'desc')
											->addAttributeToSort($sortByAttr,'desc')
											->addAttributeToSort('name');
					}else if($order == "name" ){
					//echo "name Sort";
						$this->_productCollection = $layer->getProductCollection()
											->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
											->addAttributeToSort('inventory_in_stock','desc')
											->addAttributeToSort('future_season_message', 'desc')
											->addAttributeToSort($sortByAttr);
					}else{
					//echo "sort_price Sort";
						$this->_productCollection = $layer->getProductCollection()
											->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
											->addAttributeToSort('inventory_in_stock','desc')
											->addAttributeToSort('future_season_message', 'desc')
											->addAttributeToSort($sortByAttr)
											->addAttributeToSort('name');
					}
				}else {
					// Default Sort By Best Seller
					//echo "Default revenue Sort";
					$this->_productCollection = $layer->getProductCollection()
											->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
											->addAttributeToSort('inventory_in_stock','desc')
											->addAttributeToSort('future_season_message', 'desc')
											->addAttributeToSort('till_date_revenue', 'desc')
											->addAttributeToSort('name');
				}
			} else {				
				///echo "zone Sort";
				if( $order != "name" ){
					if($sortFlag){
						//echo "zone attr Sort";
						$this->_productCollection = $layer->getProductCollection()
										->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
										->addAttributeToFilter("price", array('gt'=> 0))												
										->addAttributeToSort($sortByAttr)
										->addAttributeToSort('inventory_in_stock','desc')
										->addAttributeToSort('future_season_message', 'desc')
										->addAttributeToSort('name');
					}else{
						if( $order != "till_date_revenue" ){
						//echo "price sort zone";
						$this->_productCollection = $layer->getProductCollection()
										->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
										->addAttributeToFilter("price", array('gt'=> 0))
										->addAttributeToSort('inventory_in_stock','desc')
										->addAttributeToSort('future_season_message', 'desc')
										->addAttributeToSort($sortByAttr)
										->addAttributeToSort('name');
						}else{
						//echo "zone revenue Sort";
						$this->_productCollection = $layer->getProductCollection()
										->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
										->addAttributeToFilter("price", array('gt'=> 0))
										->addAttributeToSort('inventory_in_stock','desc')
										->addAttributeToSort('future_season_message', 'desc')
										->addAttributeToSort($sortByAttr,'desc')
										->addAttributeToSort('name');
						}
					}
				}else {
				//echo "zone name Sort";
				$this->_productCollection = $layer->getProductCollection()
											->joinField('inventory_in_stock', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id','is_in_stock>=0', 'left')
											->addAttributeToFilter("price", array('gt'=> 0))
											->addAttributeToSort('inventory_in_stock','desc')
											->addAttributeToSort('future_season_message', 'desc')
											->addAttributeToSort($sortByAttr ,'ASC');
	
				}
			}
			$this->prepareSortableFieldsByCategory($layer->getCurrentCategory());		
		    if ($origCategory) {
		        $layer->setCurrentCategory($origCategory);
		    }			
		}
        return $this->_productCollection;
    }
}
