<?php

class Springhills_Catalog_Model_Config extends Mage_Catalog_Model_Config
{
    
	/**
	 * Retrieve Attributes Used for Sort by as array
	 * key = code, value = name
	 *
	 * @return array
	 */
	public function getAttributeUsedForSortByArray()
	{
		$options = array(
				//'position'  => Mage::helper('catalog')->__('Position'),
				'till_date_revenue'  => Mage::helper('catalog')->__('Best Seller')
		);
		foreach ($this->getAttributesUsedForSortBy() as $attribute) {
			/* @var $attribute Mage_Eav_Model_Entity_Attribute_Abstract */
			$options[$attribute->getAttributeCode()] = $attribute->getStoreLabel();
		}
	
		return $options;
	}
	
}
