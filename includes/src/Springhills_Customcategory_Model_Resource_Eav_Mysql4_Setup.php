<?php
class Springhills_Customcategory_Model_Resource_Eav_Mysql4_Setup  extends Mage_Eav_Model_Entity_Setup
{ 
    //  {{{ getDefaultEntities()

    /**
     * Install a new entity
     *
     * It is very important to set the group index value.
     * If you don't - you will not see the attribute in the form in admin
     *
   
     * @return array entities to install
     * @access public
     */
    public function getDefaultEntities()
    {	
        return array(
            'catalog_category' => array(
                'entity_model'      => 'catalog/category',
                'attribute_model'   => 'catalog/resource_eav_attribute',
                'table'             => 'catalog/category',
                'additional_attribute_table' => 'catalog/eav_attribute',
                'entity_attribute_collection' => 'catalog/category_attribute_collection',
                'attributes' => array(
                    'tag_line' => array(
                        'type'              => 'varchar',
                        'backend'           => '',
                        'frontend'          => '',
                        'label'             => 'Tag Line',
                        'input'             => 'text',
                        'class'             => '',
                        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'visible'           => true,
                        'required'          => false,
                        'user_defined'      => true,
                        'searchable'        => false,
                        'filterable'        => false,
                        'comparable'        => false,
                        'visible_on_front'  => false,
                        'unique'            => false,
                        /**
                         * Need the group index in this array otherwise
                         * the attribute will not appear in the admin form.
                         *
                         * This group name matches the tabs you see in the
                         * admin section - and is case sensitive.
                         *
                         * In versions 1.4 - 1.5, the general tab is a
                         * lowercase 'general', but in 1.6+ it switched
                         * to 'General Information'.   Just look carefully
                         * and you will be alright

                         *
                         * 'group' => 'general' // v1.4-v1.5
                         * 'group' => 'General Information' // v1.6+
                         * 'group' => 'Display Settings'
                         * 'group' => 'Custom Design'
                         *
                         * You can also easily create new tabs by defining
                         * a custom group here
                         *
                         * 'group' => 'New Group Tab Name'
                         */
                        'group'             => 'General',
                    ),
					'is_featured' => array(
                        'type'              => 'int',
                        'backend'           => '',
                        'frontend'          => '',
                        'label'             => 'Is Featured',
                        'input'             => 'select',
                        'class'             => '',
                        'source'            => 'eav/entity_attribute_source_boolean',
                        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'visible'           => true,
                        'required'          => false,
                        'user_defined'      => true,
                        'default'           => 0,
                        'searchable'        => false,
                        'filterable'        => false,
                        'comparable'        => false,
                        'visible_on_front'  => false,
                        'unique'            => false,
                        'group'             => 'General',
                    ),
					'for_plants' => array(
                        'type'              => 'int',
                        'backend'           => '',
                        'frontend'          => '',
                        'label'             => 'For Plant Finder',
                        'input'             => 'select',
                        'class'             => '',
                        'source'            => 'eav/entity_attribute_source_boolean',
                        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'visible'           => true,
                        'required'          => false,
                        'user_defined'      => true,
                        'default'           => 0,
                        'searchable'        => false,
                        'filterable'        => false,
                        'comparable'        => false,
                        'visible_on_front'  => false,
                        'unique'            => false,
                        'group'             => 'General',
                    ),
					'is_left_menu' => array(
                        'type'              => 'int',
                        'backend'           => '',
                        'frontend'          => '',
                        'label'             => 'For Left Menu',
                        'input'             => 'select',
                        'class'             => '',
                        'source'            => 'eav/entity_attribute_source_boolean',
                        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                        'visible'           => true,
                        'required'          => false,
                        'user_defined'      => true,
                        'default'           => 1,
                        'searchable'        => false,
                        'filterable'        => false,
                        'comparable'        => false,
                        'visible_on_front'  => false,
                        'unique'            => false,
                        'group'             => 'General',
                    ),
					
                ),
            ),
        );
    }

    //  }}}
}
