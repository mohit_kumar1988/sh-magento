<?php
class Springhills_Customerservice_Block_Adminhtml_Customerservice_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("customerservice_form", array("legend"=>Mage::helper("customerservice")->__("Item information")));

				$fieldset->addField("name", "text", array(
				"label" => Mage::helper("customerservice")->__("Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "name",
				));
/*
				$fieldset->addField("lastname", "text", array(
				"label" => Mage::helper("customerservice")->__("Last Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "lastname",
				));
				$fieldset->addField("companyname", "text", array(
				"label" => Mage::helper("customerservice")->__("Company Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "companyname",
				)); 
				$fieldset->addField("address1", "text", array(
				"label" => Mage::helper("customerservice")->__("Address1"),
				"class" => "required-entry",
				"required" => true,
				"name" => "address1",
				)); */
                               $fieldset->addField("email", "text", array(
				"label" => Mage::helper("customerservice")->__("Email"),
				"class" => "required-entry validate-email",
				"required" => true,
				"name" => "email",
				));
                                $fieldset->addField("phonenumber", "text", array(
				"label" => Mage::helper("customerservice")->__("PhoneNumber"),
				"class" => "validate-digits validate-number8",
				"name" => "phone",
				));
				$fieldset->addField("message", "text", array(
				"label" => Mage::helper("customerservice")->__("Message"),							
				"name" => "message",
				));
/*
				$fieldset->addField("city", "text", array(
				"label" => Mage::helper("customerservice")->__("City"),
				"class" => "required-entry",
				"required" => true,
				"name" => "city",
				));
				$fieldset->addField("state", "text", array(
				"label" => Mage::helper("customerservice")->__("State"),
				"name" => "state",
				));
				$fieldset->addField("zipcode", "text", array(
				"label" => Mage::helper("customerservice")->__("ZipCode"),
				"class" => "required-entry validate-zip-international",
				"required" => true,
				"name" => "zipcode",
				)); 
				$fieldset->addField('country', 'select', array(
       			 	'name'  => 'country',
        		 	'label'     => 'Country',
					'class'  =>  'validate-select',
					'required' => true,
        			'values'    => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(),
   				 )); */
				
				




				if (Mage::getSingleton("adminhtml/session")->getCustomerserviceData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getCustomerserviceData());
					Mage::getSingleton("adminhtml/session")->setCustomerserviceData(null);
				} 
				elseif(Mage::registry("customerservice_data")) {
				    $form->setValues(Mage::registry("customerservice_data")->getData());
				}
				return parent::_prepareForm();
		}
}
