<?php

class Springhills_Customerservice_Block_Adminhtml_Customerservice_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("customerserviceGrid");
				$this->setDefaultSort("customerservice_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("customerservice/customerservice")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("customerservice_id", array(
				"header" => Mage::helper("customerservice")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "customerservice_id",
				));
				$this->addColumn("name", array(
				"header" => Mage::helper("customerservice")->__("Name"),
				"align" =>"left",
				"index" => "name",
				));

				$this->addColumn("emailaddress", array(
				"header" => Mage::helper("customerservice")->__("Email"),
				"align" =>"left",
				"index" => "email",
				));

				$this->addColumn("companyname", array(
				"header" => Mage::helper("customerservice")->__("Contact"),
				"align" =>"left",
				"index" => "phone",
				));


				$this->addColumn("phonenumber", array(
				"header" => Mage::helper("customerservice")->__("Message"),
				"align" =>"left",
				"index" => "message",
				));


				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}
