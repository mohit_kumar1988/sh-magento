<?php
class Springhills_Freecatalog_Block_Adminhtml_Freecatalog_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("freecatalog_form", array("legend"=>Mage::helper("freecatalog")->__("Item information")));

				$fieldset->addField("firstname", "text", array(
				"label" => Mage::helper("freecatalog")->__("First Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "firstname",
				));
				$fieldset->addField("lastname", "text", array(
				"label" => Mage::helper("freecatalog")->__("Last Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "lastname",
				));
				/*$fieldset->addField("company", "text", array(
				"label" => Mage::helper("freecatalog")->__("Company Name"),
				"class" => "required-entry",
				"required" => true,
				"name" => "company",
				));*/
				$fieldset->addField("address1", "text", array(
				"label" => Mage::helper("freecatalog")->__("Address1"),
				"class" => "required-entry",
				"required" => true,
				"name" => "address1",
				));
				$fieldset->addField("address2", "text", array(
				"label" => Mage::helper("freecatalog")->__("Address2"),							
				"name" => "address2",
				));
				$fieldset->addField("city", "text", array(
				"label" => Mage::helper("freecatalog")->__("City"),
				"class" => "required-entry",
				"required" => true,
				"name" => "city",
				));
				/*$fieldset->addField("state", "text", array(
				"label" => Mage::helper("freecatalog")->__("State"),
				"name" => "state",
				));*/
				
				$stateCollection = Mage::getModel('directory/country')->load('US')->getRegions();
                $states[] = (array(
				    'label' => '... Please Select ...',
				    'value' => ''
		        ));
		        
                foreach ($stateCollection as $state)
				 {  
					$states[] = (array(
					"label" => $state->getName(),
					"value" => $state->getName()
					));  				  
				 }
				$fieldset->addField('state', 'select', array(
       			 	'name'  => 'state',
        		 	'label'     => 'State',
					'class'  =>  'validate-select',
					'required' => true,
        			'values'    => $states
   				 ));
				$fieldset->addField("zipcode", "text", array(
				"label" => Mage::helper("freecatalog")->__("ZipCode"),
				"class" => "required-entry validate-zip-international",
				"required" => true,
				"name" => "zipcode",
				));
				/*$fieldset->addField('country', 'select', array(
       			 	'name'  => 'country',
        		 	'label'     => 'Country',
					'class'  =>  'validate-select',
					'required' => true,
        			'values'    => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(),
   				 ));*/
   				 $fieldset->addField('country', 'hidden', array(
       			 	'name'  => 'country',
        		 	'label'     => 'Country',
					'class'  =>  'validate-select',
					'values'    => 'US',
   				 ));
				$fieldset->addField("email", "text", array(
				"label" => Mage::helper("freecatalog")->__("Email"),
				"class" => "required-entry validate-email",
				"name" => "email",
				));
				$fieldset->addField("phone", "text", array(
				"label" => Mage::helper("freecatalog")->__("Phone"),
				"class" => "validate-digits validate-number8",
				"name" => "phone",
				));
				/*$fieldset->addField("mobile", "text", array(
				"label" => Mage::helper("freecatalog")->__("Special Offer Code"),
				"name" => "mobile",
				));*/

				$fieldset->addField("updated_at", "text", array(
				"name" => "updated_at",
				'style'=>'display:none'
				));



				if (Mage::getSingleton("adminhtml/session")->getFreecatalogData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getFreecatalogData());
					Mage::getSingleton("adminhtml/session")->setFreecatalogData(null);
				} 
				elseif(Mage::registry("freecatalog_data")) {
				    $form->setValues(Mage::registry("freecatalog_data")->getData());
				}
				return parent::_prepareForm();
		}
}
