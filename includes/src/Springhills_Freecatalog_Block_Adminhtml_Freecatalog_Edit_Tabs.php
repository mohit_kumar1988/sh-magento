<?php
class Springhills_Freecatalog_Block_Adminhtml_Freecatalog_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("freecatalog_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("freecatalog")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("freecatalog")->__("Item Information"),
				"title" => Mage::helper("freecatalog")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("freecatalog/adminhtml_freecatalog_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
