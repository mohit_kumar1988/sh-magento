<?php
class Springhills_Freecatalog_Model_Api extends Mage_Api_Model_Resource_Abstract
{
    /**
     * Retrieve list of freecatalog
     *
     * @param array $filters
     * @return array
     */
	public function items($filters=Null)
    {
		$collection = Mage::getModel("freecatalog/freecatalog")->getCollection();
		$collection->addFieldToSelect("*");
		 if (is_array($filters)) {
            try {
                foreach ($filters as $field => $value) {
                    if (isset($this->_filtersMap[$field])) {
                        $field = $this->_filtersMap[$field];
                    }

                    $collection->addFieldToFilter($field, $value);
                }
            } catch (Mage_Core_Exception $e) {
                $this->_fault('filters_invalid', $e->getMessage());
            }
        }

		$i=0;
		foreach($collection as $freecatalog){
			$data[$i]['freecatalog_id'] = $freecatalog->getFreecatalogId();
			$data[$i]['firstname'] = $freecatalog->getFirstname();
			$data[$i]['lastname'] = $freecatalog->getLastname();
			$data[$i]['company'] = $freecatalog->getCompany();
			$data[$i]['address1'] = $freecatalog->getAddress1();
			$data[$i]['address2'] = $freecatalog->getAddress2();
			$data[$i]['city'] = $freecatalog->getCity();
			$data[$i]['state'] = $freecatalog->getState();
			$data[$i]['zipcode'] = $freecatalog->getZipcode();
			$data[$i]['country'] = $freecatalog->getCountry();
			$data[$i]['email'] = $freecatalog->getEmail();
			$data[$i]['phone'] = $freecatalog->getPhone();
			$data[$i]['created_at'] = $freecatalog->getCreatedAt();
			$data[$i]['updated_at'] = $freecatalog->getUpdatedAt();
			$i++;
		}

		return $data;
    }


	/**
     * Fetch catalog request
     *
     * @param int $catalogid
     * @return array 
     */

    public function info($id)
    {
		$freecatalog = Mage::getModel("freecatalog/freecatalog")->load($id);
		
		$data['freecatalog_id'] = $freecatalog->getFreecatalogId();
		$data['firstname'] = $freecatalog->getFirstname();
		$data['lastname'] = $freecatalog->getLastname();
		$data['company'] = $freecatalog->getCompany();
		$data['address1'] = $freecatalog->getAddress1();
		$data['address2'] = $freecatalog->getAddress2();
		$data['city'] = $freecatalog->getCity();
		$data['state'] = $freecatalog->getState();
		$data['zipcode'] = $freecatalog->getZipcode();
		$data['country'] = $freecatalog->getCountry();
		$data['email'] = $freecatalog->getEmail();
		$data['phone'] = $freecatalog->getPhone();
		$data['created_at'] = $freecatalog->getCreatedAt();
		$data['updated_at'] = $freecatalog->getUpdatedAt();			

		return $data;
    }
	

	 /**
     * Create new catalog request
     *
     * @param array $data
     * @return int (request catalog id)
     */

	public function create($data=Null)
    {	  
	   $date = date("Y-m-d H:i:s");
	   $model=Mage::getModel("freecatalog/freecatalog")
				->setFirstname($data['firstname'])
				->setLastname($data['lastname'])
				->setCompany($data['company'])
				->setAddress1($data['address1'])
				->setAddress2($data['address2'])
				->setCity($data['city'])
				->setState($data['state'])
				->setZipcode($data['zipcode'])
				->setCountry($data['country'])
				->setEmail($data['email'])
				->setPhone($data['phone'])
				->setCreatedAt($date)
				->setUpdatedAt($date)
				->save();
		return $model->getId();
		
    }

	/**
     * Update catalog request
     *
     * @param int $catalogid
	 * @param array $data
     * @return string message
     */

	public function update( $freecatalog_id , $data=NUll )
    {
   	   $date = date("Y-m-d H:i:s");
	   $model=Mage::getModel("freecatalog/freecatalog")->load($freecatalog_id)
				->setFirstname($data['firstname'])
				->setLastname($data['lastname'])
				->setCompany($data['company'])
				->setAddress1($data['address1'])
				->setAddress2($data['address2'])
				->setCity($data['city'])
				->setState($data['state'])
				->setZipcode($data['zipcode'])
				->setCountry($data['country'])
				->setEmail($data['email'])
				->setPhone($data['phone'])
				->setUpdatedAt($date)
				->save();
		return "Record updated successfully";		
    }


	/**
     * delete catalog request
     *
	 * @param array $catalogids
     * @return string message 
     */

	public function delete($ids)
    {
		$flg = 0;
		foreach($ids as $id){
		    $resource = Mage::getResourceSingleton('freecatalog/freecatalog');
			$count = $resource->checkCount($id);
			if($count){
				$flg = 1;
				$model = Mage::getModel("freecatalog/freecatalog");
				$model->setId($id)->delete();
			}
		}
		if($flg){
			return "Record(s) deleted successfully" ;
		}else{
			return "There is no record(s) to delete." ;
		}
    }
}
