<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Newsletter
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Newsletter subscriber model for MySQL4
 *
 * @category    Mage
 * @package     Mage_Newsletter
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Springhills_Newsletter_Model_Mysql4_Subscriber extends Mage_Newsletter_Model_Mysql4_Subscriber
{
	/**
	 * To fetch the subscriber id with email
	 * @param string $email
	 * @return int  $subscriberId
	 */
	public function getSubscriberId($email){
         $write =Mage::getSingleton('core/resource')->getConnection('core_write');
		 $subscriberId =$write->fetchOne("SELECT subscriber_id from  newsletter_subscriber where subscriber_email='".$email."' ");
		 return $subscriberId;
	}
	
	/**
	 * To update redbackAPI status
	 * @param int $id
	 * @param tinyint $iStatus
	 */
	public function updateRedBackStatus($id,$iStatus,$ip){
		$write =Mage::getSingleton('core/resource')->getConnection('core_write');
		$write->query("update newsletter_subscriber set redback_status=".$iStatus.", redback_ip='".$ip."' where subscriber_id=".$id);
	}
	/**
	 * To check whether email id is already subscribed or not
	 * @param unknown_type $email
	 * @return int $existenceCount
	 */
	public function checkSubscriberEmail($email){
		$write =Mage::getSingleton('core/resource')->getConnection('core_write');
		$existenceCount =$write->fetchOne("SELECT count(*) as `existence` from  newsletter_subscriber where subscriber_email='".$email."' ");
		return $existenceCount;
	}
	
}
