<?php
//error_reporting(E_ALL);
require_once 'nusoap/lib/nusoap.php';

class RedbackServices {
	//URL to be used for SOAP CALLS
	//protected $url='http://192.168.10.182/RedbackCallsService/Service1.asmx?wsdl';
	
	protected $url='http://192.168.13.20/RedbackCallsService/Service1.asmx?wsdl';
	
	protected $errorMessage='';
	
	public function __construct(){
		set_time_limit ( 0 );	
	}
	
	//Connecting to the REDBACK
	protected function connectRedBack(){
		try {
			$client = new nusoap_client($this->url, true);
			$err = $client->getError();
			if ($err) {
				return false;
			}else{
				return $client;
			}
		}catch(Exception $ex){
			return false;		
		}
	}

	/**
	  *input array $data
	  *returns boolean true or false
	 **/ 	
	public function newsletterService($data){
		$client=$this->connectRedBack();
		if($client){
			try{
				$data =array(
						'Title'	=>'4',
						'CustNumber'=>'',
						'CustFirstName'=>'',
						'CustLastName'=>'',
						'CustCompany'=>'',
						'CustAddr1'=>'',
						'CustAddr2'=>'',
						'CustCity'=>'',
						'CustState'=>'',
						'CustZip'=>'',
						'CustEmail'=>'amits@kensium.com',
						'CustEmIP'=>'',
						'CustPhone'=>'',
						'PromoCode'=>'',
						'OptOutFlag'=>'',
						'ContestCode'=>'',					
						'CatErrCode'=>'',
						'CatErrMsg'=>'',
						'Bonus'=>'',
						'debug'=>'',
						'debug_id'=>'',
						'BirthMonth'=>'',
						'BirthDay'=>'',
						'ConfirmEmail'=>'',
						'IPaddress'=>$_SERVER['REMOTE_ADDR'],
						'TimeStamp'=>''
						);
				$result = $client->call('Catalog_Request', $data, '', '', false, false);
				echo "<pre>";
				print_r($data);print_r($result);
				exit;
				if ($client->fault) {
					return false;
				} else {
					$err = $client->getError();
					if ($err) {
						//echo '<h2>Error</h2><pre>' . $err . '</pre>';
						return false;
					} else {
						$responseObj =json_decode($result['Catalog_RequestResult']);
						if(isset($responseObj->Message) && $responseObj->Message== 'Success' ){
							return true;		
						}else{
							return false;
						}
					}
				}
			}catch(Exception $ex){
				return false;
			}
			
		}else{
			return false;		
		}
	}
	//Find the information of the customer	@input CustNo Zip
	public function CustMasterAccessCustNoZip($data){
		$client=$this->connectRedBack();
		if($client){
			$params =array('CustNumber'	=>	'32168530'	,
			'Title'	=>	'4'	,
			'CustName'	=>	''	,
			'CustAdd1'	=>	''	,
			'CustAdd2'	=>	''	,
			'CustState'	=>	''	,
			'CustCity'	=>	''	,
			'CustZip'	=>	'22025'	,
			'CustCountry'	=>	''	,
			'CustPhone'	=>	''	,
			'CustFax'	=>	''	,
			'CustEmail'	=>	''	,
			'CustPin'	=>	''	,
			'CustClubFlag'	=>	''	,
			'CustClubDisc'	=>	''	,
			'CustClubDate'	=>	''	,
			'CustPinHint'	=>	''	,
			'CustOptInFlag'	=>	''	,
			'CustError' => '',
			'CustMessage' => '',
			'CustOrder'	=>	''	,
			'CustFirstName'	=>	''	,
			'CustLastName'	=>	''	,
			'ClubNumber'	=>	'');
			try{			
				$result = $client->call('Cust_Master_AccessCust', $params, '', '', false, false);
				print_r($result);exit;
				if ($client->fault) {
					return false;
				} else {
					$err = $client->getError();
					if ($err) {
						return false;
					} else {
						$responseObj =json_decode($result['Cust_Master_AccessCustResult'],true);
						if(!empty($responseObj['CustNumber'])){
							return $responseObj;		
						}else{
							return false;
						}
					}
				}
			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;
		}
	}

	//Find the information of the customer	@input Email Zip
	public function CustMasterAccessEmailZip($data){
		$client=$this->connectRedBack();
		if($client){
			$data1 =array('CustNumber'	=>	'49162787'	,
			'Title'	=>	'4'	,
			'CustName'	=>	''	,
			'CustAdd1'	=>	''	,
			'CustAdd2'	=>	''	,
			'CustState'	=>	''	,
			'CustCity'	=>	''	,
			'CustZip'	=>	'20770'	,
			'CustCountry'	=>	''	,
			'CustPhone'	=>	''	,
			'CustFax'	=>	''	,
			'CustEmail'	=>	''	,
			'CustPin'	=>	''	,
			'CustClubFlag'	=>	''	,
			'CustClubDisc'	=>	''	,
			'CustClubDate'	=>	''	,
			'CustPinHint'	=>	''	,
			'CustOptInFlag'	=>	''	,
			'CustError' => '',
			'CustMessage' => '',
			'CustOrder'	=>	''	,
			'CustFirstName'	=>	''	,
			'CustLastName'	=>	''	,
			'ClubNumber'	=>	'');
			try{			
				$result = $client->call('Cust_Master_AccessCust', $data1, '', '', false, false);

				if ($client->fault) {
					 return false;
				} else {
					$err = $client->getError();
					if ($err) {
						 return false;
					} else {
					
						$responseObj =json_decode($result['Cust_Master_AccessCustResult'],true);
						echo "<pre>";
						print_r($responseObj);
						exit;
						if(!empty($responseObj['CustNumber'])){
							return $responseObj;		
						}else{
							return false;
						}
					}
				}
			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;
		}
	}
	
	//Find the information of the customer	@input CustNo Email
	public function CustMasterAccessCustNoEmail($data){
		$client=$this->connectRedBack();
		if($client){
			$params =array('CustNumber'	=>	'32168530'	,
			'Title'	=>	'4'	,
			'CustName'	=>	''	,
			'CustAdd1'	=>	''	,
			'CustAdd2'	=>	''	,
			'CustState'	=>	''	,
			'CustCity'	=>	''	,
			'CustZip'	=>	''	,
			'CustCountry'	=>	''	,
			'CustPhone'	=>	''	,
			'CustFax'	=>	''	,
			'CustEmail'	=>	'gapletona@gmail.com'	,
			'CustPin'	=>	''	,
			'CustClubFlag'	=>	''	,
			'CustClubDisc'	=>	''	,
			'CustClubDate'	=>	''	,
			'CustPinHint'	=>	''	,
			'CustOptInFlag'	=>	''	,
			'CustError' => '',
			'CustMessage' => '',
			'CustOrder'	=>	''	,
			'CustFirstName'	=>	''	,
			'CustLastName'	=>	''	,
			'ClubNumber'	=>	'');
			try{			
				$result = $client->call('Cust_Master_AccessCust', $params, '', '', false, false);
				if ($client->fault) {
					return false;
				} else {
					$err = $client->getError();
					if ($err) {
						return false;
					} else {
						echo "<pre>";
						print_r($result);
						echo"</pre>";
						exit;
						$responseObj =json_decode($result['Cust_Master_AccessCustResult']);
						if(!empty($responseObj->CustNumber)){
							return $responseObj;		
						}else{
							return false;
						}
					}
				}
			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;
		}
	}

	//Find the information of the customer	@input Email Pin
	public function CustMasterAccessEmailPin($data){
		$client=$this->connectRedBack();
		if($client){
			$params =array('CustNumber'	=>	''	,
			'Title'	=>	'4'	,
			'CustName'	=>	''	,
			'CustAdd1'	=>	''	,
			'CustAdd2'	=>	''	,
			'CustState'	=>	''	,
			'CustCity'	=>	''	,
			'CustZip'	=>	''	,
			'CustCountry'	=>	''	,
			'CustPhone'	=>	''	,
			'CustFax'	=>	''	,
			'CustEmail'	=>	'amit.rattawa1984@gmail.com'	,
			'CustPin'	=>	'47025'	,
			'CustClubFlag'	=>	''	,
			'CustClubDisc'	=>	''	,
			'CustClubDate'	=>	''	,
			'CustPinHint'	=>	''	,
			'CustOptInFlag'	=>	''	,
			'CustError' => '',
			'CustMessage' => '',
			'CustOrder'	=>	''	,
			'CustFirstName'	=>	''	,
			'CustLastName'	=>	''	,
			'ClubNumber'	=>	'');
			try{			
				$result = $client->call('Cust_Master_AccessCust', $params, '', '', false, false);
				echo "<pre>";
				print_r($result);
				exit;
				if ($client->fault) {
					 return false;
				} else {
					$err = $client->getError();
					if ($err) {
						 return false;
					} else {
						$responseObj =json_decode($result['Cust_Master_AccessCustResult']);
						if(!empty($responseObj->CustNumber)){
							return $responseObj;		
						}else{
							return false;
						}
					}
				}
			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;
		}
	}
	
	//Free catalog 
	public function freecatalogService($data){
		//Connect to Redback
		$client=$this->connectRedBack();
		if($client){
			try{			
				
				//Call to catalog request for free catalog
				$result = $client->call('Catalog_Request', $data, '', '', false, true);
				if ($client->fault) {
					return false;
				} else {
					$err = $client->getError();
					if ($err) {
						return false;
					} else {
						//If success then send true
						$responseInfo = json_decode($result['Catalog_RequestResult']);
						if(isset($responseInfo->Message)&& $responseInfo->Message =='Success'){
							return true;
						}else{
							return false;
						}
					}
				}

			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;
		}
	}
	
	//Order List	@input CustNo Zip
	public function orderListCustNumber($data){
		// This is an archaic parameter list
		$data1 =array(
			'CustNumber'=>'32168530',
			'CustName'=>'',
			'CustAdd1'=>'',
			'CustAdd2'=>'',
			'CustState'=>'',
			'CustCity'=>'',
			'CustZip'=>'22025',
			'CustCountry'=>'',
			'CustPhone'=>'',
			'CustFax'=>'',
			'CustEmail'=>''	,
			'CustPin'=>'',
			'Title'=>'4',
			'SumOrderNumbers'=>'',
			'SumOrderDates'=>'',
			'SumShipNames'=>'',
			'SumShipStatus'=>'',
			'SumOrderTotal'=>'',
			'DetOrderNumber'=>'',
			'DetOrderDate'=>'',
			'DetOrderAmount'=>'',
			'DetShipName'=>'',
			'DetShipAdd1'=>'',
			'DetShipAdd2'=>'',
			'DetShipCity'=>'',
			'DetShipState'=>'',
			'DetShipZip'=>'',
			'DetShipNumber'=>'',
			'DetShipItems'=>'',
			'DetShipQty'=>'',
			'DetShipDesc'=>'',
			'DetShipVia'=>'',
			'DetShipDate'=>'',
			'DetShipTrackNums'=>'',
			'DetShipTrackLink'=>'',
			'DetShipEstDelBegin'=>'',
			'DetShipEstDelEnd'=>'',
			'DetOpenLocs'=>'',
			'DetOpenItems'=>'',
			'DetOpenQtys'=>'',
			'DetOpenDesc'=>'',
			'DetOpenEstDelBegin'=>'',
			'DetOpenEstDelEnd'=>'',
			'StatusErr'=>'',
			'StatusMsg'=>''
			);
		
			$client=$this->connectRedBack();

		       if($client){
			try{		
				$result = $client->call('Order_Status', $data, '', '', false, true);
				if ($client->fault) {
					return false;
				} else {
					$err = $client->getError();
					if ($err) {
						return false;
					} else {
						$convertUTF = utf8_encode($result['Order_StatusResult']);
						$responseInfo = json_decode($convertUTF, true);
						if(isset($responseInfo['Message']) && $responseInfo['Message']=='Success'){
							return $responseInfo;						
						}else{
							return false;
						}
					}
				}
			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;		
		}	
	}


	//Order List	@input CustNo Email
	public function orderListEmail(){
		// This is an archaic parameter list
		$data =array(
			'CustNumber'=>'32168530',
			'CustName'=>'',
			'CustAdd1'=>'',
			'CustAdd2'=>'',
			'CustState'=>'',
			'CustCity'=>'',
			'CustZip'=>'22025',
			'CustCountry'=>'',
			'CustPhone'=>'',
			'CustFax'=>'',
			'CustEmail'=>''	,
			'CustPin'=>'',
			'Title'=>'4',
			'SumOrderNumbers'=>'',
			'SumOrderDates'=>'',
			'SumShipNames'=>'',
			'SumShipStatus'=>'',
			'SumOrderTotal'=>'',
			'DetOrderNumber'=>'',
			'DetOrderDate'=>'',
			'DetOrderAmount'=>'',
			'DetShipName'=>'',
			'DetShipAdd1'=>'',
			'DetShipAdd2'=>'',
			'DetShipCity'=>'',
			'DetShipState'=>'',
			'DetShipZip'=>'',
			'DetShipNumber'=>'',
			'DetShipItems'=>'',
			'DetShipQty'=>'',
			'DetShipDesc'=>'',
			'DetShipVia'=>'',
			'DetShipDate'=>'',
			'DetShipTrackNums'=>'',
			'DetShipTrackLink'=>'',
			'DetShipEstDelBegin'=>'',
			'DetShipEstDelEnd'=>'',
			'DetOpenLocs'=>'',
			'DetOpenItems'=>'',
			'DetOpenQtys'=>'',
			'DetOpenDesc'=>'',
			'DetOpenEstDelBegin'=>'',
			'DetOpenEstDelEnd'=>'',
			'StatusErr'=>'',
			'StatusMsg'=>''
			);
		if($client){
			$client=$this->connectRedBack();
			try{		
				$result = $client->call('Order_Status', $data, '', '', false, true);
				if ($client->fault) {
					return false;
				} else {
					$err = $client->getError();
					if ($err) {
						return false;
					} else {
						$convertUTF = utf8_encode($result['Order_StatusResult']);
						$responseInfo = json_decode($convertUTF, true);
						if(isset($$responseInfo['Message']) && $responseInfo['Message']=='Success'){
							return $$responseInfo;						
						}else{
							return false;
						}
					}
				}
			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;		
		}	
	}


	public function OrderDetailService($data){
		// This is an archaic parameter list
		$client=$this->connectRedBack();
		$data1 =array(
			'CustNumber'=>'32168530',
			'CustName'=>'',
			'CustAdd1'=>'',
			'CustAdd2'=>'',
			'CustState'=>'',
			'CustCity'=>'',
			'CustZip'=>'22025',
			'CustCountry'=>'',
			'CustPhone'=>'',
			'CustFax'=>'',
			'CustEmail'=>''	,
			'CustPin'=>'',
			'Title'=>'4',
			'SumOrderNumbers'=>'',
			'SumOrderDates'=>'',
			'SumShipNames'=>'',
			'SumShipStatus'=>'',
			'SumOrderTotal'=>'',
			'DetOrderNumber'=>22920009000,
			'DetOrderDate'=>'',
			'DetOrderAmount'=>'',
			'DetShipName'=>'',
			'DetShipAdd1'=>'',
			'DetShipAdd2'=>'',
			'DetShipCity'=>'',
			'DetShipState'=>'',
			'DetShipZip'=>'',
			'DetShipNumber'=>'',
			'DetShipItems'=>'',
			'DetShipQty'=>'',
			'DetShipDesc'=>'',
			'DetShipVia'=>'',
			'DetShipDate'=>'',
			'DetShipTrackNums'=>'',
			'DetShipTrackLink'=>'',
			'DetShipEstDelBegin'=>'',
			'DetShipEstDelEnd'=>'',
			'DetOpenLocs'=>'',
			'DetOpenItems'=>'',
			'DetOpenQtys'=>'',
			'DetOpenDesc'=>'',
			'DetOpenEstDelBegin'=>'',
			'DetOpenEstDelEnd'=>'',
			'StatusErr'=>'',
			'StatusMsg'=>''
			);
		if($client){
			try{		
				$result = $client->call('Order_Status', $data, '', '', false, true);
				if ($client->fault) {
					return false;
				} else {
					$err = $client->getError();
					if ($err) {
						return false;
					} else {
						$convertUTF = utf8_encode($result['Order_StatusResult']);
						$responseInfo = json_decode($convertUTF, true);
						if(isset($responseInfo['Message']) && $responseInfo['Message']=='Success'){
							return $responseInfo;						
						}else{
							return false;
						}
		
					}
				}
			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;		
		}
	}
	
	public function createOrderService($data){
		// This is an archaic parameter list
		$data1 =array(	"OrderNumber"=>'',
				"CustFirstName"=>trim("Vikas"),
				"CustLastName"=>trim("Pal"),
				"CustAdd1"=>trim("200 S Wacker Dr"),
				"CustAdd2"=>'',
				"CustAdd3"=>'',
				"CustCity"=>trim("Chicago"),
				"CustState"=>trim("IL"),
				"CustZip"=>trim("60606"),
				"CustCountry"=>trim("US"),
				"CustPhoneDay"=>trim("5555555555"),
				"CustPhoneEve"=>'',
				"CustEmail1"=>"vikasp@kensium.com",
				"CustEmail2"=>'',
				"CustPin"=>'23456',
				"ShipFirstName"=>trim("Vikas"),
				"ShipLastName"=>trim("Pal"),
				"ShipAdd1"=>trim("suite 3606 ivy  lane"),
				"ShipAdd2"=>'',
				"ShipAdd3"=>'',
				"ShipCity"=>trim("greenbelt"),
				"ShipState"=>trim("MD"),
				"ShipZip"=>trim("20770"),
				"ShipCountry"=>trim("US"),
				"ShipPhone"=>trim("4444444444"),
				"ShipEmail"=>trim("amolsinghc@kensium.com"),
				"ShipAttention"=>'',
				"CustNumber"=>'49150046',
				"WebReference"=>'33333',
				"PromoCode"=>'',
				"Title"=>'4',
				"ShipMethod"=>'UPS',
				"PaymentMethod"=>"VI",
				"CreditCardNumber"=>"4111111111111111",
				"CardExpDate"=>"052015",
				"CardCVV"=>"456",
				"CardAddress"=>'200 S Wacker Dr',
				"CardZip"=>'60606',
				"MerchAmount"=>"47.96",
				"CouponAmount"=>0, 
				"DiscAmount"=>0,
				"ShipAmount"=>'5.02',
				"PremShipAmount"=>'',
				"OverweightAmount"=>'',
				"TaxAmount"=>'0.0',
				"TotalAmount"=>'38.00',//base_subtotal 	base_grand_total
				"Comments"=>'',
				"Items"=>'75619', // comma separated
				"QtyOrdered"=>'2',// comma separated
				"UnitPrice"=>'15.9900',// comma separated
				"ReleaseDate"=>'',
				"FreeFlag"=>'',
				"PersonalizationCode"=>'',
				"PersonalizationDetail1"=>'',
				"PersonalizationDetail2"=>'',
				"PersonalizationDetail3"=>'',
				"PersonalizationDetail4"=>'',
				"PersonalizationDetail5"=>'',
				"PIN"=>'',
				"PINHint"=>'',
				"OptInFlag"=>'',
				"OptInDate"=>'',
				"OptOutDate"=>'',
				"Etrack"=>'',
				"IPSource"=>'',
				"GVCFlag"=>'',
				"GVCDate"=>'',
				"ValidErrCode"=>'',
				"ValidErrMsg"=>'',
				"OrderErrCode"=>'',
				"OrderErrMsg"=>'',
				"Cert1_Number"=>'',
				"Cert1_Amount"=>'',
				"Cert2_Number"=>'',
				"Cert2_Amount"=>'',
				"Cert3_Number"=>'',
				"Cert3_Amount"=>'',
				"Repc1_Number"=>'',
				"Repc1_Amount"=>'',
				"Repc2_Number"=>'',
				"Repc2_Amount"=>'',
				"Repc3_Number"=>'',
				"Repc3_Amount"=>'',
				"Web_Date"=>'',
				"Web_Time"=>''
			   );

		$client=$this->connectRedBack();
		if($client){
			try{
				$result = $client->call('Order_New', $data, '', '', false, true);
				if ($client->fault) {
					return false;
				} else {
					$err = $client->getError();
					if ($err) {
						return false;
					} else {
						$convertUTF = utf8_encode($result['Order_NewResult']);
						$responseInfo = json_decode($convertUTF, true);
						if(isset($responseInfo['Message']) && $responseInfo['Message']=='Success'){
							return $responseInfo;						
						}else{
							return false;
						}
		
					}
				}
			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;		
		}
		
	}

	public function giftReplacementService(){
		$client=$this->connectRedBack();
		if($client){
			$data =array(	
					"Cert_Number"=>'4519090CM',
					"Cert_Amount"=>'',
					"Cert_Status"=>'',
					"Cert_Err"=>'',
					"Cert_Errmsg" =>''
				    );
			try{			
				$result = $client->call('Gift_Certificate', $data, '', '', false, true);
echo "<pre>";
print_r($result);
exit;
				if ($client->fault) {
				echo '<h2>Fault (Expect - The request contains an invalid SOAP body)</h2><pre>'; print_r($result); echo '</pre>';
				} else {
					$responseInfo = json_decode($result['Gift_CertificateResult'], true);
					if(isset($responseInfo['Message']) && $responseInfo['Message']=='Success'){
						return $responseInfo;						
					}else{
						return false;
					}
				}
			}catch(Exception $ex){
				return false;
			}
		echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
		}else{
			echo "return false";
		}
		exit;
	}
	
	public function giftReplacementCertService(){
		$client=$this->connectRedBack();
		if($client){
			$data =array(	
					"Cert_Number"=>'4519090CM',
					"Cert_Amount"=>'',
					"Cert_Status"=>'',
					"Cert_Err"=>'',
					"Cert_Errmsg" =>''
				    );
			try{			
				$result = $client->call('Replace_Certificate', $data, '', '', false, true);
echo "<pre>";
print_r($result);
exit;
				if ($client->fault) {
				echo '<h2>Fault (Expect - The request contains an invalid SOAP body)</h2><pre>'; print_r($result); echo '</pre>';
				} else {
					$responseInfo = json_decode($result['Gift_CertificateResult'], true);
					if(isset($responseInfo['Message']) && $responseInfo['Message']=='Success'){
						return $responseInfo;						
					}else{
						return false;
					}
				}
			}catch(Exception $ex){
				return false;
			}
		echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
		echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
		}else{
			echo "return false";
		}
		exit;
	}
	public function keyCodeLoginService(){
		$client=$this->connectRedBack();
		if($client){
			$data =array(	
					"Keycode"=>'0430681',
					"Title"=>'4',
					"Catalog"=>'',
					"CustNumber" => '49237311',
					"CustName" => '',
					"CustFirstName"=>'',
					"CustLastName"=>'',
					"CustCompany"=>'',
					"CustAdd1" => '',
					"CustAdd2" => '',
					"CustCity"=>'',
					"CustState"=>'',
					"CustZip"=>'20770',
					"CustPhone" => '',
					"CustEmail" => '',
					"CustClubFlag"=>'',
					"CustClubDisc"=>'',
					"CustClubDate"=>'',
					"CustError" => '',
					"CustMessage" => '',
					"debug_id"=>''
				    );
			try{			
				$result = $client->call('Key_Code_Login', $data, '', '', false, true);
				if ($client->fault) {
				//echo '<h2>Fault (Expect - The request contains an invalid SOAP body)</h2><pre>'; print_r($result); echo '</pre>';
					return false;
				} else {
					$err = $client->getError();
					if ($err) {
						//echo '<h2>Error</h2><pre>' . $err . '</pre>';
						return false;
					} else {
						//$convertUTF = utf8_encode($result['Order_NewResult']);
						$responseInfo = json_decode($result['Key_Code_LoginResult'], true);
						echo "<pre>";
						print_r($responseInfo);
						exit;
						if(isset($responseInfo['Message']) && $responseInfo['Message']=='Success'){
							return $responseInfo;						
						}else{
							return false;
						}
					
					}
				}
			}catch(Exception $ex){
				return false;
			}
		}else{
			return false;
		}
	}

	public function setErrorMessage(){
		$this->errorMessage="We're sorry. It looks like there was a problem finding your information at this time. Please feel free to continue your purchase by selecting \"click here to purchase as a guest\" and continuing through the normal checkout process.  We will add this order to your existing account order history.  If you have any questions, feel free to call our customer service at 513-354-1509";
		return $this->errorMessage;
	}
	

}


$obj = new RedbackServices();
//$obj->giftReplacementService();
//$obj->orderListEmail();
//$obj->createOrderService();
//$obj->orderListCustNumber();
//$obj->OrderDetailService();
//$obj->setStatus();
//$message =$obj->setErrorMessage();
//$obj->newsletterService();
//$obj->keyCodeLoginService();
//$obj->giftReplacementService();
//$obj->giftReplacementCertService();
//$obj->CustMasterAccessCustNoZip();
//$obj->CustMasterAccessEmailZip();
//$obj->CustMasterAccessCustNoEmail();
//$obj->CustMasterAccessEmailPin();
//$obj->freecatalogService();
//$obj->orderListService();
//$obj->createOrder();
